﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Fractron9000.fractio
{
    class WebPHandling
    {
        public static byte[] ConvertFile(Bitmap incomingBitmap)
        {
            Bitmap source = incomingBitmap;
            BitmapData data = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            IntPtr unmanagedData;
            int size = WebPEncodeBGR(data.Scan0, source.Width, source.Height, data.Stride, 60, out unmanagedData);
            byte[] managedData = new byte[size];
            Marshal.Copy(unmanagedData, managedData, 0, size);

            //Marshal.FreeHGlobal(unmanagedData); // Doesn't work.
            WebPFree(unmanagedData); // Works.

            return managedData;
        }

        [DllImport("libwebp_a.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int WebPEncodeBGR(IntPtr rgb, int width, int height, int stride, float quality_factor, out IntPtr output);

        [DllImport("libwebp_a.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int WebPFree(IntPtr p);
    }
}
