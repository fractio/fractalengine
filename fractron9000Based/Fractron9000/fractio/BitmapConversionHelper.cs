﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using MTUtil;

namespace Fractron9000.fractio
{
    class BitmapConversionHelper
    {
        public static Bitmap getImageAsBitmap(Color[,] pixels)
        {
            Bitmap bmp = null;

            bmp = getFlatBitmapFromPixels(pixels);

            return bmp;
        }
        private static Bitmap getFlatBitmapFromPixels(Color[,] pixels)
        {
            Bitmap bmp = new Bitmap(pixels.GetLength(1), pixels.GetLength(0), System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            Color bg = Util.ColorFromVec4(FractalManager.Fractal.BackgroundColor);

            Color pix;
            int r, g, b, a;
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    pix = pixels[y, x];
                    a = (int)pix.A;
                    r = (int)bg.R * (255 - a) / 255 + (int)pix.R * a / 255;
                    g = (int)bg.G * (255 - a) / 255 + (int)pix.G * a / 255;
                    b = (int)bg.B * (255 - a) / 255 + (int)pix.B * a / 255;
                    bmp.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            }
            return bmp;
        }
    }
}
