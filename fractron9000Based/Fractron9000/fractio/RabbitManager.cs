﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Diagnostics;

namespace Fractron9000.fractio
{
    class RabbitManager
    {
        private IConnection connection;
        private IModel channel;
        private QueueingBasicConsumer consumer;
        private BasicDeliverEventArgs eventArguments;

        public void init() 
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.HostName = "192.168.1.67";
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.QueueDeclare("task_queue", true, false, false, null);

            //channel.BasicQos(0, 1, false);
            consumer = new QueueingBasicConsumer(channel);

            channel.BasicConsume("task_queue", false, consumer);

            Debug.WriteLine(" [*] Waiting for messages. " +
                                "To exit press CTRL+C");

        }
        public String getMsg()
        {
            try
            {
                eventArguments = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
            }
            catch (System.Exception excep)
            {
                Debug.WriteLine(excep.Message);
                return "";
            }
            byte[] body = eventArguments.Body;
            string message = System.Text.Encoding.UTF8.GetString(body);
            Debug.WriteLine(" [x] Received {0}", message);
            return message;                  
        }
        public void ackCurrentMsg()
        {
            Debug.WriteLine(" [x] Done");
            try
            {
                channel.BasicAck(eventArguments.DeliveryTag, false);
            }
            catch (System.Exception excep)
            {
                Debug.WriteLine(excep.Message);
            }
        }
    }
}

