﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StatsdClient;
namespace Fractron9000.fractio
{
    class StatsdManager
    {
        public Statsd s = new Statsd(new StatsdUDP("192.168.1.70", 8125));

        public void sendStats(int gpuTime, string mode)
        {
            s.Add<Statsd.Timing>("fractalEngine.gpu.1." + mode + "gpuTime", gpuTime);
            //s.Add<Statsd.Timing>("fractalEngine.gpu.1." + mode + "webPTime", webPTime);
            s.Send();
        }
    }
}
