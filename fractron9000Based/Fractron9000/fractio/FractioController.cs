﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Fractron9000.fractio
{
    class FractioController
    {
        private RabbitManager rabbitManager = new RabbitManager();
        private StatsdManager statsDManager = new StatsdManager();
        private MemcachedManager memcachedManager = new MemcachedManager();

        private UI.MainForm mainForm;


        private byte[] webPByte;

        private string mode = "P";

        public FractioController(UI.MainForm owner)
		{
			this.mainForm = owner;
		}

        public void init()
        {
            rabbitManager.init();
        }

        public void EngineFinished()
        {
            //statsDManager.s.Add(() => setwebPByte(), "fractalEngine.gpu.1." + mode + "webPTime"); 
            statsDManager.sendStats(Convert.ToInt32(mainForm.getSecondsSinceReset() * 1000),mode);

            System.Drawing.Bitmap tmpBitmap = BitmapConversionHelper.getImageAsBitmap(mainForm.getOutputPixels());
            
            Thread t = new Thread(() => setwebPByte(tmpBitmap));          // Kick off a new thread
            t.Start();

            //dispatch msg 

            //rabbitManager.ackCurrentMsg()``;
            //getNextMsg();
        }

        private void setwebPByte(System.Drawing.Bitmap incomingBitmap)
        {
            webPByte = WebPHandling.ConvertFile(incomingBitmap);
            memcachedManager.setOutput("test", webPByte);
        }

        public void getNextMsg()
        {
            try
            {
                String msg = rabbitManager.getMsg();

                if (msg != "")
                {
                    FractalManager.Fractals = FlameFileIO.ReadFlameXML(msg, mainForm.Config);
                    FractalManager.SetCurrentCopy(FractalManager.Fractals[0]);
                }
            }
            catch (System.Exception excep)
            {
                System.Diagnostics.Debug.WriteLine(excep.Message);
            }
        }
    }
}
