﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Enyim.Caching;
using Enyim.Caching.Memcached;

namespace Fractron9000.fractio
{    
    
    class MemcachedManager
    {
        MemcachedClient client = new MemcachedClient();

        public void setOutput(string name, byte[] data)
        {
            client.Store(StoreMode.Set, name, data, new TimeSpan(0, 2, 0));
        }
    }
}
