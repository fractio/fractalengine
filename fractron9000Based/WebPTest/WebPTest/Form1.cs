﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace WebPTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ConvertFile();
        }

        void ConvertFile()
        {
            Bitmap source = new Bitmap("Desert.jpg");
            BitmapData data = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            IntPtr unmanagedData;
            int size = WebPEncodeBGR(data.Scan0, source.Width, source.Height, data.Stride, 85, out unmanagedData);
            byte[] managedData = new byte[size];
            Marshal.Copy(unmanagedData, managedData, 0, size);
            File.WriteAllBytes("Desert.webp", managedData);

            Marshal.FreeHGlobal(unmanagedData); // Doesn't work.
            //WebPFree(unmanagedData); // Works.
        }

        [DllImport("libwebp_a.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int WebPEncodeBGR(IntPtr rgb, int width, int height, int stride, float quality_factor, out IntPtr output);

        [DllImport("libwebp_a.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern int WebPFree(IntPtr p);
    }
}
