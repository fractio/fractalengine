/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#using <mscorlib.dll>

using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::Data;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;
using namespace System::Text::RegularExpressions;

#include "Keyframe.h"
#include "..\\Flam4CUDA\\FlameData.h"
#include "..\\Flam4CUDA\\Animate.h"
#include "Palette.h"
#include "Interval.h"


public ref class Animation
{
private:
	coefs^ readCoefs(XmlNode^ coefsNode);
	coefs^ getWindow(XmlNode^ coefsNode);
	Palette^ readPalette(XmlNode^ paletteNode);
	KeyframeXform^ readXform(XmlNode^ xformNode, Dictionary<String^, int>^ varDir);
	void linkXforms(Interval^ interval, Dictionary<String^, XmlNode^>^ interpDict);
public:
	array<Keyframe^>^ keyframes;
	array<KeyframeVarDesc^>^ varDatabase;
	array<Palette^>^ palList;
	array<Interval^>^ intervalList;

	//Constructers
	Animation();
	Animation(int numKeyframes); 

	//Functions
	Flame* GetFrame(float time);
	void LoadFlam4(Stream^ infile);
	void* OpenFlam3(Stream^ inFile);
};

#pragma endregion