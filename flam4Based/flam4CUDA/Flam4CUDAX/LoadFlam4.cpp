/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Flam4CUDAX.h"
#include "Animation.h"

void Animation::LoadFlam4(Stream^ inFile)
{
	NumberFormatInfo^ numFormat = gcnew NumberFormatInfo;
	numFormat->NumberDecimalSeparator = ".";
	numFormat->NumberGroupSeparator = ",";
	XmlReaderSettings^ settings = gcnew XmlReaderSettings();
	settings->ConformanceLevel = ConformanceLevel::Fragment;
	XmlReader^ reader = XmlReader::Create(inFile,settings);
	reader->ReadToFollowing("flam4");
	XmlDocument^ flameXml = gcnew XmlDocument();
	flameXml->Load(reader->ReadSubtree());
	XmlNode^ RootNode = flameXml->SelectSingleNode("//flam4");

	//build the var dictionary

	Dictionary<String^, int>^ varDir = gcnew Dictionary<String^, int>();
	XmlNodeList^ Vars = RootNode->SelectNodes("./flame/xform/descendant::*[not(self::color or self::coefs or self::post)]");
	int n = 0;
	for each(XmlNode^ thisVar in Vars)
	{
		if (!varDir->ContainsKey(thisVar->Name))
		{
			varDir->Add(thisVar->Name,n);
			n++;
		}
	}
	varDatabase = gcnew array<KeyframeVarDesc^>(varDir->Count);
	for each (KeyValuePair<String^, int> kvp in varDir)
	{
		varDatabase[kvp.Value] = gcnew KeyframeVarDesc();
		varDatabase[kvp.Value]->name = kvp.Key;
		//first, check for a matching custom var
		XmlNode^ parameterList = RootNode->SelectSingleNode("./code/variation[@name='"+kvp.Key+"']");
		if (parameterList)
		{
			varDatabase[kvp.Value]->parameters = parameterList->Attributes->GetNamedItem("parameters")->Value->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);
			varDatabase[kvp.Value]->code = parameterList->InnerXml;
		}
		else
		{
			//If no match, grab parameters from the first var node of the same type
			parameterList = RootNode->SelectSingleNode("./flame/xform/"+kvp.Key);
			XmlNodeList^ params = parameterList->SelectNodes("@*[name()!='weight']");
			varDatabase[kvp.Value]->parameters = gcnew array<String^>(params->Count);
			for (int m = 0; m < params->Count; m++)
				varDatabase[kvp.Value]->parameters[m]=params->Item(m)->Name;
		}
	}

	//build the palette dictionary

	Dictionary<String^, Palette^>^ palDir = gcnew Dictionary<String^, Palette^>();
	XmlNodeList^ Palettes = RootNode->SelectNodes("./palette");
	palList = gcnew array<Palette^>(Palettes->Count);
	n = 0;
	for each(XmlNode^ thisPal in Palettes)
	{
		if (!palDir->ContainsKey(thisPal->Attributes->GetNamedItem("name")->Value))
		{
			palList[n] = readPalette(thisPal);
			palDir->Add(thisPal->Attributes->GetNamedItem("name")->Value,palList[n]);
			n++;
		}
	}

	//build the flame dictionary

	Dictionary<String^,int>^ flameDir = gcnew Dictionary<String^,int>();
	XmlNodeList^ flames = RootNode->SelectNodes("./flame");
	keyframes = gcnew array<Keyframe^>(flames->Count);
	for (int frame = 0; frame < keyframes->Length; frame++)
	{
		XmlNode^ CurrentParam = RootNode->SelectSingleNode("./flame[position()='"+Convert::ToString(frame+1)+"']");
		flameDir->Add(CurrentParam->Attributes->GetNamedItem("name")->Value,frame);
		keyframes[frame] = gcnew Keyframe();
		//Get window parameters
		keyframes[frame]->window = getWindow(CurrentParam->SelectSingleNode("./window"));
		keyframes[frame]->brightness = Convert::ToSingle(CurrentParam->SelectSingleNode("./gamut/@brightness")->Value);
		keyframes[frame]->gamma = Convert::ToSingle(CurrentParam->SelectSingleNode("./gamut/@gamma")->Value);
		keyframes[frame]->vibrancy = Convert::ToSingle(CurrentParam->SelectSingleNode("./gamut/@vibrancy")->Value);
		//Load xforms
		XmlNodeList^ CurrentXforms = CurrentParam->SelectNodes("./xform");
		keyframes[frame]->Xforms = gcnew array<KeyframeXform^>(CurrentXforms->Count);
		keyframes[frame]->xformDict = gcnew Dictionary<String^,KeyframeXform^>();
		for (int xform = 0; xform < CurrentXforms->Count; xform++)
		{
			keyframes[frame]->Xforms[xform] = readXform(CurrentXforms->Item(xform), varDir);
			keyframes[frame]->xformDict->Add(CurrentXforms->Item(xform)->Attributes->GetNamedItem("name")->Value,keyframes[frame]->Xforms[xform]);
		}
		//Load final xforms
		CurrentXforms = CurrentParam->SelectNodes("./finalxform");
		keyframes[frame]->FinalXforms = gcnew array<KeyframeXform^>(CurrentXforms->Count);
		keyframes[frame]->finalXformDict = gcnew Dictionary<String^,KeyframeXform^>();
		for (int xform = 0; xform < CurrentXforms->Count; xform++)
		{
			keyframes[frame]->FinalXforms[xform] = readXform(CurrentXforms->Item(xform), varDir);
			keyframes[frame]->finalXformDict->Add(CurrentXforms->Item(xform)->Attributes->GetNamedItem("name")->Value,keyframes[frame]->Xforms[xform]);
		}
		//Attach the palette
		palDir->TryGetValue(CurrentParam->SelectSingleNode("./palette")->Attributes->GetNamedItem("name")->Value,keyframes[frame]->palette);
	}

	//build interpolation xmlnode dictionary

	Dictionary<String^,XmlNode^>^ interpDir = gcnew Dictionary<String^,XmlNode^>();
	XmlNodeList^ xformInterpolations = RootNode->SelectNodes("./interpolation/xforms/links");
	array<XformLink^>^ xformInterps = gcnew array<XformLink^>(xformInterpolations->Count);
	for (int interp = 0; interp < xformInterpolations->Count; interp++)
	{
		interpDir->Add(xformInterpolations->Item(interp)->Attributes->GetNamedItem("name")->Value,xformInterpolations->Item(interp));
	}

	//Load basic interval data from parameters

	XmlNodeList^ intervals = RootNode->SelectNodes("./animation/interval");
	intervalList = gcnew array<Interval^>(intervals->Count);
	for (int interval = 0; interval < intervalList->Length; interval++)
	{
		intervalList[interval]= gcnew Interval();
		XmlNode^ currentInterval = RootNode->SelectSingleNode("./animation/interval[position()='"+Convert::ToString(interval+1)+"']");
		if (currentInterval->Attributes->GetNamedItem("past"))
		{
			int index;
			flameDir->TryGetValue(currentInterval->Attributes->GetNamedItem("past")->Value,index);
			intervalList[interval]->past = keyframes[index];
		}
		if (currentInterval->Attributes->GetNamedItem("start"))
		{
			int index;
			flameDir->TryGetValue(currentInterval->Attributes->GetNamedItem("start")->Value, index);
			intervalList[interval]->start = keyframes[index];
		}
		if (currentInterval->Attributes->GetNamedItem("stop"))
		{
			int index;
			flameDir->TryGetValue(currentInterval->Attributes->GetNamedItem("stop")->Value, index);
			intervalList[interval]->stop = keyframes[index];
		}
		if (currentInterval->Attributes->GetNamedItem("future"))
		{
			int index;
			flameDir->TryGetValue(currentInterval->Attributes->GetNamedItem("future")->Value, index);
			intervalList[interval]->future = keyframes[index];
		}
		if (currentInterval->Attributes->GetNamedItem("prevDuration"))
			intervalList[interval]->prevDuration = Convert::ToSingle(currentInterval->Attributes->GetNamedItem("prevDuration")->Value);
		if (currentInterval->Attributes->GetNamedItem("duration"))
			intervalList[interval]->duration = Convert::ToSingle(currentInterval->Attributes->GetNamedItem("duration")->Value);
		if (currentInterval->Attributes->GetNamedItem("nextDuration"))
			intervalList[interval]->nextDuration = Convert::ToSingle(currentInterval->Attributes->GetNamedItem("nextDuration")->Value);
		//build interpolations

	}
	//Infer typical behavier for omitted fields
	for (int interval = 0; interval < intervalList->Length; interval++)
	{
		if (!intervalList[(interval+1)%intervalList->Length]->start)
			intervalList[(interval+1)%intervalList->Length]->start = intervalList[interval]->stop;
		if (!intervalList[(interval+2)%intervalList->Length]->past)
			intervalList[(interval+2)%intervalList->Length]->past = intervalList[interval]->stop;
		if (!intervalList[(interval+1)%intervalList->Length]->prevDuration)
			intervalList[(interval+1)%intervalList->Length]->prevDuration = intervalList[interval]->duration;
		if (!intervalList[(interval+intervalList->Length-1)%intervalList->Length]->future)
			intervalList[(interval+intervalList->Length-1)%intervalList->Length]->future = intervalList[interval]->stop;
		if (!intervalList[(interval+intervalList->Length-1)%intervalList->Length]->nextDuration)
			intervalList[(interval+intervalList->Length-1)%intervalList->Length]->nextDuration = intervalList[interval]->duration;
	}
	//Generate time stamps
	float currentTime = 0.0f;
	for (int interval = 0; interval < intervalList->Length; interval++)
	{
		intervalList[interval]->pastTime = currentTime-intervalList[interval]->prevDuration;
		intervalList[interval]->startTime = currentTime;
		currentTime += intervalList[interval]->duration;
		intervalList[interval]->stopTime = currentTime;
		intervalList[interval]->futureTime = currentTime+intervalList[interval]->nextDuration;
	}
	//Normalize time stamps
	for (int interval = 0; interval < intervalList->Length; interval++)
	{
		intervalList[interval]->pastTime /= currentTime;
		intervalList[interval]->startTime /= currentTime;
		intervalList[interval]->stopTime /= currentTime;
		intervalList[interval]->futureTime /= currentTime;
		intervalList[interval]->prevDuration /= currentTime;
		intervalList[interval]->duration /= currentTime;
		intervalList[interval]->nextDuration /= currentTime;
	}
}

void Animation::linkXforms(Interval^ interval, Dictionary<String^, XmlNode^>^ interpDict)
{
	//Count active xforms
	Dictionary<String^,KeyframeXform^>^ intervalXformDir = gcnew Dictionary<String^,KeyframeXform^>();
	for each (KeyValuePair<String^, KeyframeXform^> kvp in interval->past->xformDict)
		if (!intervalXformDir->ContainsKey(kvp.Key))
			intervalXformDir->Add(kvp.Key,kvp.Value);
	for each (KeyValuePair<String^, KeyframeXform^> kvp in interval->start->xformDict)
		if (!intervalXformDir->ContainsKey(kvp.Key))
			intervalXformDir->Add(kvp.Key,kvp.Value);
	for each (KeyValuePair<String^, KeyframeXform^> kvp in interval->stop->xformDict)
		if (!intervalXformDir->ContainsKey(kvp.Key))
			intervalXformDir->Add(kvp.Key, kvp.Value);
	for each (KeyValuePair<String^, KeyframeXform^> kvp in interval->future->xformDict)
		if (!intervalXformDir->ContainsKey(kvp.Key))
			intervalXformDir->Add(kvp.Key, kvp.Value);
	int xformCount = intervalXformDir->Count;
	interval->xformLinks = gcnew array<XformLink^>(xformCount);

	//Link standard xforms

	//Then recursively link post xforms
}

KeyframeXform^ Animation::readXform(XmlNode^ xformNode, Dictionary<String^, int>^ varDir)
{
	KeyframeXform^ xform = gcnew KeyframeXform();
	xform->weight = Convert::ToSingle(xformNode->Attributes->GetNamedItem("weight")->Value);
	XmlNodeList^ CurrentVars = xformNode->SelectNodes("./*[not(self::color or self::coefs or self::post)]");
	xform->vars = gcnew array<KeyframeVariation^>(CurrentVars->Count);
	xform->affine = readCoefs(xformNode->SelectSingleNode("./coefs"));
	for (int var = 0; var < CurrentVars->Count; var++)
	{
		int value;
		varDir->TryGetValue(CurrentVars[var]->Name, value);
		xform->vars[var] = gcnew KeyframeVariation();
		xform->vars[var]->descIndex = value;
		xform->vars[var]->weight = Convert::ToSingle(CurrentVars[var]->Attributes->GetNamedItem("weight")->Value);
		xform->vars[var]->parameters = gcnew array<float>(varDatabase[value]->parameters->Length);
		for (int param = 0; param < varDatabase[value]->parameters->Length; param++)
			xform->vars[var]->parameters[param] = Convert::ToSingle(CurrentVars[var]->Attributes->GetNamedItem(varDatabase[value]->parameters[param])->Value);
	}
	//recursively read in post xforms
	XmlNodeList^ postXforms = xformNode->SelectNodes("./post");
	xform->post = gcnew array<KeyframeXform^>(postXforms->Count);
	xform->postXformDict = gcnew Dictionary<String^, KeyframeXform^>();
	for (int n = 0; n < xform->post->Length; n++)
	{
		xform->post[n] = readXform(postXforms->Item(n), varDir);
		xform->postXformDict->Add(postXforms->Item(n)->Attributes->GetNamedItem("name")->Value, xform->post[n]);
	}
	return xform;
}

coefs^ Animation::readCoefs(XmlNode^ coefsNode)
{
	float a,b,c,d,e,f;
	coefs^ coefsVals = gcnew coefs();
	if (coefsNode->Attributes->GetNamedItem("abcdef"))
	{
		array<String^>^ coefs = coefsNode->Attributes->GetNamedItem("abcdef")->Value->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);;
		a = Convert::ToSingle(coefs[0]);
		b = Convert::ToSingle(coefs[1]);
		c = Convert::ToSingle(coefs[2]);
		d = Convert::ToSingle(coefs[3]);
		e = Convert::ToSingle(coefs[4]);
		f = Convert::ToSingle(coefs[5]);
	}
	else if (coefsNode->Attributes->GetNamedItem("adbecf"))
	{
		array<String^>^ coefs = coefsNode->Attributes->GetNamedItem("adbecf")->Value->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);;
		a = Convert::ToSingle(coefs[0]);
		d = Convert::ToSingle(coefs[1]);
		b = Convert::ToSingle(coefs[2]);
		e = Convert::ToSingle(coefs[3]);
		c = Convert::ToSingle(coefs[4]);
		f = Convert::ToSingle(coefs[5]);
	}
	else if (coefsNode->Attributes->GetNamedItem("a"))
	{
		a = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("a")->Value);
		b = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("b")->Value);
		c = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("c")->Value);
		d = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("d")->Value);
		e = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("e")->Value);
		f = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("f")->Value);
	}
	else if (coefsNode->Attributes->GetNamedItem("cr"))
	{
		coefsVals->cr = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("cr")->Value);
		coefsVals->ctheta = 2.0f*Math::PI*(1.0f/360.0f)*Convert::ToSingle(coefsNode->Attributes->GetNamedItem("ctheta")->Value);
		coefsVals->xr = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("xr")->Value);
		coefsVals->xtheta = 2.0f*Math::PI*(1.0f/360.0f)*Convert::ToSingle(coefsNode->Attributes->GetNamedItem("xtheta")->Value);
		coefsVals->yr = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("yr")->Value);
		coefsVals->ytheta = 2.0f*Math::PI*(1.0f/360.0f)*Convert::ToSingle(coefsNode->Attributes->GetNamedItem("ytheta")->Value);
		return coefsVals;
	}
	else
	{
		float x,y,dx,dy,rot,skew;
		array<String^>^ center = coefsNode->Attributes->GetNamedItem("center")->Value->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);
		x = Convert::ToSingle(center[0]);
		y = Convert::ToSingle(center[1]);
		array<String^>^ dimension = coefsNode->Attributes->GetNamedItem("dimension")->Value->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);
		dx = Convert::ToSingle(dimension[0]);
		dy = Convert::ToSingle(dimension[1]);
		rot = 2.0f*Math::PI*(1.0f/360.0f)*Convert::ToSingle(coefsNode->Attributes->GetNamedItem("rotation")->Value);
		skew = 0.5f*Math::PI*(Convert::ToSingle(coefsNode->Attributes->GetNamedItem("skew")->Value)+1.0f);
		coefsVals->cr = Math::Sqrt(x*x+y*y);
		coefsVals->ctheta = Math::Atan2(y,x);
		coefsVals->xr = dx;
		coefsVals->yr = dy;
		coefsVals->xtheta = rot;
		coefsVals->ytheta = coefsVals->xtheta+skew;
		return coefsVals;
	}
	coefsVals->cr = Math::Sqrt(c*c+f*f);
	coefsVals->ctheta = Math::Atan2(f,c);
	coefsVals->xr = Math::Sqrt(a*a+d*d);
	coefsVals->xtheta = Math::Atan2(d,a);
	coefsVals->yr = Math::Sqrt(b*b+e*e);
	coefsVals->ytheta = Math::Atan2(e,b);
	return coefsVals;
}

coefs^ Animation::getWindow(XmlNode^ coefsNode)
{
	coefs^ coefsVals = readCoefs(coefsNode);
	float aspect = Convert::ToSingle(coefsNode->Attributes->GetNamedItem("aspectRatio")->Value);
	coefsVals->xr /= aspect;
	if (coefsVals->xr > coefsVals->yr)
	{
		coefsVals->yr = coefsVals->xr;
	}
	else
	{
		coefsVals->xr = coefsVals->yr;
	}
	coefsVals->xr *= aspect;
	return coefsVals;
}

Palette^ Animation::readPalette(XmlNode^ paletteNode)
{
	Palette^ result = gcnew Palette();
	String^ PalType = paletteNode->Attributes->GetNamedItem("format")->Value;
	if (PalType->Equals("rgbUnorm"))
	{
		array<String^>^ valueText = paletteNode->InnerText->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);;
		result->colors = gcnew array<RGBA^>(valueText->Length/3);
		for (int n = 0; n < result->colors->Length; n++)
		{
			result->colors[n] = gcnew RGBA(Convert::ToSingle(valueText[3*n]),
				Convert::ToSingle(valueText[3*n+1]),
				Convert::ToSingle(valueText[3*n+2]),
				1.0f);
		}
	}
	else if (PalType->Equals("rgb255norm"))
	{
		array<String^>^ valueText = paletteNode->InnerText->Split(gcnew array<wchar_t>{L' '},StringSplitOptions::RemoveEmptyEntries);;
		result->colors = gcnew array<RGBA^>(valueText->Length/3);
		for (int n = 0; n < result->colors->Length; n++)
		{
			result->colors[n] = gcnew RGBA(Convert::ToSingle(valueText[3*n])/255.0f,
				Convert::ToSingle(valueText[3*n+1])/255.0f,
				Convert::ToSingle(valueText[3*n+2])/255.0f,
				1.0f);
		}
	}
	else if (PalType->Equals("r8g8b8_hex"))
	{
		String^ valueText = paletteNode->InnerText;
		valueText = valueText->ToUpper();
		Regex^ rx = gcnew Regex("(?<digit1>[0-9]|[A-F])\\s*?(?<digit2>[0-9]|[A-F])");
		MatchCollection^ matches = rx->Matches(valueText);
		result->colors = gcnew array<RGBA^>(matches->Count/3);
		for (int n = 0; n < result->colors->Length; n++)
		{
			String^ red = matches[n*3]->Result("${digit1}${digit2}");
			String^ green = matches[n*3+1]->Result("${digit1}${digit2}");
			String^ blue = matches[n*3+2]->Result("${digit1}${digit2}");
			result->colors[n] = gcnew RGBA(Convert::ToSingle(Convert::ToByte(red,16))/255.0f,
				Convert::ToSingle(Convert::ToByte(green,16))/255.0f,
				Convert::ToSingle(Convert::ToByte(blue,16))/255.0f,
				1.0f);
		}
	}
	else if (PalType->Equals("r32g32b32unorm_hex"))
	{
		String^ valueText = paletteNode->InnerText;
		valueText = valueText->ToUpper();
		Regex^ rx = gcnew Regex("(?<digit1>[0-9]|[A-F])\\s*?(?<digit2>[0-9]|[A-F])\\s*?(?<digit3>[0-9]|[A-F])\\s*?(?<digit4>[0-9]|[A-F])\\s*?(?<digit5>[0-9]|[A-F])\\s*?(?<digit6>[0-9]|[A-F])\\s*?(?<digit7>[0-9]|[A-F])\\s*?(?<digit8>[0-9]|[A-F])");
		MatchCollection^ matches = rx->Matches(valueText);
		result->colors = gcnew array<RGBA^>(matches->Count/3);
		for (int n = 0; n < result->colors->Length; n++)
		{
			String^ red = matches[n*3]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int r = Convert::ToUInt32(red,16);
			String^ green = matches[n*3+1]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int g = Convert::ToUInt32(green,16);
			String^ blue = matches[n*3+2]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int b = Convert::ToUInt32(blue,16);
			result->colors[n] = gcnew RGBA(*(reinterpret_cast<float*>(&r)),
				*(reinterpret_cast<float*>(&g)),
				*(reinterpret_cast<float*>(&b)),
				1.0f);
		}
	}
	else if (PalType->Equals("r32g32b32_255norm_hex"))
		{
		String^ valueText = paletteNode->InnerText;
		valueText = valueText->ToUpper();
		Regex^ rx = gcnew Regex("(?<digit1>[0-9]|[A-F])\\s*?(?<digit2>[0-9]|[A-F])\\s*?(?<digit3>[0-9]|[A-F])\\s*?(?<digit4>[0-9]|[A-F])\\s*?(?<digit5>[0-9]|[A-F])\\s*?(?<digit6>[0-9]|[A-F])\\s*?(?<digit7>[0-9]|[A-F])\\s*?(?<digit8>[0-9]|[A-F])");
		MatchCollection^ matches = rx->Matches(valueText);
		result->colors = gcnew array<RGBA^>(matches->Count/3);
		for (int n = 0; n < result->colors->Length; n++)
		{
			String^ red = matches[n*3]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int r = Convert::ToUInt32(red,16);
			String^ green = matches[n*3+1]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int g = Convert::ToUInt32(green,16);
			String^ blue = matches[n*3+2]->Result("${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}");
			unsigned int b = Convert::ToUInt32(blue,16);
			result->colors[n] = gcnew RGBA((*(reinterpret_cast<float*>(&r)))/255.0f,
				(*(reinterpret_cast<float*>(&g)))/255.0f,
				(*(reinterpret_cast<float*>(&b)))/255.0f,
				1.0f);
		}
	}

	return result;
}