/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Palette.h"

#pragma once

public ref struct coefs
{
	float ctheta;
	float cr;
	float xtheta;
	float xr;
	float ytheta;
	float yr;
};

public ref class KeyframeVarDesc
{
public:
	String^ name;
	array<String^>^ parameters;
	String^ code;				//reserved
};

public ref class KeyframeVariation
{
public:
	int descIndex;
	float weight;
	array<float>^ parameters;
};

public ref class KeyframeXform
{
public:
	float opacity;
	float weight;
	coefs^ affine;
	array<KeyframeVariation^>^ vars;
	array<KeyframeXform^>^ post;
	Dictionary<String^,KeyframeXform^>^ postXformDict;
};

public ref class Keyframe
{
public:

	float brightness;
	float gamma;
	float vibrancy;
	coefs^ window;
	array<KeyframeXform^>^ Xforms;
	Dictionary<String^,KeyframeXform^>^ xformDict;
	array<KeyframeXform^>^ FinalXforms;
	Dictionary<String^,KeyframeXform^>^ finalXformDict;
	Palette^ palette;
};

#pragma endregion