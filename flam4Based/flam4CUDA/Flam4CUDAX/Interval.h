/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Keyframe.h"

public ref class Interpolation
{
	Interpolation();
};

public ref class VarLink
{
public:
	KeyframeVariation^ past;
	KeyframeVariation^ start;
	KeyframeVariation^ stop;
	KeyframeVariation^ future;
	Interpolation^ interpMode;
	VarLink();
};

public ref class XformLink
{
public:
	KeyframeXform^ past;
	KeyframeXform^ start;
	KeyframeXform^ stop;
	KeyframeXform^ future;
	array<VarLink^>^ varLinks;
	Interpolation^ interpMode;
	XformLink();
};

public ref class Interval
{
public:
	Keyframe^ past;
	float pastTime;
	float prevDuration;
	Keyframe^ start;
	float startTime;
	float duration;
	Keyframe^ stop;
	float stopTime;
	float nextDuration;
	Keyframe^ future;
	float futureTime;
	array<XformLink^>^ xformLinks;
	array<XformLink^>^ finalXformLinks;
	Interpolation^ interpMode;
	Interval();
};