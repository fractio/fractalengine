/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Animation.h"

using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Data;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;

//This function parses and loads the flame parameters in inFile.

void extractXform(xForm* dest, unAnimatedxForm* dest2, XmlAttributeCollection^ flameAttrs, NumberFormatInfo^ numFormat);

void* Animation::OpenFlam3(Stream^ inFile)
{
	flameParameterList* fAnimation = new flameParameterList();
	flameParameterList* currentFlame = fAnimation;
	currentFlame->nextFlame = nullptr;
	Flame* flm;
	NumberFormatInfo^ numFormat = gcnew NumberFormatInfo;
	numFormat->NumberDecimalSeparator = ".";
	numFormat->NumberGroupSeparator = ",";
	XmlReaderSettings^ settings = gcnew XmlReaderSettings();
	settings->ConformanceLevel = ConformanceLevel::Fragment;
	XmlReader^ reader = XmlReader::Create(inFile,settings);
	int flameCount = 0;
	while (reader->ReadToFollowing("flame"))
	{
		int paletteType = 0;
		XmlDocument^ flameXml = gcnew XmlDocument();
		flameXml->Load(reader->ReadSubtree());
		XmlNode^ thisNode = flameXml->SelectSingleNode("descendant::flame");
		XmlAttributeCollection^ flameAttrs = thisNode->Attributes;
		XmlNodeList^ xForms = thisNode->SelectNodes("descendant::xform");
		XmlNodeList^ palette = thisNode->SelectNodes("descendant::color");
		int count;
		int standardPaletteIndex = 0;
		float hue;
		if (flameAttrs->GetNamedItem("palette") == nullptr)
		{
			if (palette->Count == 0)
			{
				palette = thisNode->SelectNodes("descendant::palette");
				if (palette->Count == 0)
				{
					palette = thisNode->SelectNodes("descendant::colors");
					paletteType = 1;
				}
				else
				{
					paletteType = 2;
				}

				count = Convert::ToInt32(palette[0]->Attributes->GetNamedItem("count")->Value,numFormat);
			}
			else
			{
				count = palette->Count;
			}
		}
		else
		{
			int palIndex = Convert::ToInt32(flameAttrs->GetNamedItem("palette")->Value,numFormat);
			XmlDocument^ standardPalettes = gcnew XmlDocument();
			standardPalettes->Load("flam3-palettes.xml");
			palette = standardPalettes->SelectNodes("descendant::palette");
			while (palIndex != Convert::ToInt32(palette[standardPaletteIndex]->Attributes->GetNamedItem("number")->Value,numFormat))
				standardPaletteIndex++;
			paletteType = 3;
			count = 256;
		}
		flm = new Flame(xForms->Count,count);
		if (currentFlame->nextFlame != nullptr)	
			currentFlame = currentFlame->nextFlame;
		currentFlame->flame = flm;
		currentFlame->nextFlame = nullptr;
		XmlNode^ symmetryKind = thisNode->SelectSingleNode("descendant::symmetry");
		if (symmetryKind != nullptr)
		{
			flm->symmetryKind = Convert::ToSingle(symmetryKind->Attributes->GetNamedItem("kind")->Value,numFormat);
		}
		else
		{
			flm->symmetryKind = 0.0f;
		}
		currentFlame->flameMeta = new FlameMetaData();
		if (flameAttrs->GetNamedItem("name") != nullptr)
		{
			currentFlame->flameMeta->name = (wchar_t*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(flameAttrs->GetNamedItem("name")->Value).ToPointer();
		}
		else
		{
			currentFlame->flameMeta->name = (wchar_t*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(Convert::ToString(flameCount)).ToPointer();
		}
		array<String^>^ values;
		if (flameAttrs->GetNamedItem("hue") != nullptr)
		{
			hue = Convert::ToSingle(flameAttrs->GetNamedItem("hue")->Value,numFormat);
		}
		else
		{
			hue = 0.0f;
		}
		if (flameAttrs->GetNamedItem("center") != nullptr)
		{
			
			values = flameAttrs->GetNamedItem("center")->Value->Split(' ');
			flm->center[0] =  Convert::ToSingle(values[0],numFormat);
			flm->center[1] = Convert::ToSingle(values[1],numFormat);
		}
		else
		{
			flm->center[0]=0;
			flm->center[1]=0;
		}
		float x,y;
		if (flameAttrs->GetNamedItem("size")!=nullptr)
		{
			values = flameAttrs->GetNamedItem("size")->Value->Split(' ');
			x = Convert::ToSingle(values[0],numFormat);
			y = Convert::ToSingle(values[1],numFormat);
		}
		else
		{
			x = 100;
			y = 100;
		}
		flm->params.quality = Convert::ToSingle(flameAttrs->GetNamedItem("quality")->Value,numFormat);
		float scale = (flameAttrs->GetNamedItem("scale")!=nullptr)
			? Convert::ToSingle(flameAttrs->GetNamedItem("scale")->Value,numFormat)
			: 1;
		flm->size[0] = x/scale;
		flm->size[1] = y/scale;
		if (flameAttrs->GetNamedItem("background") != nullptr)
		{	
			array<String^>^ values2 = flameAttrs->GetNamedItem("background")->Value->Split(' ');
			flm->background.r = Convert::ToSingle(values2[0],numFormat);
			flm->background.g = Convert::ToSingle(values2[1],numFormat);
			flm->background.b = Convert::ToSingle(values2[2],numFormat);
		}
		else
		{
			flm->background.r = 0;
			flm->background.g = 0;
			flm->background.b = 0;
		}
		flm->background.a = 0;
		flm->brightness = (flameAttrs->GetNamedItem("brightness")!= nullptr)
			? Convert::ToSingle(flameAttrs->GetNamedItem("brightness")->Value,numFormat)
			: 4;
		flm->gamma = (flameAttrs->GetNamedItem("gamma")!=nullptr)
			?Convert::ToSingle(flameAttrs->GetNamedItem("gamma")->Value,numFormat)
			: 4;
		flm->vibrancy = (flameAttrs->GetNamedItem("vibrancy") != nullptr)
			? Convert::ToSingle(flameAttrs->GetNamedItem("vibrancy")->Value,numFormat)
			: 1;
		flm->rotation = (flameAttrs->GetNamedItem("rotate") != nullptr)
			? Convert::ToSingle(flameAttrs->GetNamedItem("rotate")->Value,numFormat)*(2.0*System::Math::PI/360.0)
			: 0.0;
		//grab the xForms
		for (int n = 0; n < xForms->Count; n++)
		{
			flameAttrs = xForms[n]->Attributes;
			extractXform(&(flm->trans[n]),&(flm->transAff[n]),flameAttrs,numFormat);
		}
		XmlNodeList^ finalXform = thisNode->SelectNodes("descendant::finalxform");
		if (finalXform->Count != 0)
		{
			flm->isFinalXform = 1;
			extractXform(&(flm->params.finalXform),nullptr,finalXform[0]->Attributes,numFormat);
		}
		else
		{
			flm->isFinalXform = 0;
		}
		//Now read in the palette
		switch (paletteType)
		{
		case 0:
			{
				for (int n = 0; n < palette->Count; n++)
				{
					flameAttrs = palette[n]->Attributes;
					int i = Convert::ToInt32(flameAttrs->GetNamedItem("index")->Value,numFormat);
					array<String^>^ values4 = flameAttrs->GetNamedItem("rgb")->Value->Split(' ');
					flm->colorIndex[i].r = Convert::ToSingle(values4[0],numFormat)/255.0f;
					flm->colorIndex[i].g = Convert::ToSingle(values4[1],numFormat)/255.0f;
					flm->colorIndex[i].b = Convert::ToSingle(values4[2],numFormat)/255.0f;
					flm->colorIndex[i].a = 1.0f;
				}
			}break;
		case 1:
			{
				String^ paletteHex = palette[0]->Attributes->GetNamedItem("data")->Value;
				paletteHex = paletteHex->ToUpper();
				array<Char>^ rawHex = paletteHex->ToCharArray();
				int index = 0;
				for (int j = 0; j < count; j++)
				{
					//Yay, we get to read in a bunch of ugly hex values!
					unsigned char i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					index += 2;  //skip over unused alpha value
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].r = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].g = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].b = ((float)i)/255.0f;
					flm->colorIndex[j].a = 1.0f;
				}
			}break;
		case 2:
			{
				String^ paletteHex = palette[0]->InnerText;
				paletteHex = paletteHex->ToUpper();
				array<Char>^ rawHex = paletteHex->ToCharArray();
				int index = 0;
				for (int j = 0; j < count; j++)
				{
					//Yay, we get to read in a bunch of ugly hex values!
					unsigned char i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].r = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].g = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].b = ((float)i)/255.0f;
					flm->colorIndex[j].a = 1.0f;
				}
			}break;
		case 3:
			{
				String^ paletteHex = palette[standardPaletteIndex]->Attributes->GetNamedItem("data")->Value;
				paletteHex = paletteHex->ToUpper();
				array<Char>^ rawHex = paletteHex->ToCharArray();
				int index = 0;
				for (int j = 0; j < count; j++)
				{
					//Yay, we get to read in a bunch of ugly hex values!
					unsigned char i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					index++;  //skip over unused alpha value
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					index++;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].r = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].g = ((float)i)/255.0f;
					i = 0;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					i = i<<4;
					while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
						index++;
					if (rawHex[index] <= '9')
						i+=rawHex[index]-'0';
					else
						i+=rawHex[index]-'A'+10;
					index++;
					flm->colorIndex[j].b = ((float)i)/255.0f;
					flm->colorIndex[j].a = 1.0f;
				}
			}break;
		}
		//Apply the hue transformation, if present, to the palette
		if (hue != 0)
		{
			for (int n = 0; n < count; n++)
			{
				float r = flm->colorIndex[n].r;
				float g = flm->colorIndex[n].g;
				float b = flm->colorIndex[n].b;
				float max = Math::Max(Math::Max(r,g),b);
				float min = Math::Min(Math::Min(r,g),b);
				float h,s,v;
				if (max==min)
					h = 0.0f;
				else if (max == r)
					h = .16666666666f*(g-b)/(max-min);
				else if (max == g)
					h = .16666666666f*(b-r)/(max-min)+.33333333f;
				else
					h = .16666666666f*(r-g)/(max-min)+.66666666f;
				if (max == 0.0f)
					s = 0.0f;
				else
					s = (max-min)/(max);
				v = max;
				h += hue;
				h = h-Math::Floor(h);
				int hi = ((int)Math::Floor(h*6.0f))%6;
				float f = h*6.0f-Math::Floor(h*6.0f);
				float p = v*(1.0f-s);
				float q = v*(1.0f-f*s);
				float t = v*(1.0f-(1.0f-f)*s);
				switch (hi)
				{
				case 0:
					{
						r = v;
						g = t;
						b = p;
					}break;
				case 1:
					{
						r = q;
						g = v;
						b = p;
					}break;
				case 2:
					{
						r = p;
						g = v;
						b = t;
					}break;
				case 3:
					{
						r = p;
						g = q;
						b = v;
					}break;
				case 4:
					{
						r = t;
						g = p;
						b = v;
					}break;
				case 5:
					{
						r = v;
						g = p;
						b = q;
					}break;
				}
				flm->colorIndex[n].r = r;
				flm->colorIndex[n].g = g;
				flm->colorIndex[n].b = b;
			}
		}
		//Now, normalize the weights and put them into intervals on [0,1]
		float sum = 0.0f;
		for (int n = 0; n < flm->numTrans; n++)
		{
			sum+=flm->trans[n].weight;
		}
		float sum2 = 0.0f;
		for (int n = 0; n < flm->numTrans; n++)
		{
			sum2 += flm->trans[n].weight/sum;
			flm->trans[n].weight = sum2;
		}
		currentFlame->nextFlame = new flameParameterList();
		flameCount++;
	}
	delete currentFlame->nextFlame;
	currentFlame->nextFlame = nullptr;
	return (void*)fAnimation;
}

//This is called by OpenFlame to parse xforms.
void extractXform(xForm* dest, unAnimatedxForm* dest2, XmlAttributeCollection^ flameAttrs, NumberFormatInfo^ numFormat)
{
	array<String^>^ values3 = flameAttrs->GetNamedItem("coefs")->Value->Split(' ');
	dest->a = Convert::ToSingle(values3[0],numFormat);
	dest->d = Convert::ToSingle(values3[1],numFormat);
	dest->b = Convert::ToSingle(values3[2],numFormat);
	dest->e = Convert::ToSingle(values3[3],numFormat);
	dest->c = Convert::ToSingle(values3[4],numFormat);
	dest->f = Convert::ToSingle(values3[5],numFormat);
	if (dest2 != nullptr)
	{
		dest2->a = dest->a;
		dest2->b = dest->b;
		dest2->d = dest->d;
		dest2->e = dest->e;
	}
	dest->weight = (flameAttrs->GetNamedItem("weight")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("weight")->Value,numFormat)
		: 0;
	dest->color = Convert::ToSingle((flameAttrs->GetNamedItem("color")->Value->Split(' '))[0],numFormat);
	dest->symmetry= (flameAttrs->GetNamedItem("symmetry")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("symmetry")->Value,numFormat)
		: 0;
	if (dest2 != nullptr)
	{
		if (dest->symmetry > 0.0f)
			dest2->rotates = 0;
		else
			dest2->rotates = 1;
	}
	dest->symmetry= (flameAttrs->GetNamedItem("color_speed")!= nullptr)
		? 0.5f - 0.5f*Convert::ToSingle(flameAttrs->GetNamedItem("color_speed")->Value,numFormat)
		: dest->symmetry;
	if (dest2 != nullptr)
	{
		dest2->rotates = (flameAttrs->GetNamedItem("animate")!= nullptr)
			? Convert::ToSingle(flameAttrs->GetNamedItem("animate")->Value,numFormat)
			: dest2->rotates;
	}
	dest->opacity = (flameAttrs->GetNamedItem("opacity")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("opacity")->Value,numFormat)
		: 1.0f;
	dest->linear= (flameAttrs->GetNamedItem("linear")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("linear")->Value,numFormat)
		: 0;
	dest->sinusoidal= (flameAttrs->GetNamedItem("sinusoidal")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("sinusoidal")->Value,numFormat)
		: 0;
	dest->spherical= (flameAttrs->GetNamedItem("spherical")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("spherical")->Value,numFormat)
		: 0;
	dest->swirl= (flameAttrs->GetNamedItem("swirl")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("swirl")->Value,numFormat)
		: 0;
	dest->horseshoe= (flameAttrs->GetNamedItem("horseshoe")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("horseshoe")->Value,numFormat)
		: 0;
	dest->polar= (flameAttrs->GetNamedItem("polar")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("polar")->Value,numFormat)
		: 0;
	dest->handkerchief= (flameAttrs->GetNamedItem("handkerchief")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("handkerchief")->Value,numFormat)
		: 0;
	dest->heart= (flameAttrs->GetNamedItem("heart")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("heart")->Value,numFormat)
		: 0;
	dest->disc= (flameAttrs->GetNamedItem("disc")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("disc")->Value,numFormat)
		: 0;
	dest->spiral= (flameAttrs->GetNamedItem("spiral")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("spiral")->Value,numFormat)
		: 0;
	dest->hyperbolic= (flameAttrs->GetNamedItem("hyperbolic")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("hyperbolic")->Value,numFormat)
		: 0;
	dest->diamond= (flameAttrs->GetNamedItem("diamond")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("diamond")->Value,numFormat)
		: 0;
	dest->ex= (flameAttrs->GetNamedItem("ex")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ex")->Value,numFormat)
		: 0;
	dest->julia= (flameAttrs->GetNamedItem("julia")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("julia")->Value,numFormat)
		: 0;
	dest->bent= (flameAttrs->GetNamedItem("bent")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bent")->Value,numFormat)
		: 0;
	dest->waves= (flameAttrs->GetNamedItem("waves")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves")->Value,numFormat)
		: 0;
	dest->fisheye= (flameAttrs->GetNamedItem("fisheye")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("fisheye")->Value,numFormat)
		: 0;
	dest->popcorn= (flameAttrs->GetNamedItem("popcorn")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("popcorn")->Value,numFormat)
		: 0;
	dest->exponential= (flameAttrs->GetNamedItem("exponential")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("exponential")->Value,numFormat)
		: 0;
	dest->power= (flameAttrs->GetNamedItem("power")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("power")->Value,numFormat)
		: 0;
	dest->cosine= (flameAttrs->GetNamedItem("cosine")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cosine")->Value,numFormat)
		: 0;
	dest->rings= (flameAttrs->GetNamedItem("rings")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rings")->Value,numFormat)
		: 0;
	dest->fan= (flameAttrs->GetNamedItem("fan")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("fan")->Value,numFormat)
		: 0;
	dest->blob= (flameAttrs->GetNamedItem("blob")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blob")->Value,numFormat)
		: 0;
	dest->blob_high= (flameAttrs->GetNamedItem("blob_high")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blob_high")->Value,numFormat)
		: 0;
	dest->blob_low= (flameAttrs->GetNamedItem("blob_low")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blob_low")->Value,numFormat)
		: 0;
	dest->blob_waves= (flameAttrs->GetNamedItem("blob_waves")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blob_waves")->Value,numFormat)
		: 0;
	dest->pdj= (flameAttrs->GetNamedItem("pdj")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pdj")->Value,numFormat)
		: 0;
	dest->pdj_a= (flameAttrs->GetNamedItem("pdj_a")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pdj_a")->Value,numFormat)
		: 0;
	dest->pdj_b= (flameAttrs->GetNamedItem("pdj_b")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pdj_b")->Value,numFormat)
		: 0;
	dest->pdj_c= (flameAttrs->GetNamedItem("pdj_c")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pdj_c")->Value,numFormat)
		: 0;
	dest->pdj_d= (flameAttrs->GetNamedItem("pdj_d")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pdj_d")->Value,numFormat)
		: 0;
	dest->fan2= (flameAttrs->GetNamedItem("fan2")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("fan2")->Value,numFormat)
		: 0;
	dest->fan2_x= (flameAttrs->GetNamedItem("fan2_x")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("fan2_x")->Value,numFormat)
		: 0;
	dest->fan2_y= (flameAttrs->GetNamedItem("fan2_y")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("fan2_y")->Value,numFormat)
		: 0;
	dest->rings2= (flameAttrs->GetNamedItem("rings2")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rings2")->Value,numFormat)
		: 0;
	dest->rings2_val= (flameAttrs->GetNamedItem("rings2_val")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rings2_val")->Value,numFormat)
		: 0;
	dest->eyefish= (flameAttrs->GetNamedItem("eyefish")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("eyefish")->Value,numFormat)
		: 0;
	dest->bubble= (flameAttrs->GetNamedItem("bubble")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bubble")->Value,numFormat)
		: 0;
	dest->cylinder= (flameAttrs->GetNamedItem("cylinder")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cylinder")->Value,numFormat)
		: 0;
	dest->perspective= (flameAttrs->GetNamedItem("perspective")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("perspective")->Value,numFormat)
		: 0;
	dest->perspective_angle= (flameAttrs->GetNamedItem("perspective_angle")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("perspective_angle")->Value,numFormat)*Math::PI*0.5f
		: 0;
	dest->perspective_dist= (flameAttrs->GetNamedItem("perspective_dist")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("perspective_dist")->Value,numFormat)
		: 0;
	dest->noise= (flameAttrs->GetNamedItem("noise")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("noise")->Value,numFormat)
		: 0;
	dest->julian= (flameAttrs->GetNamedItem("julian")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("julian")->Value,numFormat)
		: 0;
	dest->julian_power= (flameAttrs->GetNamedItem("julian_power")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("julian_power")->Value,numFormat)
		: 0;
	dest->julian_dist= (flameAttrs->GetNamedItem("julian_dist")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("julian_dist")->Value,numFormat)
		: 0;
	dest->juliascope= (flameAttrs->GetNamedItem("juliascope")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("juliascope")->Value,numFormat)
		: 0;
	dest->juliascope_power= (flameAttrs->GetNamedItem("juliascope_power")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("juliascope_power")->Value,numFormat)
		: 0;
	dest->juliascope_dist= (flameAttrs->GetNamedItem("juliascope_dist")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("juliascope_dist")->Value,numFormat)
		: 0;
	dest->blur= (flameAttrs->GetNamedItem("blur")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blur")->Value,numFormat)
		: 0;
	dest->gaussian_blur= (flameAttrs->GetNamedItem("gaussian_blur")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("gaussian_blur")->Value,numFormat)
		: 0;
	dest->radial_blur= (flameAttrs->GetNamedItem("radial_blur")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("radial_blur")->Value,numFormat)
		: 0;
	dest->radial_blur_angle= (flameAttrs->GetNamedItem("radial_blur_angle")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("radial_blur_angle")->Value,numFormat)
		: 0;
	dest->pie= (flameAttrs->GetNamedItem("pie")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pie")->Value,numFormat)
		: 0;
	dest->pie_slices= (flameAttrs->GetNamedItem("pie_slices")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pie_slices")->Value,numFormat)
		: 0;
	dest->pie_rotation= (flameAttrs->GetNamedItem("pie_rotation")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pie_rotation")->Value,numFormat)
		: 0;
	dest->pie_thickness= (flameAttrs->GetNamedItem("pie_thickness")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pie_thickness")->Value,numFormat)
		: 0;
	dest->ngon= (flameAttrs->GetNamedItem("ngon")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ngon")->Value,numFormat)
		: 0;
	dest->ngon_sides= (flameAttrs->GetNamedItem("ngon_sides")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ngon_sides")->Value,numFormat)
		: 0;
	dest->ngon_power= (flameAttrs->GetNamedItem("ngon_power")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ngon_power")->Value,numFormat)
		: 0;
	dest->ngon_circle= (flameAttrs->GetNamedItem("ngon_circle")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ngon_circle")->Value,numFormat)
		: 0;
	dest->ngon_corners= (flameAttrs->GetNamedItem("ngon_corners")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("ngon_corners")->Value,numFormat)
		: 0;
	dest->curl= (flameAttrs->GetNamedItem("curl")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curl")->Value,numFormat)
		: 0;
	dest->curl_c1= (flameAttrs->GetNamedItem("curl_c1")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curl_c1")->Value,numFormat)
		: 0;
	dest->curl_c2= (flameAttrs->GetNamedItem("curl_c2")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curl_c2")->Value,numFormat)
		: 0;
	dest->rectangles= (flameAttrs->GetNamedItem("rectangles")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rectangles")->Value,numFormat)
		: 0;
	dest->rectangles_x= (flameAttrs->GetNamedItem("rectangles_x")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rectangles_x")->Value,numFormat)
		: 0;
	dest->rectangles_y= (flameAttrs->GetNamedItem("rectangles_y")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rectangles_y")->Value,numFormat)
		: 0;
	dest->arch= (flameAttrs->GetNamedItem("arch")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("arch")->Value,numFormat)
		: 0;
	dest->tangent= (flameAttrs->GetNamedItem("tangent")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("tangent")->Value,numFormat)
		: 0;
	dest->square= (flameAttrs->GetNamedItem("square")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("square")->Value,numFormat)
		: 0;
	dest->rays= (flameAttrs->GetNamedItem("rays")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("rays")->Value,numFormat)
		: 0;
	dest->blade= (flameAttrs->GetNamedItem("blade")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("blade")->Value,numFormat)
		: 0;
	dest->secant= (flameAttrs->GetNamedItem("secant")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("secant")->Value,numFormat)
		: 0;
	dest->twintrian= (flameAttrs->GetNamedItem("twintrian")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("twintrian")->Value,numFormat)
		: 0;
	dest->cross= (flameAttrs->GetNamedItem("cross")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cross")->Value,numFormat)
		: 0;
	dest->disc2= (flameAttrs->GetNamedItem("disc2")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("disc2")->Value,numFormat)
		: 0;
	dest->disc2_rot= (flameAttrs->GetNamedItem("disc2_rot")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("disc2_rot")->Value,numFormat)
		: 0;
	dest->disc2_twist= (flameAttrs->GetNamedItem("disc2_twist")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("disc2_twist")->Value,numFormat)
		: 0;
	dest->supershape= (flameAttrs->GetNamedItem("super_shape")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape")->Value,numFormat)
		: 0;
	dest->supershape_m= (flameAttrs->GetNamedItem("super_shape_m")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_m")->Value,numFormat)
		: 0;
	dest->supershape_n1= (flameAttrs->GetNamedItem("super_shape_n1")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_n1")->Value,numFormat)
		: 0;
	dest->supershape_n2= (flameAttrs->GetNamedItem("super_shape_n2")!= nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_n2")->Value,numFormat)
		: 0;
	dest->supershape_n3= (flameAttrs->GetNamedItem("super_shape_n3")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_n3")->Value,numFormat)
		: 0;
	dest->supershape_holes= (flameAttrs->GetNamedItem("super_shape_holes")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_holes")->Value,numFormat)
		: 0;
	dest->supershape_rnd= (flameAttrs->GetNamedItem("super_shape_rnd")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("super_shape_rnd")->Value,numFormat)
		: 0;
	dest->flower= (flameAttrs->GetNamedItem("flower")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("flower")->Value,numFormat)
		: 0;
	dest->flower_holes= (flameAttrs->GetNamedItem("flower_holes")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("flower_holes")->Value,numFormat)
		: 0;
	dest->flower_petals= (flameAttrs->GetNamedItem("flower_petals")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("flower_petals")->Value,numFormat)
		: 0;
	dest->conic = (flameAttrs->GetNamedItem("conic")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("conic")->Value,numFormat)
		: 0;
	dest->conic_holes = (flameAttrs->GetNamedItem("conic_holes")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("conic_holes")->Value,numFormat)
		: 0;
	dest->conic_eccen = (flameAttrs->GetNamedItem("conic_eccentricity")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("conic_eccentricity")->Value,numFormat)
		: 0;
	dest->parabola = (flameAttrs->GetNamedItem("parabola")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("parabola")->Value,numFormat)
		: 0;
	dest->parabola_height = (flameAttrs->GetNamedItem("parabola_height")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("parabola_height")->Value,numFormat)
		: 0;
	dest->parabola_width = (flameAttrs->GetNamedItem("parabola_width")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("parabola_width")->Value,numFormat)
		: 0;
	dest->bent2 = (flameAttrs->GetNamedItem("bent2")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bent2")->Value,numFormat)
		: 0;
	dest->bent2_x = (flameAttrs->GetNamedItem("bent2_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bent2_x")->Value,numFormat)
		: 0;
	dest->bent2_y = (flameAttrs->GetNamedItem("bent2_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bent2_y")->Value,numFormat)
		: 0;
	dest->bipolar = (flameAttrs->GetNamedItem("bipolar")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bipolar")->Value,numFormat)
		: 0;
	dest->bipolar_shift = (flameAttrs->GetNamedItem("bipolar_shift")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("bipolar_shift")->Value,numFormat)
		: 0;
	dest->boarders = (flameAttrs->GetNamedItem("boarders")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("boarders")->Value,numFormat)
		: 0;
	dest->butterfly = (flameAttrs->GetNamedItem("butterfly")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("butterfly")->Value,numFormat)
		: 0;
	dest->cell = (flameAttrs->GetNamedItem("cell")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cell")->Value,numFormat)
		: 0;
	dest->cell_size = (flameAttrs->GetNamedItem("cell_size")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cell_size")->Value,numFormat)
		: 0;
	dest->cpow = (flameAttrs->GetNamedItem("cpow")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cpow")->Value,numFormat)
		: 0;
	dest->cpow_i = (flameAttrs->GetNamedItem("cpow_i")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cpow_i")->Value,numFormat)
		: 0;
	dest->cpow_r = (flameAttrs->GetNamedItem("cpow_r")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cpow_r")->Value,numFormat)
		: 0;
	dest->cpow_power = (flameAttrs->GetNamedItem("cpow_power")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cpow_power")->Value,numFormat)
		: 0;
	dest->curve = (flameAttrs->GetNamedItem("curve")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curve")->Value,numFormat)
		: 0;
	dest->curve_xamp = (flameAttrs->GetNamedItem("curve_xamp")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curve_xamp")->Value,numFormat)
		: 0;
	dest->curve_yamp = (flameAttrs->GetNamedItem("curve_yamp")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curve_yamp")->Value,numFormat)
		: 0;
	dest->curve_xlength = (flameAttrs->GetNamedItem("curve_xlength")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curve_xlength")->Value,numFormat)
		: 0;
	dest->curve_ylength = (flameAttrs->GetNamedItem("curve_ylength")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("curve_ylength")->Value,numFormat)
		: 0;
	dest->edisc = (flameAttrs->GetNamedItem("edisc")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("edisc")->Value,numFormat)
		: 0;
	dest->elliptic = (flameAttrs->GetNamedItem("elliptic")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("elliptic")->Value,numFormat)
		: 0;
	dest->escher = (flameAttrs->GetNamedItem("escher")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("escher")->Value,numFormat)
		: 0;
	dest->escher_beta = (flameAttrs->GetNamedItem("escher_beta")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("escher_beta")->Value,numFormat)
		: 0;
	dest->foci = (flameAttrs->GetNamedItem("foci")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("foci")->Value,numFormat)
		: 0;
	dest->lazysusan = (flameAttrs->GetNamedItem("lazysusan")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan")->Value,numFormat)
		: 0;
	dest->lazysusan_x = (flameAttrs->GetNamedItem("lazysusan_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan_x")->Value,numFormat)
		: 0;
	dest->lazysusan_y = (flameAttrs->GetNamedItem("lazysusan_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan_y")->Value,numFormat)
		: 0;
	dest->lazysusan_spin = (flameAttrs->GetNamedItem("lazysusan_spin")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan_spin")->Value,numFormat)
		: 0;
	dest->lazysusan_space = (flameAttrs->GetNamedItem("lazysusan_space")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan_space")->Value,numFormat)
		: 0;
	dest->lazysusan_twist = (flameAttrs->GetNamedItem("lazysusan_twist")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("lazysusan_twist")->Value,numFormat)
		: 0;
	dest->loonie = (flameAttrs->GetNamedItem("loonie")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("loonie")->Value,numFormat)
		: 0;
	dest->pre_blur = (flameAttrs->GetNamedItem("pre_blur")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("pre_blur")->Value,numFormat)
		: 0;
	dest->modulus = (flameAttrs->GetNamedItem("modulus")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("modulus")->Value,numFormat)
		: 0;
	dest->modulus_x = (flameAttrs->GetNamedItem("modulus_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("modulus_x")->Value,numFormat)
		: 0;
	dest->modulus_y = (flameAttrs->GetNamedItem("modulus_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("modulus_y")->Value,numFormat)
		: 0;
	dest->oscilloscope = (flameAttrs->GetNamedItem("oscilloscope")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("oscilloscope")->Value,numFormat)
		: 0;
	dest->oscilloscope_separation = (flameAttrs->GetNamedItem("oscilloscope_separation")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("oscilloscope_separation")->Value,numFormat)
		: 0;
	dest->oscilloscope_frequency = (flameAttrs->GetNamedItem("oscilloscope_frequency")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("oscilloscope_frequency")->Value,numFormat)
		: 0;
	dest->oscilloscope_amplitude = (flameAttrs->GetNamedItem("oscilloscope_amplitude")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("oscilloscope_amplitude")->Value,numFormat)
		: 0;
	dest->oscilloscope_damping = (flameAttrs->GetNamedItem("oscilloscope_damping")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("oscilloscope_damping")->Value,numFormat)
		: 0;
	dest->polar2 = (flameAttrs->GetNamedItem("polar2")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("polar2")->Value,numFormat)
		: 0;
	dest->popcorn2 = (flameAttrs->GetNamedItem("popcorn2")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("popcorn2")->Value,numFormat)
		: 0;
	dest->popcorn2_x = (flameAttrs->GetNamedItem("popcorn2_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("popcorn2_x")->Value,numFormat)
		: 0;
	dest->popcorn2_y = (flameAttrs->GetNamedItem("popcorn2_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("popcorn2_y")->Value,numFormat)
		: 0;
	dest->popcorn2_c = (flameAttrs->GetNamedItem("popcorn2_c")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("popcorn2_c")->Value,numFormat)
		: 0;
	dest->scry = (flameAttrs->GetNamedItem("scry")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("scry")->Value,numFormat)
		: 0;
	dest->separation = (flameAttrs->GetNamedItem("separation")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("separation")->Value,numFormat)
		: 0;
	dest->separation_x = (flameAttrs->GetNamedItem("separation_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("separation_x")->Value,numFormat)
		: 0;
	dest->separation_xinside = (flameAttrs->GetNamedItem("separation_xinside")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("separation_xinside")->Value,numFormat)
		: 0;
	dest->separation_y = (flameAttrs->GetNamedItem("separation_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("separation_y")->Value,numFormat)
		: 0;
	dest->separation_yinside = (flameAttrs->GetNamedItem("separation_yinside")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("separation_yinside")->Value,numFormat)
		: 0;
	dest->split = (flameAttrs->GetNamedItem("split")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("split")->Value,numFormat)
		: 0;
	dest->split_xsize = (flameAttrs->GetNamedItem("split_xsize")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("split_xsize")->Value,numFormat)
		: 0;
	dest->split_ysize = (flameAttrs->GetNamedItem("split_ysize")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("split_ysize")->Value,numFormat)
		: 0;
	dest->splits = (flameAttrs->GetNamedItem("splits")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("splits")->Value,numFormat)
		: 0;
	dest->splits_x = (flameAttrs->GetNamedItem("splits_x")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("splits_x")->Value,numFormat)
		: 0;
	dest->splits_y = (flameAttrs->GetNamedItem("splits_y")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("splits_y")->Value,numFormat)
		: 0;
	dest->stripes = (flameAttrs->GetNamedItem("stripes")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("stripes")->Value,numFormat)
		: 0;
	dest->stripes_space = (flameAttrs->GetNamedItem("stripes_space")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("stripes_space")->Value,numFormat)
		: 0;
	dest->stripes_warp = (flameAttrs->GetNamedItem("stripes_warp")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("stripes_warp")->Value,numFormat)
		: 0;
	dest->wedge = (flameAttrs->GetNamedItem("wedge")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge")->Value,numFormat)
		: 0;
	dest->wedge_angle = (flameAttrs->GetNamedItem("wedge_angle")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_angle")->Value,numFormat)
		: 0;
	dest->wedge_hole = (flameAttrs->GetNamedItem("wedge_hole")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_hole")->Value,numFormat)
		: 0;
	dest->wedge_count = (flameAttrs->GetNamedItem("wedge_count")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_count")->Value,numFormat)
		: 0;
	dest->wedge_swirl = (flameAttrs->GetNamedItem("wedge_swirl")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_swirl")->Value,numFormat)
		: 0;
	dest->wedge_julia = (flameAttrs->GetNamedItem("wedge_julia")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_julia")->Value,numFormat)
		: 0;
	dest->wedge_julia_angle = (flameAttrs->GetNamedItem("wedge_julia_angle")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_julia_angle")->Value,numFormat)
		: 0;
	dest->wedge_julia_count = (flameAttrs->GetNamedItem("wedge_julia_count")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_julia_count")->Value,numFormat)
		: 0;
	dest->wedge_julia_power = (flameAttrs->GetNamedItem("wedge_julia_power")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_julia_power")->Value,numFormat)
		: 0;
	dest->wedge_julia_dist = (flameAttrs->GetNamedItem("wedge_julia_dist")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_julia_dist")->Value,numFormat)
		: 0;
	dest->wedge_sph = (flameAttrs->GetNamedItem("wedge_sph")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_sph")->Value,numFormat)
		: 0;
	dest->wedge_sph_angle = (flameAttrs->GetNamedItem("wedge_sph_angle")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_sph_angle")->Value,numFormat)
		: 0;
	dest->wedge_sph_hole = (flameAttrs->GetNamedItem("wedge_sph_hole")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_sph_hole")->Value,numFormat)
		: 0;
	dest->wedge_sph_count = (flameAttrs->GetNamedItem("wedge_sph_count")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_sph_count")->Value,numFormat)
		: 0;
	dest->wedge_sph_swirl = (flameAttrs->GetNamedItem("wedge_sph_swirl")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("wedge_sph_swirl")->Value,numFormat)
		: 0;
	dest->whorl = (flameAttrs->GetNamedItem("whorl")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("whorl")->Value,numFormat)
		: 0;
	dest->whorl_inside = (flameAttrs->GetNamedItem("whorl_inside")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("whorl_inside")->Value,numFormat)
		: 0;
	dest->whorl_outside = (flameAttrs->GetNamedItem("whorl_outside")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("whorl_outside")->Value,numFormat)
		: 0;
	dest->waves2 = (flameAttrs->GetNamedItem("waves2")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves2")->Value,numFormat)
		: 0;
	dest->waves2_scalex = (flameAttrs->GetNamedItem("waves2_scalex")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves2_scalex")->Value,numFormat)
		: 0;
	dest->waves2_scaley = (flameAttrs->GetNamedItem("waves2_scaley")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves2_scaley")->Value,numFormat)
		: 0;
	dest->waves2_freqx = (flameAttrs->GetNamedItem("waves2_freqx")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves2_freqx")->Value,numFormat)
		: 0;
	dest->waves2_freqy = (flameAttrs->GetNamedItem("waves2_freqy")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("waves2_freqy")->Value,numFormat)
		: 0;
	dest->v_exp = (flameAttrs->GetNamedItem("exp")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("exp")->Value,numFormat)
		: 0;
	dest->v_log = (flameAttrs->GetNamedItem("log")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("log")->Value,numFormat)
		: 0;
	dest->v_sin = (flameAttrs->GetNamedItem("sin")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("sin")->Value,numFormat)
		: 0;
	dest->v_cos = (flameAttrs->GetNamedItem("cos")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cos")->Value,numFormat)
		: 0;
	dest->v_tan = (flameAttrs->GetNamedItem("tan")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("tan")->Value,numFormat)
		: 0;
	dest->v_sec = (flameAttrs->GetNamedItem("sec")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("sec")->Value,numFormat)
		: 0;
	dest->v_csc = (flameAttrs->GetNamedItem("csc")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("csc")->Value,numFormat)
		: 0;
	dest->v_cot = (flameAttrs->GetNamedItem("cot")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cot")->Value,numFormat)
		: 0;
	dest->v_sinh = (flameAttrs->GetNamedItem("sinh")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("sinh")->Value,numFormat)
		: 0;
	dest->v_cosh = (flameAttrs->GetNamedItem("cosh")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("cosh")->Value,numFormat)
		: 0;
	dest->v_tanh = (flameAttrs->GetNamedItem("tanh")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("tanh")->Value,numFormat)
		: 0;
	dest->v_sech = (flameAttrs->GetNamedItem("sech")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("sech")->Value,numFormat)
		: 0;
	dest->v_csch = (flameAttrs->GetNamedItem("csch")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("csch")->Value,numFormat)
		: 0;
	dest->v_coth = (flameAttrs->GetNamedItem("coth")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("coth")->Value,numFormat)
		: 0;
	dest->auger = (flameAttrs->GetNamedItem("auger")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("auger")->Value,numFormat)
		: 0;
	dest->auger_freq = (flameAttrs->GetNamedItem("auger_freq")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("auger_freq")->Value,numFormat)
		: 0;
	dest->auger_scale = (flameAttrs->GetNamedItem("auger_scale")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("auger_scale")->Value,numFormat)
		: 0;
	dest->auger_weight = (flameAttrs->GetNamedItem("auger_weight")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("auger_weight")->Value,numFormat)
		: 0;
	dest->auger_sym = (flameAttrs->GetNamedItem("auger_sym")!=nullptr)
		? Convert::ToSingle(flameAttrs->GetNamedItem("auger_sym")->Value,numFormat)
		: 0;
	String^ str = (flameAttrs->GetNamedItem("post")!=nullptr)
		? flameAttrs->GetNamedItem("post")->Value
		: "1 0 0 1 0 0";
	values3 = str->Split(' ');
	dest->pa = Convert::ToSingle(values3[0],numFormat);
	dest->pd = Convert::ToSingle(values3[1],numFormat);
	dest->pb = Convert::ToSingle(values3[2],numFormat);
	dest->pe = Convert::ToSingle(values3[3],numFormat);
	dest->pc = Convert::ToSingle(values3[4],numFormat);
	dest->pf = Convert::ToSingle(values3[5],numFormat);
}