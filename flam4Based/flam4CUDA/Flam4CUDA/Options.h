/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#include "Defines.h"
#include "FWMInterop.h"
#include "Animate.h"

extern bool optionsOpen;

namespace Flam4CUDA {

	/// <summary>
	/// Summary for Options
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Options : public System::Windows::Forms::Form
	{
	public:
		Options(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Options()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TrackBar^  QualityTrackbar;
	protected: 

	private: System::Windows::Forms::Label^  QualityDisplay;

	private: System::Windows::Forms::Button^  ApplyButton;
	private: System::Windows::Forms::Button^  CancelButton;
	private: System::Windows::Forms::Button^  OkButton;
	private: System::Windows::Forms::CheckBox^  animateCheckBox;



	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->QualityTrackbar = (gcnew System::Windows::Forms::TrackBar());
			this->QualityDisplay = (gcnew System::Windows::Forms::Label());
			this->ApplyButton = (gcnew System::Windows::Forms::Button());
			this->CancelButton = (gcnew System::Windows::Forms::Button());
			this->OkButton = (gcnew System::Windows::Forms::Button());
			this->animateCheckBox = (gcnew System::Windows::Forms::CheckBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->QualityTrackbar))->BeginInit();
			this->SuspendLayout();
			// 
			// QualityTrackbar
			// 
			this->QualityTrackbar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->QualityTrackbar->LargeChange = 100;
			this->QualityTrackbar->Location = System::Drawing::Point(12, 25);
			this->QualityTrackbar->Maximum = 2001;
			this->QualityTrackbar->Minimum = 1;
			this->QualityTrackbar->Name = L"QualityTrackbar";
			this->QualityTrackbar->Size = System::Drawing::Size(1050, 45);
			this->QualityTrackbar->TabIndex = 0;
			this->QualityTrackbar->TickFrequency = 100;
			this->QualityTrackbar->TickStyle = System::Windows::Forms::TickStyle::TopLeft;
			this->QualityTrackbar->Value = 30;
			this->QualityTrackbar->Scroll += gcnew System::EventHandler(this, &Options::QualityTrackbar_Scroll);
			// 
			// QualityDisplay
			// 
			this->QualityDisplay->AutoSize = true;
			this->QualityDisplay->Location = System::Drawing::Point(9, 9);
			this->QualityDisplay->Name = L"QualityDisplay";
			this->QualityDisplay->Size = System::Drawing::Size(83, 13);
			this->QualityDisplay->TabIndex = 1;
			this->QualityDisplay->Text = L"Total Iterations: ";
			// 
			// ApplyButton
			// 
			this->ApplyButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->ApplyButton->Location = System::Drawing::Point(967, 76);
			this->ApplyButton->Name = L"ApplyButton";
			this->ApplyButton->Size = System::Drawing::Size(95, 22);
			this->ApplyButton->TabIndex = 2;
			this->ApplyButton->Text = L"Apply";
			this->ApplyButton->UseVisualStyleBackColor = true;
			this->ApplyButton->Click += gcnew System::EventHandler(this, &Options::ApplyButton_Click);
			// 
			// CancelButton
			// 
			this->CancelButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->CancelButton->Location = System::Drawing::Point(866, 76);
			this->CancelButton->Name = L"CancelButton";
			this->CancelButton->Size = System::Drawing::Size(95, 22);
			this->CancelButton->TabIndex = 3;
			this->CancelButton->Text = L"Cancel";
			this->CancelButton->UseVisualStyleBackColor = true;
			this->CancelButton->Click += gcnew System::EventHandler(this, &Options::CancelButton_Click);
			// 
			// OkButton
			// 
			this->OkButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->OkButton->Location = System::Drawing::Point(765, 76);
			this->OkButton->Name = L"OkButton";
			this->OkButton->Size = System::Drawing::Size(95, 22);
			this->OkButton->TabIndex = 4;
			this->OkButton->Text = L"Ok";
			this->OkButton->UseVisualStyleBackColor = true;
			this->OkButton->Click += gcnew System::EventHandler(this, &Options::OkButton_Click);
			// 
			// animateCheckBox
			// 
			this->animateCheckBox->AutoSize = true;
			this->animateCheckBox->Checked = true;
			this->animateCheckBox->CheckState = System::Windows::Forms::CheckState::Checked;
			this->animateCheckBox->Location = System::Drawing::Point(12, 76);
			this->animateCheckBox->Name = L"animateCheckBox";
			this->animateCheckBox->Size = System::Drawing::Size(64, 17);
			this->animateCheckBox->TabIndex = 5;
			this->animateCheckBox->Text = L"Animate";
			this->animateCheckBox->UseVisualStyleBackColor = true;
			// 
			// Options
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1074, 110);
			this->ControlBox = false;
			this->Controls->Add(this->animateCheckBox);
			this->Controls->Add(this->OkButton);
			this->Controls->Add(this->CancelButton);
			this->Controls->Add(this->ApplyButton);
			this->Controls->Add(this->QualityDisplay);
			this->Controls->Add(this->QualityTrackbar);
			this->Name = L"Options";
			this->Text = L"Options";
			this->Load += gcnew System::EventHandler(this, &Options::Options_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->QualityTrackbar))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void ApplyButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 quality = this->QualityTrackbar->Value;
				 isAnimating = this->animateCheckBox->Checked;
				 flameParameterList* temp = g_pFlameParams;
				 while (temp != nullptr)
				 {
					 temp->flame->quality = (int)quality;
					 temp = temp->nextFlame;
				 }
			 }
private: System::Void CancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 optionsOpen = false;
			 this->Close();
		 }
private: System::Void OkButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 quality = this->QualityTrackbar->Value;
			 isAnimating = this->animateCheckBox->Checked;
			 flameParameterList* temp = g_pFlameParams;
				 while (temp != nullptr)
				 {
					 temp->flame->quality = (int)quality;
					 temp = temp->nextFlame;
				 }
			 optionsOpen = false;
			 this->Close();
		 }
private: System::Void QualityTrackbar_Scroll(System::Object^  sender, System::EventArgs^  e) {
			 this->QualityDisplay->Text = "Total Iterations: "+Convert::ToString(((long long)(QualityTrackbar->Value))*1024LL*32LL*NUM_ITERATIONS);
		 }
private: System::Void Options_Load(System::Object^  sender, System::EventArgs^  e) {
			 this->QualityDisplay->Text = "Total Iterations: "+Convert::ToString(((long long)(quality))*1024LL*32LL*NUM_ITERATIONS);
			 this->QualityTrackbar->Value = quality;
			 this->animateCheckBox->Checked = isAnimating;
		 }
};
}
