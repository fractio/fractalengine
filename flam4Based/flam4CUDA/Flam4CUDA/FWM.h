/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "FWMInterop.h"
#include "Options.h"
#include "PNGSettings.h"
#include "AVIOptions.h"
#include "Animate.h"
#include "RenderThread.h"
#include "PNGBatchSettings.h"
using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;


flameParameterList* OpenFlame(Stream^ inFile);
void extractXform(xForm* dest, unAnimatedxForm* dest2, XmlAttributeCollection^ flameAttrs,NumberFormatInfo^ numFormat);
bool renderingActive;
bool cudaActive = false;
bool optionsOpen = false;
bool suspendRendering = false;
volatile bool isRendering = false;
int renderWindowHandle = 0;

namespace Flam4_CUDA {

	/// <summary>
	/// Summary for FWM
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class FWM : public System::Windows::Forms::Form
	{
	public:
		FWM(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~FWM()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  fileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  openToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  exitToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  exitToolStripMenuItem1;
	private: System::Windows::Forms::ToolStripMenuItem^  renderToolStripMenuItem;
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::ToolStripMenuItem^  optionsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  renderLoopToFileToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::ComponentModel::IContainer^  components;
	private: System::Windows::Forms::ToolStripMenuItem^  renderImageToFileToolStripMenuItem;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog2;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::FormWindowState windowState;
	private: System::Windows::Forms::ToolStripMenuItem^  renderImageBatchToolStripMenuItem;
	private: System::Diagnostics::Stopwatch stopWatch;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->fileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->renderImageToFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->renderLoopToFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->optionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exitToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->exitToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->renderToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->saveFileDialog2 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->renderImageBatchToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->fileToolStripMenuItem, 
				this->renderToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1280, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this->fileToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {this->openToolStripMenuItem, 
				this->renderImageToFileToolStripMenuItem, this->renderLoopToFileToolStripMenuItem, this->renderImageBatchToolStripMenuItem, this->optionsToolStripMenuItem, 
				this->exitToolStripMenuItem, this->exitToolStripMenuItem1});
			this->fileToolStripMenuItem->Name = L"fileToolStripMenuItem";
			this->fileToolStripMenuItem->Size = System::Drawing::Size(37, 20);
			this->fileToolStripMenuItem->Text = L"File";
			// 
			// openToolStripMenuItem
			// 
			this->openToolStripMenuItem->Name = L"openToolStripMenuItem";
			this->openToolStripMenuItem->Size = System::Drawing::Size(191, 22);
			this->openToolStripMenuItem->Text = L"Open...";
			this->openToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::openToolStripMenuItem_Click);
			// 
			// renderImageToFileToolStripMenuItem
			// 
			this->renderImageToFileToolStripMenuItem->Name = L"renderImageToFileToolStripMenuItem";
			this->renderImageToFileToolStripMenuItem->Size = System::Drawing::Size(191, 22);
			this->renderImageToFileToolStripMenuItem->Text = L"Render Image to File...";
			this->renderImageToFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::renderImageToFileToolStripMenuItem_Click);
			// 
			// renderLoopToFileToolStripMenuItem
			// 
			this->renderLoopToFileToolStripMenuItem->Name = L"renderLoopToFileToolStripMenuItem";
			this->renderLoopToFileToolStripMenuItem->Size = System::Drawing::Size(191, 22);
			this->renderLoopToFileToolStripMenuItem->Text = L"Render Loop to File...";
			this->renderLoopToFileToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::renderLoopToFileToolStripMenuItem_Click);
			// 
			// optionsToolStripMenuItem
			// 
			this->optionsToolStripMenuItem->Name = L"optionsToolStripMenuItem";
			this->optionsToolStripMenuItem->Size = System::Drawing::Size(191, 22);
			this->optionsToolStripMenuItem->Text = L"Options";
			this->optionsToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::optionsToolStripMenuItem_Click);
			// 
			// exitToolStripMenuItem
			// 
			this->exitToolStripMenuItem->Name = L"exitToolStripMenuItem";
			this->exitToolStripMenuItem->Size = System::Drawing::Size(188, 6);
			// 
			// exitToolStripMenuItem1
			// 
			this->exitToolStripMenuItem1->Name = L"exitToolStripMenuItem1";
			this->exitToolStripMenuItem1->Size = System::Drawing::Size(191, 22);
			this->exitToolStripMenuItem1->Text = L"Exit";
			this->exitToolStripMenuItem1->Click += gcnew System::EventHandler(this, &FWM::exitToolStripMenuItem1_Click);
			// 
			// renderToolStripMenuItem
			// 
			this->renderToolStripMenuItem->Name = L"renderToolStripMenuItem";
			this->renderToolStripMenuItem->Size = System::Drawing::Size(59, 20);
			this->renderToolStripMenuItem->Text = L"Render!";
			this->renderToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::renderToolStripMenuItem_Click);
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->Filter = L"Flam4 files (*.flam4)|*.flam4|Apophysis flame files (*.flame)|*.flame|All files (" 
				L"*.*)|*.*";
			this->openFileDialog1->FilterIndex = 2;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 5;
			this->timer1->Tick += gcnew System::EventHandler(this, &FWM::timer1_Tick);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->Filter = L"AVI files (*.avi)|*.avi|All Files (*.*)|*.*";
			// 
			// saveFileDialog2
			// 
			this->saveFileDialog2->Filter = L"PNG files (*.png)|*.png|All Files (*.*)|*.*";
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::SystemColors::AppWorkspace;
			this->panel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panel1->Location = System::Drawing::Point(0, 24);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(1280, 960);
			this->panel1->TabIndex = 1;
			// 
			// renderImageBatchToolStripMenuItem
			// 
			this->renderImageBatchToolStripMenuItem->Name = L"renderImageBatchToolStripMenuItem";
			this->renderImageBatchToolStripMenuItem->Size = System::Drawing::Size(191, 22);
			this->renderImageBatchToolStripMenuItem->Text = L"Render Image Batch...";
			this->renderImageBatchToolStripMenuItem->Click += gcnew System::EventHandler(this, &FWM::renderImageBatchToolStripMenuItem_Click);
			// 
			// FWM
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1280, 984);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"FWM";
			this->Text = L"Flames";
			this->Load += gcnew System::EventHandler(this, &FWM::FWM_Load);
			this->ResizeBegin += gcnew System::EventHandler(this, &FWM::FWM_ResizeBegin);
			this->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &FWM::FWM_Paint);
			this->Resize += gcnew System::EventHandler(this, &FWM::FWM_Resize);
			this->ResizeEnd += gcnew System::EventHandler(this, &FWM::FWM_ResizeEnd);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void FWM_Load(System::Object^  sender, System::EventArgs^  e) {
				 windowState = this->WindowState;
			 }
	private: System::Void renderToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 renderingActive = !renderingActive;
			 }
	private: System::Void openToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (isRendering)
					 return;
				 Stream^ inFile;
				 //this->openFileDialog1->InitialDirectory = Environment::GetFolderPath(Environment::SpecialFolder::MyDocuments) + "\\Flames";
				 this->openFileDialog1->InitialDirectory = Environment::GetFolderPath(Environment::SpecialFolder::ProgramFiles) + "\\Apophysis 2.0";
				 if (this->openFileDialog1->ShowDialog() == ::DialogResult::OK)
				 {
					 if ((inFile = openFileDialog1->OpenFile()) != nullptr)
					 {
						 array<String^>^ Filename = openFileDialog1->FileName->Split('.');
						 if (Filename[Filename->Length-1]->Equals("flam4"))
						 {
							 Animation^ ani = gcnew Animation();
							 ani->LoadFlam4(inFile);
						 }
						 else
						 {
							 Animation^ ani = gcnew Animation();
							 g_pFlameParams = (flameParameterList*)ani->OpenFlam3(inFile);
							 inFile->Close();
							 flameParameterList* temp = g_pFlameParams;
							 while (temp != nullptr)
							 {
								temp->flame->quality = quality;
								temp = temp->nextFlame;
							}
						 }
					 }
				 }
				 this->Text = L"Flames - "+openFileDialog1->FileName;
			 }
	private: System::Void exitToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (cudaActive)
				 {
					 StopCuda();
					 cudaActive = false;
				 }
				 this->Close();
			 }
	private: System::Void FWM_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {

			 }
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
				 if (isRendering)
					 return;
				 if (renderingActive && !suspendRendering)
				 {
					 renderingActive = false;
					 Render((int)this->panel1->Handle,this->panel1->ClientRectangle.Width, this->panel1->ClientRectangle.Height,g_pFlameParams);
					 renderingActive = true;
					 __int64 time = stopWatch.ElapsedMilliseconds;
					 double t = 1000.0/(((double)time)+.00001);
					 stopWatch.Reset();
					 stopWatch.Start();
					 this->Text = L"Flames - "+openFileDialog1->FileName+" "+Convert::ToString(t);
				 }

			 }
	private: System::Void FWM_ResizeBegin(System::Object^  sender, System::EventArgs^  e) {
				 if (isRendering)
					 return;
				 suspendRendering = true;
				 if (cudaActive)
				 {
					 StopCuda();
					 cudaActive = false;
				 }
			 }
	private: System::Void FWM_ResizeEnd(System::Object^  sender, System::EventArgs^  e) {
				 if (isRendering)
					 return;
				 suspendRendering = false;
			 }
	private: System::Void optionsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 Flam4CUDA::Options^ OptionForm = gcnew Flam4CUDA::Options();
				 if (!optionsOpen)
				 {
					 OptionForm->Show();
					 optionsOpen = true;
				 }
			 }
	private: System::Void renderImageToFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (!g_pFlameParams)
					 return;
				 if (isRendering)
					 return;
				 renderingActive = false;
				 Flam4CUDA::PNGSettings^ PNGSettingsForm = gcnew Flam4CUDA::PNGSettings();
				 PNGSettingsForm->ShowDialog(this);
			 }
	private: System::Void FWM_Resize(System::Object^  sender, System::EventArgs^  e) {
				 if (isRendering)
					 return;
				 if ((this->WindowState == FormWindowState::Maximized) || (windowState == FormWindowState::Maximized))
				 {
				 }
				 windowState = this->WindowState;
			 }
	private: System::Void renderLoopToFileToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (!g_pFlameParams)
					 return;
				 if (isRendering)
					 return;
				 renderingActive = false;
				 renderWindowHandle = (int)(this->panel1->Handle);
				 Flam4CUDA::AVIOptions^ AVIOptionsForm = gcnew Flam4CUDA::AVIOptions();
				 AVIOptionsForm->ShowDialog(this);
				
			 }
private: System::Void renderImageBatchToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (!g_pFlameParams)
					 return;
				 if (isRendering)
					 return;
				 renderingActive = false;
				 Flam4CUDA::PNGBatchSettings^ PNGBatchSettingsForm = gcnew Flam4CUDA::PNGBatchSettings();
				 PNGBatchSettingsForm->ShowDialog(this);
		 }
};
}
