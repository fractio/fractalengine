#pragma once

using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;

#include "fwminterop.h"
#include "RenderThread.h"


namespace Flam4CUDA {

	/// <summary>
	/// Summary for PNGBatchSettings
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class PNGBatchSettings : public System::Windows::Forms::Form
	{
	public:
		PNGBatchSettings(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PNGBatchSettings()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::CheckBox^  alphaCheckBox1;
	protected: 
	private: System::Windows::Forms::Label^  ActualQualityLabel;
	private: System::Windows::Forms::TextBox^  qualityText;
	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
	private: System::Windows::Forms::Button^  cancelButton;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Button^  renderButton;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  heightText;
	private: System::Windows::Forms::TextBox^  widthText;
	private: System::Windows::Forms::ProgressBar^  progressBar1;
	private: System::Windows::Forms::ProgressBar^  progressBar2;
	private: System::ComponentModel::IContainer^  components;
	private: Thread^ oThread;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->alphaCheckBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->ActualQualityLabel = (gcnew System::Windows::Forms::Label());
			this->qualityText = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->cancelButton = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->renderButton = (gcnew System::Windows::Forms::Button());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->heightText = (gcnew System::Windows::Forms::TextBox());
			this->widthText = (gcnew System::Windows::Forms::TextBox());
			this->progressBar1 = (gcnew System::Windows::Forms::ProgressBar());		
			this->progressBar2 = (gcnew System::Windows::Forms::ProgressBar());
			this->SuspendLayout();
			// 
			// alphaCheckBox1
			// 
			this->alphaCheckBox1->AutoSize = true;
			this->alphaCheckBox1->Location = System::Drawing::Point(296, 7);
			this->alphaCheckBox1->Name = L"alphaCheckBox1";
			this->alphaCheckBox1->Size = System::Drawing::Size(15, 14);
			this->alphaCheckBox1->TabIndex = 27;
			this->alphaCheckBox1->UseVisualStyleBackColor = true;
			// 
			// ActualQualityLabel
			// 
			this->ActualQualityLabel->AutoSize = true;
			this->ActualQualityLabel->Location = System::Drawing::Point(162, 51);
			this->ActualQualityLabel->Name = L"ActualQualityLabel";
			this->ActualQualityLabel->Size = System::Drawing::Size(78, 13);
			this->ActualQualityLabel->TabIndex = 25;
			this->ActualQualityLabel->Text = L"Actual Quality: ";
			// 
			// qualityText
			// 
			this->qualityText->Location = System::Drawing::Point(56, 48);
			this->qualityText->Name = L"qualityText";
			this->qualityText->Size = System::Drawing::Size(100, 20);
			this->qualityText->TabIndex = 17;
			this->qualityText->TextChanged += gcnew System::EventHandler(this, &PNGBatchSettings::qualityText_TextChanged);
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(12, 51);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(42, 13);
			this->label5->TabIndex = 23;
			this->label5->Text = L"Quality:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(165, 8);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(125, 13);
			this->label6->TabIndex = 26;
			this->label6->Text = L"Transparent Background";
			// 
			// folderBrowserDialog1
			// 
			this->folderBrowserDialog1->Description = L"Select a folder to save output images to:";
			// 
			// cancelButton
			// 
			this->cancelButton->Location = System::Drawing::Point(12, 113);
			this->cancelButton->Name = L"cancelButton";
			this->cancelButton->Size = System::Drawing::Size(75, 23);
			this->cancelButton->TabIndex = 22;
			this->cancelButton->Text = L"Cancel";
			this->cancelButton->UseVisualStyleBackColor = true;
			this->cancelButton->Click += gcnew System::EventHandler(this, &PNGBatchSettings::cancelButton_Click);
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &PNGBatchSettings::timer1_Tick);
			// 
			// renderButton
			// 
			this->renderButton->Location = System::Drawing::Point(231, 113);
			this->renderButton->Name = L"renderButton";
			this->renderButton->Size = System::Drawing::Size(75, 23);
			this->renderButton->TabIndex = 24;
			this->renderButton->Text = L"Render!";
			this->renderButton->UseVisualStyleBackColor = true;
			this->renderButton->Click += gcnew System::EventHandler(this, &PNGBatchSettings::renderButton_Click);
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(9, 74);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(51, 13);
			this->label4->TabIndex = 21;
			this->label4->Text = L"Progress:";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(162, 25);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(41, 13);
			this->label3->TabIndex = 20;
			this->label3->Text = L"Height:";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(12, 25);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(38, 13);
			this->label2->TabIndex = 19;
			this->label2->Text = L"Width:";
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(60, 13);
			this->label1->TabIndex = 18;
			this->label1->Text = L"Resolution:";
			// 
			// heightText
			// 
			this->heightText->Location = System::Drawing::Point(209, 22);
			this->heightText->Name = L"heightText";
			this->heightText->Size = System::Drawing::Size(100, 20);
			this->heightText->TabIndex = 16;
			this->heightText->TextChanged += gcnew System::EventHandler(this, &PNGBatchSettings::heightText_TextChanged);
			// 
			// widthText
			// 
			this->widthText->Location = System::Drawing::Point(56, 22);
			this->widthText->Name = L"widthText";
			this->widthText->Size = System::Drawing::Size(100, 20);
			this->widthText->TabIndex = 15;
			this->widthText->TextChanged += gcnew System::EventHandler(this, &PNGBatchSettings::widthText_TextChanged);
			// 
			// progressBar1
			// 
			this->progressBar1->Location = System::Drawing::Point(66, 74);
			this->progressBar1->Maximum = 100000;
			this->progressBar1->Name = L"progressBar1";
			this->progressBar1->Size = System::Drawing::Size(240, 23);
			this->progressBar1->TabIndex = 14;
			//
			// progressBar2
			//
			this->progressBar2->Location = System::Drawing::Point(66, 97);
			this->progressBar2->Maximum = 100000;
			this->progressBar2->Name = L"progressBar2";
			this->progressBar2->Size = System::Drawing::Size(240, 10);
			this->progressBar2->TabIndex = 15;
			// 
			// PNGBatchSettings
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(314, 146);
			this->Controls->Add(this->alphaCheckBox1);
			this->Controls->Add(this->ActualQualityLabel);
			this->Controls->Add(this->qualityText);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->cancelButton);
			this->Controls->Add(this->renderButton);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->heightText);
			this->Controls->Add(this->widthText);
			this->Controls->Add(this->progressBar1);
			this->Controls->Add(this->progressBar2);
			this->Name = L"PNGBatchSettings";
			this->Text = L"PNGBatchSettings";
			this->Load += gcnew System::EventHandler(this, &PNGBatchSettings::PNGSettings_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void renderButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 wchar_t* outDirectory;
				this->folderBrowserDialog1->SelectedPath = Environment::GetFolderPath(Environment::SpecialFolder::MyPictures)+ "\\Flames";
				if (this->folderBrowserDialog1->ShowDialog() == ::DialogResult::OK)
				{
					outDirectory = (wchar_t*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(folderBrowserDialog1->SelectedPath).ToPointer();
				}
			 else
				 return;
			 if (cudaActive)
			 {
				 StopCuda();
				 cudaActive = false;
			 }
			 this->renderButton->Enabled = false;
			 RenderThread^ thread = gcnew RenderThread;
			 thread->flm = g_pFlameParams;
			 thread->width = Convert::ToInt32(this->widthText->Text);
			 thread->height = Convert::ToInt32(this->heightText->Text);
			 flameParameterList* f = thread->flm;
			 int frameNum = 0;
			 do
			 {
				f->flame->quality = (double)thread->width*(double)thread->height*Convert::ToDouble(this->qualityText->Text)/(double)((1024*32*NUM_ITERATIONS)+1);
				f = f->nextFlame;
				frameNum++;
			 } while (f != nullptr);
			 thread->useAlpha = this->alphaCheckBox1->Checked;
			 this->progressBar1->Maximum = frameNum;
			 this->progressBar2->Maximum = thread->flm->flame->quality;
			 thread->outFile = outDirectory;
			 thread->hwnd = (int)this->Owner->Handle;
			 oThread = gcnew Thread( gcnew ThreadStart(thread, &RenderThread::RenderPNGBatchProc ));
			 oThread->SetApartmentState(ApartmentState::MTA);
			 oThread->Name = "Render Thread";
			 oThread->Start();
		 }
private: System::Void PNGSettings_Load(System::Object^  sender, System::EventArgs^  e) {
			 this->widthText->Text = Convert::ToString(this->Owner->ClientRectangle.Width);
			 this->heightText->Text = Convert::ToString(this->Owner->ClientRectangle.Height-24);
			 this->qualityText->Text = Convert::ToString(((double)1024*32*NUM_ITERATIONS*g_pFlameParams->flame->quality)/((double)this->Owner->ClientRectangle.Width*(double)(this->Owner->ClientRectangle.Height-27)));
			 progress = 0;
			 frameNumber = 0;
		 }
private: System::Void cancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 isDone = false;
			 if (isRendering)
				abortRender = true;
			 while (abortRender)
			 {}
			 this->Close();
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 if (isDone)
			 {
				 isDone = false;
				 this->Close();
			 }
			 try
			 {
				 this->progressBar1->Value = frameNumber;
				 this->progressBar2->Value = progress;
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
		 }
private: System::Void qualityText_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->qualityText->Text);
				 width = Convert::ToInt32(this->widthText->Text);
				 height = Convert::ToInt32(this->heightText->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityLabel->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
private: System::Void widthText_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->qualityText->Text);
				 width = Convert::ToInt32(this->widthText->Text);
				 height = Convert::ToInt32(this->heightText->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityLabel->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
private: System::Void heightText_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->qualityText->Text);
				 width = Convert::ToInt32(this->widthText->Text);
				 height = Convert::ToInt32(this->heightText->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityLabel->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
};
}