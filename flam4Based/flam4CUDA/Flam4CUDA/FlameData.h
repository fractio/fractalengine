/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#define MAX_XFORMS 75 //We're limited to 64KB constant memory for compute capacity 1.0.
                       //All xForms must fit in this.
#define VAR_COUNT 97

struct rgba
{
	float r;
	float g;
	float b;
	float a;
};

struct xForm
{
	union
	{
		struct
		{
			float a;
			float b;
			float c;
			float d;
			float e;
			float f;

			float pre_blur;
			float linear;
			float sinusoidal;
			float spherical;
			float swirl;
			float horseshoe;
			float polar;
			float handkerchief;
			float heart;
			float disc;
			float spiral;
			float hyperbolic;
			float diamond;
			float ex;
			float julia;
			float bent;
			float waves;
			float fisheye;
			float popcorn;
			float exponential;
			float power;
			float cosine;
			float rings;
			float fan;
			float blob;
			float pdj;
			float fan2;
			float rings2;
			float eyefish;
			float bubble;
			float cylinder;
			float perspective;
			float noise;
			float julian;
			float juliascope;
			float blur;
			float gaussian_blur;
			float radial_blur;
			float pie;
			float ngon;
			float curl;
			float rectangles;
			float arch;
			float tangent;
			float square;
			float rays;
			float blade;
			float secant;
			float twintrian;
			float cross;
			float disc2;
			float supershape;
			float flower;
			float conic;
			float parabola;
			float bent2;
			float bipolar;
			float boarders;
			float butterfly;
			float cell;
			float cpow;
			float curve;
			float edisc;
			float elliptic;
			float escher;
			float foci;
			float lazysusan;
			float loonie;
			float modulus;
			float oscilloscope;
			float polar2;
			float popcorn2;
			float scry;
			float separation;
			float split;
			float splits;
			float stripes;
			float wedge;
			float wedge_julia;
			float wedge_sph;
			float whorl;
			float waves2;
			float v_exp;
			float v_log;
			float v_sin;
			float v_cos;
			float v_tan;
			float v_sec;
			float v_csc;
			float v_cot;
			float v_sinh;
			float v_cosh;
			float v_tanh;
			float v_sech;
			float v_csch;
			float v_coth;
			float auger;

			float pa;
			float pb;
			float pc;
			float pd;
			float pe;
			float pf;

			float blob_high;
			float blob_low;
			float blob_waves;
			float pdj_a;
			float pdj_b;
			float pdj_c;
			float pdj_d;
			float fan2_x;
			float fan2_y;
			float rings2_val;
			float perspective_angle;
			float perspective_dist;
			float julian_power;
			float julian_dist;
			float juliascope_power;
			float juliascope_dist;
			float radial_blur_angle;
			float pie_slices;
			float pie_rotation;
			float pie_thickness;
			float ngon_power;
			float ngon_sides;
			float ngon_corners;
			float ngon_circle;
			float curl_c1;
			float curl_c2;
			float rectangles_x;
			float rectangles_y;
			float disc2_rot;
			float disc2_twist;
			float supershape_m;
			float supershape_n1;
			float supershape_n2;
			float supershape_n3;
			float supershape_holes;
			float supershape_rnd;
			float flower_holes;
			float flower_petals;
			float conic_holes;
			float conic_eccen;
			float parabola_height;
			float parabola_width;
			float bent2_x;
			float bent2_y;
			float bipolar_shift;
			float cell_size;
			float cpow_r;
			float cpow_i;
			float cpow_power;
			float curve_xamp;
			float curve_yamp;
			float curve_xlength;
			float curve_ylength;
			float escher_beta;
			float lazysusan_space;
			float lazysusan_twist;
			float lazysusan_spin;
			float lazysusan_x;
			float lazysusan_y;
			float modulus_x;
			float modulus_y;
			float oscilloscope_separation;
			float oscilloscope_frequency;
			float oscilloscope_amplitude;
			float oscilloscope_damping;
			float popcorn2_c;
			float popcorn2_x;
			float popcorn2_y;
			float separation_x;
			float separation_xinside;
			float separation_y;
			float separation_yinside;
			float split_xsize;
			float split_ysize;
			float splits_x;
			float splits_y;
			float stripes_space;
			float stripes_warp;
			float wedge_angle;
			float wedge_hole;
			float wedge_count;
			float wedge_swirl;
			float wedge_julia_power;
			float wedge_julia_dist;
			float wedge_julia_count;
			float wedge_julia_angle;
			float wedge_sph_angle;
			float wedge_sph_hole;
			float wedge_sph_count;
			float wedge_sph_swirl;
			float whorl_inside;
			float whorl_outside;
			float waves2_scalex;
			float waves2_scaley;
			float waves2_freqx;
			float waves2_freqy;
			float auger_weight;
			float auger_freq;
			float auger_scale;
			float auger_sym;

			float color;
			float symmetry;		
			float weight;
			float opacity;
		};
		float xFormVars[213];
	};
};

struct unAnimatedxForm
{
	float a;
	float b;
	float d;
	float e;
	int rotates;
};

struct FlameParams
{
	float center[2];				//{x,y}
	float size[2];					//size/(scale*zoom)
	float hue;
	float quality;
	float rotation;
	float symmetryKind;
	rgba background;
	float brightness;
	float gamma;
	float vibrancy;
	int numTrans;
	int isFinalXform;
	xForm finalXform;
};


struct Flame
{
	union
	{
		struct
		{
			float center[2];				//{x,y}
			float size[2];					//size/(scale*zoom)
			float hue;
			float quality;
			float rotation;
			float symmetryKind;
			rgba background;
			float brightness;
			float gamma;
			float vibrancy;
			int numTrans;
			int isFinalXform;
			xForm finalXform;
		};
		FlameParams params;
	};
	int numColors;
	xForm *trans;
	unAnimatedxForm *transAff;
	rgba *colorIndex;
	Flame() {}
	Flame(int numTrans,int paletteSize)
	{
		this->numTrans = numTrans;
		trans = new xForm[numTrans];
		transAff = new unAnimatedxForm[numTrans];
		this->numColors = paletteSize;
		colorIndex = new rgba[paletteSize];
	}
	void Clone(Flame** target)
	{
		if (!*target)
		{
			*target = new Flame(this->numTrans, this->numColors);
		}
		if (((*target)->numTrans != this->numTrans) || ((*target)->numColors != this->numColors))
		{
			delete *target;
			*target = new Flame(this->numTrans, this->numColors);
		}
		for (int n = 0; n < this->numTrans; n++)
		{
			(*target)->trans[n] = this->trans[n];
			(*target)->transAff[n] = this->transAff[n];
		}
		for (int n = 0; n < this->numColors; n++)
		{
			(*target)->colorIndex[n] = this->colorIndex[n];
		}
		(*target)->params = this->params;
	}
	~Flame()
	{
		delete trans;
		delete transAff;
		delete colorIndex;
	}
};
#pragma endregion
