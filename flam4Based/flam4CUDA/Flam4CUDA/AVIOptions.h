#pragma once

using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;

#include "fwminterop.h"
#include "RenderThread.h"
#include "Globals.h"

extern bool cudaActive;
extern int renderWindowHandle;
extern int g_NumFrames;
extern int g_EncoderFramerate;
extern int g_EncoderBitrate;


namespace Flam4CUDA {

	/// <summary>
	/// Summary for AVIOptions
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class AVIOptions : public System::Windows::Forms::Form
	{
	public:
		AVIOptions(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~AVIOptions()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::TextBox^  WidthTextBox;
	private: System::Windows::Forms::TextBox^  HeightTextbox;
	private: System::Windows::Forms::TextBox^  QualtiyTextbox;
	private: System::Windows::Forms::Label^  ActualQualityText;

	private: System::Windows::Forms::Label^  label5;
	private: System::Windows::Forms::Label^  CurrentFrameText;

	private: System::Windows::Forms::ProgressBar^  TotalProgressBar;

	private: System::Windows::Forms::ProgressBar^  FrameProgressBar;
	private: System::Windows::Forms::Button^  RenderButton;
	private: System::Windows::Forms::Button^  CancelButton;
	private: System::Windows::Forms::Label^  label7;
	private: System::Windows::Forms::Label^  label8;
	private: System::Windows::Forms::TextBox^  FramesTextbox;
	private: System::Windows::Forms::TextBox^  FramerateTextbox;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::SaveFileDialog^  saveFileDialog1;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::TextBox^  BitrateTextbox;
	private: System::Windows::Forms::Label^  FilesizeText;
	private: System::Windows::Forms::FolderBrowserDialog^  folderBrowserDialog1;
	private: System::Windows::Forms::Label^  label6;
	private: System::Windows::Forms::ComboBox^  OutputTypeComboBox;
	private: System::Windows::Forms::CheckBox^  AlphaCheckBox;
	private: System::ComponentModel::IContainer^  components;



	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->WidthTextBox = (gcnew System::Windows::Forms::TextBox());
			this->HeightTextbox = (gcnew System::Windows::Forms::TextBox());
			this->QualtiyTextbox = (gcnew System::Windows::Forms::TextBox());
			this->ActualQualityText = (gcnew System::Windows::Forms::Label());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->CurrentFrameText = (gcnew System::Windows::Forms::Label());
			this->TotalProgressBar = (gcnew System::Windows::Forms::ProgressBar());
			this->FrameProgressBar = (gcnew System::Windows::Forms::ProgressBar());
			this->RenderButton = (gcnew System::Windows::Forms::Button());
			this->CancelButton = (gcnew System::Windows::Forms::Button());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->FramesTextbox = (gcnew System::Windows::Forms::TextBox());
			this->FramerateTextbox = (gcnew System::Windows::Forms::TextBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->saveFileDialog1 = (gcnew System::Windows::Forms::SaveFileDialog());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->BitrateTextbox = (gcnew System::Windows::Forms::TextBox());
			this->FilesizeText = (gcnew System::Windows::Forms::Label());
			this->folderBrowserDialog1 = (gcnew System::Windows::Forms::FolderBrowserDialog());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->OutputTypeComboBox = (gcnew System::Windows::Forms::ComboBox());
			this->AlphaCheckBox = (gcnew System::Windows::Forms::CheckBox());
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(12, 15);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Width";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(175, 15);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(38, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"Height";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(325, 15);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(39, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"Qualtiy";
			// 
			// WidthTextBox
			// 
			this->WidthTextBox->Location = System::Drawing::Point(53, 12);
			this->WidthTextBox->Name = L"WidthTextBox";
			this->WidthTextBox->Size = System::Drawing::Size(100, 20);
			this->WidthTextBox->TabIndex = 3;
			this->WidthTextBox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::WidthTextBox_TextChanged);
			// 
			// HeightTextbox
			// 
			this->HeightTextbox->Location = System::Drawing::Point(219, 12);
			this->HeightTextbox->Name = L"HeightTextbox";
			this->HeightTextbox->Size = System::Drawing::Size(100, 20);
			this->HeightTextbox->TabIndex = 4;
			this->HeightTextbox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::HeightTextbox_TextChanged);
			// 
			// QualtiyTextbox
			// 
			this->QualtiyTextbox->Location = System::Drawing::Point(370, 11);
			this->QualtiyTextbox->Name = L"QualtiyTextbox";
			this->QualtiyTextbox->Size = System::Drawing::Size(100, 20);
			this->QualtiyTextbox->TabIndex = 5;
			this->QualtiyTextbox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::QualtiyTextbox_TextChanged);
			// 
			// ActualQualityText
			// 
			this->ActualQualityText->AutoSize = true;
			this->ActualQualityText->Location = System::Drawing::Point(476, 15);
			this->ActualQualityText->Name = L"ActualQualityText";
			this->ActualQualityText->Size = System::Drawing::Size(76, 13);
			this->ActualQualityText->TabIndex = 6;
			this->ActualQualityText->Text = L"Actual quality: ";
			// 
			// label5
			// 
			this->label5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(12, 73);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(75, 13);
			this->label5->TabIndex = 7;
			this->label5->Text = L"Total Progress";
			// 
			// CurrentFrameText
			// 
			this->CurrentFrameText->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->CurrentFrameText->AutoSize = true;
			this->CurrentFrameText->Location = System::Drawing::Point(291, 143);
			this->CurrentFrameText->Name = L"CurrentFrameText";
			this->CurrentFrameText->Size = System::Drawing::Size(76, 13);
			this->CurrentFrameText->TabIndex = 8;
			this->CurrentFrameText->Text = L"Current Frame:";
			this->CurrentFrameText->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// TotalProgressBar
			// 
			this->TotalProgressBar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->TotalProgressBar->Location = System::Drawing::Point(12, 89);
			this->TotalProgressBar->Margin = System::Windows::Forms::Padding(3, 3, 3, 0);
			this->TotalProgressBar->Name = L"TotalProgressBar";
			this->TotalProgressBar->Size = System::Drawing::Size(629, 34);
			this->TotalProgressBar->TabIndex = 9;
			// 
			// FrameProgressBar
			// 
			this->FrameProgressBar->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->FrameProgressBar->BackColor = System::Drawing::SystemColors::Control;
			this->FrameProgressBar->ForeColor = System::Drawing::Color::Red;
			this->FrameProgressBar->Location = System::Drawing::Point(12, 123);
			this->FrameProgressBar->Margin = System::Windows::Forms::Padding(3, 0, 3, 3);
			this->FrameProgressBar->Name = L"FrameProgressBar";
			this->FrameProgressBar->Size = System::Drawing::Size(628, 12);
			this->FrameProgressBar->TabIndex = 10;
			// 
			// RenderButton
			// 
			this->RenderButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->RenderButton->Location = System::Drawing::Point(569, 138);
			this->RenderButton->Name = L"RenderButton";
			this->RenderButton->Size = System::Drawing::Size(75, 23);
			this->RenderButton->TabIndex = 11;
			this->RenderButton->Text = L"Render!";
			this->RenderButton->UseVisualStyleBackColor = true;
			this->RenderButton->Click += gcnew System::EventHandler(this, &AVIOptions::RenderButton_Click);
			// 
			// CancelButton
			// 
			this->CancelButton->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->CancelButton->Location = System::Drawing::Point(15, 138);
			this->CancelButton->Name = L"CancelButton";
			this->CancelButton->Size = System::Drawing::Size(75, 23);
			this->CancelButton->TabIndex = 12;
			this->CancelButton->Text = L"Cancel";
			this->CancelButton->UseVisualStyleBackColor = true;
			this->CancelButton->Click += gcnew System::EventHandler(this, &AVIOptions::CancelButton_Click);
			// 
			// label7
			// 
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 41);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(41, 13);
			this->label7->TabIndex = 13;
			this->label7->Text = L"Frames";
			// 
			// label8
			// 
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(159, 41);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(54, 13);
			this->label8->TabIndex = 14;
			this->label8->Text = L"Framerate";
			// 
			// FramesTextbox
			// 
			this->FramesTextbox->Location = System::Drawing::Point(53, 38);
			this->FramesTextbox->Name = L"FramesTextbox";
			this->FramesTextbox->Size = System::Drawing::Size(100, 20);
			this->FramesTextbox->TabIndex = 15;
			this->FramesTextbox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::FramesTextbox_TextChanged);
			// 
			// FramerateTextbox
			// 
			this->FramerateTextbox->Location = System::Drawing::Point(219, 38);
			this->FramerateTextbox->Name = L"FramerateTextbox";
			this->FramerateTextbox->Size = System::Drawing::Size(100, 20);
			this->FramerateTextbox->TabIndex = 16;
			this->FramerateTextbox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::FramerateTextbox_TextChanged);
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &AVIOptions::timer1_Tick);
			// 
			// saveFileDialog1
			// 
			this->saveFileDialog1->Filter = L"AVI files (*.avi)|*.avi|All Files (*.*)|*.*";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(327, 40);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(37, 13);
			this->label4->TabIndex = 17;
			this->label4->Text = L"Bitrate";
			// 
			// BitrateTextbox
			// 
			this->BitrateTextbox->Location = System::Drawing::Point(370, 37);
			this->BitrateTextbox->Name = L"BitrateTextbox";
			this->BitrateTextbox->Size = System::Drawing::Size(100, 20);
			this->BitrateTextbox->TabIndex = 18;
			this->BitrateTextbox->TextChanged += gcnew System::EventHandler(this, &AVIOptions::BitrateTextbox_TextChanged);
			// 
			// FilesizeText
			// 
			this->FilesizeText->AutoSize = true;
			this->FilesizeText->Location = System::Drawing::Point(476, 41);
			this->FilesizeText->Name = L"FilesizeText";
			this->FilesizeText->Size = System::Drawing::Size(101, 13);
			this->FilesizeText->TabIndex = 19;
			this->FilesizeText->Text = L"Estimated File Size: ";
			// 
			// folderBrowserDialog1
			// 
			this->folderBrowserDialog1->Description = L"Select a folder to save output images to:";
			// 
			// label6
			// 
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(147, 64);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(66, 13);
			this->label6->TabIndex = 20;
			this->label6->Text = L"Output Type";
			// 
			// OutputTypeComboBox
			// 
			this->OutputTypeComboBox->FormattingEnabled = true;
			this->OutputTypeComboBox->Items->AddRange(gcnew cli::array< System::Object^  >(2) {L"AVI", L"PNG Sequence"});
			this->OutputTypeComboBox->Location = System::Drawing::Point(219, 61);
			this->OutputTypeComboBox->Name = L"OutputTypeComboBox";
			this->OutputTypeComboBox->Size = System::Drawing::Size(121, 21);
			this->OutputTypeComboBox->TabIndex = 21;
			this->OutputTypeComboBox->Text = L"AVI";
			this->OutputTypeComboBox->SelectionChangeCommitted += gcnew System::EventHandler(this, &AVIOptions::OutputTypeComboBox_SelectionChangeCommitted);
			// 
			// AlphaCheckBox
			// 
			this->AlphaCheckBox->AutoSize = true;
			this->AlphaCheckBox->Enabled = false;
			this->AlphaCheckBox->Location = System::Drawing::Point(370, 64);
			this->AlphaCheckBox->Name = L"AlphaCheckBox";
			this->AlphaCheckBox->Size = System::Drawing::Size(75, 17);
			this->AlphaCheckBox->TabIndex = 22;
			this->AlphaCheckBox->Text = L"Use Alpha";
			this->AlphaCheckBox->UseVisualStyleBackColor = true;
			// 
			// AVIOptions
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(656, 170);
			this->Controls->Add(this->AlphaCheckBox);
			this->Controls->Add(this->OutputTypeComboBox);
			this->Controls->Add(this->label6);
			this->Controls->Add(this->FilesizeText);
			this->Controls->Add(this->BitrateTextbox);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->FramerateTextbox);
			this->Controls->Add(this->FramesTextbox);
			this->Controls->Add(this->label8);
			this->Controls->Add(this->label7);
			this->Controls->Add(this->CancelButton);
			this->Controls->Add(this->RenderButton);
			this->Controls->Add(this->FrameProgressBar);
			this->Controls->Add(this->TotalProgressBar);
			this->Controls->Add(this->CurrentFrameText);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->ActualQualityText);
			this->Controls->Add(this->QualtiyTextbox);
			this->Controls->Add(this->HeightTextbox);
			this->Controls->Add(this->WidthTextBox);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->Name = L"AVIOptions";
			this->Text = L"Render to AVI";
			this->Load += gcnew System::EventHandler(this, &AVIOptions::AVIOptions_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void CancelButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (isRendering)
				abortRender = true;
			 while (abortRender)
			 {}
			 this->Close();
		 }
private: System::Void RenderButton_Click(System::Object^  sender, System::EventArgs^  e) {
			if (this->OutputTypeComboBox->SelectedItem->ToString()->CompareTo("AVI") == 0)
			{
				wchar_t* outFile;
				this->saveFileDialog1->InitialDirectory = Environment::GetFolderPath(Environment::SpecialFolder::MyPictures)+ "\\Flames";
				if (this->saveFileDialog1->ShowDialog() == ::DialogResult::OK)
				{
					outFile = (wchar_t*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(saveFileDialog1->FileName).ToPointer();
				}
				else
					return;
				if (cudaActive)
				{
					StopCuda();
					cudaActive = false;
				}
				this->RenderButton->Enabled = false;
				RenderThread^ thread = gcnew RenderThread;
				thread->flm = g_pFlameParams;
				thread->width = Convert::ToInt32(this->WidthTextBox->Text);
				thread->height = Convert::ToInt32(this->HeightTextbox->Text);
				g_NumFrames = Convert::ToInt32(this->FramesTextbox->Text);
				g_EncoderFramerate = Convert::ToInt32(this->FramerateTextbox->Text);
				g_EncoderBitrate = Convert::ToInt32(this->BitrateTextbox->Text);
				flameParameterList* temp;
				temp = thread->flm;
				while (temp != nullptr)
				{
					temp->flame->quality = (long long)thread->width*(long long)thread->height*Convert::ToSingle(this->QualtiyTextbox->Text)/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
					temp = temp->nextFlame;
				}
				this->FrameProgressBar->Maximum = thread->flm->flame->quality;
				this->TotalProgressBar->Maximum = g_NumFrames-1;
				thread->outFile = outFile;
				thread->hwnd = (int)renderWindowHandle;
				Thread^ oThread = gcnew Thread( gcnew ThreadStart(thread, &RenderThread::RenderAVIProc ));
				oThread->SetApartmentState(ApartmentState::MTA);
				oThread->Name = "Render Thread";
				oThread->Start();
			}
			else
			{
				wchar_t* outDirectory;
				this->folderBrowserDialog1->SelectedPath = Environment::GetFolderPath(Environment::SpecialFolder::MyPictures)+ "\\Flames";
				if (this->folderBrowserDialog1->ShowDialog() == ::DialogResult::OK)
				{
					outDirectory = (wchar_t*)System::Runtime::InteropServices::Marshal::StringToHGlobalUni(folderBrowserDialog1->SelectedPath).ToPointer();
				}
				else
					return;
				if (cudaActive)
				{
					StopCuda();
					cudaActive = false;
				}
				this->RenderButton->Enabled = false;
				RenderThread^ thread = gcnew RenderThread;
				thread->flm = g_pFlameParams;
				thread->width = Convert::ToInt32(this->WidthTextBox->Text);
				thread->height = Convert::ToInt32(this->HeightTextbox->Text);
				thread->useAlpha = this->AlphaCheckBox->Checked;
				g_NumFrames = Convert::ToInt32(this->FramesTextbox->Text);
				flameParameterList* temp;
				temp = thread->flm;
				while (temp != nullptr)
				{
					temp->flame->quality = (long long)thread->width*(long long)thread->height*Convert::ToSingle(this->QualtiyTextbox->Text)/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
					temp = temp->nextFlame;
				}
				this->FrameProgressBar->Maximum = thread->flm->flame->quality;
				this->TotalProgressBar->Maximum = g_NumFrames-1;
				thread->outFile = outDirectory;
				thread->hwnd = (int)renderWindowHandle;
				Thread^ oThread = gcnew Thread( gcnew ThreadStart(thread, &RenderThread::RenderPNGSeqProc ));
				oThread->SetApartmentState(ApartmentState::MTA);
				oThread->Name = "Render Thread";
				oThread->Start();
			}
		 }
private: System::Void QualtiyTextbox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->QualtiyTextbox->Text);
				 width = Convert::ToInt32(this->WidthTextBox->Text);
				 height = Convert::ToInt32(this->HeightTextbox->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityText->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
private: System::Void WidthTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->QualtiyTextbox->Text);
				 width = Convert::ToInt32(this->WidthTextBox->Text);
				 height = Convert::ToInt32(this->HeightTextbox->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityText->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
private: System::Void HeightTextbox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 int qual;
			 int width;
			 int height;
			 try
			 {
				 qual = Convert::ToInt32(this->QualtiyTextbox->Text);
				 width = Convert::ToInt32(this->WidthTextBox->Text);
				 height = Convert::ToInt32(this->HeightTextbox->Text);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
			 qual = (long long)width*(long long)height*qual/((long long)1024LL*32LL*NUM_ITERATIONS)+1;
			 this->ActualQualityText->Text = L"Actual Quality: " + Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*qual)/((long long)width*(long long)height));
		 }
private: System::Void AVIOptions_Load(System::Object^  sender, System::EventArgs^  e) {
			 this->WidthTextBox->Text = Convert::ToString(this->Owner->ClientRectangle.Width);
			 this->HeightTextbox->Text = Convert::ToString(this->Owner->ClientRectangle.Height-24);
			 this->QualtiyTextbox->Text = Convert::ToString(((long long)1024LL*32LL*NUM_ITERATIONS*g_pFlameParams->flame->quality)/((long long)this->Owner->ClientRectangle.Width*(long long)(this->Owner->ClientRectangle.Height-27)));
			 this->FramesTextbox->Text = Convert::ToString(g_NumFrames);
			 this->FramerateTextbox->Text = Convert::ToString(g_EncoderFramerate);
			 this->BitrateTextbox->Text = Convert::ToString(g_EncoderBitrate);
			 progress = 0;
			 frameNumber = 0;
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 if (isDone)
			 {
				 isDone = false;
				 this->Close();
			 }
			 try
			 {
				 this->TotalProgressBar->Value = frameNumber;
				 this->FrameProgressBar->Value = progress;
				 this->CurrentFrameText->Text = L"Current Frame: "+Convert::ToString(frameNumber+1)+L" of "+Convert::ToString(g_NumFrames);
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
		 }
private: System::Void BitrateTextbox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 try
			 {
				 this->FilesizeText->Text = L"Estimated File Size: " + Convert::ToString( Convert::ToSingle(this->BitrateTextbox->Text)*Convert::ToSingle(this->FramesTextbox->Text)/(8000000.0f*Convert::ToSingle(this->FramerateTextbox->Text)))+L"MB";
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
		 }
private: System::Void FramesTextbox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			  try
			 {
				 this->FilesizeText->Text = L"Estimated File Size: " + Convert::ToString( Convert::ToSingle(this->BitrateTextbox->Text)*Convert::ToSingle(this->FramesTextbox->Text)/(8000000.0f*Convert::ToSingle(this->FramerateTextbox->Text)))+L"MB";
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
		 }
private: System::Void FramerateTextbox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			  try
			 {
				 this->FilesizeText->Text = L"Estimated File Size: " + Convert::ToString( Convert::ToSingle(this->BitrateTextbox->Text)*Convert::ToSingle(this->FramesTextbox->Text)/(8000000.0f*Convert::ToSingle(this->FramerateTextbox->Text)))+L"MB";
			 }
			 catch (Exception^ ex)
			 {
				 return;
			 }
		 }
private: System::Void OutputTypeComboBox_SelectionChangeCommitted(System::Object^  sender, System::EventArgs^  e) {
			 if (this->OutputTypeComboBox->SelectedItem->ToString()->CompareTo("AVI") == 0)
			 {
				 this->FramerateTextbox->Enabled = true;
				 this->BitrateTextbox->Enabled = true;
				 this->FilesizeText->Visible = true;
				 this->AlphaCheckBox->Enabled = false;
			 }
			 else
			 {
				 this->FramerateTextbox->Enabled = false;
				 this->BitrateTextbox->Enabled = false;
				 this->FilesizeText->Visible = false;
				 this->AlphaCheckBox->Enabled = true;
			 }
		 }
};
}
