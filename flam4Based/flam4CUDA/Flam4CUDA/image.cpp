/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#define __WIN32
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <vfw.h>
#include <time.h>
#include "../png++-0.2.3/png.hpp"


//This function saves an image pointed to by image to filename.  xDim and yDim are the dimensions of the image.
int exportImageToPNG(wchar_t* filename, int xDim, int yDim, unsigned char * image,bool useAlpha)
{
	if (useAlpha)
	{
		png::image<png::rgba_pixel> img(xDim, yDim);
		for (unsigned int y = 0; y < img.get_height(); ++y)
		{
			for (unsigned int x = 0; x < img.get_width(); ++x)
			{
				img[img.get_height()-y-1][x] = png::rgba_pixel(image[4*(y*xDim+x)], image[4*(y*xDim+x)+1], image[4*(y*xDim+x)+2],image[4*(y*xDim+x)+3]);
			}
		}
		char str[1024];
		wcstombs(str, filename,1024);
		img.write(str);
	}
	else
	{
		png::image<png::rgb_pixel> img(xDim, yDim);
		for (unsigned int y = 0; y < img.get_height(); ++y)
		{
			for (unsigned int x = 0; x < img.get_width(); ++x)
			{
				img[img.get_height()-y-1][x] = png::rgb_pixel(image[4*(y*xDim+x)], image[4*(y*xDim+x)+1], image[4*(y*xDim+x)+2]);
			}
		}
		char str[1024];
		wcstombs(str, filename,1024);
		img.write(str);
	}
	return 0;
}