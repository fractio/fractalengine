/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "FlameData.h"
#include "FlameMetaData.h"

#pragma once
struct flameAnimation
{
	xForm emptyXform;
	flameAnimation();
	void Interpolate(float time, Flame* startFlame, Flame* endFlame, Flame** result);
};

struct flameParameterList
{
	Flame* flame;
	FlameMetaData* flameMeta;
	flameParameterList* nextFlame;
};
#pragma endregion
