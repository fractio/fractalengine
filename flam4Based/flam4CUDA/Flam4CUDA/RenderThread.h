/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "FlameData.h"
#include "FWMInterop.h"
#include "Animate.h"

extern volatile bool isRendering;
extern bool renderingActive;

using namespace System;
using namespace System::Globalization;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;
using namespace System::Xml;
using namespace System::Threading;

public ref class RenderThread
{
public:
	RenderThread() {}
	flameParameterList* flm;
	int width;
	int height;
	wchar_t* outFile;
	int hwnd;
	bool useAlpha;
	void RenderPNGProc();
	void RenderAVIProc();
	void RenderPNGSeqProc();
	void RenderPNGBatchProc();
};