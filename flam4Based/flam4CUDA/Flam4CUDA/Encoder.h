/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <xvid.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>
#include <vfw.h>
#include <time.h>

bool isXvidInitialized = false;
void* bitStream;
int g_EncoderFramerate = FRAME_RATE;
int g_EncoderBitrate = BITRATE;
extern int g_NumFrames;

struct XvidEncoder
{
	xvid_enc_create_t encoder;
	int xdim;
	int ydim;
	PAVIFILE myAVIFile;
	PAVISTREAM myAVIStream;
	AVISTREAMINFO myAVIStreamInfo;
	BITMAPINFOHEADER myBitmapInfoHeader;
	int BeginEncoding(int xDim, int yDim, wchar_t* outFile)
	{
		if (!isXvidInitialized)
		{
			xvid_gbl_init_t init;
			memset(&init,0,sizeof(init));
			init.version = XVID_VERSION;
			xvid_global(NULL,XVID_GBL_INIT,&init,NULL);
			isXvidInitialized = true;
		}
		memset(&encoder,0,sizeof(xvid_enc_create_t));
		encoder.version = XVID_VERSION;
		encoder.profile = 0xf5;
		encoder.width = xDim;
		encoder.height = yDim;
		encoder.max_bframes = 3;
		encoder.bquant_ratio = 150;
		encoder.bquant_offset = 100;
		encoder.fincr = 1;
		encoder.fbase = g_EncoderFramerate;
		xvid_enc_plugin_t plugins[8];
		encoder.plugins = plugins;
		xvid_plugin_single_t single;
		memset(&single,0,sizeof(xvid_plugin_single_t));
		single.version = XVID_VERSION;
		single.bitrate = g_EncoderBitrate;
		single.reaction_delay_factor = 16;
		single.averaging_period = 100;
		single.buffer = 100;
		plugins[encoder.num_plugins].func = xvid_plugin_single;
		plugins[encoder.num_plugins].param = &single;
		encoder.num_plugins++;
		encoder.num_threads = 4;
		encoder.max_key_interval = 300;
		encoder.min_quant[0]=2;
		encoder.min_quant[1]=2;
		encoder.min_quant[2]=2;
		encoder.max_quant[0]=31;
		encoder.max_quant[1]=31;
		encoder.max_quant[2]=31;
		encoder.frame_drop_ratio = 0;
		encoder.global = XVID_GLOBAL_PACKED | XVID_GLOBAL_CLOSED_GOP;
		if (0>xvid_encore(NULL,XVID_ENC_CREATE,&encoder,NULL))
			return -1;
		bitStream = new char[54000000];
		xdim=xDim;
		ydim=yDim;
		AVIFileInit();
		FILE* scrub;
		scrub= _wfopen(outFile,L"w+b");
		fclose(scrub);
		memset(&myAVIStreamInfo,0,sizeof(AVISTREAMINFO));
		myAVIStreamInfo.fccType = streamtypeVIDEO;
		myAVIStreamInfo.fccHandler = MAKEFOURCC('x', 'v', 'i', 'd');
		myAVIStreamInfo.dwScale = encoder.fincr;
		myAVIStreamInfo.dwRate = encoder.fbase;
		myAVIStreamInfo.dwLength = g_NumFrames;
		myAVIStreamInfo.dwQuality = 10000;
		SetRect(&myAVIStreamInfo.rcFrame,0,0,ydim,xdim);
		AVIFileOpenW(&myAVIFile, outFile, OF_CREATE|OF_WRITE, NULL);
		AVIFileCreateStream(myAVIFile,&myAVIStream,&myAVIStreamInfo);
		memset(&myBitmapInfoHeader,0,sizeof(BITMAPINFOHEADER));
		myBitmapInfoHeader.biHeight = ydim;
		myBitmapInfoHeader.biWidth = xdim;
		myBitmapInfoHeader.biPlanes = 1;
		myBitmapInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
		myBitmapInfoHeader.biCompression = MAKEFOURCC('X', 'V', 'I', 'D');
		myBitmapInfoHeader.biBitCount = 12;
		myBitmapInfoHeader.biSizeImage = 6*xdim*ydim;
		AVIStreamSetFormat(myAVIStream,0,&myBitmapInfoHeader,sizeof(myBitmapInfoHeader));
		return 0;
	}
	void AddFrameToStream(unsigned char* rgbaFrame, int frameNum)
	{
		int key;
		xvid_enc_frame_t frame;
		memset(&frame,0,sizeof(xvid_enc_frame_t));
		frame.version = XVID_VERSION;
		frame.bitstream = bitStream;
		frame.length = -1;
		xvid_image_t image;
		image.csp = XVID_CSP_RGBA | XVID_CSP_VFLIP;
		image.plane[0] = rgbaFrame;
		image.stride[0] = sizeof(char)*4*encoder.width;
		frame.input = image;
		frame.vol_flags = XVID_VOL_MPEGQUANT;
		frame.quant_inter_matrix = NULL;
		frame.quant_intra_matrix = NULL;
		frame.par = XVID_PAR_11_VGA;
		frame.par_height = ydim;
		frame.par_width = xdim;
		frame.vop_flags = XVID_VOP_INTER4V | XVID_VOP_HALFPEL | XVID_VOP_HQACPRED | XVID_VOP_TRELLISQUANT | XVID_VOP_MODEDECISION_RD;
		frame.type = XVID_TYPE_AUTO;
		frame.quant = 0;
		frame.motion = XVID_ME_CHROMA_PVOP | XVID_ME_CHROMA_BVOP | XVID_ME_HALFPELREFINE16 | XVID_ME_EXTSEARCH16 | XVID_ME_HALFPELREFINE8 | XVID_ME_USESQUARES16 |
			XVID_ME_FASTREFINE16 | XVID_ME_FASTREFINE8 | XVID_ME_SKIP_DELTASEARCH | XVID_ME_FAST_MODEINTERPOLATE | XVID_ME_BFRAME_EARLYSTOP;
		int size = xvid_encore(encoder.handle,XVID_ENC_ENCODE,&frame,NULL);
		key=frame.out_flags & XVID_KEYFRAME;
		AVIStreamWrite(myAVIStream, frameNum, 1, bitStream, size, key ? AVIIF_KEYFRAME : 0, NULL, NULL);
	}
	void FinishEncoding()
	{
		AVIStreamRelease(myAVIStream);
		AVIFileRelease(myAVIFile);
		AVIFileExit();
		xvid_encore(encoder.handle,XVID_ENC_DESTROY,NULL,NULL);
		delete bitStream;
	}
};