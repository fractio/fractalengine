/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "FWM.h"
#include "Animate.h"


using namespace Flam4_CUDA;

public ref class RunThread
{
public:
	static void ThreadProc()
	{
		// Enabling Windows XP visual effects before any controls are created
		Application::EnableVisualStyles();
		Application::SetCompatibleTextRenderingDefault(false); 

		// Create the main window and run it
		Application::Run(gcnew FWM());
	}
};


//This stub simply starts a new thread for the GUI
void OpenWindow()
{
	Thread^ oThread = gcnew Thread( gcnew ThreadStart( &RunThread::ThreadProc));  //Spawn another thread so we can explicitly set apartment state
	oThread->SetApartmentState(ApartmentState::STA);
	oThread->Name = "Dialogue Thread";
	oThread->Start();
}
