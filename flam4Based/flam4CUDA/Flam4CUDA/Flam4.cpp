/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <windows.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <math.h>
#include "FWMInterop.h"
#include "Encoder.h"
#include "image.h"

HINSTANCE	g_hInst;
flameParameterList*		g_pFlameParams = NULL;
Flame*					g_pFlame = NULL;
extern "C" __declspec(dllimport) void cuStartCuda(unsigned int hwnd, int x, int y);
extern "C" __declspec(dllimport) void cuStopCuda();
extern "C" __declspec(dllimport) void cuRunFuse(Flame* flame);
extern "C" __declspec(dllimport) void cuStartFrame(unsigned int hwnd, int x, int y, Flame* flame);
extern "C" __declspec(dllimport) void cuRenderBatch(Flame* flame);
extern "C" __declspec(dllimport) void cuFinishFrame(Flame* flame, void* image, bool useAlpha);

int oldtime;
int quality = 30;
bool isAnimating = true;
volatile int progress = 0;
volatile int frameNumber = 0;
volatile bool isDone = false;
volatile bool abortRender = false;
int g_NumFrames = NUM_FRAMES;

int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
	oldtime = GetTickCount();
	OpenWindow();
	return 0;
}

//This function renders a frame directly to the view window
void Render(unsigned int hwnd, int x, int y, flameParameterList *flameParams)
{
	int newtime = GetTickCount();
	double timeElapsed = (double)(newtime-oldtime);
	double rotationSpeed = .0002;
	if (flameParams == NULL)
		return;
	flameAnimation* animation = new flameAnimation();
	if (flameParams->nextFlame)
		if (flameParams->nextFlame->flame->numTrans > flameParams->flame->numTrans)
			flameParams->nextFlame->flame->Clone(&g_pFlame);
		else
			flameParams->flame->Clone(&g_pFlame);
	else
		flameParams->flame->Clone(&g_pFlame);
	if (flameParams->nextFlame)
		animation->Interpolate(fmod(timeElapsed*rotationSpeed/(2.0*3.141592653589793),1.0), flameParams->flame, flameParams->nextFlame->flame, &g_pFlame);
	double sinTheta = 0.0;
	double cosTheta = 1.0;
	cuStartFrame(hwnd,x,y,g_pFlame);
	for (int n = 0; n < flameParams->flame->quality; n++)
	{
		
		newtime = GetTickCount();
		timeElapsed = (double)(newtime-oldtime);
		if (flameParams->nextFlame)
			animation->Interpolate(fmod(timeElapsed*rotationSpeed/(2.0*3.141592653589793),1.0), flameParams->flame, flameParams->nextFlame->flame, &g_pFlame);
		if (isAnimating)
		{
			sinTheta = sin(rotationSpeed*timeElapsed);
			cosTheta = cos(rotationSpeed*timeElapsed);
		}
		for (int n = 0; n < g_pFlame->numTrans; n++)
		{
			if (g_pFlame->transAff[n].rotates != 0)
			{
				double a = g_pFlame->transAff[n].a;
				double b = g_pFlame->transAff[n].b;
				double d = g_pFlame->transAff[n].d;
				double e = g_pFlame->transAff[n].e;
				g_pFlame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
				g_pFlame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
				g_pFlame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
				g_pFlame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
			}
		}
		cuRenderBatch(g_pFlame);
	}	
	cuFinishFrame(g_pFlame,NULL, false);
	delete animation;
}

//This function renders an animation loop, and saves it to outFile.  hwnd is the handle to the window to display the results to.
//Use NULL to suppress on screen output
void RenderLoop(flameParameterList *flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd)
{
	XvidEncoder encoder;
	if (encoder.BeginEncoding(x,y,outFile) != 0)
	{
		MessageBox(NULL,"Encoder failed to initialize output file stream.","Error",MB_OK);
		return;
	}
	if (flameParams == NULL)
		return;
	Flame* flame = NULL;
	if (flameParams->nextFlame)
		if (flameParams->nextFlame->flame->numTrans > flameParams->flame->numTrans)
			flameParams->nextFlame->flame->Clone(&flame);
		else
			flameParams->flame->Clone(&flame);
	else
		flameParams->flame->Clone(&flame);
	cuStartCuda(hwnd,x,y);
	flameAnimation* animation = new flameAnimation();
	for (int frame = 0; frame < g_NumFrames; frame++)
	{
		progress = 0;
		double rotationSpeed = 2.0*3.1415926535/(double)g_NumFrames;
		double sinTheta = sin(rotationSpeed*((double)(frame+1)));
		double cosTheta = cos(rotationSpeed*((double)(frame+1)));
		if (flameParams->nextFlame)
			animation->Interpolate((float)frame/(float)g_NumFrames, flameParams->flame, flameParams->nextFlame->flame, &flame);
		for (int n = 0; n < flame->numTrans; n++)
		{
			if (flame->transAff[n].rotates != 0)
			{
				double a = flame->transAff[n].a;
				double b = flame->transAff[n].b;
				double d = flame->transAff[n].d;
				double e = flame->transAff[n].e;
				flame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
				flame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
				flame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
				flame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
			}
		}
		cuStartFrame(hwnd,x,y,flame);
		cuRunFuse(flame);
		for (int subSample = 0; subSample < flame->quality; subSample++)
		{
			rotationSpeed = 2.0*3.1415926535/(double)g_NumFrames;
			sinTheta = sin(rotationSpeed*((double)(frame+1)-(double)subSample/(double)flame->quality));
			cosTheta = cos(rotationSpeed*((double)(frame+1)-(double)subSample/(double)flame->quality));
			if (flameParams->nextFlame)
				animation->Interpolate((float)frame/(float)g_NumFrames+(float)subSample/((float)flame->quality*(float)g_NumFrames), flameParams->flame, flameParams->nextFlame->flame, &flame);
			for (int n = 0; n < flame->numTrans; n++)
			{
				if (flame->transAff[n].rotates != 0)
				{
					double a = flame->transAff[n].a;
					double b = flame->transAff[n].b;
					double d = flame->transAff[n].d;
					double e = flame->transAff[n].e;
					flame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
					flame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
					flame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
					flame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
				}
			}
			cuRenderBatch(flame);
			if (abortRender)
			{
				cuStopCuda();
				delete flame;
				delete animation;
				return;
			}
			progress++;
		}
		unsigned char* img = new unsigned char[4*x*y];
		cuFinishFrame(flame,img, false);
		encoder.AddFrameToStream(img,frame);
		delete img;
		frameNumber = frame;
	}
	encoder.FinishEncoding();
	cuStopCuda();
	delete flame;
	delete animation;
	isDone = true;
	MessageBeep(MB_ICONASTERISK);
}

//This function renders an animation loop, and saves it to outFile.  hwnd is the handle to the window to display the results to.
//Use NULL to suppress on screen output
void RenderLoopAsPNGSeq(flameParameterList *flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd, bool useAlpha)
{
	if (flameParams == NULL)
		return;
	Flame* flame = NULL;
	if (flameParams->nextFlame)
		if (flameParams->nextFlame->flame->numTrans > flameParams->flame->numTrans)
			flameParams->nextFlame->flame->Clone(&flame);
		else
			flameParams->flame->Clone(&flame);
	else
		flameParams->flame->Clone(&flame);
	cuStartCuda(hwnd,x,y);
	flameAnimation* animation = new flameAnimation();
	for (int frame = 0; frame < g_NumFrames; frame++)
	{
		progress = 0;
		double rotationSpeed = 2.0*3.1415926535/(double)g_NumFrames;
		double sinTheta = sin(rotationSpeed*((double)(frame+1)));
		double cosTheta = cos(rotationSpeed*((double)(frame+1)));
		if (flameParams->nextFlame)
			animation->Interpolate((float)frame/(float)g_NumFrames, flameParams->flame, flameParams->nextFlame->flame, &flame);
		for (int n = 0; n < flame->numTrans; n++)
		{
			if (flame->transAff[n].rotates != 0)
			{
				double a = flame->transAff[n].a;
				double b = flame->transAff[n].b;
				double d = flame->transAff[n].d;
				double e = flame->transAff[n].e;
				flame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
				flame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
				flame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
				flame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
			}
		}
		cuStartFrame(hwnd,x,y,flame);
		cuRunFuse(flame);
		for (int subSample = 0; subSample < flame->quality; subSample++)
		{
			rotationSpeed = 2.0*3.1415926535/(double)g_NumFrames;
			sinTheta = sin(rotationSpeed*((double)(frame+1)-(double)subSample/(double)flame->quality));
			cosTheta = cos(rotationSpeed*((double)(frame+1)-(double)subSample/(double)flame->quality));
			if (flameParams->nextFlame)
				animation->Interpolate((float)frame/(float)g_NumFrames+(float)subSample/((float)flame->quality*(float)g_NumFrames), flameParams->flame, flameParams->nextFlame->flame, &flame);
			for (int n = 0; n < flame->numTrans; n++)
			{
				if (flame->transAff[n].rotates != 0)
				{
					double a = flame->transAff[n].a;
					double b = flame->transAff[n].b;
					double d = flame->transAff[n].d;
					double e = flame->transAff[n].e;
					flame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
					flame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
					flame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
					flame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
				}
			}
			cuRenderBatch(flame);
			if (abortRender)
			{
				cuStopCuda();
				delete flame;
				delete animation;
				return;
			}
			progress++;
		}
		unsigned char* img = new unsigned char[4*x*y];
		cuFinishFrame(flame,img, useAlpha);
		wchar_t frameStr[100];
		_itow(g_NumFrames,frameStr,10);
		int length = wcslen(frameStr);
		_itow(frame+1,frameStr,10);
		length -= wcslen(frameStr);
		wchar_t fileNameStr[1024];
		fileNameStr[0]=0;	//clear the string in preparation to recieve the next file name
		wcscat(fileNameStr,outFile);
		wcscat(fileNameStr,L"\\");
		for (int l = 0; l < length; l++)
			wcscat(fileNameStr,L"0");
		wcscat(fileNameStr,frameStr);
		wcscat(fileNameStr,L".PNG");
		exportImageToPNG(fileNameStr, x, y, img,useAlpha);
		delete img;
		frameNumber = frame;
	}
	cuStopCuda();
	delete flame;
	delete animation;
	isDone = true;
	MessageBeep(MB_ICONASTERISK);
}

//This function renders a single frame and saves it to outFile.
//hwnd should be NULL if you're rendering a frame larger than 4096x4096
//since openGL cannot handle textures that large.
void RenderFrame(flameParameterList *flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd,bool useAlpha)
{
	cuStartCuda(NULL,x,y);
	Flame* flame = flameParams->flame;
	if (flame == NULL)
		return;
	cuStartFrame(hwnd,x,y,flame);
	cuRunFuse(flame);
	for (int subSample = 0; subSample < flame->quality; subSample++)
	{
		cuRenderBatch(flame);
		progress++;
		if (abortRender)
		{
			cuStopCuda();
			return;
		}
	}
	unsigned char* img = new unsigned char[4*x*y];
	cuFinishFrame(flame,img,useAlpha);
	exportImageToPNG(outFile, x, y, img,useAlpha);
	cuStopCuda();
	delete img;
	isDone = true;
	MessageBeep(MB_ICONASTERISK);
}

void RenderFrameBatch(flameParameterList *flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd,bool useAlpha)
{
	do
	{
		progress = 0;
		Flame* flame = flameParams->flame;
		if (flame == NULL)
			return;
		cuStartFrame(hwnd,x,y,flame);
		cuRunFuse(flame);
		for (int subSample = 0; subSample < flame->quality; subSample++)
		{
			cuRenderBatch(flame);
			progress++;
			if (abortRender)
			{
				cuStopCuda();
				return;
			}
		}
		unsigned char* img = new unsigned char[4*x*y];
		cuFinishFrame(flame,img,useAlpha);
		wchar_t outFilePath[1024];
		outFilePath[0] = 0;
		wcscat(outFilePath,outFile);
		wcscat(outFilePath,L"\\");
		wcscat(outFilePath,flameParams->flameMeta->name);
		wcscat(outFilePath,L".png");
		exportImageToPNG(outFilePath, x, y, img,useAlpha);
		delete img;
		flameParams = flameParams->nextFlame;
		frameNumber++;
	} while  (flameParams!= NULL);
	isDone = true;
	MessageBeep(MB_ICONASTERISK);
}

//This function starts up CUDA and allocates GPU memory for rendering
void StartCuda(unsigned int hwnd, int x, int y)
{
	cuStartCuda(hwnd,x,y);
}

//This function stops CUDA and frees all GPU memory
void StopCuda()
{
	cuStopCuda();
}