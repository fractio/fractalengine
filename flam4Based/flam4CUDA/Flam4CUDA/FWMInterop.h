/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Defines.h"
#include "FlameData.h"
#include "Globals.h"
#include "Animate.h"

//This header is a barrier between the native and CLR sections of code.
//This is needed because the native and CLR libries have nameing conflicts
void OpenWindow();
void Render(unsigned int hwnd, int x, int y, flameParameterList* flameParams);
void RenderLoop(flameParameterList* flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd);
void RenderLoopAsPNGSeq(flameParameterList* flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd, bool useAlpha);
void RenderFrame(flameParameterList* flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd, bool useAlpha);
void RenderFrameBatch(flameParameterList *flameParams, int x, int y, wchar_t* outFile, unsigned int hwnd,bool useAlpha);
void StartCuda(unsigned int hwnd, int x, int y);
void StopCuda();