/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "Flam4.h"
#include "Flam4CUDAThreading.h"

int xDim;
int yDim;
unsigned int g_hWnd;
CUDAThreadingManager* threads;
bool cudaRunning = false;
volatile bool badState = false;	//Something catastrophic has happened - abort render calls

float4** d_dst = NULL;  //This either points to a device buffer or to a openGL buffer, depending on the mode.

int deviceID = 0;

extern "C" __declspec(dllexport) void cuStartCuda(unsigned int hwnd, int x, int y)
{
	g_hWnd = hwnd;
	xDim = x;
	yDim = y;
	threads = new CUDAThreadingManager();
	threads->DispatchStartCuda(hwnd,x,y);
	cudaRunning = true;
}

extern "C" __declspec(dllexport) void cuStopCuda()
{
	cudaRunning = false;
	threads->DispatchStopCuda();
	delete threads;
}

extern "C" __declspec(dllexport) void cuRunFuse(Flame* flame)
{
	if (badState) return;
	threads->DispatchRunFuse(flame);
}

extern "C" __declspec(dllexport) void cuStartFrame(unsigned int hwnd, int x, int y, Flame* flame)
{
	if ((xDim != x)||(yDim != y)||(hwnd != g_hWnd)||(!cudaRunning))  //Recreate cuda buffers if the render target has changed
	{
		if (cudaRunning)
			cuStopCuda();
		cuStartCuda(hwnd,x,y);
	}
	threads->DispatchStartFrame(flame);
}

extern "C" __declspec(dllexport) void cuRenderBatch(Flame* flame)
{
	if (badState)
		return;
	threads->DispatchRenderBatch(flame);
}

extern "C" __declspec(dllexport)void cuFinishFrame(Flame* flame, void* image, unsigned int useAlpha)
{
	if (badState)
		return;
	threads->DispatchFinishFrame(flame, &image, useAlpha);
}

extern "C" __declspec(dllexport)void cuEzRenderFrame(unsigned int hwnd, int x, int y, Flame* flame, void* image, unsigned int useAlpha, int batches)
{
	flame->quality = batches;
	cuStartFrame(hwnd,x,y,flame);
	cuRunFuse(flame);
	for (int n = 0; n < batches; n++)
		cuRenderBatch(flame);
	cuFinishFrame(flame, image, useAlpha);
}

extern "C" __declspec(dllexport)int cuCalcNumBatches(int xDim, int yDim, int quality)
{
	return ceil(((float)xDim*(float)yDim*(float)quality)/(1024.*32.*(float)NUM_ITERATIONS));
}