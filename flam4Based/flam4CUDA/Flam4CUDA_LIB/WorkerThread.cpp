/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include "Flam4.h"
#include "WorkerThread.h"

extern float4** d_dst;
bool dontShowErrors = false;

#define BUFFER_DATA(i) ((char *)0 + i)

WorkerThread::WorkerThread(int device)
{
	terminateSignal = false;
	deviceNumber = device;
	cudaRunning = false;
	xDim = 0;
	yDim = 0;
	batchCounter = 0;
	useAlpha = false;
	gl_PBO = NULL;
	gl_Tex = NULL;
	h_Src = NULL;
	g_hDC = NULL;
	g_hRC = NULL;
	g_hWnd = NULL;
	image = NULL;

	currentParameters = NULL;
	nextFunction = NULL;
	readyForCommand = true;
}

DWORD WINAPI WorkerThread::workerThreadMessageProcInternal()
{
	while (!terminateSignal)
	{
		MemoryBarrier();
		if (!nextFunction)
		{
			SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_BELOW_NORMAL);
			SwitchToThread();
		}
		else
			(this->*nextFunction)();
	}
	return 0;
}

DWORD WINAPI WorkerThread::workerThreadMessageProc(LPVOID lpParam)
{
	((WorkerThread*)lpParam)->workerThreadMessageProcInternal();
	return NULL;
}

void WorkerThread::CloseThread()
{
	nextFunction = NULL;
	terminateSignal = true;
	readyForCommand = true;
}

void WorkerThread::StartCuda()
{
	badState = false;
	nextFunction = NULL;
	cudaSetDevice(deviceNumber);
	cudaSetDeviceFlags(cudaDeviceScheduleYield);
	if (g_hWnd != 0)
	{
		RECT rc;
		GetClientRect((HWND)g_hWnd,&rc);
		int x = rc.right;
		int y = rc.bottom;
		g_hDC = GetDC((HWND)g_hWnd);
		int nPixelFormat;
		PIXELFORMATDESCRIPTOR pfd;
		ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 64;
		nPixelFormat = ChoosePixelFormat(g_hDC, &pfd);
		SetPixelFormat(g_hDC,nPixelFormat,&pfd);
		g_hRC = wglCreateContext(g_hDC);
		wglMakeCurrent(g_hDC, g_hRC);
		glewInit();
		glViewport(0, 0, x, y);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
		h_Src = new float4[xDim*yDim];
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1,&gl_Tex);
		glBindTexture(GL_TEXTURE_2D, gl_Tex);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xDim, yDim, 0, GL_RGBA, GL_FLOAT,h_Src);
		glGenBuffers(1, &gl_PBO);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,gl_PBO);
		glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB,xDim*yDim*sizeof(float4),h_Src,GL_STREAM_COPY);
		cudaGLRegisterBufferObject(gl_PBO);
		createCUDAbuffers(NULL,deviceNumber, &readyForCommand);
	}
	else
	{
		createCUDAbuffers(&(d_dst[deviceNumber]),deviceNumber, &readyForCommand);
	}
	cudaRunning = true;
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Start CUDA\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
}

void WorkerThread::RunFuse()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	runFuse(currentParameters, deviceNumber, &readyForCommand);
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Run Fuse\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
}

void WorkerThread::StartFrame()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	if (g_hWnd != 0)
	{
		cudaGLMapBufferObject((void**)&(d_dst[deviceNumber]),gl_PBO);
	}
	else
	{
		//no GL context, use internal buffer
	}
	startFrame(currentParameters,d_dst[deviceNumber], deviceNumber, &readyForCommand);
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Start Frame\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
	batchCounter = 0;
}


void WorkerThread::RenderBatch()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	//if (batchCounter < 10)		//Disable the new iteration engine for now
	//{
		renderBatch(currentParameters, d_dst[deviceNumber], deviceNumber, &readyForCommand);
	//}
	//else
	//{
		//if (batchCounter == 10)
		//{
			//deNoise(d_dst[deviceNumber], deviceNumber, &readyForCommand);
		//}
		//renderBatchMH(currentParameters, d_dst[deviceNumber], deviceNumber, &readyForCommand,(float)batchCounter);
	//}
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Render Batch\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
	batchCounter++;
}

void WorkerThread::GrabFrame()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	grabFrame(d_dst[deviceNumber], deviceNumber, &readyForCommand);
}

void WorkerThread::MergeFrames()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	mergeFrames(d_dst[deviceNumber],deviceNumber, &readyForCommand);
}

void WorkerThread::FinishFrame()
{
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	finishFrame(currentParameters, d_dst[deviceNumber], (uchar4*)image, deviceNumber, &readyForCommand,useAlpha);
	if (g_hWnd != 0)
	{
		cudaGLUnmapBufferObject(gl_PBO);
		glTexSubImage2D(GL_TEXTURE_2D,0,0,0,xDim,yDim,GL_RGBA,GL_FLOAT,BUFFER_DATA(0));
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f,0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(1.0f,0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(1.0f,1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f,1.0f);
		glEnd();
		SwapBuffers(g_hDC);
	}
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Finish Frame\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
}

void WorkerThread::StopCuda()
{
	nextFunction = NULL;
	cudaRunning = false;
	if (g_hWnd != 0)
	{
		deleteCUDAbuffers(NULL, deviceNumber, &readyForCommand);
		if (h_Src)
			delete h_Src;
		h_Src = 0;
		if (gl_Tex)
			glDeleteTextures(1,&gl_Tex);
		gl_Tex = 0;
		if (gl_PBO)
		{
			cudaGLUnregisterBufferObject(gl_PBO);
			glDeleteBuffers(1,&gl_PBO);
		}
		gl_PBO = 0;
		wglMakeCurrent(g_hDC, NULL);
		wglDeleteContext(g_hRC);
		ReleaseDC((HWND)g_hWnd,g_hDC);
	}
	else
	{
		deleteCUDAbuffers(&(d_dst[deviceNumber]),deviceNumber, &readyForCommand);
	}
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		badState = true;
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"Error in Stop CUDA\n Device: %d (%s)",deviceNumber,props.name);
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
	}
}

WorkerThread::~WorkerThread()
{
}