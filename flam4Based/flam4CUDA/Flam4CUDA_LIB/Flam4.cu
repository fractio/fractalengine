/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Flam4.h"
#include "Flam4Grid.cuh"
#include "Flam4_Kernal.cuh"
#include "Flam4_Kernal.cu"

cudaArray** d_g_Palette = NULL;
float4** accumBuffer = NULL;
uchar4** resultStagingBuffer = NULL;
point** pointList = NULL;
unsigned int** perThreadRandSeeds = NULL;
unsigned int** perWarpRandSeeds = NULL;

textureReference** texRefPtr = NULL;

float4* mergeStagingBuffer = NULL;


int warpsPerBlock = 2;

void makeVarAccelStructure(Flame* flm)
{	
	unsigned int accel[(MAX_XFORMS+1)];
	for (int xform = 0; xform < flm->numTrans; xform++)
	{
		accel[xform]=0;
		for (int var = 0; var < VAR_COUNT; var++)
		{
			if (flm->trans[xform].xFormVars[var+6]!=0)
				accel[xform] |= (1 << (var>>2));
		}
	}
	if (flm->isFinalXform != 0)
	{
		accel[MAX_XFORMS]=0;
		for (int var = 0; var < VAR_COUNT; var++)
		{
			if (flm->finalXform.xFormVars[var+6]!=0)
				accel[MAX_XFORMS] |= (1 << (var >> 2));
		}
	}
	cudaMemcpyToSymbol(d_g_varAccel,
		&accel,
		sizeof(unsigned int)*(MAX_XFORMS+1),
		0,
		cudaMemcpyHostToDevice);
}

void createCUDAbuffers(float4 ** renderTarget,int deviceID, bool* ready)
{
	cudaDeviceProp devProp;
	cudaGetDeviceProperties(&devProp,0);
	int warpSize = devProp.warpSize;
	BLOCKDIM = warpSize*warpsPerBlock;
	cudaMalloc((void**)&(pointList[deviceID]), sizeof(point)*NUM_POINTS*BLOCKSX*BLOCKSY);
	cudaMalloc((void**)&(accumBuffer[deviceID]), sizeof(float4)*xDim*yDim);
	if (renderTarget != NULL)
	{
		cudaMalloc((void**)renderTarget, sizeof(float4)*xDim*yDim);
	}
	cudaMalloc((void**)&(resultStagingBuffer[deviceID]), sizeof(uchar4)*xDim*yDim);
	cudaMalloc((void**)&(perThreadRandSeeds[deviceID]), sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM);
	cudaMalloc((void**)&(perWarpRandSeeds[deviceID]), sizeof(int)*BLOCKSX*BLOCKSY*warpsPerBlock);
	point* pts = (point*)malloc(NUM_POINTS*BLOCKSX*BLOCKSY*sizeof(point));
	int* seeds = (int*)malloc(sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM);
	int* seeds2 = (int*)malloc(sizeof(int)*BLOCKSX*BLOCKSY*warpsPerBlock);
	for (int n = 0; n < NUM_POINTS*BLOCKSX*BLOCKSY; n++)
	{
		pts[n].x = (float)rand()/(float)RAND_MAX;
		pts[n].y = (float)rand()/(float)RAND_MAX;
		pts[n].pal = (float)rand()/(float)RAND_MAX;
	}
	for (int n = 0; n < BLOCKSX*BLOCKSY*BLOCKDIM; n++)
		seeds[n] = rand();
	for (int n = 0; n < BLOCKSX*BLOCKSY*warpsPerBlock; n++)
		seeds2[n] = rand();
	cudaError_t err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		badState = true;
		return;
	}
	cudaMemcpy(pointList[deviceID],pts,NUM_POINTS*BLOCKSX*BLOCKSY*sizeof(point),cudaMemcpyHostToDevice);
	cudaMemcpy(perThreadRandSeeds[deviceID],seeds,BLOCKSX*BLOCKSY*BLOCKDIM*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(perWarpRandSeeds[deviceID],seeds2,BLOCKSX*BLOCKSY*warpsPerBlock*sizeof(int),cudaMemcpyHostToDevice);
	*ready = true;
	free(pts);
	free(seeds);
	free(seeds2);
}
void deleteCUDAbuffers(float4** renderTarget,int deviceID, bool* ready)
{
	cudaThreadSynchronize();
	if (pointList[deviceID] != NULL)
	{
		cudaFree(pointList[deviceID]);
		pointList[deviceID] = NULL;
	}
	if (accumBuffer[deviceID]!= NULL)
	{
		cudaFree(accumBuffer[deviceID]);
		accumBuffer[deviceID] = NULL;
	}
	if (renderTarget != NULL)
	{
		if (*renderTarget != NULL)
		{
			cudaFree(*renderTarget);
			*renderTarget = NULL;
		}
	}
	if (resultStagingBuffer[deviceID]!=NULL)
	{
		cudaFree(resultStagingBuffer[deviceID]);
		resultStagingBuffer[deviceID] = NULL;
	}
	if (perThreadRandSeeds[deviceID]!=NULL)
	{
		cudaFree(perThreadRandSeeds[deviceID]);
		perThreadRandSeeds[deviceID] = NULL;
	}
	if (perWarpRandSeeds[deviceID]!=NULL)
	{
		cudaFree(perWarpRandSeeds[deviceID]);
		perWarpRandSeeds[deviceID] = NULL;
	}
	if (d_g_Palette[deviceID] != NULL)
	{
		cudaFreeArray(d_g_Palette[deviceID]);
		d_g_Palette[deviceID] = NULL;
	}
	*ready = true;
}


void runFuse(Flame* flm,int deviceID, bool* ready)
{
	//deep copy flm to the device
	cudaMemcpyToSymbol(d_g_Flame, &(flm->params), sizeof(FlameParams),0, cudaMemcpyHostToDevice);
	for (int n = 0; n < flm->numTrans; n++)
	{
		cudaMemcpyToSymbol(d_g_Xforms,
			&(flm->trans[n]),
			sizeof(flm->trans[n]),
			sizeof(xForm)*n,
			cudaMemcpyHostToDevice);
	}
	makeVarAccelStructure(flm);
	//Render it!
	dim3 threads(BLOCKDIM);
	dim3 grid(BLOCKSX,BLOCKSY);
	int* seeds = (int*)malloc(sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM);
	int* seeds2 = (int*)malloc(sizeof(int)*BLOCKSX*BLOCKSY*warpsPerBlock);
	for (int n = 0; n < BLOCKSX*BLOCKSY*BLOCKDIM; n++)
		seeds[n] = rand();
	for (int n = 0; n < BLOCKSX*BLOCKSY*warpsPerBlock; n++)
		seeds2[n] = rand();
	cudaMemcpy(perThreadRandSeeds[deviceID],seeds,BLOCKSX*BLOCKSY*BLOCKDIM*sizeof(int),cudaMemcpyHostToDevice);
	cudaMemcpy(perWarpRandSeeds[deviceID],seeds2,BLOCKSX*BLOCKSY*warpsPerBlock*sizeof(int),cudaMemcpyHostToDevice);
	free(seeds);
	free(seeds2);
	resetPointsKernal<<<grid,threads>>>(pointList[deviceID],perThreadRandSeeds[deviceID]);
	for (int n = 0; n < 5; n++)
	{
		fuseKernal<<<grid,threads>>>(pointList[deviceID],perThreadRandSeeds[deviceID], perWarpRandSeeds[deviceID], xDim, yDim);
		cudaThreadSynchronize();
	}
	*ready = true;
}

void startFrame(Flame* flm, float4* renderTarget, int deviceID, bool* ready)
{
	//deep copy flm to the device
	cudaMemcpyToSymbol(d_g_Flame, &(flm->params), sizeof(FlameParams),0, cudaMemcpyHostToDevice);
	for (int n = 0; n < flm->numTrans; n++)
	{
		cudaMemcpyToSymbol(d_g_Xforms,
			&(flm->trans[n]),
			sizeof(flm->trans[n]),
			sizeof(xForm)*n,
			cudaMemcpyHostToDevice);
	}
	if (d_g_Palette[deviceID] != NULL)
	{
		cudaFreeArray(d_g_Palette[deviceID]);
		d_g_Palette[deviceID] = NULL;
	}
	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();
	cudaMallocArray(&(d_g_Palette[deviceID]),&channelDesc,flm->numColors,1);
	cudaMemcpyToArray(d_g_Palette[deviceID],
		0,
		0,
		flm->colorIndex,
		sizeof(rgba)*flm->numColors,
		cudaMemcpyHostToDevice);
	cudaGetTextureReference((const textureReference**)&(texRefPtr[deviceID]), "texRef");
	cudaError_t err = cudaGetLastError();
	texRefPtr[deviceID]->normalized = 1;
	texRefPtr[deviceID]->filterMode = cudaFilterModeLinear;
	cudaBindTextureToArray(texRef, d_g_Palette[deviceID]);
	//Render it!
	*ready = true;
	dim3 threads2(16,16);
	dim3 grid2(xDim/16+1,yDim/16+1);
	setBufferKernal<<<grid2,threads2>>>(accumBuffer[deviceID],make_float4(0.0f,0.0f,0.0f,0.0f), xDim, yDim);
	setBufferKernal<<<grid2,threads2>>>(renderTarget,make_float4(0.0f,0.0f,0.0f,0.0f),xDim,yDim);
}

void renderBatch(Flame* flm, float4* renderTarget, int deviceID, bool* ready)
{
	//deep copy flm to the device
	cudaMemcpyToSymbol(d_g_Flame, &(flm->params), sizeof(FlameParams),0, cudaMemcpyHostToDevice);
	cudaMemcpyToArray(d_g_Palette[deviceID],
		0,
		0,
		flm->colorIndex,
		sizeof(rgba)*flm->numColors,
		cudaMemcpyHostToDevice);
	for (int n = 0; n < flm->numTrans; n++)
	{
		cudaMemcpyToSymbol(d_g_Xforms,
			&(flm->trans[n]),
			sizeof(flm->trans[n]),
			sizeof(xForm)*n,
			cudaMemcpyHostToDevice);
	}
	makeVarAccelStructure(flm);
	//Render it!
	*ready = true;
	dim3 threads(BLOCKDIM);
	dim3 grid(BLOCKSX,BLOCKSY);
	iteratePointsKernal<<<grid,threads>>>(renderTarget,pointList[deviceID], perThreadRandSeeds[deviceID], perWarpRandSeeds[deviceID],xDim,yDim);
	cudaThreadSynchronize();
}

void renderBatchMH(Flame* flm, float4* renderTarget, int deviceID, bool* ready, float batchNum)
{
	//deep copy flm to the device
	cudaMemcpyToSymbol(d_g_Flame, &(flm->params), sizeof(FlameParams),0, cudaMemcpyHostToDevice);
	cudaMemcpyToArray(d_g_Palette[deviceID],
		0,
		0,
		flm->colorIndex,
		sizeof(rgba)*flm->numColors,
		cudaMemcpyHostToDevice);
	for (int n = 0; n < flm->numTrans; n++)
	{
		cudaMemcpyToSymbol(d_g_Xforms,
			&(flm->trans[n]),
			sizeof(flm->trans[n]),
			sizeof(xForm)*n,
			cudaMemcpyHostToDevice);
	}
	makeVarAccelStructure(flm);
	//Render it!
	*ready = true;
	dim3 threads(BLOCKDIM);
	dim3 grid(BLOCKSX,BLOCKSY);
	//iteratePointsMHKernal<<<grid,threads>>>(renderTarget,pointList[deviceID], perThreadRandSeeds[deviceID], perWarpRandSeeds[deviceID],xDim,yDim, batchNum, 1.0f/batchNum);
	cudaThreadSynchronize();
}

void deNoise(float4* renderTarget, int deviceID, bool* ready)
{
	dim3 threads3(16,16);
	dim3 grid3(xDim/16+1,yDim/16+1);
	deNoiseKernal<<<grid3,threads3>>>(renderTarget,xDim,yDim);
	cudaThreadSynchronize();
}

void grabFrame(float4* renderTarget, int deviceID, bool* ready)
{
	cudaMemcpy(mergeStagingBuffer, renderTarget, xDim*yDim*sizeof(float4),cudaMemcpyDeviceToHost);
	*ready = true;
	cudaThreadSynchronize();
}

void mergeFrames(float4* renderTarget, int deviceID, bool* ready)
{
	cudaMemcpy(accumBuffer[deviceID], mergeStagingBuffer,xDim*yDim*sizeof(float4),cudaMemcpyHostToDevice);
	dim3 threads2(16,16);
	dim3 grid2(xDim/16+1,yDim/16+1);
	MergeKernal<<<grid2,threads2>>>(renderTarget,accumBuffer[deviceID],xDim,yDim);
	cudaThreadSynchronize();
	*ready = true;
}

void finishFrame(Flame* flm, float4* renderTarget, uchar4* outputBuffer, int deviceID, bool* ready, bool useAlpha, bool useDensityEstimation)
{
	//deep copy flm to the device
	cudaMemcpyToSymbol(d_g_Flame, &(flm->params), sizeof(FlameParams),0, cudaMemcpyHostToDevice);
	for (int n = 0; n < flm->numTrans; n++)
	{
		cudaMemcpyToSymbol(d_g_Xforms,
			&(flm->trans[n]),
			sizeof(flm->trans[n]),
			sizeof(xForm)*n,
			cudaMemcpyHostToDevice);
	}
	//Render it!
	dim3 threads2(16,16);
	dim3 grid2(xDim/16+1,yDim/16+1);
	dim3 threads3(16,16);
	dim3 grid3(xDim/16+1,yDim/16+1);
	if (useDensityEstimation == true)
	{
		DensityEstimationKernal<<<grid3,threads3>>>(accumBuffer[deviceID],renderTarget,xDim,yDim,.9f);
	}
	else
	{
		cudaMemcpy(accumBuffer[deviceID],renderTarget,xDim*yDim*sizeof(float4),cudaMemcpyDeviceToDevice);
	}
	setBufferKernal<<<grid2,threads2>>>(renderTarget,make_float4(flm->background.r,flm->background.g,flm->background.b,flm->background.a),xDim,yDim);
	postProcessKernal<<<grid2,threads2>>>(renderTarget,accumBuffer[deviceID],xDim,yDim);
	if (outputBuffer != NULL)
	{
		RGBA128FtoRGBA32UKernal<<<grid2,threads2>>>(resultStagingBuffer[deviceID], renderTarget, xDim, yDim, useAlpha);
		cudaMemcpy(outputBuffer, resultStagingBuffer[deviceID],xDim*yDim*sizeof(uchar4),cudaMemcpyDeviceToHost);
	}
	*ready = true;
}

