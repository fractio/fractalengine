/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include "Flam4.h"
#include "Flam4CUDAThreading.h"
#include "Flam4_Kernal.cuh"

extern cudaArray** d_g_Palette;
extern float4** accumBuffer;
extern uchar4** resultStagingBuffer;
extern point** pointList;
extern unsigned int** perThreadRandSeeds;
extern unsigned int** perWarpRandSeeds;
extern float4** d_dst;
extern textureReference** texRefPtr;
extern float4* mergeStagingBuffer;
bool isMergePinned = true;

CUDAThreadingManager::CUDAThreadingManager()
{
	cudaGetDeviceCount(&numDevices);
	deviceThreadHandles = new HANDLE[numDevices];
	threadIds = new DWORD[numDevices];
	threadObjects = new WorkerThread*[numDevices];
	d_g_Palette = new cudaArray*[numDevices];
	accumBuffer = new float4*[numDevices];
	resultStagingBuffer = new uchar4*[numDevices];
	pointList = new point*[numDevices];
	perThreadRandSeeds = new unsigned int*[numDevices];
	perWarpRandSeeds = new unsigned int*[numDevices];
	d_dst = new float4*[numDevices];
	texRefPtr = new textureReference*[numDevices];
	for (int n = 0; n < numDevices; n++)
	{
		d_g_Palette[n] = NULL;
		accumBuffer[n] = NULL;
		resultStagingBuffer[n] = NULL;
		pointList[n] = NULL;
		perThreadRandSeeds[n] = NULL;
		perWarpRandSeeds[n] = NULL;
		d_dst[n] = NULL;
		texRefPtr[n]=NULL;
		threadObjects[n] = new WorkerThread(n);
		threadObjects[n]->parentThread = GetCurrentThread();
		deviceThreadHandles[n] = CreateThread(
			NULL,
			0,
			WorkerThread::workerThreadMessageProc,
			threadObjects[n],
			0,
			&threadIds[n]);
	}
}

void CUDAThreadingManager::DispatchStartCuda(unsigned int hwnd, int x, int y)
{
	isMergePinned = true;
	cudaHostAlloc((void**)&mergeStagingBuffer, sizeof(float4)*xDim*yDim,cudaHostAllocPortable|cudaHostAllocWriteCombined);
	if (cudaGetLastError() == cudaErrorMemoryAllocation)
	{
		mergeStagingBuffer = new float4[xDim*yDim];
		isMergePinned = false;
	}
	this->hwnd = (HWND) hwnd;
	threadObjects[0]->g_hWnd = hwnd;
	threadObjects[0]->xDim = x;
	threadObjects[0]->yDim = y;
	while (!threadObjects[0]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
	threadObjects[0]->readyForCommand = false;
	threadObjects[0]->nextFunction = &WorkerThread::StartCuda;
	SetThreadPriority(deviceThreadHandles[0],THREAD_PRIORITY_NORMAL);
	for (int n = 1; n < numDevices; n++)
	{
		threadObjects[n]->g_hWnd = NULL;
		threadObjects[n]->xDim = x;
		threadObjects[n]->yDim = y;
		while (!threadObjects[n]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = &WorkerThread::StartCuda;
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
	}
}

void CUDAThreadingManager::DispatchRunFuse(Flame *flame)
{
	for (int n = 0; n < numDevices; n++)
	{
		MemoryBarrier();
		while (!threadObjects[n]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
		flame->Clone(&threadObjects[n]->currentParameters);
		MemoryBarrier();
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = &WorkerThread::RunFuse;
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
	}
}

void CUDAThreadingManager::DispatchStartFrame(Flame *flame)
{
	for (int n = 0; n < numDevices; n++)
	{MemoryBarrier();
		while (!threadObjects[n]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
		flame->Clone(&threadObjects[n]->currentParameters);
		MemoryBarrier();
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = &WorkerThread::StartFrame;
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
	}
}

void CUDAThreadingManager::DispatchRenderBatch(Flame *flame)
{
	bool done = false;
	while (!done)
	{
		for (int n = 0; n < numDevices; n++)
		{
			MemoryBarrier();
			if (threadObjects[n]->readyForCommand)
			{
				flame->Clone(&threadObjects[n]->currentParameters);
				MemoryBarrier();
				threadObjects[n]->readyForCommand = false;
				threadObjects[n]->nextFunction = &WorkerThread::RenderBatch;
				SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
				done = true;
				break;
			}
		}
		if (!done)
			SwitchToThread();
	}
}

void CUDAThreadingManager::DispatchFinishFrame(Flame *flame, void** image, bool useAlpha)
{
	for (int n = 1; n < numDevices; n++)
	{
		while ((!threadObjects[0]->readyForCommand) || (!threadObjects[n]->readyForCommand)) {SwitchToThread(); MemoryBarrier();}
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = &WorkerThread::GrabFrame;
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
		while ((!threadObjects[0]->readyForCommand) || (!threadObjects[n]->readyForCommand)) {SwitchToThread(); MemoryBarrier();}
		threadObjects[0]->readyForCommand = false;
		threadObjects[0]->nextFunction = &WorkerThread::MergeFrames;
		SetThreadPriority(deviceThreadHandles[0],THREAD_PRIORITY_NORMAL);
	}
	while (!threadObjects[0]->readyForCommand){SwitchToThread(); MemoryBarrier();}
	threadObjects[0]->image = *image;
	threadObjects[0]->useAlpha = useAlpha;
	MemoryBarrier();
	threadObjects[0]->readyForCommand = false;
	threadObjects[0]->nextFunction = &WorkerThread::FinishFrame;
	SetThreadPriority(deviceThreadHandles[0],THREAD_PRIORITY_NORMAL);
	while (!threadObjects[0]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
	*image = threadObjects[0]->image;
}

void CUDAThreadingManager::DispatchStopCuda()
{
	for (int n = 0; n < numDevices; n++)
	{
		while (!threadObjects[n]->readyForCommand) {SwitchToThread();MemoryBarrier();}
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = &WorkerThread::StopCuda;
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
	}
	if (isMergePinned)
	{
		cudaFreeHost(mergeStagingBuffer);
	}
	else
	{
		delete mergeStagingBuffer;
	}
}


CUDAThreadingManager::~CUDAThreadingManager()
{
	if (deviceThreadHandles)
	{
		delete deviceThreadHandles;
		deviceThreadHandles = NULL;
	}
	if (threadIds)
	{
		delete threadIds;
		threadIds = NULL;
	}
	if (threadObjects)
	{
		for (int n = 0; n < numDevices; n++)
		{
			while (!threadObjects[n]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
			threadObjects[n]->readyForCommand = false;
			threadObjects[n]->nextFunction = &WorkerThread::CloseThread;
			while (!threadObjects[n]->readyForCommand) {SwitchToThread(); MemoryBarrier();}
			delete threadObjects[n];
			MemoryBarrier();
			threadObjects[n] = NULL;
		}
		delete threadObjects;
		threadObjects = NULL;
	}
}