/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <windows.h>
#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <windows.h>
#include <stdio.h>
#include "..\\Flam4CUDA\\Defines.h"

class WorkerThread
{
private:
	bool terminateSignal;
	int deviceNumber;
	bool cudaRunning;

public:
	int xDim;
	int yDim;
	int batchCounter;
	GLuint gl_PBO;
	GLuint gl_Tex;
	float4 *h_Src;
	HDC g_hDC;
	HGLRC g_hRC;
	unsigned int g_hWnd;
	void* image;
	bool useAlpha;

	Flame* currentParameters;
	void (WorkerThread::*nextFunction)(void);
	bool readyForCommand;
	HANDLE parentThread;

	WorkerThread(int device);
	static DWORD WINAPI workerThreadMessageProc( LPVOID lpParam);
	DWORD WINAPI workerThreadMessageProcInternal();

	void StartCuda();

	void RunFuse();

	void StartFrame();

	void RenderBatch();

	void GrabFrame();

	void MergeFrames();

	void FinishFrame();

	void CloseThread();

	void StopCuda();

	~WorkerThread();
};