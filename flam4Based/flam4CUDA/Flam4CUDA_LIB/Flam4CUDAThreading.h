/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <windows.h>
#include <stdio.h>
#include "WorkerThread.h"

class CUDAThreadingManager
{
public:
	HANDLE* deviceThreadHandles;
	DWORD* threadIds;
	WorkerThread** threadObjects;
	int numDevices;
	HWND hwnd;

	CUDAThreadingManager();
	
	~CUDAThreadingManager();

	void DispatchStartCuda(unsigned int hwnd, int x, int y);
	void DispatchStopCuda();
	void DispatchRunFuse(Flame* flame);
	void DispatchStartFrame(Flame* flame);
	void DispatchRenderBatch(Flame* flame);
	void DispatchFinishFrame(Flame* flame, void** image, bool useAlpha);
};
