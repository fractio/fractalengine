﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Program

open System
open System.IO
open System.Net
open System.Net.Sockets
open AsyncSockets
open ProtoBuf
open TcpCommands
open System.Text
open CommandLine


[<STAThread>]
[<EntryPoint>]
let main(args) =
    if args.Length = 0 then
        let window = Flam4GUI.MainWindow.Create()
        (new System.Windows.Application()).Run(window) |> ignore
    else
        let commands = readCommandLine (Array.toList args)
        Diagnostics.Process.Start ("flam4OCL.exe","-server") |> ignore
        async {
            let endpoint = new System.Net.DnsEndPoint("localhost",4530)
            use sock = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            do! sock.ConnectAsync(endpoint)

            for conversion in commands.conversions do
                use input = new FileStream(fst conversion,FileMode.Open)
                use output = new FileStream(snd conversion,FileMode.Create)
                let response = new MemoryStream()
                let s = new MemoryStream()
                let command = new Command(Convert(input))
                Serializer.Serialize<Command> (s,command)
                let! _ = sock.SendAsync(s)
                let! _ = sock.ReceiveAsync(response)
                response.Position <- 0L
                let result = Serializer.Deserialize<Response>(response)
                let isDone = ref false
                while !isDone<>true do
                    match result.response with
                    | Info(str) -> printfn "%s" str
                    | Error(str) -> 
                        isDone := true
                        failwithf "%s" str
                    | Result(data) -> 
                        isDone := true
                        data.CopyTo output
                ()
            let renders = List.zip commands.inputs commands.outputs
            for render in renders do
                use input = new FileStream(fst render,FileMode.Open)
                use output = new FileStream(snd render,FileMode.Create)
                let response = new MemoryStream()
                let s = new MemoryStream()
                let format = IO.Path.GetExtension(snd render)
                let command = new Command(Render(input,commands.quality,fst commands.resolution,snd commands.resolution,format))
                Serializer.Serialize<Command> (s,command)
                let! _ = sock.SendAsync(s)
                let! _ = sock.ReceiveAsync(response)
                response.Position <- 0L
                let result = Serializer.Deserialize<Response>(response)
                let isDone = ref false
                while !isDone<>true do
                    match result.response with
                    | Info(str) -> printfn "%s" str
                    | Error(str) -> 
                        isDone := true
                        failwithf "%s" str
                    | Result(data) -> 
                        isDone := true
                        data.CopyTo output
                ()


            let s = new MemoryStream()
            let command = new Command(End)
            do Serializer.Serialize<Command> (s,command)
            let! _ =  sock.SendAsync(s)
            sock.Close()
            ()
            } |> Async.RunSynchronously
    
    0