﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

namespace Flam4GUI
open System
open System.IO
open System.Windows
open System.Windows.Markup
open System.Windows.Input
open System.Windows.Controls
open System.Windows.Media.Media3D
open System.Collections.ObjectModel

type Global()=
    static let _Library = new GlobalLibrary()
    static member Library = _Library
    static member Init() = 
        _Library.VarLibrary <- new ObservableCollection<VariationDef>()
        _Library.XformLibrary <- new ObservableCollection<Xform>()

and GlobalLibrary() =
    inherit DependencyObject()
    static let XformLibraryProperty = DependencyProperty.Register("XformLibrary",typeof<ObservableCollection<Xform>>,typeof<GlobalLibrary>)
    static let VarLibraryProperty   = DependencyProperty.Register("VarLibrary",typeof<ObservableCollection<VariationDef>>,typeof<GlobalLibrary>)
    member s.XformLibrary   with get() = s.GetValue(XformLibraryProperty):?>ObservableCollection<Xform>
                            and set x = s.SetValue(XformLibraryProperty,x)
    member s.VarLibrary     with get() = s.GetValue(VarLibraryProperty):?>ObservableCollection<VariationDef>
                            and set x = s.SetValue(VarLibraryProperty,x)

and Xform() =
    inherit DependencyObject()
    let mutable _Transformation = new Affine()
    static let NameProperty = DependencyProperty.Register("Name",typeof<string>,typeof<Xform>)
    static let VarsProperty = DependencyProperty.Register("Vars",typeof<ObservableCollection<Variation>>,typeof<Xform>)
    member s.Name           with get() = s.GetValue(NameProperty):?>string
                            and set newName = s.SetValue(NameProperty,newName)
    member s.Transformation with get() = _Transformation
                            and set x = _Transformation <- x
    member s.Vars           with get() = s.GetValue(VarsProperty):?>ObservableCollection<Variation>
                            and set x = s.SetValue(VarsProperty,x)
    static member Create name=
        let xform = new Xform()
        xform.Name <- name
        xform
    override s.ToString() =
        s.Name

and NodeLink() =
    inherit DependencyObject()
    //static let TargetProperty       = DependencyProperty.Register("Target",typeof<Xform>,typeof<NodeLink>)
    static let WeightProperty       = DependencyProperty.Register("Weight",typeof<float>,typeof<NodeLink>)
    static let ColorIndexProperty   = DependencyProperty.Register("ColorIndex",typeof<float>,typeof<NodeLink>)
    static let ColorSpeedProperty   = DependencyProperty.Register("ColorSpeed",typeof<float>,typeof<NodeLink>)
    member s.Weight     with get()  = s.GetValue(WeightProperty):?>float
                        and set x   = s.SetValue(WeightProperty,x)
    member s.ColorIndex with get()  = s.GetValue(ColorIndexProperty):?>float
                        and set x   = s.SetValue(ColorIndexProperty,x)
    member s.ColorSpeed with get()  = s.GetValue(ColorSpeedProperty):?>float
                        and set x   = s.SetValue(ColorSpeedProperty,x)

and Affine() =
    inherit DependencyObject()

    let mutable _a = 1.0
    let mutable _b = 0.0
    let mutable _c = 0.0
    let mutable _d = 0.0
    let mutable _e = 0.0
    let mutable _f = 1.0
    let mutable _g = 0.0
    let mutable _h = 0.0
    let mutable _i = 0.0
    let mutable _j = 0.0
    let mutable _k = 1.0
    let mutable _l = 0.0
    static let MatrixProperty = DependencyProperty.Register("Matrix",typeof<Matrix3D>,typeof<Affine>)
    member s.a  with get() = _a
                and set x = 
                    _a <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.b  with get() = _b
                and set x = 
                    _b <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.c  with get() = _c
                and set x = 
                    _c <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.d  with get() = _d
                and set x = 
                    _d <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.e  with get() = _e
                and set x = 
                    _e <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.f  with get() = _f
                and set x = 
                    _f <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.g  with get() = _g
                and set x = 
                    _g <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.h  with get() = _h
                and set x = 
                    _h <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.i  with get() = _i
                and set x = 
                    _i <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.j  with get() = _j
                and set x = 
                    _j <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.k  with get() = _k
                and set x = 
                    _k <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.l  with get() = _l
                and set x = 
                    _l <- x
                    s.Matrix <- new Matrix3D(_a,_e,_i,0.0,_b,_f,_j,0.0,_c,_g,_k,0.0,_d,_h,_l,1.0)
    member s.Matrix with get() = s.GetValue(MatrixProperty):?>Matrix3D
                    and set x = s.SetValue(MatrixProperty, x)

and [<AllowNullLiteral>] VariationDef() =
    inherit DependencyObject()
    static let NameProperty   = DependencyProperty.Register("Name",typeof<string>,typeof<VariationDef>)
    static let ParamsProperty = DependencyProperty.Register("Params",typeof<ObservableCollection<string>>,typeof<VariationDef>)
    static let CodeProperty   = DependencyProperty.Register("Code",typeof<string>,typeof<VariationDef>)
    member s.Params with get() = s.GetValue(ParamsProperty):?>ObservableCollection<string>
                    and set x = s.SetValue(ParamsProperty,x)
    member s.Name   with get() = s.GetValue(NameProperty):?>string
                    and set x = s.SetValue(NameProperty,x)
    member s.Code   with get() = s.GetValue(CodeProperty) :?> string
                    and set x = s.SetValue(CodeProperty,x)
    override s.ToString() =
        s.Name

and Variation()=
    inherit DependencyObject()
    let mutable _definition = null:VariationDef
    static let ParamsProperty = DependencyProperty.Register("Params",typeof<ObservableCollection<VariationParameter>>,typeof<Variation>)
    member s.Params     with get() = s.GetValue(ParamsProperty):?>ObservableCollection<VariationParameter>
                        and set x = s.SetValue(ParamsProperty,x)
    member s.Definition with get() = _definition
                        and private set x = _definition <- x
    static member Create(def) =
        let var = new Variation()
        var.Definition <- def
        let parms = new ObservableCollection<VariationParameter>()
        parms.Add (VariationParameter.Create("weight",0.0))
        for param in def.Params do
            parms.Add (VariationParameter.Create(param,0.0))
        var.Params <- parms
        var
    override s.ToString() =
        s.Definition.Name

and VariationParameter() =
    inherit DependencyObject()
    static let ParamNameProperty  = DependencyProperty.Register("ParamName",typeof<string>,typeof<VariationParameter>)
    static let ParamValueProperty = DependencyProperty.Register("ParamValue",typeof<float>,typeof<VariationParameter>)
    member s.ParamName  with get() = s.GetValue(ParamNameProperty):?>string
                        and set x = s.SetValue(ParamNameProperty, x)
    member s.ParamValue with get() = s.GetValue(ParamValueProperty):?>float
                        and set x = s.SetValue(ParamValueProperty, x)
    static member Create(name,value) =
        let varParam = new VariationParameter()
        varParam.ParamName <- name
        varParam.ParamValue <- value
        varParam