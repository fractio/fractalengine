﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Program
open System
open System.IO
open CommandLine

[<STAThread>]
[<EntryPoint>]
let main(args) =
    try

        let commands = readCommandLine (args |> Array.toList)  
        if commands.help=true then
            printfn "\nUsage:
flam4OC -render <filename1> <filename2> ... -output <output1> <output2> ...
[-resolution <x> <y>] [-quality <quality>]
[-convert <old flam3file> <new flam4file>] [-verbose]
[--help] [-server] [-device <deviceId> <deviceId> ...]


Note that -convert converts the file before the rendering process starts, thus

flam4OCL -convert oldFile.flam3 newFile.flam4 -render newFile.flam4
-output newFile.png

is valid and will render oldFile.flam3 as converted to newFile.flam4

Press any key to continue..."
            let _ = Console.ReadKey()
            ()
        for conversion in commands.conversions do
            use input = File.Open (fst conversion,FileMode.Open)
            use output = File.Open (snd conversion,FileMode.Create)
            ConvertFlam3.ConvertFlam3 input output
        for input,output in List.zip commands.inputs commands.outputs do
            let format = Path.GetExtension(output)
            let output = File.Open(output,FileMode.Create)
            do 
                System.IO.File.Open (input,System.IO.FileMode.Open)
                |> LoadFlam4.OpenFlam4
                |> Render.Render commands.resolution commands.quality output format commands.verbose commands.deviceOverride commands.previewMode None
            ()
        if commands.serverMode=true then
            //printfn "Gui is presently not working.  Use flam4OCL --help for command line usage"

            let policyServer = new Flam4Server.SocketPolicyServer()
            let flam4Server = new Flam4Server.Flam4Server()
            printfn "Press any key to continue..."
            System.Console.ReadLine() |> ignore
            //Flam4Server.runFlam4Server()


            //run GUI if no commands passed
            (*let window = Flam4GUI.MainWindow.Create()
            (new System.Windows.Application()).Run(window) |> ignore
            ()*)
        (Helper.serial:>IDisposable).Dispose()
        try
            System.Console.CursorVisible <- true
        with
        | _ -> ()
        0
    with
        | ex ->
            let rec printEx (ex:exn) =
                match ex.InnerException with
                | null ->
                    ()
                | x ->
                    printEx x
                printfn "\nError: %s\n\n%s" ex.Message ex.StackTrace
            printEx ex
            (Helper.serial:>IDisposable).Dispose()
            try
                System.Console.CursorVisible <- true
            with
            | _ -> ()
            -1

