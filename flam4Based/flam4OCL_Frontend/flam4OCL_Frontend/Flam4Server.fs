﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.
module Flam4Server

open System
open System.IO
open System.Net
open System.Net.Sockets
open System.Text
open AsyncSockets
open ProtoBuf
open TcpCommands

//NOTE:  All strings are passed with the UTF-8 encoding.

//Silverlight allows ports 4502-4534

let standardPolicyFile = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<access-policy>
    <cross-domain-access>
        <policy>
            <allow-from>
                <domain uri=\"*\"/>
            </allow-from>
            <grant-to>
                <socket-resource post=\"4530\" protocol=\"tcp\"/>
            </grant-to>
        </policy>
    </cross-domain-access>
</access-policy>"

type Flam4Connection (client,id,server) =
    member s.start () =
        async {
            let finished = ref false
            while !finished<>true do
                let stream = new MemoryStream()
                let! _ = (client:Socket).ReceiveAsync(stream)
                stream.Position <- 0L
                let message = Serializer.Deserialize<Command> stream
                match message.command with
                    | End -> 
                        printfn "Ending..."
                        finished:=true
                    | Convert(input) -> 
                        printfn "Converting..."
                        let output = new MemoryStream()
                        Flam4CoreAPI.ImportFlam3 input output
                        let result = new Response(Result(output))
                        let s = new MemoryStream()
                        Serializer.Serialize<Response>(s,result)
                        let! _ = client.SendAsync s

                        ()
                    | Render(input,q,x,y,format) -> 
                        printfn "Rendering..."
                        let output = new MemoryStream()
                        let param = LoadFlam4.OpenFlam4 input
                        Flam4CoreAPI.RenderStillImage param q x y format output
                        let result = new Response(Result(output))
                        let s = new MemoryStream()
                        Serializer.Serialize<Response>(s,result)
                        let! _ = client.SendAsync s
                        ()
                    | Open(x) -> ()
                    | _ -> ()
            } |> Async.Start

type Flam4Server () as s =
    let listener = new Socket (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
    let clientNo = ref 0
    let clients = new System.Collections.Generic.List<Flam4Connection>()
    let mutable isRunning = false
    
    do listener.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, 0)
    do listener.Bind(new IPEndPoint(IPAddress.Any, 4530))
    do listener.Listen(10)
    do listener.BeginAccept(new AsyncCallback(s.onConnection), null) |> ignore
    do isRunning <- true
    member s.onConnection ar=
        if isRunning=false then
            ()
        else
            printfn "Received connection"
            incr clientNo
            listener.BeginAccept (new AsyncCallback(s.onConnection),null) |> ignore
            let client = listener.EndAccept ar
            let newClient = new Flam4Connection (client,"client "+clientNo.ToString(),s)
            newClient.start()
            lock (clients.Add(newClient)) |> ignore


type SocketPolicyConnection (client:TcpClient,policy) as s=
    static let policyRequestString = "<policy-file-request/>"
    let connection = client
    let buffer = Array.create policyRequestString.Length 0uy
    let received=ref 0
    let policy = policy
    do  try
            connection.Client.BeginReceive (buffer,0,policyRequestString.Length,SocketFlags.None, new AsyncCallback(s.onReceive), null) |> ignore
        with
        :? SocketException ->
            connection.Close()
    member s.onReceive res =
        try
            received := !received + connection.Client.EndReceive(res)
            if !received < policyRequestString.Length then
                connection.Client.BeginReceive (buffer, !received, policyRequestString.Length - !received, SocketFlags.None, new AsyncCallback(s.onReceive), null) |> ignore
                ()
            else
                let request = Encoding.UTF8.GetString(buffer,0,!received)
                if StringComparer.InvariantCultureIgnoreCase.Compare(request, policyRequestString) <> 0 then
                    connection.Close()
                    ()
                else
                    connection.Client.BeginSend (policy,0,policy.Length,SocketFlags.None, new AsyncCallback(s.onSend),null) |> ignore
                    ()
        with
        :? SocketException ->
            connection.Close()
    member s.onSend res =
        connection.Close()
        ()


type SocketPolicyServer () as s=
    let listener = new TcpListener(IPAddress.Any,943)
    let policy = Encoding.UTF8.GetBytes(standardPolicyFile)
    do listener.Start()
    do listener.BeginAcceptTcpClient (new AsyncCallback (s.OnConnection),null) |> ignore
    member s.OnConnection ar =
            let client = listener.EndAcceptTcpClient ar
            let pCon = new SocketPolicyConnection (client, policy)
            listener.BeginAcceptTcpClient (new AsyncCallback (s.OnConnection),null) |> ignore
    member s.Close =
        listener.Stop()


(*let sendSocketPolicy (client:TcpClient) policy =
    async {
        printfn "Serving client"
        let policyRequestString = "<policy-file-request/>"
        let buffers = Array.create policyRequestString.Length 0uy
        let! _ = client.Client.ReceiveAsync(buffers,SocketFlags.None)
        let request = Encoding.UTF8.GetString(buffers,0,buffers.Length)
        if StringComparer.InvariantCultureIgnoreCase.Compare(request, policyRequestString) = 0 then
            let! _ = client.Client.SendAsync(policy,SocketFlags.None)
            printfn "%s" (Encoding.UTF8.GetString(policy,0,policy.Length))
        client.Close()
        }
        

let runFlam4Server ()=
    let policyServerTask = async {
        let listener = new TcpListener(IPAddress.Any,943)
        let policy = Encoding.UTF8.GetBytes(standardPolicyFile)
        listener.Start()
        while true do
            let! client = listener.AcceptTcpClientAsync()
            sendSocketPolicy client policy |> Async.Start
            ()
        listener.Stop()
        printfn "Server has finished."
        return ()
        }
    let cancelServerHandler (ex) =
        printfn "Server session has been terminated."
    Async.TryCancelled (policyServerTask,cancelServerHandler)
    |> Async.Start
    printfn "Server is running.  Press any key to terminate..."
    System.Console.ReadKey() |> ignore
    Async.CancelDefaultToken()
    ()*)