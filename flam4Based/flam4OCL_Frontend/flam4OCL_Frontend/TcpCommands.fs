﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.module TcpCommands

module TcpCommands
open System
open System.IO
open System.Runtime.Serialization
open System.Text
open ProtoBuf

let bufToStream (buf:byte []) =
    let s = new MemoryStream(buf)
    s

let streamToBuf (s:Stream) =
    s.Position <- 0L
    let buf = Array.create (int s.Length) 0uy
    async {
        let! _ = s.AsyncRead(buf,0,buf.Length)
        ()
        } |> Async.RunSynchronously
    buf

type command = 
    | End
    | Convert of Stream
    | Render of Stream*float32*int*int*string
    | Open of Stream
    | SetQuality of float32
    | SetResolution of int*int

[<ProtoContract>]
[<Serializable>]
type Command (command,q,x,y,format) =
    let mutable _commandType,_stream,_q,_x,_y,_format = match command with
                                                        | End                -> 0,null,0.f,0,0,""
                                                        | Convert(s)         -> 1,streamToBuf(s),q,x,y,format
                                                        | Render(s,q,x,y,fmt)-> 2,streamToBuf(s),q,x,y,fmt
                                                        | Open(s)            -> 3,streamToBuf(s),q,x,y,format
                                                        | SetQuality(q)      -> 4,null,q,x,y,format
                                                        | SetResolution(x,y) -> 5,null,q,x,y,format
    new (command) = new Command(command,1000.f,800,600,".png")

    [<ProtoMember(1,IsRequired=true)>]
    member s.commandType    with get() = _commandType
                            and set x = _commandType <- x
    [<ProtoMember(2,IsRequired=true)>]
    member s.stream with get () = _stream
                    and set x = _stream <- x
    [<ProtoMember(3,IsRequired=true)>]
    member s.quality    with get() = _q
                        and set x = _q <- x
    [<ProtoMember(4,IsRequired=true)>]
    member s.xDim   with get() = _x
                    and set x = _x <- x
    [<ProtoMember(5,IsRequired=true)>]
    member s.yDim   with get() = _y
                    and set x = _y <- x
    [<ProtoMember(6,IsRequired=true)>]
    member s.format with get() = _format
                    and set x = _format <- x

    new() =
        new Command (End)

    member s.command = match s.commandType with
                        | 0 -> End
                        | 1 -> Convert(bufToStream(s.stream))
                        | 2 -> Render(bufToStream(s.stream),s.quality,s.xDim,s.yDim,s.format)
                        | 3 -> Open(bufToStream(s.stream))
                        | 4 -> SetQuality(s.quality)
                        | 5 -> SetResolution(s.xDim,s.yDim)
                        | _ -> failwith "Bad command"

type response =
    | Result of Stream
    | Error of string
    | Info of string

[<ProtoContract>]
[<Serializable>]
type Response (responseType) =
    let mutable _responseType,_data = match responseType with
                                        | Result(data)  -> 0,streamToBuf(data)
                                        | Error(str)    -> 1,Encoding.UTF8.GetBytes(str)
                                        | Info(str)     -> 2,Encoding.UTF8.GetBytes(str)
    [<ProtoMember(1,IsRequired=true)>]
    member s.responseType   with get() = _responseType
                            and set x  = _responseType <- x
    [<ProtoMember(2,IsRequired=true)>]
    member s.data           with get() = _data
                            and set x  = _data <- x

    new() = new Response(Error("Not Initialized"))

    member s.response = match s.responseType with
                        | 0 -> Result(bufToStream(s.data))
                        | 1 -> Error(Encoding.UTF8.GetString(s.data))
                        | 2 -> Info(Encoding.UTF8.GetString(s.data))
                        | _ -> failwith "Bad response"