﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module StandardKernals

let ClearBufferKernal = "
kernel void clearBuffer(global float4* buffer, int dim)
{
    int i = get_global_id(0);
    if ((i < dim))
    {
        buffer[i] = (float4)(0.0f,0.0f,0.0f,0.0f);
    }
}
"

let PostProcessKernel = @"
kernel void postProcess(global float4* buffer, int xDim, int yDim, int pageSize, float xDimension, float yDimension, float brightness, float gamma, float vibrancy,float numIters)
{
    int x = get_global_id(0);
    int y = get_global_id(1);
    int k = y*get_global_size(0)+x;
    if (k < pageSize/16)
    {
        float k1 = brightness;
        float area = fabs(xDimension*yDimension);
        float k2 = ((float)(xDim*yDim))/(numIters*area);
        float4 rgba = buffer[k];
        float a = (k1* native_log(1.0f+k2*rgba.w));
        float ls = a/rgba.w;
        rgba.x = ls*rgba.x;
        rgba.y = ls*rgba.y;
        rgba.z = ls*rgba.z;
        float alpha = native_powr(a, 1.0f/gamma-1.0f);
        ls = vibrancy*alpha;
        rgba.x = ls*rgba.x+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.x),1.0f/gamma),rgba.x);
        rgba.y = ls*rgba.y+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.y),1.0f/gamma),rgba.y);
        rgba.z = ls*rgba.z+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.z),1.0f/gamma),rgba.z);
        alpha = native_powr(a, 1.0f/gamma);
        alpha = min(alpha,1.0f);
        if (isfinite(rgba.x))
        {
            buffer[k].x=rgba.x;
            buffer[k].y=rgba.y;
            buffer[k].z=rgba.z;
            buffer[k].w=alpha;
        }
        else
        {
            buffer[k].x=0.0f;
            buffer[k].y=0.0f;
            buffer[k].z=0.0f;
            buffer[k].w=0.0f;
        }
    }
}
"