﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module FlameData
open Helper

type VariationDef = 
    {
    name : string
    code : string
    parameters : string list
    fromInclude : bool
    }
type Variation = 
    {
    vardef : VariationDef
    weight : float32
    parameters : float32 list
    }
type Affine = 
    {
    a : float32
    b : float32
    c : float32
    d : float32
    e : float32
    f : float32
    g : float32
    h : float32
    i : float32
    j : float32
    k : float32
    l : float32
    }
type Xform = 
    {
    name : string
    offset : int
    coefs : Affine
    colorIndex : float32
    colorSpeed : float32
    vars : Variation list
    }
type XformNode = 
    {
    name : string
    self : Xform
    index : int32
    mutable next : XformNodeLink list
    }
and XformNodeLink =
    { 
    offset : int
    next : XformNode
    weight : float32
    opacity : float32
    state : int32
    discard : int32
    }
type Palette =
    {
    data : (float32*float32*float32*float32) list
    }
type Viewport = 
    {
    minimumCoverage : Affine
    aspectRatio : float32
    }
type Gamut =
    {
    Brightness : float32
    Gamma : float32
    Vibrancy : float32
    }
type FlameParameter =
    {
    size : int
    viewport : Viewport
    gamut : Gamut
    xformNodeRepo: dict<string,XformNode>
    xformRepo: dict<string,Xform>
    varRepo : VariationDef list
    allVars : Map<VariationDef,int>
    xforms : XformNode list
    palette : Palette
    }
type Filter =
    {
    name : string
    filter : string
    }
type StageOption =
| Flame of FlameParameter
| Filter of Filter
type StageChannelMapping =
    {
    inChannel : int
    outChannel : int
    }
type Stage =
    {
    operation : StageOption
    inputStages : (Stage*StageChannelMapping list) list
    outputStages : Stage list
    }





type XformSpan =
    {
    past : XformNode*float32
    start : XformNode*float32
    stop : XformNode*float32
    future : XformNode*float32
    }
type TimeSpan =
    {
    startTime : float32
    endTime : float32
    parameterRange: FlameParameter
    xformSpans : XformSpan list
    }
type Animation =
    {
    segments : TimeSpan list
    }

let getMotionSample time animation =
    let currentSegments = List.filter (fun segment -> ((segment.startTime<=time) && (segment.endTime>time))) animation.segments
    currentSegments.[0]