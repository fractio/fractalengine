//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module GeneratePreviewKernel

open FlameData
open FlameMarshal
open Helper
open System.Text.RegularExpressions
open Cloo
open FlameClass

let generateIterateKernel flame (device:ComputeDevice) numPages (varRepo:Map<VariationDef,int>) =
        let pragmas = @"#pragma OPENCL_EXTENSION cl_khr_fp64 : disable
#pragma OPENCL EXTENSION cl_amd_printf : enable" 
        let readPalette =   if device.ImageSupport = true then
                                @"read_imagef(palette, paletteSampler, (float2)(point.w,0.0f))"
                            else
                                @"read_image(palette,palLength,point.w)"
        let paletteSampler =    if device.ImageSupport = true then
                                    @"const sampler_t paletteSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_FILTER_LINEAR;"
                                else
                                    @"float4 read_image(global float * image, int length, float index)
{
    float clampedIndex = index - floor(index);
    float scaledIndex = clampedIndex*(float)length;
    int iLow = floor(scaledIndex);
    int iHigh = ceil(scaledIndex);
    float iFract = scaledIndex - floor(scaledIndex);
    float4 c0 = ((global float4 *) image)[iLow];
    float4 c1 = ((global float4 *) image)[iHigh];
    return iFract*c1+(1.0f-iFract)*c0;
}"
        let palette =   if device.ImageSupport=true then
                            @"read_only image2d_t palette"
                        else
                            @"global float* palette"

        let varWrapper = @"float4 @varName@(float4 pos, float weight,@varParameterDecs@ float r2, float r, float r2D2, float r2D, float theta, float phi, float psi, local unsigned int * randStates, constant float* flameBuffer,int offset)
{
    float xout,yout,zout=0.0f;
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    float a = flameBuffer[offset];
    float b = flameBuffer[offset+1];
    float c = flameBuffer[offset+2];
    float d = flameBuffer[offset+3];
    float e = flameBuffer[offset+4];
    float f = flameBuffer[offset+5];
    float g = flameBuffer[offset+6];
    float h = flameBuffer[offset+7];
    float i = flameBuffer[offset+8];
    float j = flameBuffer[offset+9];
    float k = flameBuffer[offset+10];
    float l = flameBuffer[offset+11];
    zout=z;
	@varCode@
    return (float4) (xout,yout,zout,0.0f);
}

@var@"

        let varCaseWrapper = @"case @varIndex@:
        {
            outpos += flameBuffer[varOffset+1]*@varName@(pos,flameBuffer[varOffset+1],@varParameterDefs@ r2, r, r2D2, r2D, theta, phi, psi, randStates, flameBuffer, *offset+1);
            varOffset += @varLength@;
        } break;
        @varCaseWrapper@"

        let iterateCode = "@pragmas@
#define PI 3.1415926536f

constant int shift1[4] = {6, 2, 13, 3};
constant int shift2[4] = {13, 27, 21, 12};
constant int shift3[4] = {18, 2, 7, 13};
constant unsigned int MODULUS[4] = {4294967294, 4294967288, 4294967280, 4294967168};

@paletteSampler@

unsigned int TausStep(unsigned int z, int s1, int s2, int s3, unsigned int M)
{
    unsigned int b = ((z << s1) ^ z) >> s2;
    return (((z & M) << s3) ^b);
}

unsigned int RAND_INT(local volatile unsigned int* randStates)
{
    unsigned int index = get_local_id(0);
    unsigned int i2 = get_local_id(0)&~(WARP_SIZE-1);
    randStates[index] = TausStep(randStates[index], shift1[index&3], shift2[index&3], shift3[index&3], MODULUS[index&3]);
    return (randStates[index&(WARP_SIZE-1)+i2]^randStates[(index+1)&(WARP_SIZE-1)+i2]^randStates[(index+2)&(WARP_SIZE-1)+i2]^randStates[(index+3)&(WARP_SIZE-1)+i2]);
}

#define randInt(void) RAND_INT(randStates)

unsigned int RAND_INT_WARP(local volatile unsigned int* randStates)
{
    unsigned int index = get_local_id(0);
    unsigned int i2 = get_local_id(0)&~(WARP_SIZE-1);
    randStates[index] = TausStep(randStates[index], shift1[index&3], shift2[index&3], shift3[index&3], MODULUS[index&3]);
    return (randStates[i2]^randStates[i2+1]^randStates[i2+2]^randStates[i2+3]);
}

#define randIntWarp(void) RAND_INT_WARP(randStates)

float RAND_FLOAT(local volatile unsigned int* randStates)
{
    unsigned int y = randInt();
    return as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

#define randFloat(void) RAND_FLOAT(randStates)

float RAND_FLOAT_1_2(local volatile unsigned int* randStates)
{
    unsigned int y = randInt();
    return as_float((y&0x007FFFFF)|0x3F800000);
}

#define randFloat12(void) RAND_FLOAT_1_2(randStates)

float RAND_FLOAT_WARP(local volatile unsigned int* randStates)
{
    unsigned int y = randIntWarp();
    return as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

#define randFloatWarp(void) RAND_FLOAT_WARP(randStates)

float RAND_FLOAT_WARP_1_2(local volatile unsigned int* randStates)
{
    unsigned int y = randIntWarp();
    return as_float((y&0x007FFFFF)|0x3F800000);
}

#define randFloatWarp12(void) RAND_FLOAT_WARP_1_2(randStates)

#ifdef NVIDIA
float4 loadPoint(int nodeIndex, local volatile unsigned int * randStates,local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointOffset[index] = 4*(nodeIndex*NUM_POINTS_PER_NODE+randInt()%NUM_POINTS_PER_NODE);
    for (int n = (index&~3); n < ((index&~3)+4);n++)
    {
        pointStage[n*4+(index&3)]=pointPool[pointOffset[n]+(index&3)];
    }
    return (float4)(pointStage[index*4],pointStage[index*4+1],pointStage[index*4+2],pointStage[index*4+3]);
}
#else
float4 loadPoint(int nodeIndex, local volatile unsigned int * randStates,local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointOffset[index] = 4*(nodeIndex*NUM_POINTS_PER_NODE+randInt()%NUM_POINTS_PER_NODE);
    global float4 * pointSrc = (global float4 *) pointPool;
    return pointSrc[pointOffset[index]/4];
}
#endif

#ifdef NVIDIA
void storePoint(float4 point, local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointStage[index*4] = point.x;
    pointStage[index*4+1]=point.y;
    pointStage[index*4+2]=point.z;
    pointStage[index*4+3]=point.w;
    for (int n = (index&~3); n < ((index&~3)+4); n++)
    {
        pointPool[pointOffset[n]+(index&3)]=pointStage[n*4+(index&3)];
    }
}
#else
void storePoint(float4 point, local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    global float4 * pointDest = (global float4 *) pointPool;
    pointDest[pointOffset[index]/4] = point;
}
#endif

int4 rasterizePoint(float4 point, int2 dim, local volatile unsigned int* randStates, constant float* flameBuffer, local int* pageLookupTable, int numPages)
{
    point = point+(float4)(flameBuffer[3],flameBuffer[7],flameBuffer[11],0.0f);
    float4 pos = point.xxxx*(float4)(flameBuffer[0],flameBuffer[4],flameBuffer[8],0.0f)+point.yyyy*(float4)(flameBuffer[1],flameBuffer[5],flameBuffer[9],0.0f)+point.zzzz*(float4)(flameBuffer[2],flameBuffer[6],flameBuffer[10],0.0f);
    float xpos = /*@numSlices*pixelAdjust@*/((float)numPages)*(0.5f+0.5f*pos.x);
    float slice = floor(xpos);
    int page = -1;
    if ((slice < numPages) && (slice >=0))
        page = pageLookupTable[(int) slice];
    float xd = (xpos-slice)*ceil(((float)dim.x)/((float)numPages));
    float yd = ((0.5f+0.5f*pos.y))*(float)dim.y;
    return (int4)((int)xd,(int)yd,page,0);
}

void drawPoint(global volatile float4 ** renderBuffers, @palette@, int2 dim, float4 point, local volatile unsigned int* randStates, float opacity, constant float* flameBuffer, int palLength,local int * pageLookupTable, int numPages,int pageSize)
{
    int4 coords = rasterizePoint(point, dim, randStates,flameBuffer,pageLookupTable,numPages);
    if ((coords.y >= 0.0f)&&(coords.y < dim.y)&&(coords.z != -1))
    {
        float4 color = opacity*@readPalette@;
        renderBuffers[coords.z][coords.y*(int)ceil(((float)dim.x)/((float)numPages))+coords.x]+=color;
    }
}

@var@

float4 Xform(float4 point, local unsigned int * randStates, constant float* flameBuffer,int *offset)
{
    float4 outpos = (float4)(0.0f,0.0f,0.0f,0.0f);
    float4 pos = point.xxxx*(float4)(flameBuffer[*offset+1],flameBuffer[*offset+5],flameBuffer[*offset+9],0.0f)+point.yyyy*(float4)(flameBuffer[*offset+2],flameBuffer[*offset+6],flameBuffer[*offset+10],0.0f)+point.zzzz*(float4)(flameBuffer[*offset+3],flameBuffer[*offset+7],flameBuffer[*offset+11],0.0f)+(float4)(flameBuffer[*offset+4],flameBuffer[*offset+8],flameBuffer[*offset+12],0.0f);
    float r2D2 = pos.x*pos.x+pos.y*pos.y;
    float r2 = pos.x*pos.x+pos.y*pos.y+pos.z*pos.z;
    float r2D = sqrt(r2D2);
    float r = sqrt(r2);
    float theta = atan2(pos.y,pos.x);
    float phi = atan2(pos.z,r2D);
    float psi = atan2(pos.x,pos.y);
    int varOffset = *offset+15;
    while (as_int(flameBuffer[varOffset])>=0)
    {
        switch(as_int(flameBuffer[varOffset]))
        {
        @varCaseWrapper@
        }
    }


    float col = mix(point.w,flameBuffer[*offset+13],flameBuffer[*offset+14]);
    *offset=varOffset;
    return (float4)(outpos.x,outpos.y,outpos.z,col);
}

int getNextXformNode(int oldStateOffset, float rand, constant float* flameBuffer)
{
    int nextLink = oldStateOffset;
    while (flameBuffer[nextLink]>=1.0)
    {
        if (rand < flameBuffer[nextLink])
            nextLink=as_int(flameBuffer[nextLink+1]);
        else
            nextLink=as_int(flameBuffer[nextLink+2]);
    }
    return as_int(nextLink);
}


kernel void renderBatch(global unsigned int* randSeeds, global unsigned int* discardStates, global volatile float *pointPool, global int* stateStacks, global int* oldStateIndices, global int* pageLookups, constant float* flameBuffer, @palette@, int xdim, int ydim, int palLength, int numPages, int pageSize,@pageBuffers@)
{
    global volatile float4 * renderBuffers[@pageCount@];@assignPageBufferPointers@
    int2 dimension = (int2)(xdim,ydim);
    local volatile unsigned int randStates[BLOCK_SIZE];
    randStates[get_local_id(0)]=randSeeds[get_global_id(1)*get_global_size(0)+get_global_id(0)];
    local volatile unsigned int pointOffset[BLOCK_SIZE];
    local volatile float pointStage[BLOCK_SIZE*4];
    local int stateStack[BLOCK_SIZE];
    local int pageLookupTable[PAGE_LOOKUP_TABLE_LENGTH];
    const int warpOffset = get_local_id(0)&~(WARP_SIZE-1);
    float4 oldPoint;
    int dstate = 0;
    float rnd = 0.0f;
    float4 point;
    for (int n = 0; n < PAGE_LOOKUP_TABLE_LENGTH; n += get_global_size(0))
    {
        int i = n+get_local_id(0);
        if (i < PAGE_LOOKUP_TABLE_LENGTH)
            pageLookupTable[i] = pageLookups[i];
    }
    barrier(CLK_LOCAL_MEM_FENCE);


    unsigned int discard = discardStates[(get_global_id(1)*get_global_size(0)+get_global_id(0))>>5];
    stateStack[get_local_id(0)] = stateStacks[get_global_id(1)*get_global_size(0)+get_global_id(0)];
    unsigned int stateIndex=oldStateIndices[(get_global_id(1)*get_global_size(0)+get_global_id(0))>>5];


    rnd = randFloatWarp12();
    int linkOffset = stateStack[stateIndex+warpOffset];
    int xformIndex = -as_int(flameBuffer[linkOffset-1])-1;
    oldPoint = loadPoint(xformIndex,randStates,pointOffset,pointStage,pointPool);
    int nextNodeOffset = getNextXformNode(linkOffset,rnd,flameBuffer);
    stateIndex += as_int(flameBuffer[nextNodeOffset+1]);
    dstate = as_int(flameBuffer[nextNodeOffset+1]);
    stateIndex &= (WARP_SIZE-1);
    float opacity = flameBuffer[nextNodeOffset+2];
    discard = as_int(flameBuffer[nextNodeOffset+3]);
    int xformOffset = as_int(flameBuffer[nextNodeOffset]);

    for (int iterations = 0; iterations<MAX_ITERATIONS; iterations++)
    {
        if (discard != 0)
        {
            xformIndex = as_int(flameBuffer[xformOffset]);
            oldPoint = loadPoint(xformIndex,randStates,pointOffset,pointStage,pointPool);
        }
        int iterCount = as_int(oldPoint.w)&0xFF800000;
        float colIndex = as_float((as_int(oldPoint.w)&0x007FFFFF)|0x3F800000) -1.0f;
        oldPoint.w=colIndex;
        float4 point = Xform(oldPoint,randStates,flameBuffer,&xformOffset);
        linkOffset = xformOffset+1;
        if (iterCount > (100<<23))
        {
            if (opacity!=0.0f)
                drawPoint(renderBuffers,palette,dimension,point,randStates,opacity,flameBuffer,palLength,pageLookupTable,numPages,pageSize);
        }
        else
            iterCount += 1<<23;
        if (isfinite(point.x+point.y+point.z)==0)
        {
            point.x=randFloat();
            point.y=randFloat();
            point.z=randFloat();
            point.w=randFloat();
            iterCount=85<<23;
        }
        point.w=as_float(iterCount|(as_int(point.w+1.0f)&0x007FFFFF));
        stateStack[stateIndex+warpOffset]=linkOffset;
        oldPoint = loadPoint(-as_int(flameBuffer[linkOffset-1])-1,randStates,pointOffset,pointStage,pointPool);
        storePoint(point,pointOffset,pointStage,pointPool);
        rnd = randFloatWarp12();
        nextNodeOffset = getNextXformNode(linkOffset,rnd,flameBuffer);
        stateIndex += as_int(flameBuffer[nextNodeOffset+1]);
        dstate = as_int(flameBuffer[nextNodeOffset+1]);
        stateIndex &= (WARP_SIZE-1);
        opacity = flameBuffer[nextNodeOffset+2];
        discard = as_int(flameBuffer[nextNodeOffset+3]);
        xformOffset = as_int(flameBuffer[nextNodeOffset]);
    }

    randSeeds[get_global_id(1)*get_global_size(0)+get_global_id(0)]=randStates[get_local_id(0)];
    stateStacks[get_global_id(1)*get_global_size(0)+get_global_id(0)]=stateStack[get_local_id(0)];
    oldStateIndices[(get_global_id(1)*get_global_size(0)+get_global_id(0))/WARP_SIZE] = stateIndex;
    discardStates[(get_global_id(1)*get_global_size(0)+get_global_id(0))/WARP_SIZE] = discard;
}"
        let readPaletteRX = new Regex "@readPalette@"
        let paletteSamplerRX = new Regex "@paletteSampler@"
        let paletteRX = new Regex "@palette@"
        let numSlices_pixelAdjustRX = new Regex "@numSlices*pixelAdjust@"
        let pragmaRX = new Regex "@pragmas@"
        let varNameRX = new Regex "@varName@"
        let varCodeRX = new Regex "@varCode@"
        let varParameterDecsRX = new Regex "@varParameterDecs@"
        let varParameterDefsRX = new Regex "@varParameterDefs@"
        let varIndexRX = new Regex "@varIndex@"
        let varLengthRX = new Regex "@varLength@"
        let varCaseWrapperRX = new Regex "@varCaseWrapper@"
        let varRX = new Regex "@var@"
        let pageBuffersRX = new Regex "@pageBuffers@"
        let pageCountRX = new Regex "@pageCount@"
        let assignPageBufferPointersRX = new Regex "@assignPageBufferPointers@"
 
        let pageBufferDecls numPages=
            let rec pageBufferDecls numPages code index =
                if index = numPages then
                    code
                else
                    code + pageBufferDecls numPages (sprintf ",global volatile float * renderBuffer%i" index) (index+1)
            pageBufferDecls numPages "global volatile float * renderBuffer0" 1
        let pageBufferPointerAssignments numPages =
            let rec pageBufferPointerAssignments numPages code index =
                if index = numPages then
                    code
                else
                    code + pageBufferPointerAssignments numPages (sprintf "\n\trenderBuffers[%i] = (global volatile float4 *) renderBuffer%i;" index index) (index+1)
            pageBufferPointerAssignments numPages "" 0

        let vars = [for var in varRepo -> (var.Key,var.Value)] |> List.sortBy (fun var -> snd var)
        let varCode = vars |> List.map (fun (var,index) ->
                                            let rec buildVarParams varParams code= 
                                                match varParams with
                                                | head::tail ->
                                                    (code," float "+head+",@varParameterDecs@") |> varParameterDecsRX.Replace |> buildVarParams tail
                                                | _ ->
                                                    (code,"") |> varParameterDecsRX.Replace
                                            let varWithParams = buildVarParams var.parameters varWrapper
                                            let varWithParamsAndCode = (varWithParams,var.code) |> varCodeRX.Replace
                                            let varWithParamsAndCodeAndName = (varWithParamsAndCode,var.name) |> varNameRX.Replace
                                            varWithParamsAndCodeAndName)
        let iterateCodeWithVarDefs =
            let rec buildVarCode (varCode:string list) code=
                match varCode with
                | head::tail -> 
                    (code, head) |> varRX.Replace |> buildVarCode tail
                | _ ->
                    (code,"") |> varRX.Replace
            buildVarCode varCode iterateCode
        let varCalls = vars |> List.map (fun (var,index) ->
                                            let rec buildVarParams varParams n code =
                                                match varParams with
                                                | head::tail ->
                                                    (code," flameBuffer[varOffset+" + string (2+n) + "],@varParameterDefs@") |> varParameterDefsRX.Replace |> buildVarParams tail (n+1)
                                                | _ ->
                                                    (code,"") |> varParameterDefsRX.Replace
                                            let varCall = buildVarParams var.parameters 0 varCaseWrapper
                                            let varCallWithName = (varCall,var.name) |> varNameRX.Replace
                                            let varCallWithNameAndIndex = (varCallWithName,string index) |> varIndexRX.Replace
                                            let varCallWithNameAndIndexAndLength = (varCallWithNameAndIndex,string (var.parameters.Length+2)) |> varLengthRX.Replace
                                            varCallWithNameAndIndexAndLength)
        let iterateCodeWithVars =
            let rec buildVarCode (varCode:string list) code=
                match varCode with
                | head::tail ->
                    (code, head ) |> varCaseWrapperRX.Replace |> buildVarCode tail
                | _ ->
                    (code,"") |> varCaseWrapperRX.Replace
            buildVarCode varCalls iterateCodeWithVarDefs
        let finalCode = ((((((( iterateCodeWithVars,pragmas) |> 
                                pragmaRX.Replace,palette) |>
                                paletteRX.Replace,paletteSampler) |>
                                paletteSamplerRX.Replace,readPalette) |>
                                readPaletteRX.Replace, pageBufferDecls <| numPages) |>
                                pageBuffersRX.Replace, string <| numPages) |>
                                pageCountRX.Replace, pageBufferPointerAssignments <| numPages) |>
                                assignPageBufferPointersRX.Replace                                    
        finalCode