﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module ConvertFlam3

open Flam3VarData
open System.IO
open System.Xml
open System.Globalization
open System.Text.RegularExpressions
open Helper

let ConvertFlam3 (input:Stream) (output:Stream) =
    let numFormat = new NumberFormatInfo()
    numFormat.NumberDecimalSeparator <- "."
    numFormat.NumberGroupSeparator <- ","
    let settings = new XmlReaderSettings()
    settings.ConformanceLevel <- ConformanceLevel.Fragment
    use Flam3Reader = XmlReader.Create(input,settings)
    let Flam3XML = new System.Xml.XmlDocument()
    Flam3XML.Load Flam3Reader
    let writeSettings = new XmlWriterSettings()
    writeSettings.Indent <- true
    writeSettings.OmitXmlDeclaration <- true
    use Flam4Writer = XmlWriter.Create(output,writeSettings)
    let Flam4XML = new System.Xml.XmlDocument()

    let CreateElement name =
        Flam4XML.CreateNode (XmlNodeType.Element,name,null)
    let CreateAttribute name value =
        let attr = Flam4XML.CreateAttribute name
        attr.Value <- value
        attr

    let flam4Node = CreateElement "flam4"|> Flam4XML.AppendChild
    let includeNode = flam4Node <+ CreateElement "include"
    includeNode <+ CreateAttribute "library" "flam3LegacyVars.xml" |> ignore
    let flam3Keyframes = Flam3XML.SelectNodes("descendant::flame")
    for keyframe in flam3Keyframes do
        let flam4Keyframe = flam4Node <+ CreateElement "flame"

        let flam4Viewport = flam4Keyframe <+ CreateElement "viewport"
        flam4Viewport <+ CreateAttribute "aspect_ratio" "1" |> ignore
        let viewportCoefs = flam4Viewport <+ CreateElement "coefs"
        viewportCoefs <+ CreateAttribute "center" (keyframe @?> ("center","0 0")) |> ignore
        viewportCoefs <+ CreateAttribute "size" ((keyframe @?> ("size","0 0")).Split(' ') |>
                                                    (fun dims ->
                                                        let scale = keyframe @?> ("scale","0") |> float32
                                                        dims |>
                                                        Array.mapi (fun i d->
                                                                    if i=1 then 
                                                                        -(d |> float32)/scale
                                                                    else
                                                                        (d |> float32)/scale
                                                                    |> string) |>
                                                        Array.reduce (fun a b -> a+" "+b))) |> ignore
        viewportCoefs <+ CreateAttribute "rotation" (keyframe @?> ("rotate","0")) |> ignore
        viewportCoefs <+ CreateAttribute "skew" "0" |> ignore
                                                    
        let flam4Gamut = flam4Keyframe <+ CreateElement "gamut"
        flam4Gamut <+ CreateAttribute "brightness" (keyframe @?> ("brightness","1")) |> ignore
        flam4Gamut <+ CreateAttribute "gamma" (keyframe @?> ("gamma","1")) |> ignore
        flam4Gamut <+ CreateAttribute "vibrancy" (keyframe @?> ("vibrancy","1")) |> ignore

        
        let isFinalXform = match keyframe.SelectSingleNode("child::finalxform") with
                            | null ->
                                false
                            | _ ->
                                true

        let readXform ((xform,thisIsFinal):XmlNode*bool) (nodeName:string) (xformName:string) =    
            let currentXformNode = flam4Keyframe <+ CreateElement "xform_node"
            currentXformNode <+ CreateAttribute "name" nodeName |> ignore
            let currentXform = currentXformNode <+ CreateElement "xform"
            currentXform <+ CreateAttribute "name" xformName |> ignore
            currentXform <+ CreateAttribute "color_index" (xform @?> ("color","0")) |> ignore
            currentXform <+ CreateAttribute "color_speed" (xform @?> ("color_speed",
                                                                                    (xform @?> ("symmetry","0")) |>
                                                                                    (fun sym ->
                                                                                        sym |> 
                                                                                        float32 |>
                                                                                        (fun sym -> 0.5f-0.5f*sym) |>
                                                                                        string))) |> ignore
            let coefs = currentXform <+ CreateElement "coefs"
            coefs <+ CreateAttribute "adbecf" (xform @?> ("coefs","0 0 0 0 0 0")) |> ignore
            let vars = [ for attr in xform.Attributes do
                            let name = getFlam3Alias attr.Name
                            match flam3Variations.TryGetValue name with
                            | false,_ ->
                                ()
                            | _,parameters ->
                                let var = CreateElement "var"
                                var <+ CreateAttribute "name" name |> ignore
                                var <+ CreateAttribute "weight" attr.Value |> ignore
                                for parameter in parameters do
                                    for name in deAliasParameterNames parameter do
                                        if xform @? name then
                                            var <+ CreateAttribute parameter (xform @!> name) |> ignore
                                yield var
                       ]
            List.iter (fun var -> currentXform <+ var|>ignore) vars
            let post=xform@?>("post",null)
            if post<>null then
                let name = (currentXformNode @!> "name" )+"_post"
                let xformName = (currentXformNode.SelectSingleNode("child::xform") @!> "name")+"_post"
                let postNode = currentXformNode <+ CreateElement "xform_node"
                postNode <+ CreateAttribute "name" name |> ignore
                postNode <+ CreateAttribute "weight" "1" |> ignore
                postNode <+ CreateAttribute "opacity" (
                            if xform @? "plotmode"  then
                                "0"
                            else
                                xform @?> ("opacity","1")) |> ignore
                let postXform = postNode <+ CreateElement "xform"
                postXform <+ CreateAttribute "name" xformName |> ignore
                postXform <+ CreateAttribute "color_index" "0" |> ignore
                postXform <+ CreateAttribute "color_speed" "0" |> ignore
                let postCoefs = postXform <+ CreateElement "coefs"
                postCoefs <+ CreateAttribute "adbecf" post |> ignore
                let postVar = postXform <+ CreateElement "var"
                postVar <+ CreateAttribute "name" "linear" |> ignore
                postVar <+ CreateAttribute "weight" "1" |> ignore
                postNode
            else
                currentXformNode
        let finalXform = match keyframe.SelectSingleNode("child::finalxform") with
                            | null ->
                                null
                            | finalXform ->
                                let finalXformNode = readXform (finalXform,true) "finalXformNode" "finalXform"
                                let mutable index2 = 1
                                let chaos = (finalXform @?> ("chaos","")).Split ([|' '|],System.StringSplitOptions.RemoveEmptyEntries) |> Array.map (fun x -> float32 x)
                                for xform in keyframe.SelectNodes("child::xform") do
                                    let weight =
                                        let weight = xform @?> ("weight","0")
                                        let chaos = if index2>chaos.Length then
                                                        1.0f
                                                    else
                                                        chaos.[index2-1]
                                        weight |> float32 |> (*) chaos |> string
                                    if float32 weight>0.0f then
                                        let nextXform = finalXformNode <+ CreateElement "xform_node"
                                        nextXform <+ CreateAttribute "name" ("Node"+string index2) |> ignore               
                                        nextXform <+ CreateAttribute "weight" weight |> ignore
                                        nextXform <+ CreateAttribute "state" "pop" |> ignore
                                        nextXform <+ CreateAttribute "opacity" "0" |> ignore
                                    index2<-index2+1
                                finalXform
  

        let mutable index=1
        for xform in keyframe.SelectNodes("child::xform") do
            let mutable currentXformNode = readXform (xform,false) ("Node"+string index) ("Xform"+string index)
            if isFinalXform then
                let nextXform = currentXformNode <+ CreateElement "xform_node"
                nextXform <+ CreateAttribute "name" "finalXformNode" |> ignore
                nextXform <+ CreateAttribute "weight" "1" |> ignore
                nextXform <+ CreateAttribute "state" "push" |> ignore
                nextXform <+ CreateAttribute "opacity" (
                    if xform @? "plotmode" || finalXform @? "post" then
                        "0"
                    else
                        xform @?> ("opacity","1")) |> ignore
            else
                currentXformNode <+ CreateAttribute "opacity" (xform @?> ("opacity","1")) |> ignore
                let mutable index2=1
                let chaos = (xform @?> ("chaos","")).Split ([|' '|],System.StringSplitOptions.RemoveEmptyEntries) |> Array.map (fun x -> float32 x)
                for xform in keyframe.SelectNodes("child::xform") do
                    let weight =
                        let weight = xform @?> ("weight","0")
                        let chaos = if index2>chaos.Length then
                                        1.0f
                                    else
                                        chaos.[index2-1]
                        weight |> float32 |> (*) chaos |> string
                    if float32 weight>0.0f then
                        let nextXform = currentXformNode <+ CreateElement "xform_node"
                        nextXform <+ CreateAttribute "name" ("Node"+string index2) |> ignore               
                        nextXform <+ CreateAttribute "weight" weight |> ignore
                        if xform @? "post" then
                            nextXform <+ CreateAttribute "opacity" "0" |> ignore
                        else
                            nextXform <+ CreateAttribute "opacity" (
                                if xform @? "plotmode"  then
                                    "0"
                                else
                                    xform @?> ("opacity","1")) |> ignore
                    index2<-index2+1
            index <- index+1
            ()

        let flam4Palette = flam4Keyframe <+ CreateElement "palette"
        if keyframe.SelectNodes("child::color").Count<>0 then
            let paletteAttrs = keyframe.SelectNodes("child::color") |>
                                (fun palAttrs ->
                                    [for attr in palAttrs -> attr]) |>
                                List.sortBy (fun attr ->
                                    attr @?> ("index","0") |> float32)
            let palString = paletteAttrs |> 
                                    List.map (fun (attr:XmlNode) -> attr @?> ("rgb","0 0 0")) |>
                                    List.fold (fun a b -> a+"\n\t\t"+b) "" |>
                                    (fun x -> x+"\n\t")
            flam4Palette <+ CreateAttribute "format" "rgb_255norm" |> ignore
            flam4Palette.InnerText<-palString
        elif keyframe.SelectSingleNode("child::colors")<>null then
            let palNode = keyframe.SelectSingleNode("child::colors")
            let palString = palNode.Attributes.GetNamedItem("data").Value
            let deAlphaRX = new Regex @"(?<digit1>[0-9]|[A-F])\s*?(?<digit2>[0-9]|[A-F])\s*?(?<digit3>[0-9]|[A-F])\s*?(?<digit4>[0-9]|[A-F])\s*?(?<digit5>[0-9]|[A-F])\s*?(?<digit6>[0-9]|[A-F])\s*?(?<digit7>[0-9]|[A-F])\s*?(?<digit8>[0-9]|[A-F])"
            let processedPalString = deAlphaRX.Replace (palString,@"${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}")
            flam4Palette <+ CreateAttribute "format" "rgb8_hex" |> ignore
            flam4Palette.InnerText<-processedPalString
        else
            let palNode = keyframe.SelectSingleNode("child::palette")
            match palNode.Attributes.GetNamedItem("format").Value with
            | "RGB" ->
                flam4Palette <+ CreateAttribute "format" "rgb8_hex" |> ignore
                flam4Palette.InnerText<-palNode.InnerText
            | f ->
                failwith "Unknown palette format: %s" f
                                

        ()
    Flam4XML.Save Flam4Writer
    ()