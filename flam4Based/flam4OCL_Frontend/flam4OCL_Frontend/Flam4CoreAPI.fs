﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.


module Flam4CoreAPI

//TODO:  Implement these
let RenderStillImage input q x y format output =
    Render.Render (x,y) q output format false [] false None input

let ValidatePreviewKernel input kernel x y =
    ()

let SetPreviewKernel input x y =
    ()

let RenderPreview input q x y format output =
    ()


let ImportFlam3 input output =
    ConvertFlam3.ConvertFlam3 input output

let ReadFlameXML file =
    LoadFlam4.OpenFlam4 file

let WriteFlameXML () =
    ()