﻿namespace Flam4GUI
open System
open System.IO
open System.Windows
open System.Windows.Markup
open System.Windows.Input
open System.Windows.Controls

type Xform(name) =
    let mutable m_name = name
    member s.Name   with get() = m_name
                    and set newName = m_name <- newName
    override s.ToString() =
        s.Name

type NodePanel() as s=
    inherit UserControl()

    let mutable _contentLoaded = false
    do s.InitializeComponent()

    member s.InitializeComponent()=
        if _contentLoaded<>true then
            _contentLoaded <- true
            s.Loaded.Add (fun _ ->
                let X = s.FindName "CloseButton" :?> Button
                X.Click.Add (fun _ -> (s.Parent :?> Panel).Children.Remove(s:>UIElement))
                let newXformButtonContainer = s.FindName "NewXformButtonContainer"
                let xformBox = s.FindName "XformComboBox" :?> ComboBox
                let newXformButton = s.FindName "NewXformButton" :?> Button
                newXformButton.Click.Add (fun _-> xformBox.Items.Add (new Xform "NewXform") |> ignore)
                xformBox.SelectionChanged.Add (fun e ->
                    let lastSelection = match e.RemovedItems with
                                        | x when x.Count > 0 ->
                                            x.Item(0)
                                        | _ ->
                                            null
                    if xformBox.SelectedItem = newXformButtonContainer then
                        xformBox.SelectedItem <- lastSelection
                    )
                )

    static member Create() =
        let fs = new FileStream(@".\Resources\NodePanel.xaml",FileMode.Open)
        System.Windows.Markup.XamlReader.Load(fs) :?> (NodePanel)