﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Render

open FlameData
open Core
open System
open System.IO

let Render (xdim,ydim) (quality:float32) (outputFile:Stream) format verbose deviceOverride previewMode (engine:Flam4BackEnd option) (flame:FlameParameter)=
    use flam4 = new Flam4BackEnd (deviceOverride,previewMode)

    flam4.createKernal flame verbose <| int64 xdim <| int64 ydim

    flam4.startFrame flame <| int64 xdim <| int64 ydim
    let numBatches = (int64 xdim*int64 ydim*int64 quality)/(32L*32L*64L*100L)
    printfn "Compilation complete, now rendering..."

    //flam4.renderFlame flame numBatches (float32 numBatches*float32(32*32*64*100))

    let time = new System.Diagnostics.Stopwatch()
    time.Start()
    let mutable pagesCount = 1
    let mutable totalPages = 0
    while pagesCount <> 0 do
        pagesCount <- flam4.renderBatch flame totalPages numBatches
        totalPages <- totalPages + pagesCount
    flam4.flushPages()
    flam4.finishFrame flame (float32 ((32L*32L*64L*100L)*numBatches))
    let t = time.ElapsedMilliseconds
    printfn "Total rendering time: %ims" t
    printfn "Writing image to file..."
    flam4.writePng outputFile format
