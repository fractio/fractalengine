﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module ExportPNG

open Cloo
open System
open System.IO
open System.Drawing
open System.Drawing.Imaging

let writeImage (buffer:float32[][]) (xdim:int64) (ydim:int64) (output:Stream) (format:string)=
    let bitmap = new Bitmap(int xdim,int ydim,Imaging.PixelFormat.Format24bppRgb)
    match format.ToLower() with
    | "raw8" ->
        let pitch = int(xdim*4L)
        let pixels = Array.create(pitch*int ydim) 0uy
        for page in 0..buffer.Length-1 do
            for x in 0..buffer.[page].Length/4-1 do
                let pageWidth = int (Math.Ceiling(float xdim/float buffer.Length))
                let yCoord = x/pageWidth
                let xCoord = (page-yCoord)*pageWidth + x
                if xCoord < int xdim && yCoord < int ydim then
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*4]  <-byte <| 255.0f*min(max buffer.[page].[int x*4] 0.0f)   1.0f
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*4+1]<-byte <| 255.0f*min(max buffer.[page].[int x*4+1] 0.0f) 1.0f
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*4+2]<-byte <| 255.0f*min(max buffer.[page].[int x*4+2] 0.0f) 1.0f
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*4+3]<-255uy
        printfn "output size: %i" pixels.Length
        output.Write(pixels,0,pixels.Length)
    | format ->
        let rc = new Rectangle(0,0,int xdim,int ydim)
        let bufferData = bitmap.LockBits(rc,Imaging.ImageLockMode.WriteOnly,Imaging.PixelFormat.Format24bppRgb)
        let pitch = bufferData.Stride
        let pixels = Array.create(pitch*int ydim) 0uy
        for page in 0..buffer.Length-1 do
            for x in 0..buffer.[page].Length/4-1 do
                let pageWidth = int (Math.Ceiling(float xdim/float buffer.Length))
                let yCoord = x/pageWidth
                let xCoord = (page-yCoord)*pageWidth + x
                if xCoord < int xdim && yCoord < int ydim then //BGR format!
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*3+2]<-byte <| 255.0f*min(max buffer.[page].[int x*4] 0.0f)   1.0f
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*3+1]<-byte <| 255.0f*min(max buffer.[page].[int x*4+1] 0.0f) 1.0f
                    pixels.[((int ydim)-yCoord-1)*pitch+xCoord*3]  <-byte <| 255.0f*min(max buffer.[page].[int x*4+2] 0.0f) 1.0f
        System.Runtime.InteropServices.Marshal.Copy(pixels,0,bufferData.Scan0,pixels.Length)
        bitmap.UnlockBits(bufferData)
        let format = match format with
                        | ".bmp"  -> ImageFormat.Bmp
                        | ".emf"  -> ImageFormat.Emf
                        | ".exif" -> ImageFormat.Exif
                        | ".gif"  -> ImageFormat.Gif
                        | ".icon" -> ImageFormat.Icon
                        | ".jpeg" -> ImageFormat.Jpeg
                        | ".png"  -> ImageFormat.Png
                        | ".tiff" -> ImageFormat.Tiff
                        | ".wmf"  -> ImageFormat.Wmf
                        | _ -> failwithf "Bad image format: %s" format
        bitmap.Save(output,format)
    ()