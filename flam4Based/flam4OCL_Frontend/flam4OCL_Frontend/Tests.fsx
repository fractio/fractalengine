﻿let TestCommandLine =
    let startVar = 0
    let numVars = 96
    let firstNames = [for n in startVar..numVars ->
                        "var"+string n+".flame "]
    let lastNames = [for n in startVar..numVars ->
                        "var"+string n+".flam4 "]
    let outputs = [for n in startVar..numVars ->
                        string n+".png "]
    let conversions = List.fold2 (fun prev first last->prev+"-convert "+first+last) "" firstNames lastNames
    let renders = List.fold (fun prev name->prev+name) "-render " lastNames    
    let outputs = List.fold (fun prev name->prev+name) "-output " outputs
    "flam4OCL "+conversions+renders+outputs
printfn "%s" TestCommandLine

