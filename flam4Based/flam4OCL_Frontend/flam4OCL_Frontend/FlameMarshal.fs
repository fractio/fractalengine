﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module FlameMarshal

open FlameData
open Helper



(*let polarToAffine (t:Affine) =
    (
        t.xr*cos(t.xtheta)*cos(t.xphi),
        t.xr*sin(t.xtheta)*cos(t.xphi),
        t.xr*sin(t.xphi),
        t.cr*cos(t.ctheta)*cos(t.cphi),
        t.yr*cos(t.ytheta)*cos(t.yphi),
        t.yr*sin(t.ytheta)*cos(t.yphi),
        t.yr*sin(t.yphi),
        t.cr*sin(t.ctheta)*cos(t.cphi),
        t.zr*cos(t.ztheta)*cos(t.zphi),
        t.zr*sin(t.ztheta)*cos(t.zphi),
        t.zr*sin(t.zphi),
        t.cr*sin(t.zphi)
    )
let invertPolar (t:Affine) =
    {   xr = 1.0f/t.xr;
        yr = 1.0f/t.yr;
        zr = 1.0f/t.zr;
        cr = -t.cr;
        xtheta = -t.xtheta;
        ytheta = -t.ytheta;
        ztheta = -t.ztheta;
        ctheta = t.ctheta;
        xphi = -t.xphi;
        yphi = -t.yphi;
        zphi = -t.zphi;
        cphi = t.cphi}*)

let invertAffine (t:Affine) =
    let a,b,c,d,e,f,g,h,i,j,k,l = t.a,t.b,t.c,t.d,t.e,t.f,t.g,t.h,t.i,t.j,t.k,t.l
    let det = a*(f*k-g*j)-b*(e*k-g*i)+c*(e*j-f*i)
    {   a=  (f*k-g*j)/det;
        b= -(b*k-c*j)/det;
        c=  (b*g-c*f)/det;
        d= -d;
        e= -(e*k-g*i)/det;
        f=  (a*k-b*i)/det;
        g= -(a*g-c*e)/det;
        h= -h;
        i=  (e*j-f*i)/det;
        j= -(a*j-b*i)/det;
        k=  (a*f-b*e)/det;
        l= -l;   }
        

let flattenFlame flame =
    let output = Array.create flame.size 0.0f
    for node in flame.xforms do
        let xform = node.self
        let t = xform.coefs
        let a,b,c,d,e,f,g,h,i,j,k,l = t.a,t.b,t.c,t.d,t.e,t.f,t.g,t.h,t.i,t.j,t.k,t.l
        output.[xform.offset]    <- a
        output.[xform.offset+1]  <- b
        output.[xform.offset+2]  <- c
        output.[xform.offset+3]  <- d
        output.[xform.offset+4]  <- e
        output.[xform.offset+5]  <- f
        output.[xform.offset+6]  <- g
        output.[xform.offset+7]  <- h
        output.[xform.offset+8]  <- i
        output.[xform.offset+9]  <- j
        output.[xform.offset+10] <- k
        output.[xform.offset+11] <- l
        output.[xform.offset+12] <- xform.colorIndex
        output.[xform.offset+13] <- xform.colorSpeed
        let mutable varOffset = xform.offset+14
        for var in xform.vars do
            output.[varOffset] <- var.weight
            varOffset <- varOffset+1
            for param in var.parameters do
                output.[varOffset] <- param
                varOffset <- varOffset+1
        for link in node.next do
            output.[link.offset] <- link.weight
            output.[link.offset+1] <- !*->*.* link.state
            output.[link.offset+2] <- link.opacity
            output.[link.offset+3] <- !*->*.* link.discard
    let t = flame.viewport.minimumCoverage |> invertAffine
    let a,b,c,d,e,f,g,h,i,j,k,l = t.a,t.b,t.c,t.d,t.e,t.f,t.g,t.h,t.i,t.j,t.k,t.l
    output.[flame.size-12] <- a
    output.[flame.size-11] <- b
    output.[flame.size-10] <- c
    output.[flame.size-9]  <- d
    output.[flame.size-8]  <- e
    output.[flame.size-7]  <- f
    output.[flame.size-6]  <- g
    output.[flame.size-5]  <- h
    output.[flame.size-4]  <- i
    output.[flame.size-3]  <- j
    output.[flame.size-2]  <- k
    output.[flame.size-1]  <- l
    output