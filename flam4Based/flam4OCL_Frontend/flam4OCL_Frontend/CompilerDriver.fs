﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module CompilerDriver

open System
open System.IO
open System.IO.Pipes
open System.Text
open System.Diagnostics

type CompilerService (pipe:NamedPipeServerStream)= 
    let pipe = pipe
    let readMessage() =
        async {
                let! length = (pipe:>Stream).AsyncRead(sizeof<int>)
                let buf = Array.create(BitConverter.ToInt32 (length,0)) 0uy
                let! msg = (pipe:>Stream).AsyncRead(buf)
                return buf
            } |> Async.RunSynchronously
    let writeMessage (msg:byte[])=
        let lengthBuf = BitConverter.GetBytes (msg.Length)
        pipe.Write (Array.append lengthBuf msg,0,msg.Length+sizeof<int>)

    new(deviceName) =
        //ensure a unique pipe name
        let pipeName = Guid.NewGuid().ToString()
        let pipe = new NamedPipeServerStream(pipeName,PipeDirection.InOut,1,PipeTransmissionMode.Message)
        let compilerProcess = new Process()
        let startInfo = new ProcessStartInfo()
        startInfo.FileName <- @".\Flam4CompileService.exe"
        startInfo.Arguments <- pipeName + " " + "\"" + deviceName + "\""
        startInfo.UseShellExecute <- false
        compilerProcess.StartInfo <- startInfo
        compilerProcess.Start() |> ignore
        pipe.WaitForConnection()
        new CompilerService(pipe)

    interface System.IDisposable with
        member s.Dispose() =
            writeMessage <| Encoding.UTF8.GetBytes("END")
            pipe.WaitForPipeDrain()
            pipe.Close()
            pipe.Dispose()

    member s.Compile (kernel:string) =
        let buf = Encoding.UTF8.GetBytes(kernel)
        writeMessage buf
        pipe.WaitForPipeDrain()
        readMessage()
