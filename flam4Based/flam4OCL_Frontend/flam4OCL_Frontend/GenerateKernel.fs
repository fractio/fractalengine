﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module GenerateKernel

open FlameData
open FlameMarshal
open Helper
open System.Text.RegularExpressions
open Cloo

type DecisionTree =
    | Branch of float32*DecisionTree*DecisionTree
    | Leaf of XformNodeLink

let generateIterateKernel flame (device:ComputeDevice) numPages=
        let codeRepo = flame.xformNodeRepo
        let xformRepo = flame.xformRepo
        let varRepo = flame.varRepo

        let iterateCode = @"@pragmas@
#define PI 3.1415926536f

constant int shift1[4] = {6, 2, 13, 3};
constant int shift2[4] = {13, 27, 21, 12};
constant int shift3[4] = {18, 2, 7, 13};
constant unsigned int offset[4] = {4294967294, 4294967288, 4294967280, 4294967168};

@paletteSampler@

unsigned int TausStep(unsigned int z, int s1, int s2, int s3, unsigned int M)
{
    unsigned int b = ((z << s1) ^ z) >> s2;
    return (((z & M) << s3) ^b);
}

unsigned int RAND_INT(local volatile unsigned int* randStates)
{
    unsigned int index = get_local_id(0);
    unsigned int i2 = get_local_id(0)&~(WARP_SIZE-1);
    randStates[index] = TausStep(randStates[index], shift1[index&3], shift2[index&3], shift3[index&3], offset[index&3]);
    return (randStates[index&(WARP_SIZE-1)+i2]^randStates[(index+1)&(WARP_SIZE-1)+i2]^randStates[(index+2)&(WARP_SIZE-1)+i2]^randStates[(index+3)&(WARP_SIZE-1)+i2]);
}

#define randInt(void) RAND_INT(randStates)

unsigned int RAND_INT_WARP(local volatile unsigned int* randStates)
{
    unsigned int index = get_local_id(0);
    unsigned int i2 = get_local_id(0)&~(WARP_SIZE-1);
    randStates[index] = TausStep(randStates[index], shift1[index&3], shift2[index&3], shift3[index&3], offset[index&3]);
    return (randStates[i2]^randStates[i2+1]^randStates[i2+2]^randStates[i2+3]);
}

#define randIntWarp(void) RAND_INT_WARP(randStates)

float RAND_FLOAT(local volatile unsigned int* randStates)
{
    unsigned int y = randInt();
    return as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

#define randFloat(void) RAND_FLOAT(randStates)

float RAND_FLOAT_WARP(local volatile unsigned int* randStates)
{
    unsigned int y = randIntWarp();
    return as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

#define randFloatWarp(void) RAND_FLOAT_WARP(randStates)

#ifdef NVIDIA
float4 loadPoint(int nodeIndex, local volatile unsigned int * randStates,local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointOffset[index] = 4*(nodeIndex*NUM_POINTS_PER_NODE+randInt()%NUM_POINTS_PER_NODE);
    for (int n = (index&~3); n < ((index&~3)+4);n++)
    {
        pointStage[n*4+(index&3)]=pointPool[pointOffset[n]+(index&3)];
    }
    return (float4)(pointStage[index*4],pointStage[index*4+1],pointStage[index*4+2],pointStage[index*4+3]);
}
#else
float4 loadPoint(int nodeIndex, local volatile unsigned int * randStates,local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointOffset[index] = 4*(nodeIndex*NUM_POINTS_PER_NODE+randInt()%NUM_POINTS_PER_NODE);
    global float4 * pointSrc = (global float4 *) pointPool;
    return pointSrc[pointOffset[index]/4];
}
#endif

@pointXchg@

@rasterizePoint@

void drawPoint(global volatile float4 ** renderBuffers, @palette@, int2 dim, float4 point, local volatile unsigned int* randStates, float opacity, constant float* flameBuffer, int palLength,local int * pageLookupTable, int numPages,int pageSize)
{
    int4 coords = rasterizePoint(point, dim, randStates,flameBuffer,pageLookupTable,numPages);
    if ((coords.y >= 0.0f)&&(coords.y < dim.y)&&(coords.z != -1))
    {
        float4 color = opacity*@readPalette@;
        renderBuffers[coords.z][coords.y*(int)ceil(((float)dim.x)/((float)numPages))+coords.x]+=color;
    }
}

@var@
@nextXform@

kernel void renderBatch(global unsigned int* randSeeds, global unsigned int* discardStates, global volatile float *pointPool, global int* stateStacks, global int* oldStateIndices, global int* pageLookups, constant float* flameBuffer, @palette@, int xdim, int ydim, int palLength, int numPages, int pageSize,@pageBuffers@)
{   
    global volatile float4 * renderBuffers[@pageCount@];@assignPageBufferPointers@
    int2 dimension = (int2)(xdim,ydim);
    local volatile unsigned int randStates[BLOCK_SIZE];
    randStates[get_local_id(0)]=randSeeds[get_global_id(1)*get_global_size(0)+get_global_id(0)];
    local volatile unsigned int pointOffset[BLOCK_SIZE];
    local volatile float pointStage[BLOCK_SIZE*4];
    local int stateStack[BLOCK_SIZE];
    local int pageLookupTable[PAGE_LOOKUP_TABLE_LENGTH];
    const int warpOffset = get_local_id(0)&~(WARP_SIZE-1);
    float4 oldPoint;
    int dstate = 0;
    float rnd = 0.0f;
    float4 point;
    float colIndex;
    float opacity;
    for (int n = 0; n < PAGE_LOOKUP_TABLE_LENGTH; n += get_global_size(0))
    {
        int i = n+get_local_id(0);
        if (i < PAGE_LOOKUP_TABLE_LENGTH)
            pageLookupTable[i] = pageLookups[i];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    unsigned int discard = discardStates[(get_global_id(1)*get_global_size(0)+get_global_id(0))>>5];
    unsigned int iterCount;
    stateStack[get_local_id(0)] = stateStacks[get_global_id(1)*get_global_size(0)+get_global_id(0)];
    unsigned int state=oldStateIndices[(get_global_id(1)*get_global_size(0)+get_global_id(0))>>5];
    unsigned int nextState=0;
    rnd = randFloatWarp();
    switch (stateStack[state+warpOffset])
    {
    @branch@
    }
    for (int iterations=0; iterations<MAX_ITERATIONS; iterations++)
    {
        switch (nextState)
        {
        @nextNode@
        }
    }
    randSeeds[get_global_id(1)*get_global_size(0)+get_global_id(0)]=randStates[get_local_id(0)];
    stateStacks[get_global_id(1)*get_global_size(0)+get_global_id(0)]=stateStack[get_local_id(0)];
    oldStateIndices[(get_global_id(1)*get_global_size(0)+get_global_id(0))/WARP_SIZE] = state;
    discardStates[(get_global_id(1)*get_global_size(0)+get_global_id(0))/WARP_SIZE] = discard;
}"
        let selectXformWrapper = "if (rnd <= (float)@weight@)
@TAB@{
@TAB@\t@left@
@TAB@}
@TAB@else
@TAB@{
@TAB\t@right@
@TAB@}"


        let xformNodeWrapper = @"case @nodeIndex@:
        {
            if (discard != 0)
                @loadPoint@
            iterCount = as_int(oldPoint.w)&0xFF800000;
            colIndex = as_float((as_int(oldPoint.w)&0x007FFFFF)|0x3F800000) -1.0f;
            oldPoint.w=colIndex;
            point = @xform@(oldPoint, randStates, flameBuffer);
            if (iterCount > (100<<23))
            {
                if (opacity!=0.0f)
                    drawPoint(renderBuffers,palette,dimension,point,randStates,opacity,flameBuffer,palLength,pageLookupTable,numPages,pageSize);
            }
            else
                iterCount += 1<<23;
            if (isfinite(point.x+point.y+point.z)==0)
            {
                point.x=randFloat();
                point.y=randFloat();
                point.z=randFloat();
                point.w=randFloat();
                iterCount=85<<23;
            }
            point.w=as_float(iterCount|(as_int(point.w+1.0f)&0x007FFFFF));
            stateStack[state+warpOffset]=@nodeIndex@;
            @loadPoint@
            @storePoint@
            rnd = randFloatWarp();
            @continue@
        }break;

    @nextNode@"

        let branchWrapper = "if (rnd <= (float)@weight@)
@TAB@{
@TAB@\t@left@
@TAB@}
@TAB@else
@TAB@{
@TAB@\t@right@
@TAB@}"
        let tab = "\t"


        let switchCaseWrapper = "state += @state@;
@TAB@dstate = @state@;
@TAB@state &= (WARP_SIZE-1);
@TAB@opacity = @opacity@;
@TAB@discard = @discard@;
@TAB@nextState = @nodeIndex@;"

        let initialSwitchCaseWrapper = "case @nodeIndex@:
    {
        oldPoint = loadPoint(@nodeIndex@,randStates,pointOffset,pointStage,pointPool);
        @continue@
    }break;
    @branch@"

        let xformWrapper = @"
float4 @xform@(float4 point, local unsigned int * randStates, constant float* flameBuffer)
{
    float4 outpos = (float4)(0.0f,0.0f,0.0f,0.0f);
    float4 pos = point.xxxx*(float4)(@a@,@e@,@i@,0.0f)+point.yyyy*(float4)(@b@,@f@,@j@,0.0f)+point.zzzz*(float4)(@c@,@g@,@k@,0.0f)+(float4)(@d@,@h@,@l@,0.0f);
    float r2D2 = pos.x*pos.x+pos.y*pos.y;
    float r2 = pos.x*pos.x+pos.y*pos.y+pos.z*pos.z;
    float r2D = sqrt(r2D2);
    float r = sqrt(r2);
    float theta = atan2(pos.y,pos.x);
    float phi = atan2(pos.z,r2D);
    float psi = atan2(pos.x,pos.y);
    @var@
    float col = mix(point.w,@colIndex@,@colSpeed@);
    return (float4)(outpos.x,outpos.y,outpos.z,col);
}
@nextXform@"

        let varWrapper = @"outpos+=((float)@weight@)*@insideVar@(pos@varParam@, @weight@, r2, r, r2D2, r2D, theta, phi, psi, randStates, flameBuffer,@offset@);
    @var@"
        let varCodeWrapper = @"float4 @insideVar@(float4 pos@varParam@, float weight, float r2, float r, float r2D2, float r2D, float theta, float phi, float psi, local unsigned int * randStates, constant float* flameBuffer,int offset)
{
    float xout,yout,zout=0.0f;
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    float a = flameBuffer[offset];
    float b = flameBuffer[offset+1];
    float c = flameBuffer[offset+2];
    float d = flameBuffer[offset+3];
    float e = flameBuffer[offset+4];
    float f = flameBuffer[offset+5];
    float g = flameBuffer[offset+6];
    float h = flameBuffer[offset+7];
    float i = flameBuffer[offset+8];
    float j = flameBuffer[offset+9];
    float k = flameBuffer[offset+10];
    float l = flameBuffer[offset+11];
    zout=z;
    @varCode@
    return (float4) (xout,yout,zout,0.0f);
}

@var@"
        let storeCode,loadCode,exchangePoints,pragmas = (   @"storePoint(point,pointOffset,pointStage,pointPool);",
                                                            @"oldPoint = loadPoint(stateStack[state+warpOffset],randStates,pointOffset,pointStage,pointPool);",
                                                            @"#ifdef NVIDIA
void storePoint(float4 point, local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    pointStage[index*4] = point.x;
    pointStage[index*4+1]=point.y;
    pointStage[index*4+2]=point.z;
    pointStage[index*4+3]=point.w;
    for (int n = (index&~3); n < ((index&~3)+4); n++)
    {
        pointPool[pointOffset[n]+(index&3)]=pointStage[n*4+(index&3)];
    }
}
#else
void storePoint(float4 point, local volatile unsigned int * pointOffset,local volatile float * pointStage, global volatile float* pointPool)
{
    unsigned int index = get_local_id(0);
    global float4 * pointDest = (global float4 *) pointPool;
    pointDest[pointOffset[index]/4] = point;
}
#endif",
                                                                @"#pragma OPENCL_EXTENSION cl_khr_fp64 : disable" )
        //Not 64bit safe for very large GPU memory!
        let rasterizePoint = "int4 rasterizePoint(float4 point, int2 dim, local volatile unsigned int* randStates, constant float* flameBuffer, local int* pageLookupTable, int numPages)
{
    point = point+(float4)(@d@,@h@,@l@,0.0f);
    float4 pos = point.xxxx*(float4)(@a@,@e@,@i@,0.0f)+point.yyyy*(float4)(@b@,@f@,@j@,0.0f)+point.zzzz*(float4)(@c@,@g@,@k@,0.0f);
    float xpos = /*@numSlices*pixelAdjust@*/((float)numPages)*(0.5f+0.5f*pos.x);
    float slice = floor(xpos);
    int page = -1;
    if ((slice < numPages) && (slice >=0))
        page = pageLookupTable[(int) slice];
    float xd = (xpos-slice)*ceil(((float)dim.x)/((float)numPages));
    float yd = ((0.5f+0.5f*pos.y))*(float)dim.y;
    return (int4)((int)xd,(int)yd,page,0);
}"

        let readPalette =   if device.ImageSupport = true then
                                @"read_imagef(palette, paletteSampler, (float2)(point.w,0.0f))"
                            else
                                @"read_image(palette,palLength,point.w)"
        let paletteSampler =    if device.ImageSupport = true then
                                    @"const sampler_t paletteSampler = CLK_NORMALIZED_COORDS_TRUE | CLK_FILTER_LINEAR;"
                                else
                                    @"float4 read_image(global float * image, int length, float index)
{
    float clampedIndex = index - floor(index);
    float scaledIndex = clampedIndex*(float)length;
    int iLow = floor(scaledIndex);
    int iHigh = ceil(scaledIndex);
    float iFract = scaledIndex - floor(scaledIndex);
    float4 c0 = ((global float4 *) image)[iLow];
    float4 c1 = ((global float4 *) image)[iHigh];
    return iFract*c1+(1.0f-iFract)*c0;
}"
        let palette =   if device.ImageSupport=true then
                            @"read_only image2d_t palette"
                        else
                            @"global float* palette"
            

        let labelRX = new Regex "@label@"
        let weightRX = new Regex "@weight@"
        let branchRX = new Regex "@branch@"
        let tabRX = new Regex "@TAB@"
        let leftRX = new Regex "@left@"
        let rightRX = new Regex "@right@"
        let nodeIndexRX = new Regex "@nodeIndex@"
        let opacityRX = new Regex "@opacity@"
        let stateRX = new Regex "@state@"
        let discardRX = new Regex "@discard@"
        let xformRX = new Regex "@xform@"
        let nextXformRX = new Regex "@nextXform@"
        let nextNodeRX = new Regex "@nextNode@"
        let varRX = new Regex "@var@"
        let insideVarRX = new Regex "@insideVar@"
        let varCodeRX = new Regex "@varCode@"
        let colIndexRX = new Regex "@colIndex@"
        let colSpeedRX = new Regex "@colSpeed@"
        let varParamRX = new Regex "@varParam@"
        let pointXchgRX = new Regex "@pointXchg@"
        let storePointRX = new Regex "@storePoint@"
        let loadPointRX = new Regex "@loadPoint@"
        let numNodesRX = new Regex "@numNodes@"
        let pragmasRX = new Regex "@pragmas@"
        let rasterizePointRX = new Regex "@rasterizePoint@"
        let selectXformRX = new Regex "@selectXform@"
        let offsetRX = new Regex "@offset@"
        let continueRX = new Regex "@continue@"
        let readPaletteRX = new Regex "@readPalette@"
        let paletteSamplerRX = new Regex "@paletteSampler@"
        let paletteRX = new Regex "@palette@"
        let numSlices_pixelAdjustRX = new Regex "@numSlices*pixelAdjust@"
        let pageBuffersRX = new Regex "@pageBuffers@"
        let pageCountRX = new Regex "@pageCount@"
        let assignPageBufferPointersRX = new Regex "@assignPageBufferPointers@"
        let aRX = new Regex "@a@"
        let bRX = new Regex "@b@"
        let cRX = new Regex "@c@"
        let dRX = new Regex "@d@"
        let eRX = new Regex "@e@"
        let fRX = new Regex "@f@"
        let gRX = new Regex "@g@"
        let hRX = new Regex "@h@"
        let iRX = new Regex "@i@"
        let jRX = new Regex "@j@"
        let kRX = new Regex "@k@"
        let lRX = new Regex "@l@"


        let fromFlameBuffer offset =
            "flameBuffer["+(string offset)+"]"
        let fromFlameBufferInt offset =
            "as_int(flameBuffer["+(string offset)+"])"

        let pageBufferDecls numPages=
            let rec pageBufferDecls numPages code index =
                if index = numPages then
                    code
                else
                    code + pageBufferDecls numPages (sprintf ",global volatile float * renderBuffer%i" index) (index+1)
            pageBufferDecls numPages "global volatile float * renderBuffer0" 1
        let pageBufferPointerAssignments numPages =
            let rec pageBufferPointerAssignments numPages code index =
                if index = numPages then
                    code
                else
                    code + pageBufferPointerAssignments numPages (sprintf "\n\trenderBuffers[%i] = (global volatile float4 *) renderBuffer%i;" index index) (index+1)
            pageBufferPointerAssignments numPages "" 0

        let buildDecisionTreeCode (xformNode:XformNodeLink list) (initial:bool) (code:string)=
            let getWeight decisionTree =
                match decisionTree with
                | Leaf(n) ->
                    n.weight
                | Branch(n,_,_) ->
                    n
            let buildDecisionTree (xformNode:XformNodeLink list)=
                let rec buildDecisionTree (decisionTree:DecisionTree list) =
                    match decisionTree with
                    | [_] ->
                        decisionTree
                    | _ ->
                        let sortedTree = decisionTree |> List.sortBy getWeight
                        buildDecisionTree (Branch(((getWeight sortedTree.Head) + (getWeight sortedTree.Tail.Head)),sortedTree.Head,sortedTree.Tail.Head)::sortedTree.Tail.Tail)
                let decisionTree = [for node in xformNode -> Leaf(node)]
                buildDecisionTree decisionTree

            let decisionTree = (buildDecisionTree xformNode).Head
            let rec buildDecisionTreeCode (decisionTree:DecisionTree) (code:string) (tabStop:int) (weight:float32):string=
                match decisionTree with
                | Branch(w,left,right) ->
                    let l = buildDecisionTreeCode left code (tabStop+1) weight
                    let r = buildDecisionTreeCode right code (tabStop+1) (weight+getWeight left)
                    ((((code, string (weight+(getWeight left))) |>
                        weightRX.Replace , l) |>
                        leftRX.Replace, r) |>
                        rightRX.Replace, (tab |> String.replicate(tabStop))) |>
                        tabRX.Replace
                | Leaf(nodeLink) ->
                    ((((((( if initial=true then initialSwitchCaseWrapper else switchCaseWrapper),
                            nodeLink.next.name) |> labelRX.Replace,
                            (tab |> String.replicate(tabStop))) |> tabRX.Replace,
                            fromFlameBufferInt  (nodeLink.offset+1)) |> stateRX.Replace,
                            fromFlameBufferInt  (nodeLink.offset+3)) |> discardRX.Replace,
                            fromFlameBuffer     (nodeLink.offset+2)) |> opacityRX.Replace,
                            string nodeLink.next.index) |> nodeIndexRX.Replace
            (code,(buildDecisionTreeCode decisionTree branchWrapper 1 0.0f))

        let rec buildInitialSwitchCode (codeSegs:string list) (code:string) =
            match codeSegs with
            | head::tail ->
                branchRX.Replace(code,codeSegs.Head) |> buildInitialSwitchCode codeSegs.Tail
            | _ ->
                branchRX.Replace(code,"")

        let rec buildXformNodeCode (codeSegs:string list) (code:string) =
            match codeSegs with
            | head::tail ->
                nextNodeRX.Replace(code,codeSegs.Head) |> buildXformNodeCode codeSegs.Tail
            | _ ->
                nextNodeRX.Replace(code, "")

        let rec buildVarParams (varParamVals:float32 list) (code:string) offset=
            match varParamVals with
            | head::tail ->
                (code,","+(fromFlameBuffer offset)+"@varParam@") |> varParamRX.Replace |> buildVarParams varParamVals.Tail <| offset+1
            | _ ->
                (code,"") |> varParamRX.Replace,offset

        let rec buildVarCalls (vars:Variation list) varOffset baseOffset (code:string)=
            match vars with
            | head::tail ->
                let vc,newOffset = buildVarParams (vars.Head.parameters) (( varWrapper,vars.Head.vardef.name) |>
                                    insideVarRX.Replace) (varOffset+1)
                let varCode = ((vc,fromFlameBuffer varOffset) |>
                                weightRX.Replace,
                                string baseOffset) |> offsetRX.Replace
                (code,varCode) |> varRX.Replace |> buildVarCalls (vars.Tail) newOffset baseOffset
            | _ ->
                (code,"") |> varRX.Replace

        let rec buildXformCode (codeSegs:string list) (code:string) =
            match codeSegs with
            | head::tail ->
                nextXformRX.Replace(code,codeSegs.Head) |> buildXformCode codeSegs.Tail
            | _ ->
                nextXformRX.Replace(code, "")

        let rec buildVarCode (vars:VariationDef list) (code:string) =
            let genVarCode (var:VariationDef) =
                let rec buildVarParams (parms:string list) (code:string) =
                    match parms with
                    | head::tail ->
                        (code,", float "+parms.Head+"@varParam@") |> varParamRX.Replace |> buildVarParams parms.Tail
                    | _ ->
                        (code,"") |> varParamRX.Replace
                ((  varCodeWrapper,var.code) |> 
                    varCodeRX.Replace,var.name) |>
                    insideVarRX.Replace |>
                    buildVarParams var.parameters
            match vars with
            | head::tail ->
                (code,genVarCode vars.Head) |> varRX.Replace |> buildVarCode vars.Tail
            | _ ->
                (code,"") |> varRX.Replace
        
        let initialXformNodeList = [for node in flame.xformNodeRepo.Values ->
                                        node] 
        let initialSwitchCaseCodeSegs = initialXformNodeList |> List.map (fun node -> (initialSwitchCaseWrapper,string node.index) |>
                                                                                       nodeIndexRX.Replace |>
                                                                                       buildDecisionTreeCode [for n in node.next -> n] false |> continueRX.Replace)
        let xformNodeCodeSegs = [   for n in codeRepo do
                                    yield
                                        (((((   xformNodeWrapper,n.Value.self.name) |> xformRX.Replace,
                                                string n.Value.index)               |> labelRX.Replace,
                                                storeCode)                          |> storePointRX.Replace,
                                                loadCode)                           |> loadPointRX.Replace,
                                                string n.Value.index)               |> nodeIndexRX.Replace |>
                                                buildDecisionTreeCode [for node in n.Value.next -> node] false |> continueRX.Replace]
        let xformCodeSegs = [   for n in xformRepo do
                                let t = n.Value.coefs
                                let a,b,c,d,e,f,g,h,i,j,k,l = (t.a,t.b,t.c,t.d,t.e,t.f,t.g,t.h,t.i,t.j,t.k,t.l)
                                let baseOffset = n.Value.offset
                                yield
                                    ((((((((((((((( xformWrapper,n.Value.name)        |> xformRX.Replace,
                                                    fromFlameBuffer <| baseOffset)    |> aRX.Replace,
                                                    fromFlameBuffer <| baseOffset+1)  |> bRX.Replace,
                                                    fromFlameBuffer <| baseOffset+2)  |> cRX.Replace,
                                                    fromFlameBuffer <| baseOffset+3)  |> dRX.Replace,
                                                    fromFlameBuffer <| baseOffset+4)  |> eRX.Replace,
                                                    fromFlameBuffer <| baseOffset+5)  |> fRX.Replace,
                                                    fromFlameBuffer <| baseOffset+6)  |> gRX.Replace,
                                                    fromFlameBuffer <| baseOffset+7)  |> hRX.Replace,
                                                    fromFlameBuffer <| baseOffset+8)  |> iRX.Replace,
                                                    fromFlameBuffer <| baseOffset+9)  |> jRX.Replace,
                                                    fromFlameBuffer <| baseOffset+10) |> kRX.Replace,
                                                    fromFlameBuffer <| baseOffset+11) |> lRX.Replace,
                                                    fromFlameBuffer <| baseOffset+12) |> colIndexRX.Replace,
                                                    fromFlameBuffer <| baseOffset+13) |> colSpeedRX.Replace |>
                                                    buildVarCalls n.Value.vars (baseOffset+14) baseOffset  ]
        let buildRasterizeCode = 
            let t = flame.viewport.minimumCoverage |> invertAffine
            let a,b,c,d,e,f,g,h,i,j,k,l = (t.a,t.b,t.c,t.d,t.e,t.f,t.g,t.h,t.i,t.j,t.k,t.l)
            let baseOffset = flame.size-12
            ((((((((((((    rasterizePoint,
                            fromFlameBuffer <| baseOffset)    |> aRX.Replace,
                            fromFlameBuffer <| baseOffset+1)  |> bRX.Replace,
                            fromFlameBuffer <| baseOffset+2)  |> cRX.Replace,
                            fromFlameBuffer <| baseOffset+3)  |> dRX.Replace,
                            fromFlameBuffer <| baseOffset+4)  |> eRX.Replace,
                            fromFlameBuffer <| baseOffset+5)  |> fRX.Replace,
                            fromFlameBuffer <| baseOffset+6)  |> gRX.Replace,
                            fromFlameBuffer <| baseOffset+7)  |> hRX.Replace,
                            fromFlameBuffer <| baseOffset+8)  |> iRX.Replace,
                            fromFlameBuffer <| baseOffset+9)  |> jRX.Replace,
                            fromFlameBuffer <| baseOffset+10) |> kRX.Replace,
                            fromFlameBuffer <| baseOffset+11) |> lRX.Replace
        let finalCode = ((((((((((  buildXformCode xformCodeSegs <| buildXformNodeCode xformNodeCodeSegs iterateCode |>
                                    buildVarCode  varRepo,exchangePoints) |>
                                    pointXchgRX.Replace, string flame.xformNodeRepo.Values.Count) |>
                                    numNodesRX.Replace, pragmas) |>
                                    pragmasRX.Replace, buildRasterizeCode) |>
                                    rasterizePointRX.Replace, readPalette) |>
                                    readPaletteRX.Replace, paletteSampler) |>
                                    paletteSamplerRX.Replace, palette) |>
                                    paletteRX.Replace |>
                                    buildInitialSwitchCode initialSwitchCaseCodeSegs, pageBufferDecls <| numPages) |>
                                    pageBuffersRX.Replace, string <| numPages) |>
                                    pageCountRX.Replace, pageBufferPointerAssignments <| numPages) |>
                                    assignPageBufferPointersRX.Replace

        finalCode