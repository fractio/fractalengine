﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.


namespace Flam4GUI
open System
open System.IO
open System.Windows
open System.Windows.Markup
open System.Windows.Input
open System.Windows.Controls
open System.Windows.Media
open System.Collections.ObjectModel
open System.ComponentModel


type MainWindow() as s= 
    inherit Window()
    
    let mutable _contentLoaded = false
    do s.InitializeComponent()

    member s.InitializeComponent()=
        if _contentLoaded = false then
            _contentLoaded <- true
            Global.Init()
            //Once the window is loaded, we can safely bind events to the controls
            s.Loaded.Add (fun _ ->
                let button = s.FindName("NewNodeButton"):?>Button
                button.Click.Add (fun _ -> 
                                    let target = s.FindName("NodeChart"):?>NodeCanvas
                                    let newNode = NodePanel.Create(target)
                                    target.Children.Add (newNode :> UIElement) |> ignore
                                    target.AddDescendent (newNode :> UIElement)
                                    ())
                let menuQuit = s.FindName("MenuQuit") :?> MenuItem
                menuQuit.Click.Add (fun _ -> s.Close() )
                )
            ()


    static member Create() =
        let fs = new FileStream(@".\Resources\MainWindow.xaml",FileMode.Open)
        System.Windows.Markup.XamlReader.Load(fs) :?> (MainWindow)



//We have to define all these in the same file since this is the only way F# allows for circular dependencies :-(

and NodePanel() as s=
    inherit UserControl()
    let mutable _contentLoaded = false
    do s.InitializeComponent()
    let mutable xPos = 0.0
    let mutable yPos = 0.0
    let mutable owner:NodeCanvas = null
    let mutable childBox = null

    member private s.oldPosX with get () = xPos
                             and set x = xPos <- x
    member private s.oldPosY with get () = yPos
                             and set x = yPos <- x
    member s.Owner  with internal get () = owner
                    and private set x = owner <- x
    member internal s.ChildBox  with get () = childBox
                                and set x = childBox <- x

    member s.InitializeComponent()=
        if _contentLoaded<>true then
            _contentLoaded <- true
            s.Loaded.Add (fun _ ->
                let X = s.FindName "CloseButton" :?> Button
                X.Click.Add (fun e -> 
                                (s.Parent :?> Panel).Children.Remove(s:>UIElement)
                                s.Owner.RemoveDescendent s
                                e.Handled <- true)
                let newXformButtonContainer = s.FindName "NewXformButtonContainer"
                let xformBox = s.FindName "XformComboBox" :?> ComboBox
                let newXformButton = s.FindName "NewXformButton" :?> Button
                let selector = s.FindName "XformSelector" :?> DockPanel
                let noneContainer = s.FindName "NoneContainer" :?> ComboBoxItem
                newXformButton.Click.Add (fun _-> 
                                            let newXform = Xform.Create "xform"
                                            Global.Library.XformLibrary.Add newXform
                                            xformBox.SelectedItem <- newXform)
                xformBox.SelectionChanged.Add (fun e ->
                    let lastSelection = match e.RemovedItems with
                                        | x when x.Count > 0 ->
                                            x.Item(0)
                                        | _ ->
                                            null
                    match xformBox.SelectedItem with
                    | selection when selection=newXformButtonContainer ->
                        xformBox.SelectedItem <- lastSelection
                    | selection when selection:?Xform=true ->
                        selector.Children.Clear()
                        selector.Children.Add  (XformPanel.Create selection) |> ignore
                    | selection when selection=null ->
                        xformBox.SelectedItem <- lastSelection
                    | _ ->
                        selector.Children.Clear()
                        xformBox.SelectedItem <-noneContainer
                    )
                let SelectedItemChangedDescriptor = DependencyPropertyDescriptor.FromProperty(ComboBox.TextProperty,typeof<ComboBox>)
                SelectedItemChangedDescriptor.AddValueChanged(xformBox,new EventHandler(fun sender e -> 
                                                                                            match xformBox.SelectedItem with
                                                                                            | xform when xform:?Xform=true ->
                                                                                                (xform:?>Xform).Name <- xformBox.Text
                                                                                            | _ -> ()))
                    
                let dragger = s.FindName "MoveThumb" :?> Shapes.Rectangle
                let deNaN x = if Double.IsNaN(x) then 0.0 else x
                dragger.MouseLeftButtonDown.Add (fun e->    e.MouseDevice.Capture(dragger) |> ignore
                                                            s.oldPosX <- e.GetPosition(s.Parent:?>UIElement).X
                                                            s.oldPosY <- e.GetPosition(s.Parent:?>UIElement).Y
                                                            e.Handled <- true
                                                            )
                dragger.MouseLeftButtonUp.Add (fun e -> e.MouseDevice.Capture(null) |> ignore
                                                        let targetPos,mousePos =
                                                            match s.Owner.GetChildBoxHit() with
                                                            | Some(hit:UIElement) ->
                                                                let target = (hit:?>NodePanel).ChildBox
                                                                let targetPos = Mouse.GetPosition(target)
                                                                let mousePos = e.GetPosition(s)
                                                                let hit = ((hit:?>NodePanel).ChildBox)
                                                                (s.Parent :?> Panel).Children.Remove(s:>UIElement)
                                                                (hit:Canvas).Children.Add s |> ignore
                                                                targetPos,mousePos
                                                            | None ->
                                                                let targetPos = Mouse.GetPosition(s.Owner)
                                                                let mousePos = e.GetPosition(s)
                                                                (s.Parent:?> Panel).Children.Remove(s)
                                                                s.Owner.Children.Add s |> ignore
                                                                targetPos,mousePos
                                                        let adjustedX = targetPos.X-mousePos.X
                                                        let adjustedY = targetPos.Y-mousePos.Y
                                                        Canvas.SetLeft(s,adjustedX)
                                                        Canvas.SetTop( s,adjustedY)
                                                        (s.Parent:?>UIElement).Measure(new Size())
                                                        e.Handled <- true
                                                        )
                dragger.MouseMove.Add ( fun e-> if e.LeftButton = MouseButtonState.Pressed then
                                                        let newX = e.GetPosition(s.Parent:?>UIElement).X
                                                        let newY = e.GetPosition(s.Parent:?>UIElement).Y
                                                        let x = newX-s.oldPosX + deNaN (Canvas.GetLeft s)
                                                        let y = newY-s.oldPosY + deNaN (Canvas.GetTop s)
                                                        s.oldPosX <- newX
                                                        s.oldPosY <- newY
                                                        Canvas.SetLeft(s,x)
                                                        Canvas.SetTop(s,y)
                                                        e.Handled <- true
                                                    )
                s.ChildBox <- s.FindName "ChildrenBox" :?> Canvas
                //selector.Children.Add (XformPanel.Create()) |> ignore
                ()
                )
    member s.ChildBoxHitTest() =
        let s = (childBox:?>ResizingCanvas).IsMouseOver
        s

    static member Create(owner) =
        let fs = new FileStream(@".\Resources\NodePanel.xaml",FileMode.Open,FileAccess.Read)
        let panel = System.Windows.Markup.XamlReader.Load(fs) :?> (NodePanel)
        panel.Owner <- owner
        panel

and XformPanel() as s=
    inherit UserControl()
    let mutable _contentLoaded = false
    static let XformProperty = DependencyProperty.Register("Xform",typeof<Xform>,typeof<XformPanel>)
    do s.InitializeComponent()
    member s.InitializeComponent()=
        if _contentLoaded<>true then
            _contentLoaded <- true
            s.Loaded.Add (fun _ ->
                let manageVarsButton = s.FindName ("ManageVars") :?> Button
                let snapInPanel = s.FindName ("SnapIn") :?> DockPanel
                manageVarsButton.Click.Add (fun _ ->
                    snapInPanel.Children.Add (VarManager.Create()) |> ignore)
                let addButton = s.FindName "AddButton" :?> Button
                let removeButton = s.FindName "RemoveButton" :?> Button
                let varLibraryPanel = s.FindName "VarLibrary" :?> ListBox
                let xformVarsPanel = s.FindName "XformVars" :?> ListBox
                addButton.Click.Add (fun _ ->
                    let selectedItem = Variation.Create(varLibraryPanel.SelectedItem:?>VariationDef)
                    xformVarsPanel.Items.Add selectedItem |> ignore
                    )
                removeButton.Click.Add (fun _ ->
                    let selectedItem = xformVarsPanel.SelectedItem
                    xformVarsPanel.Items.Remove selectedItem
                    )
                )
                
        ()
    member public s.Xform   with get() = s.GetValue(XformProperty):?>Xform
                            and set x = s.SetValue(XformProperty,x)

    static member Create(xform) =
        let fs = new FileStream(@".\Resources\XformPanel.xaml",FileMode.Open,FileAccess.Read)
        let panel = System.Windows.Markup.XamlReader.Load(fs) :?> (XformPanel)
        panel.Xform <- xform
        panel

and VarManager() as s = 
    inherit UserControl()
    let mutable _contentLoaded=false
    do s.InitializeComponent()
    member s.InitializeComponent()=
        if _contentLoaded<>true then
            _contentLoaded <- true
            s.Loaded.Add (fun _ ->
                let importButton = s.FindName("ImportButton"):?>Button
                importButton.Click.Add(fun _ ->
                    let dlg = new Microsoft.Win32.OpenFileDialog()
                    dlg.DefaultExt <- ".code"
                    dlg.Filter <- "Code library (.code)|*.code|Import from flame (.flam4)|*.flam4|XML file (.xml)|*.xml"
                    match dlg.ShowDialog() with
                    | result when result.HasValue=true ->
                        if result.Value=true then
                            System.IO.File.Open (dlg.FileName,System.IO.FileMode.Open)
                            |> LoadFlam4.OpenLibrary
                            |> List.iter (fun varDef ->
                                            let newVar = new VariationDef()
                                            newVar.Name <- varDef.name
                                            newVar.Params <- new ObservableCollection<string>()
                                            List.iter (fun param -> 
                                                        newVar.Params.Add param
                                                     ) varDef.parameters
                                            newVar.Code <- varDef.code
                                            Global.Library.VarLibrary.Add newVar
                                        )
                    | _ -> ()
                    )
                let closeButton = s.FindName("CloseButton"):?>Button
                closeButton.Click.Add(fun _ ->
                    match s.Parent with
                    | parent when parent:?Window=true ->
                        (parent:?>Window).Close()
                    | parent when parent:?Panel=true ->
                        (parent:?>Panel).Children.Clear()
                    | _ ->
                        failwith("VarManager caught with unknown parentage!")
                    )

                )
        ()
    static member Create() =
        let fs = new FileStream(@".\Resources\VarManager.xaml",FileMode.Open,FileAccess.Read)
        let panel = System.Windows.Markup.XamlReader.Load(fs) :?> (VarManager)
        panel


and [<AllowNullLiteral>] NodeCanvas() =
    inherit ResizingCanvas()

    let mutable descendents = new System.Collections.Generic.HashSet<_>()
    
    member s.AddDescendent newChild =
        descendents.Add newChild |> ignore

    member s.RemoveDescendent oldChild =
        descendents.Remove oldChild |> ignore

    member s.GetChildBoxHit() =
        let result = ref None
        let pos = Mouse.GetPosition s
        let parameters = new PointHitTestParameters (pos)
        let hitTestResultFilter (canidate:HitTestResult)=
            match canidate.VisualHit with
            | x when (x:?NodeCanvas) ->
                HitTestResultBehavior.Continue
            | x when (x:?ResizingCanvas) ->
                result := Some(((((x:?>ResizingCanvas).Parent:?>GroupBox).Parent:?>StackPanel).Parent:?>Grid).Parent:?>UIElement)
                HitTestResultBehavior.Stop
            | _ ->
                HitTestResultBehavior.Continue
        VisualTreeHelper.HitTest(s,null,hitTestResultFilter,parameters)
        !result



and [<AllowNullLiteral>] ResizingCanvas() as s=
    inherit Canvas()
    let mutable _contentLoaded = false
    do s.InitializeComponent()
    member s.InitializeComponent()=
        if _contentLoaded<>true then
            _contentLoaded <- true
        s.Loaded.Add(fun _ ->
            s.MouseWheel.Add(fun e ->
                let pos = e.GetPosition s
                let mat = s.LayoutTransform.Value
                let scale =
                    let d = (float e.Delta)/120.
                    Math.Pow(1.1,d)
                mat.ScaleAt(scale,scale,pos.X,pos.Y)
                s.LayoutTransform <- new MatrixTransform(mat)
                e.Handled <- true
                )
            ())
    override s.MeasureOverride(sizeConstraint) =
        let mutable size = new Size()
        let mutable xOff = Double.MaxValue
        let mutable yOff = Double.MaxValue
        let clip x = if Double.IsNaN(x) then 0.0 else x
        for element in s.Children do
            let left = clip <| Canvas.GetLeft element
            let top = clip <| Canvas.GetTop element
            element.Measure sizeConstraint
            let desiredSize = element.DesiredSize
            size.Width <- Math.Max(size.Width,left + clip ( desiredSize.Width))
            size.Height <- Math.Max(size.Height, top + clip (desiredSize.Height))
            xOff <- Math.Min(xOff, left)
            yOff <- Math.Min(yOff, top)
        for element in s.Children do
            let x,y = clip <| Canvas.GetLeft element,clip <| Canvas.GetTop element
            Canvas.SetLeft (element,x-xOff+10.0)
            Canvas.SetTop (element, y-yOff+10.0)
        size.Width <- size.Width-Math.Min(xOff,size.Width)+20.0
        size.Height <- size.Height-Math.Min(yOff,size.Height)+20.0
        size