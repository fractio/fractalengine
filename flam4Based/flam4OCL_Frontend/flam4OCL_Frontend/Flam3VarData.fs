﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Flam3VarData

open System.Collections.Generic

let getFlam3Alias name =
    let nameAliases=new Dictionary<string,string>()
                    |> (fun d x->List.iter (fun x->d.Add x) x; d) <|
                    [
                        ("exp",                 "v_exp")
                        ("log",                 "v_log")
                        ("sin",                 "v_sin")
                        ("cos",                 "v_cos")
                        ("tan",                 "v_tan")
                        ("sec",                 "v_sec")
                        ("csc",                 "v_csc")
                        ("cot",                 "v_cot")
                        ("sinh",                "v_sinh")
                        ("cosh",                "v_cosh")
                        ("tanh",                "v_tanh")
                        ("sech",                "v_sech")
                        ("csch",                "v_csch")
                        ("coth",                "v_coth")
                        ("cross",               "v_cross")
                    ]
    let found,newName = nameAliases.TryGetValue(name)
    if found then
        newName
    else
        name

let deAliasParameterNames name =
    let nameDeAliases=new Dictionary<string,string list>()
                        |>(fun d x -> List.iter (fun x->d.Add x) x; d) <|
                        [
                            ("oscilloscope_separation", ["oscope_separation"])
                            ("oscilloscope_frequency",  ["oscope_frequency"])
                            ("oscilloscope_amplitude",  ["oscope_amplitude"])
                            ("oscilloscope_damping",    ["oscope_damping"])
                        ]
    let found,newNames = nameDeAliases.TryGetValue name
    if found then
        name::newNames
    else
        [name]

let flam3Variations=new Dictionary<string,string list>()
                    |> (fun d x->List.iter (fun x-> d.Add x) x; d) <| 
                    [
                    //flam3 2.8 vars
                        ("linear",      [])
                        ("sinusoidal",  [])
                        ("spherical",   [])
                        ("swirl",       [])
                        ("horseshoe",   [])
                        ("polar",       [])
                        ("handkerchief",[])
                        ("heart",       [])
                        ("disc",        [])
                        ("spiral",      [])
                        ("hyperbolic",  [])
                        ("diamond",     [])
                        ("ex",          [])
                        ("julia",       [])
                        ("bent",        [])
                        ("waves",       [])
                        ("fisheye",     [])
                        ("popcorn",     [])
                        ("exponential", [])
                        ("power",       [])
                        ("cosine",      [])
                        ("rings",       [])
                        ("fan",         [])
                        ("blob",        ["blob_high";"blob_low";"blob_waves"])
                        ("pdj",         ["pdj_a";"pdj_b";"pdj_c";"pdj_d"])
                        ("fan2",        ["fan2_x";"fan2_y"])
                        ("rings2",      ["rings2_val"])
                        ("eyefish",     [])
                        ("bubble",      [])
                        ("cylinder",    [])
                        ("perspective", ["perspective_angle";"perspective_dist"])
                        ("noise",       [])
                        ("julian",      ["julian_power";"julian_dist"])
                        ("juliascope",  ["juliascope_power";"juliascope_dist"])
                        ("blur",        [])
                        ("gaussian_blur",[])
                        ("radial_blur", ["radial_blur_angle"])
                        ("pie",         ["pie_slices";"pie_rotation";"pie_thickness"])
                        ("ngon",        ["ngon_power";"ngon_sides";"ngon_corners";"ngon_circle"])
                        ("curl",        ["curl_c1";"curl_c2"])
                        ("rectangles",  ["rectangles_x";"rectangles_y"])
                        ("arch",        [])
                        ("tangent",     [])
                        ("square",      [])
                        ("rays",        [])
                        ("blade",       [])
                        ("secant",      [])
                        ("twintrian",   [])
                        ("v_cross",     [])
                        ("disc2",       ["disc2_rot";"disc2_twist"])
                        ("super_shape", ["super_shape_m";"super_shape_n1";"super_shape_n2";"super_shape_n3";"super_shape_holes";"super_shape_rnd"])
                        ("flower",      ["flower_holes";"flower_petals"])
                        ("conic",       ["conic_holes";"conic_eccentricity"])
                        ("parabola",    ["parabola_height";"parabola_width"])
                        ("bent2",       ["bent2_x";"bent2_y"])
                        ("bipolar",     ["bipolar_shift"])
                        ("boarders",    [])
                        ("butterfly",   [])
                        ("cell",        ["cell_size"])
                        ("cpow",        ["cpow_r";"cpow_i";"cpow_power"])
                        ("curve",       ["curve_xamp";"curve_yamp";"curve_xlength";"curve_ylength"])
                        ("edisc",       [])
                        ("elliptic",    [])
                        ("escher",      ["escher_beta"])
                        ("foci",        [])
                        ("lazysusan",   ["lazysusan_space";"lazysusan_twist";"lazysusan_spin";"lazysusan_x";"lazysusan_y"])
                        ("loonie",      [])
                        ("modulus",     ["modulus_x";"modulus_y"])
                        ("oscilloscope",["oscilloscope_separation";"oscilloscope_frequency";"oscilloscope_amplitude";"oscilloscope_damping"])
                        ("polar2",      [])
                        ("popcorn2",    ["popcorn2_c";"popcorn2_x";"popcorn2_y"])
                        ("scry",        [])
                        ("separation",  ["separation_x";"separation_xinside";"separation_y";"separation_yinside"])
                        ("split",       ["split_xsize";"split_ysize"])
                        ("splits",      ["splits_x";"splits_y"])
                        ("stripes",     ["stripes_space";"stripes_warp"])
                        ("wedge",       ["wedge_angle";"wedge_hole";"wedge_count";"wedge_swirl"])
                        ("wedge_julia", ["wedge_julia_power";"wedge_julia_dist";"wedge_julia_count";"wedge_julia_angle"])
                        ("wedge_sph",   ["wedge_sph_angle";"wedge_sph_hole";"wedge_sph_count";"wedge_sph_swirl"])
                        ("whorl",       ["whorl_inside";"whorl_outside"])
                        ("waves2",      ["waves2_scalex";"waves2_scaley";"waves2_freqx";"waves2_freqy"])
                        ("v_exp",       [])
                        ("v_log",       [])
                        ("v_sin",       [])
                        ("v_cos",       [])
                        ("v_tan",       [])
                        ("v_sec",       [])
                        ("v_csc",       [])
                        ("v_cot",       [])
                        ("v_sinh",      [])
                        ("v_cosh",      [])
                        ("v_tanh",      [])
                        ("v_sech",      [])
                        ("v_csch",      [])
                        ("v_coth",      [])
                        ("auger",       ["auger_weight";"auger_freq";"auger_scale";"auger_sym"])
                    //3D 2.08 Hack vars
                        ("Linear3D",    ["Linear3D"])
                        ("sinusoidal3d",["sinusoidal3d"])
                        ("bubble3d",    ["bubble3d"])
                        ("cylinder3d",  ["cylinder3d"])
                        ("zscale",      ["zscale"])
                        ("ZCone",       ["ZCone"])
                        ("Spherical3D", ["Spherical3D"])
                        ("swirl3D",     ["swirl3D"])
                        ("horseshoe3D", ["horseshoe3D"; "horseshoe3D_sc";"horseshoe3D_fZsc"])
                        ("pdj3D",       ["pdj3D";       "pdj3D_A";"pdj3D_B";"pdj3D_C";"pdj3D_D";"pdj3D_E";"pdj3D_F"])
                        ("foci_3D",     ["foci_3D"])

                    //Need to be implemented
                        ("zblur",       ["zblur"])
                        ("blur3D",      ["blur3D"])
                        ("ztranslate",  ["ztranslate"])
                        ("zcone",       ["zcone"])
                        ("julia3D",     ["julia3D";     "julia3D_power"])
                        ("julia3Dz",    ["julia3Dz";    "julia3Dz_power"])
                        ("curl3D",      ["curl3D";      "curl3D_cx";"curl3D_cy";"curl3D_cz"])
                        ("hemisphere",  ["hemisphere"])
                    ]

let flam3PreVariations = new Dictionary<string,string list>()
                            |> (fun d x->List.iter (fun x-> d.Add x) x; d) <|
                            [
                            //flam3 2.8 pre_vars
                                ("pre_blur",        ["pre_blur"])
                            //3D Hack 2.08 pre_vars
                                ("pre_zscale",      ["pre_zscale"])
                                ("pre_ztranslate",  ["pre_ztranslate"])
                                ("pre_rotate_x",    ["pre_rotate_x"])
                                ("pre_rotate_y",    ["pre_rotate_y"])
                            ]

let flam3PostVariations = new Dictionary<string,string list>()
                            |> (fun d x->List.iter (fun x -> d.Add x) x; d) <|
                            [
                            //flam3 2.8 post_vars
                            //3D Hack 2.08 post_vars
                                ("post_rotate_x",   ["post_rotate_x"])
                                ("post_rotate_y",   ["post_rotate_y"])
                            ]