﻿


open System
open System.IO
open System.IO.Pipes
open System.Text
open System.Diagnostics
open System.Threading.Tasks

let ClearBufferKernal = @"
kernel void clearBuffer(global float4* buffer, int xdim, int ydim)
{
    int x = get_global_id(0);
    int y = get_global_id(1);
    if ((x < xdim) && (y < ydim))
    {
        buffer[y*xdim+x] = (float4)(0.0f,0.0f,0.0f,0.0f);
    }
}
"

let PostProcessKernel = @"
kernel void postProcess(global float4* buffer, int xDim, int yDim, float xDimension, float yDimension, float brightness, float gamma, float vibrancy,float numIters)
{
	int x = get_global_id(0);
    int y = get_global_id(1);
	if ((x < xDim)&&(y < yDim))
	{
		float k1 = brightness;
        float area = fabs(xDimension*yDimension);
		float k2 = ((float)(xDim*yDim))/(numIters*area);
		float4 rgba = buffer[y*xDim+x];
		float a = (k1* native_log(1.0f+k2*rgba.w));
		float ls = a/rgba.w;
		rgba.x = ls*rgba.x;
		rgba.y = ls*rgba.y;
		rgba.z = ls*rgba.z;
		float alpha = native_powr(a, 1.0f/gamma-1.0f);
		ls = vibrancy*alpha;
		rgba.x = ls*rgba.x+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.x),1.0f/gamma),rgba.x);
		rgba.y = ls*rgba.y+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.y),1.0f/gamma),rgba.y);
		rgba.z = ls*rgba.z+(1.0f-vibrancy)*copysign(native_powr(fabs(rgba.z),1.0f/gamma),rgba.z);
		alpha = native_powr(a, 1.0f/gamma);
		alpha = min(alpha,1.0f);
		if (isfinite(rgba.x))
		{
			buffer[y*xDim+x].x=rgba.x;
			buffer[y*xDim+x].y=rgba.y;
			buffer[y*xDim+x].z=rgba.z;
			buffer[y*xDim+x].w=alpha;
		}
        else
        {
            buffer[y*xDim+x].x=0.0f;
            buffer[y*xDim+x].y=0.0f;
            buffer[y*xDim+x].z=0.0f;
            buffer[y*xDim+x].w=0.0f;
        }
	}
}
"

let readMessage (pipe:NamedPipeServerStream)=
    let lengthBuf = Array.create(sizeof<int>) 0uy
    pipe.Read (lengthBuf,0,sizeof<int>) |> ignore
    let length = BitConverter.ToInt32 (lengthBuf,0)
    let newBuf = Array.create length 0uy
    pipe.Read (newBuf,0,length) |> ignore
    newBuf
let writeMessage (pipe:NamedPipeServerStream) (msg:byte[])=
    let lengthBuf = BitConverter.GetBytes (msg.Length)
    pipe.Write (Array.append lengthBuf msg,0,msg.Length+sizeof<int>)


let pTask ind =
    let pipeName = Guid.NewGuid().ToString()
    let pipe = new NamedPipeServerStream(pipeName,PipeDirection.InOut,1,PipeTransmissionMode.Message)
    try
        let compilerProcess = new Process()
        let startInfo = new ProcessStartInfo()
        startInfo.FileName <- @"C:\Users\Steven\Documents\Visual Studio 10\Projects\flam4OCL_Frontend\Flam4CompileService\bin\Release\Flam4CompileService.exe"
        startInfo.Arguments <- pipeName + " " + "\"" + "GeForce GTX 295" + "\""
        startInfo.UseShellExecute <- false
        compilerProcess.StartInfo <- startInfo
        compilerProcess.Start() |> ignore
        pipe.WaitForConnection()
        printfn "Server Connected"
        let buf = Encoding.UTF8.GetBytes(ClearBufferKernal)
        writeMessage pipe buf
        pipe.Flush()
        printfn "Sent Message 1"
        let msg = readMessage pipe
        printfn "Recieved result, %i bytes" msg.Length
        let buf3 = Encoding.UTF8.GetBytes(PostProcessKernel)
        writeMessage pipe buf3
        pipe.Flush()
        printfn "Sent Message 2"
        let msg2 = readMessage pipe
        printfn "Recieved result, %i bytes" msg2.Length
        let buf3 = Encoding.UTF8.GetBytes(PostProcessKernel)
        pipe.WaitForPipeDrain()
        let buf2 = Encoding.UTF8.GetBytes("END")
        writeMessage pipe buf2
        pipe.Flush()
        printfn "Sent End Message"
        pipe.WaitForPipeDrain()
    finally
        pipe.Close()

let _ = Parallel.For(0,30,new Action<int>(pTask))

