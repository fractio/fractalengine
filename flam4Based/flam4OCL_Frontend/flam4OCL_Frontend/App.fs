﻿namespace Flam4GUI

open System
open System.Windows

type App() =
    inherit Application()

    
    member public s.InitializeComponent() =
        s.StartupUri <- new System.Uri("MainWindow.xaml", System.UriKind.Relative)
        ()