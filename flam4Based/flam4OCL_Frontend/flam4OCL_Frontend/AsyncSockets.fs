﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.


module AsyncSockets

open System
open System.IO
open System.Net
open System.Net.Sockets
open System.Collections.Generic
open System.Text

let toIList<'T> (data : 'T array) =
        let segment = new System.ArraySegment<'T>(data)
        let data = new List<System.ArraySegment<'T>>() :> IList<System.ArraySegment<'T>>
        data.Add(segment)
        data


type TcpListener with
    member s.AcceptTcpClientAsync() =
        Async.FromBeginEnd((fun (callback, state) -> s.BeginAcceptTcpClient(callback,state)),
                                    s.EndAcceptTcpClient)

type Socket with
    member s.ReceiveAsync ((buffer:byte[]),(flags:SocketFlags),offset) =
        Async.FromBeginEnd(buffer, flags,offset,
                            (fun (buffer,flags,offset,callback,state) -> s.BeginReceive(buffer,offset,buffer.Length-offset,flags,callback,state)),
                            s.EndReceive)
    member s.SendAsync (buffers,flags) =
        Async.FromBeginEnd(toIList buffers,flags,
                            (fun (buffers,flags,callback,state) -> s.BeginSend(buffers,flags,callback,state)),
                            s.EndSend)
    member s.ConnectAsync endpoint =
        Async.FromBeginEnd(endpoint,
                            (fun (endpoint,callback,state) -> s.BeginConnect(endpoint,callback,state)),
                            s.EndConnect)
    member s.SendAsync (stream:Stream) =
        async {
            stream.Position <- 0L
            let buf = Array.create (int stream.Length) 0uy
            let _ =  stream.Read(buf,0,int buf.Length)
            let lengthBuf = BitConverter.GetBytes (buf.Length)
            let! _ = s.SendAsync (lengthBuf,SocketFlags.None)
            let! _ = s.SendAsync (buf, SocketFlags.None)
            ()
            }
    member s.ReceiveAsync (stream:Stream) =
        async {
            stream.Position <- 0L
            let lengthBuf = Array.create (sizeof<int>) 0uy
            let! _ = s.ReceiveAsync(lengthBuf,SocketFlags.None,0)
            let buf = Array.create(BitConverter.ToInt32 (lengthBuf,0)) 0uy
            let received = ref 0
            while !received < buf.Length do
                let! more = s.ReceiveAsync(buf,SocketFlags.None,!received)
                received := !received+more
            stream.Write(buf,0,buf.Length)
            ()
            }