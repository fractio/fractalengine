﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module GPUMem

#nowarn "9"

open System
open System.Runtime.InteropServices
open System.Threading.Tasks
open System.Text.RegularExpressions
open Cloo
open CompilerDriver
open Helper
open StandardKernals

type CLDevice =
    {
    clDevice : ComputeDevice
    clContext : ComputeContext
    clQueue : ComputeCommandQueue 
    queueDepth : int
    mutable clEvents : ComputeEventBase [] 
    compilerService : CompilerService Option
    clPlatform : ComputePlatform
    }

type BufferRequest =
    {
    Width : int64
    Height : int64
    NumChannels : int64
    SliceRadius : int64
    TotalRadius : int64
    }

type BufferData =
    {
        IsInput : bool
        IsOutput: bool
        OffLoadFunction : int -> unit
    }

type PageDescription =
    {
    mutable AbsolutePageNumber : int
    mutable MappedGpuPageSlot : int
    }

    

//Our page size will be fixed at 64Mb
//Ideally it would be the largest common factor of the maximal allocation size of each device, or "almost factor".
let maxPageSize=67108864L

let getAvailableMemory (device:CLDevice)=
    //if device.clPlatform.Name="AMD Accelerated Parallel Processing" 
    //then 
    //    float device.clDevice.GlobalMemorySize*0.23 |> int64
    //else
    let totalMemory = device.clDevice.GlobalMemorySize
    let maxSlices = totalMemory/maxPageSize
    let bufferRX = new Regex("@buffers@")
    let pageBufferDecls numPages=
        let rec pageBufferDecls numPages code index =
            if index = numPages then
                code
            else
                code + pageBufferDecls numPages (sprintf ",global volatile float * renderBuffer%i" index) (index+1)
        pageBufferDecls numPages "global volatile float * renderBuffer0" 1
    let bufferString = pageBufferDecls (int maxSlices)
    let memTestKernel = ("kernel void memTest(@buffers@) { }",bufferString) |> bufferRX.Replace
    let devList = new System.Collections.Generic.List<ComputeDevice>()
    devList.Add device.clDevice
    let deviceList = new System.Collections.ObjectModel.ReadOnlyCollection<ComputeDevice>(devList)
    let testProg = new ComputeProgram(device.clContext, memTestKernel)
    testProg.Build(deviceList,"",null, IntPtr 0)
    let testBuffers = [| for n in 0L..maxSlices-1L -> new ComputeBuffer<float32>(device.clContext,ComputeMemoryFlags.ReadWrite,maxPageSize/4L) |]
    let kernel = testProg.CreateKernel "memTest"
    let rec performBufferTest activePages totalPages =
        if activePages < 0L then failwith "Not enough memory for a single page!"
        try
            for n in 0L..activePages-1L do
                kernel.SetMemoryArgument(int n,testBuffers.[int n])
            for n in activePages..totalPages-1L do
                kernel.SetMemoryArgument(int n,testBuffers.[0])
            device.clQueue.Execute(kernel,null,[|1L;1L|],[|1L;1L|],null)
            device.clQueue.Finish()
            max 1L (activePages-1L)
        with
        :? Cloo.MemoryObjectAllocationFailureComputeException ->
            performBufferTest (activePages-2L) totalPages
    let availablePages = performBufferTest maxSlices maxSlices
    for buffer in testBuffers do
        buffer.Dispose()
    //float device.clDevice.GlobalMemorySize*0.75 |> int64
    printfn "Memory available for %i pages on %s" availablePages device.clDevice.Name
    maxPageSize*availablePages

let deleteBuffers (buffers:ComputeBuffer<float> list) =
    buffers |> List.map (fun buffer -> buffer.Dispose()) |> ignore

(*let allocateIndependentBuffers requestedBuffers (device:ComputeDevice) (context:ComputeContext)=
    let availableMemory = getAvailableMemory device

    let sliceMemorySize buffer slices =
        Math.Ceiling((float ((buffer.Width+(buffer.TotalRadius-buffer.SliceRadius)*2L) * (buffer.Height+buffer.TotalRadius*2L))/(float slices)) + (float ((buffer.Height+buffer.TotalRadius*2L) * buffer.SliceRadius*2L))) |> int64 |> (*) buffer.NumChannels
    let requestedMemory = List.fold (fun mem buf -> mem+(buf.Width+buf.TotalRadius*2L)
                                                        *(buf.Height+buf.TotalRadius*2L)
                                                        *buf.NumChannels*4L) 0L requestedBuffers
    let columnSize= List.fold (fun mem buf -> mem+(buf.Height+buf.TotalRadius*2L)
                                                    *buf.NumChannels) 0L requestedBuffers
    let curtainSize= List.fold (fun mem buf -> mem+(buf.Height+buf.TotalRadius*2L)
                                                    *buf.SliceRadius*buf.NumChannels*2L) 0L requestedBuffers
    if curtainSize+columnSize>availableMemory then
        failwith "Impossibly large buffer size requested"
    let slices =
        let rec checkSlices slices =
            //Vertical Slices
            let sliceMem = curtainSize+List.fold (fun mem buf -> sliceMemorySize buf slices |> (+) mem) 0L requestedBuffers
            if sliceMem>availableMemory then
                checkSlices (slices+1L)
            else
                slices
        checkSlices 1L
    seq { for n in 1L..slices ->
            [for buf in requestedBuffers ->
                new ComputeBuffer<float>(context,ComputeMemoryFlags.ReadOnly,sliceMemorySize buf slices)] }*)


let offLoadBufferAdd (source:nativeint) (dest:float32 []) length=
    lock dest (fun () ->
        let destPointer = NativeInterop.NativePtr.ofNativeInt source
        for n in 0..length-1 do 
            let value = NativeInterop.NativePtr.read (NativeInterop.NativePtr.add destPointer n)
            dest.[n] <- dest.[n] + value
        )

let offLoadBufferReplace (source:nativeint) (dest:float32 []) length=
    lock dest (fun () ->
        Marshal.Copy(source,dest,0,length)
    )

///Since these buffers are for accumulating iterations, there is no overlap allowed.
[<AllowNullLiteral>]
type BufferManager (requestedBuffer,devices,clClearBufferProgram:ComputeProgram []) =

    let sliceMemorySize buffer slices =
        Math.Ceiling((float ((buffer.Width+(buffer.TotalRadius-buffer.SliceRadius)*2L) * (buffer.Height+buffer.TotalRadius*2L))/(float slices)) + (float ((buffer.Height+buffer.TotalRadius*2L) * buffer.SliceRadius*2L))) |> int64 |> (*) (buffer.NumChannels*4L)
    let newWidth=requestedBuffer.Width+requestedBuffer.TotalRadius*2L
    let newHeight = requestedBuffer.Height+requestedBuffer.TotalRadius*2L
    let requestedMemory = newWidth*newHeight*requestedBuffer.NumChannels*4L
    let columnSize= newHeight*requestedBuffer.NumChannels*4L
    do if columnSize>maxPageSize then
        failwith "Impossibly large buffer size requested"
    let devices = devices
    let slices,_pageSize =
        let rec checkSlices slices =
            //Vertical Slices
            let sliceMem = sliceMemorySize requestedBuffer slices
            if sliceMem>maxPageSize then
                checkSlices (slices+1L)
            else
                slices,sliceMem
        checkSlices 1L
    do printfn "This render uses %i pages of memory total" slices
    let batchesCount = Array.create(int slices) 0
    let activeSliceCounts = (devices:CLDevice list) |> List.map (fun device -> 
                                                                    let memory = getAvailableMemory(device)
                                                                    //printfn "%i" memory
                                                                    Math.Min(Math.Ceiling((float memory)/(float _pageSize)) |> int64,slices))
    let deviceStagingBuffers = devices |> List.mapi (fun n device ->
                                                        [|for m in 0L..activeSliceCounts.[n]-1L -> Marshal.AllocHGlobal(nativeint _pageSize)|])
    let deviceBuffers = devices |> List.mapi (fun n device ->
        (deviceStagingBuffers.[n] |> Array.map (fun _ ->
            new ComputeBuffer<float32>(device.clContext,ComputeMemoryFlags.ReadWrite,_pageSize/4L))))
    let mergeBuffer = Array.create(int slices) [|0.0f|]
    do mergeBuffer |> Array.iteri (fun i item -> mergeBuffer.[i] <- Array.create (int _pageSize/4) 0.0f)
    let pageDescs = devices |> List.mapi (fun n device -> []:PageDescription list) |> List.toArray
    let batchesSinceLastPageRefresh = [|for device in devices -> 0|]
                                                
    do for buf in mergeBuffer do
        Array.Clear (buf,0,buf.Length)
    let stagingBufferTask = [| for buffers in deviceStagingBuffers ->
                                [| for buffer in buffers ->
                                    Task.Factory.StartNew<unit>(fun () -> ())|] |]

    interface System.IDisposable with
        member s.Dispose() =
            for buffers in deviceStagingBuffers do
                for buffer in buffers do
                    Marshal.FreeHGlobal(buffer)
            for buffers in deviceBuffers do
                for buffer in buffers do
                    buffer.Dispose()

    member s.prepareBuffer deviceIndex index=
        //clearBuffer(global float4* buffer, int dim)
        let kernels = [for program in clClearBufferProgram ->
                       program.CreateKernel("clearBuffer")]
        kernels.[deviceIndex].SetMemoryArgument(0, deviceBuffers.[deviceIndex].[index])
        kernels.[deviceIndex].SetValueArgument(1, int _pageSize/16)
        let localSize = [|256L; 1L|]
        let globalSize = [|localSize.[0]*int64 (ceil((float32 _pageSize/16.0f)/(float32 localSize.[0]))); 1L|]
        devices.[deviceIndex].clQueue.Execute(kernels.[deviceIndex],null,globalSize,localSize, null) |> ignore
        for kernel in kernels do
            kernel.Dispose()
        
    member s.onLoadBuffers deviceIndex=
        let descs = pageDescs.[deviceIndex]
        for tasks in stagingBufferTask do
            for task in tasks do
                ignore task.Result
        for desc in descs do
            Marshal.Copy(mergeBuffer.[desc.AbsolutePageNumber],0,deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot],int _pageSize/4)
            devices.[deviceIndex].clQueue.Write(deviceBuffers.[deviceIndex].[desc.MappedGpuPageSlot],true,0L,_pageSize/4L,deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot],null)
        devices.[deviceIndex].clQueue.Finish()


    member s.offLoadBuffersAccum deviceIndex=
        let descs = pageDescs.[deviceIndex]
        //Wait for the staging buffer to be ready
        for task in stagingBufferTask.[deviceIndex] do
            task.Wait()
        for desc in descs do
            devices.[deviceIndex].clQueue.Read(deviceBuffers.[deviceIndex].[desc.MappedGpuPageSlot],true,0L,_pageSize/4L,deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot],null)
            devices.[deviceIndex].clQueue.Finish()
        for desc in descs do
            stagingBufferTask.[deviceIndex].[desc.MappedGpuPageSlot] <- Task.Factory.StartNew<unit>(fun () -> offLoadBufferAdd (deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot]) (mergeBuffer.[desc.AbsolutePageNumber]) (int _pageSize/4))
        //We randomly crash if we don't force the tasks to complete now
        //Oddly, this only happens in the accum phase, never in the replace phase
        for task in stagingBufferTask.[deviceIndex] do
            try
                ignore task.Result
            with
            | _ -> printfn "Exception caught in accum buffers"

    member s.offLoadBuffersReplace deviceIndex=
        let descs = pageDescs.[deviceIndex]
        //Wait for the staging buffer to be ready
        for task in stagingBufferTask.[deviceIndex] do
            task.Wait()
        for desc in descs do
            devices.[deviceIndex].clQueue.Read(deviceBuffers.[deviceIndex].[desc.MappedGpuPageSlot],true,0L,_pageSize/4L,deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot],null)
        devices.[deviceIndex].clQueue.Finish()
        for desc in descs do 
            stagingBufferTask.[deviceIndex].[desc.MappedGpuPageSlot] <- Task.Factory.StartNew<unit>(fun () -> offLoadBufferReplace (deviceStagingBuffers.[deviceIndex].[desc.MappedGpuPageSlot]) (mergeBuffer.[desc.AbsolutePageNumber]) (int _pageSize/4))

    member s.getSinglePage bufferData numBatches deviceIndex=
        
        //if bufferData.IsOutput = true then
        devices.[deviceIndex].clQueue.Finish()
        bufferData.OffLoadFunction deviceIndex
        devices.[deviceIndex].clQueue.Finish()
        let pageCount = 1
        let sortedPages = [0..int slices-1] |> 
                            List.zip (batchesCount |> Array.toList) |>
                            List.sortBy (fun (batches,page) -> batches)
        let rec accumulatePages index (pages: PageDescription list) =
            if index < sortedPages.Length && pages.Length < pageCount then
                if fst sortedPages.[index] < numBatches then
                    //printfn "assigned device %i absolute %i gpu %i" deviceIndex index pages.Length
                    ({AbsolutePageNumber=snd sortedPages.[index]; MappedGpuPageSlot=pages.Length}::pages) |> accumulatePages (index+1)
                else
                    pages |> accumulatePages (index+1)
            else
                pages
        pageDescs.[deviceIndex] <- accumulatePages 0 ([]:PageDescription list)
        if bufferData.IsInput=true then
            s.onLoadBuffers deviceIndex
            devices.[deviceIndex].clQueue.Finish()
        else
            if pageDescs.[deviceIndex].Length>0 then
                s.prepareBuffer deviceIndex 0
        for page in pageDescs.[deviceIndex] do
            batchesCount.[page.AbsolutePageNumber] <- batchesCount.[page.AbsolutePageNumber]+1
        pageDescs.[deviceIndex]

    member s.getPageSet bufferData numBatches deviceIndex=
        let needsNewPages = if pageDescs.[deviceIndex].Length > 0 
                                then
                                    if batchesSinceLastPageRefresh.[deviceIndex] >= 5000 then
                                        true
                                    else
                                        List.fold (fun state page -> 
                                            if batchesCount.[page.AbsolutePageNumber] >= numBatches then
                                                true
                                            else
                                                state) false pageDescs.[deviceIndex]
                                else
                                    true
        if needsNewPages=true then
            batchesSinceLastPageRefresh.[deviceIndex] <- 0
            devices.[deviceIndex].clQueue.Finish()
            bufferData.OffLoadFunction deviceIndex
            devices.[deviceIndex].clQueue.Finish()
            let pageCount = int activeSliceCounts.[deviceIndex]
            let sortedPages = [0..(int slices-1)] |> 
                                List.zip (batchesCount |> Array.toList) |>
                                List.sortBy (fun (batches,page) -> batches)
            let rec accumulatePages index (pages: PageDescription list) =
                if index < sortedPages.Length && pages.Length < pageCount then
                    if fst sortedPages.[index] < numBatches then
                        //printfn "assigned device %i absolute %i gpu %i" deviceIndex index pages.Length
                        ({AbsolutePageNumber=snd sortedPages.[index]; MappedGpuPageSlot=pages.Length}::pages) |> accumulatePages (index+1)
                    else
                        pages |> accumulatePages (index+1)
                else
                    pages
            pageDescs.[deviceIndex] <- accumulatePages 0 ([]:PageDescription list)
            if bufferData.IsInput=true then
                s.onLoadBuffers deviceIndex
            else
                for desc in pageDescs.[deviceIndex] do
                    s.prepareBuffer deviceIndex desc.MappedGpuPageSlot
        for page in pageDescs.[deviceIndex] do
            batchesCount.[page.AbsolutePageNumber] <- batchesCount.[page.AbsolutePageNumber]+1
        batchesSinceLastPageRefresh.[deviceIndex] <- batchesSinceLastPageRefresh.[deviceIndex]+1
        pageDescs.[deviceIndex],needsNewPages

    member s.deviceBuffer deviceIndex = deviceBuffers.[deviceIndex]
    member s.outputBuffers ()= 
        for tasks in stagingBufferTask do
            for task in tasks do
                (Async.AwaitTask(task)) |> Async.RunSynchronously
        mergeBuffer
    member s.resetBatchCount () =
        for n in 0..batchesCount.Length-1 do
            batchesCount.[n] <- 0
        for n in 0..devices.Length-1 do
            batchesSinceLastPageRefresh.[n] <- 0
    member s.numSlices = slices
    member s.pageSize = _pageSize
    member s.finishAsyncTasks deviceIndex =
        for task in stagingBufferTask.[deviceIndex] do
            task.Wait()

