﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module FlameClass
open System.Collections.Generic
open Helper
open FlameData
open FlameMarshal

type varParameter (offset,flameArrayBase:float32 [],value) as s =
    let mutable _base = flameArrayBase
    do s.value <- value
    member s.flameArrayBase with get() = _base
                            and set x = _base <- x
    member s.value  with get() = flameArrayBase.[offset]
                    and set x  = flameArrayBase.[offset] <- x

[<AllowNullLiteral>]
type var (offset,flameArrayBase:float32 []) =
    let mutable _base = flameArrayBase
    let mutable _parameters = [||]:varParameter []
    member s.offset = offset
    member s.flameArrayBase with get() = _base
                            and set x = _base <- x
    member s.varType    with get() = !*.*->* s.flameArrayBase.[offset]
                        and set x  = s.flameArrayBase.[offset] <- !*->*.* x
    member s.weight with get() = s.flameArrayBase.[offset+1]
                    and set x  = s.flameArrayBase.[offset+1] <- x
    member s.parameters with get() = _parameters
                        and set x  = _parameters <- x
    member s.setVar variation (varRepo:Map<string,int>) =
        s.varType <- varRepo.[variation.vardef.name]
        s.weight <- variation.weight
        s.parameters <- [|for param,index in (List.zip variation.parameters [0..variation.parameters.Length-1])-> new varParameter(index+s.offset+2,s.flameArrayBase,param)|]


[<AllowNullLiteral>]
type Xform (offset,flameArrayBase:float32 []) =
    let mutable _base = flameArrayBase
    let mutable _decisionTree = Empty
    let mutable _flatDecisionTree = FlatEmpty
    let mutable _vars = [||]:var []
    member s.flameArrayBase with get() = _base
                            and set x = _base <- x
    member s.offset = offset
    member s.xformIndex with get() = !*.*->* s.flameArrayBase.[offset]
                        and set x  = s.flameArrayBase.[offset] <- !*->*.* x
    member s.a  with get() = s.flameArrayBase.[offset+1]
                and set x  = s.flameArrayBase.[offset+1] <- x
    member s.b  with get() = s.flameArrayBase.[offset+2]
                and set x  = s.flameArrayBase.[offset+2] <- x
    member s.c  with get() = s.flameArrayBase.[offset+3]
                and set x  = s.flameArrayBase.[offset+3] <- x
    member s.d  with get() = s.flameArrayBase.[offset+4]
                and set x  = s.flameArrayBase.[offset+4] <- x
    member s.e  with get() = s.flameArrayBase.[offset+5]
                and set x  = s.flameArrayBase.[offset+5] <- x
    member s.f  with get() = s.flameArrayBase.[offset+6]
                and set x  = s.flameArrayBase.[offset+6] <- x
    member s.g  with get() = s.flameArrayBase.[offset+7]
                and set x  = s.flameArrayBase.[offset+7] <- x
    member s.h  with get() = s.flameArrayBase.[offset+8]
                and set x  = s.flameArrayBase.[offset+8] <- x
    member s.i  with get() = s.flameArrayBase.[offset+9]
                and set x  = s.flameArrayBase.[offset+9] <- x
    member s.j  with get() = s.flameArrayBase.[offset+10]
                and set x  = s.flameArrayBase.[offset+10] <- x
    member s.k  with get() = s.flameArrayBase.[offset+11]
                and set x  = s.flameArrayBase.[offset+11] <- x
    member s.l  with get() = s.flameArrayBase.[offset+12]
                and set x  = s.flameArrayBase.[offset+12] <- x
    member s.colorIndex with get() = s.flameArrayBase.[offset+13]
                        and set x  = s.flameArrayBase.[offset+13] <- x
    member s.colorSpeed with get() = s.flameArrayBase.[offset+14]
                        and set x  = s.flameArrayBase.[offset+14] <- x
    member s.decisionTree   with get() = _decisionTree
                            and set x  = _decisionTree <- x
    member s.flatDecisionTree   with get() = _flatDecisionTree
                                and set x  = _flatDecisionTree <- x
    member s.vars   with get() = _vars
                    and set x  = _vars <- x

    member s.setXform xform =
        let coefs = xform.coefs
        s.a <- coefs.a
        s.b <- coefs.b
        s.c <- coefs.c
        s.d <- coefs.d
        s.e <- coefs.e
        s.f <- coefs.f
        s.g <- coefs.g
        s.h <- coefs.h
        s.i <- coefs.i
        s.j <- coefs.j
        s.k <- coefs.k
        s.l <- coefs.l
        s.colorIndex <- xform.colorIndex
        s.colorSpeed <- xform.colorSpeed
        ()

and DecisionTree =
    | Branch of float32*DecisionTree*DecisionTree
    | Leaf of XformNodeLink
    | Empty

and FlatDecisionTree =
    | FlatBranch of int*XformDecisionTreeBranch*FlatDecisionTree*FlatDecisionTree
    | FlatLeaf of int*XformDecisionTreeLeaf
    | FlatEmpty

and XformDecisionTreeLeaf (offset,flameArrayBase:float32 []) =
    let mutable _base = flameArrayBase
    member s.flameArrayBase with get() = _base
                            and set x = _base <- x
    member s.offset = offset
    member s.xformOffset    with get() = !*.*->* s.flameArrayBase.[offset]
                            and set x = s.flameArrayBase.[offset] <- !*->*.* x
    member s.state  with get() = !*.*->* s.flameArrayBase.[offset+1]
                    and set x = s.flameArrayBase.[offset+1] <- !*->*.* x
    member s.opacity    with get() = s.flameArrayBase.[offset+2]
                        and set x  = s.flameArrayBase.[offset+2] <- x
    member s.discard    with get() = !*.*->* s.flameArrayBase.[offset+3]
                        and set x = s.flameArrayBase.[offset+3] <- !*->*.* x

    member s.setLeaf node offset=
        s.xformOffset <- offset
        s.state <- node.state
        s.opacity <- node.opacity
        s.discard <- node.discard

and XformDecisionTreeBranch (offset,flameArrayBase:float32 []) =
    let mutable _base = flameArrayBase
    member s.flameArrayBase with get() = _base
                            and set x = _base <- x
    member s.offset = offset
    member s.splitWeight    with get() = s.flameArrayBase.[offset]
                            and set x  = s.flameArrayBase.[offset] <- x
    member s.left   with get() = !*.*->* s.flameArrayBase.[offset+1]
                    and set x = s.flameArrayBase.[offset+1] <- !*->*.* x
    member s.right  with get() = !*.*->* s.flameArrayBase.[offset+2]
                    and set x = s.flameArrayBase.[offset+2] <- !*->*.* x

    member s.setBranch weight leftOffset rightOffset = 
        s.splitWeight <- weight+1.0f
        s.left <- leftOffset
        s.right <- rightOffset
        

  
type Flame (flame,varRepo:Map<VariationDef,int>) as s=
    let afterword = AfterwordBuilder()
    let mutable offset = 12
    let mutable xforms = Array.create flame.xforms.Length null
    let mutable flameArray = [||]
    let mutable xformDict = Map.empty<string,Xform>
    let varStringRepo = varRepo |> Map.toSeq |> Seq.map (fun (key,value) -> (key.name,value)) |> Map.ofSeq
    let getWeight decisionTree =
        match decisionTree with
        | Leaf(n) ->
            n.weight
        | Branch(n,_,_) ->
            n
        | Empty -> failwith "Missing decision tree!"
    let buildDecisionTree (xformNode:XformNodeLink list)=
        let rec buildDecisionTree (decisionTree:DecisionTree list) =
            match decisionTree with
            | [_] ->
                decisionTree.[0]
            | _ ->
                let sortedTree = decisionTree |> List.sortBy getWeight
                buildDecisionTree (Branch(((getWeight sortedTree.Head) + (getWeight sortedTree.Tail.Head)),sortedTree.Head,sortedTree.Tail.Head)::sortedTree.Tail.Tail)
        let decisionTree = [for node in xformNode -> Leaf(node)]
        buildDecisionTree decisionTree


    do flame.xforms |> List.iteri (fun n xform ->
        xforms.[n] <- new Xform(offset,[||])
        let decisionTree = buildDecisionTree xform.next
        xforms.[n].decisionTree <- decisionTree
        offset <- xforms.[n].offset+15
        xformDict <- xformDict.Add(xform.name,xforms.[n])
        let vars = [| for vr in xform.self.vars do
                        let v = new var(offset,null)
                        offset <- offset+2+vr.parameters.Length
                        yield v |]
        offset <- offset+1
        xforms.[n].vars <- vars
        let rec createFlatDecisionTree decisionTree offset=
            match decisionTree with
            | Branch(weight,left,right) ->
                let leftOffset = offset+3
                let fleft,rightOffset = createFlatDecisionTree left leftOffset
                let fright,newOffset = createFlatDecisionTree right rightOffset
                let res = FlatBranch(offset,new XformDecisionTreeBranch(offset,[||]),fleft,fright)
                (res,newOffset)
            | Leaf(xformNode) ->
                let res = FlatLeaf(offset,new XformDecisionTreeLeaf(offset,[||]))
                res,offset+4
            | Empty -> failwith "Missing decision tree!"
        let ft,o = createFlatDecisionTree decisionTree offset
        xforms.[n].flatDecisionTree <- ft
        offset <- o)

    
    let getWeight decisionTree =
                match decisionTree with
                | Leaf(n) ->
                    n.weight
                | Branch(n,_,_) ->
                    n
                | _ -> failwith "Malformed decisionTree"

    let rec copyDecisionTree tree flatTree weight= 
        match tree with
        | Leaf(node) ->
            match flatTree with
            | FlatLeaf(offset,dest) ->
                dest.flameArrayBase <- flameArray
                dest.setLeaf node (xformDict.Item(node.next.name)).offset
            | _ -> failwith "Bad decision tree combination"
        | Branch(w,left,right) ->
            match flatTree with
            | FlatBranch(offset,dest,fleft,fright) ->
                copyDecisionTree left fleft weight
                copyDecisionTree right fright (weight + getWeight left)
                dest.flameArrayBase <- flameArray
                let leftOffset = match fleft with
                                    | FlatBranch(offset,_,_,_) -> offset
                                    | FlatLeaf(offset,_) -> offset
                                    | _ -> failwith "Bad node in decision tree"
                let rightOffset = match fright with
                                    | FlatBranch(offset,_,_,_) -> offset
                                    | FlatLeaf(offset,_) -> offset
                                    | _ -> failwith "Bad node in decision tree"
                dest.setBranch (weight + getWeight left) leftOffset rightOffset
            | _ -> failwith "Bad decision tree combination"
        | _ -> failwith "Bad decision tree combination"
    do flameArray <- Array.create(offset) 0.0f
    do xforms |> Array.iteri (fun i x ->
                            x.flameArrayBase <- flameArray
                            x.setXform(flame.xforms.[i].self)
                            do x.vars |> Array.iteri (fun j v ->
                                v.flameArrayBase <- flameArray
                                v.setVar flame.xforms.[i].self.vars.[j] varStringRepo)
                            match x.flatDecisionTree with
                            | FlatLeaf(offset,_) -> flameArray.[offset-1] <- !*->*.* (-i-1)
                            | FlatBranch(offset,_,_,_) -> flameArray.[offset-1] <- !*->*.* (-i-1)
                            | _ -> failwith "Bad node in decision tree"
                            x.xformIndex <- i
                            copyDecisionTree x.decisionTree x.flatDecisionTree 0.0f)
    let coverage = flame.viewport.minimumCoverage |> invertAffine
    do s.a <- coverage.a
    do s.b <- coverage.b
    do s.c <- coverage.c
    do s.d <- coverage.d
    do s.e <- coverage.e
    do s.f <- coverage.f
    do s.g <- coverage.g
    do s.h <- coverage.h
    do s.i <- coverage.i
    do s.j <- coverage.j
    do s.k <- coverage.k
    do s.l <- coverage.l
    member s.flame = flameArray
    member s.a  with get() = s.flame.[0]
                and set x  = s.flame.[0] <- x
    member s.b  with get() = s.flame.[1]
                and set x  = s.flame.[1] <- x
    member s.c  with get() = s.flame.[2]
                and set x  = s.flame.[2] <- x
    member s.d  with get() = s.flame.[3]
                and set x  = s.flame.[3] <- x
    member s.e  with get() = s.flame.[4]
                and set x  = s.flame.[4] <- x
    member s.f  with get() = s.flame.[5]
                and set x  = s.flame.[5] <- x
    member s.g  with get() = s.flame.[6]
                and set x  = s.flame.[6] <- x
    member s.h  with get() = s.flame.[7]
                and set x  = s.flame.[7] <- x
    member s.i  with get() = s.flame.[8]
                and set x  = s.flame.[8] <- x
    member s.j  with get() = s.flame.[9]
                and set x  = s.flame.[9] <- x
    member s.k  with get() = s.flame.[10]
                and set x  = s.flame.[10] <- x
    member s.l  with get() = s.flame.[11]
                and set x  = s.flame.[11] <- x
    member s.varRepository = varRepo
    member s.firstLinkOffset = match xforms.[0].flatDecisionTree with
                                | FlatBranch(offset,_,_,_) -> offset
                                | FlatLeaf(offset,_) -> offset
                                | _ -> failwith("Malformed flatDecisionTree")
 
