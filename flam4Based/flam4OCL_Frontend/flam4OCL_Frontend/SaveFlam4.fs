﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module SaveFlam4

open System
open System.IO
open System.Xml

open FlameData
open Helper
open LoadFlam4

let SaveFlame (data:FlameParameter) (includes:String list) (output:Stream) =
    let writeSettings = new XmlWriterSettings()
    writeSettings.Indent <- true
    writeSettings.OmitXmlDeclaration <- true
    let writer = XmlWriter.Create(output,writeSettings)
    let outputXML = new XmlDocument()

    let CreateElement name =
        outputXML.CreateNode (XmlNodeType.Element,name,null)
    let CreateAttribute name value =
        let attr = outputXML.CreateAttribute name
        attr.Value <- value
        attr

    let CreateCoefsNode (coefs:Affine) =
        let coefsNode = CreateElement "coefs"
        let _ = coefsNode   <+ CreateAttribute "a" (string coefs.a)
        let _ = coefsNode   <+ CreateAttribute "b" (string coefs.b)
        let _ = coefsNode   <+ CreateAttribute "c" (string coefs.c)
        let _ = coefsNode   <+ CreateAttribute "d" (string coefs.d)
        let _ = coefsNode   <+ CreateAttribute "e" (string coefs.e)
        let _ = coefsNode   <+ CreateAttribute "f" (string coefs.f)
        let _ = coefsNode   <+ CreateAttribute "g" (string coefs.g)
        let _ = coefsNode   <+ CreateAttribute "h" (string coefs.h)
        let _ = coefsNode   <+ CreateAttribute "i" (string coefs.i)
        let _ = coefsNode   <+ CreateAttribute "j" (string coefs.j)
        let _ = coefsNode   <+ CreateAttribute "k" (string coefs.k)
        let _ = coefsNode   <+ CreateAttribute "l" (string coefs.l)
        coefsNode

    let flam4Node = CreateElement "flam4" |> outputXML.AppendChild
    let flameNode = flam4Node <+ CreateElement "flame"
    for incl in includes do
        let _ = flameNode <+ CreateElement "include" <+ CreateAttribute "library" incl
        ()
    let viewportNode = flameNode <+ CreateElement "viewport"
    let _ = viewportNode <+ CreateAttribute "aspect_ratio" (string data.viewport.aspectRatio)
    let _ = viewportNode <+ CreateCoefsNode data.viewport.minimumCoverage

    let gamutNode = flameNode <+ CreateElement "gamut"
    let _ = gamutNode <+ CreateAttribute "brightness" (string data.gamut.Brightness)
    let _ = gamutNode <+ CreateAttribute "gamma" (string data.gamut.Gamma)
    let _ = gamutNode <+ CreateAttribute "vibrancy" (string data.gamut.Vibrancy)

    for xformNode in data.xforms do
        let node = flameNode <+ CreateElement "xform_node"
        let _ = node <+ CreateAttribute "name" (UnMangleName (xformNode.name))
        let xform = xformNode.self
        let xformXml = node <+ CreateElement "xform"
        let _ = xformXml <+ CreateAttribute "name" (UnMangleName (xform.name))
        let _ = xformXml <+ CreateAttribute "color_index" (string xform.colorIndex)
        let _ = xformXml <+ CreateAttribute "color_speed" (string xform.colorSpeed)
        let _ = xformXml <+ CreateCoefsNode xform.coefs
        for var in xform.vars do
            let varNode = xformXml <+ CreateElement "var"
            let _ = varNode <+ CreateAttribute "name" (UnMangleName (var.vardef.name))
            let _ = varNode <+ CreateAttribute "weight" (string var.weight)
            let varParms = List.zip var.vardef.parameters var.parameters
            for name,weight in varParms do
                let _ = varNode <+ CreateAttribute name (string weight)
                ()
            ()
        for next in xformNode.next do
            let nextNode = node <+ CreateElement "xform_node"
            let _ = nextNode <+ CreateAttribute "name" (UnMangleName next.next.name)
            let _ = nextNode <+ CreateAttribute "weight" (string next.weight)
            let stateString = match next.discard with
                                | 0 when next.state=0->
                                    "hold"
                                | 0 ->
                                    "push "+string next.state
                                | _ ->
                                    "pop "+string -next.state
            let _ = nextNode <+ CreateAttribute "state" stateString
            let _ = nextNode <+ CreateAttribute "opacity" (string next.opacity)
            ()
        ()
    let varsWithCode = List.filter (fun x -> not x.fromInclude) data.varRepo
    if varsWithCode.Length<>0 then
        let codeNode = flameNode <+ CreateElement "code"
        for var in varsWithCode do
            let varNode = codeNode <+ CreateElement "var"
            let _ = varNode <+ CreateAttribute "name" (UnMangleName var.name)
            let parmString = List.fold (fun str next -> str+" "+next) "" var.parameters
            let _ = varNode <+ CreateAttribute "parameters" parmString
            varNode.InnerText <- var.code
            ()
    else
        ()
    let paletteNode = flameNode <+ CreateElement "palette"
    let _ = paletteNode <+ CreateAttribute "format" "rgb32_unorm_hex"
    paletteNode.InnerText <- List.fold (fun palString next -> palString+"\n"+next) "" (List.map (fun (r,g,b,a) -> [r;g;b] |> List.fold (fun str channel -> str+(Convert.ToString (!*.*->*  channel,16))) "") data.palette.data)
    outputXML.Save writer
    ()