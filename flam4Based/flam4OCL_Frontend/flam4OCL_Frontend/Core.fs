﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Core

#nowarn "9"

open FlameData
open Helper
open System.Text.RegularExpressions
open System
open System.Runtime.InteropServices
open Cloo
open GPUMem
open FlameMarshal
open GenerateKernel
open CompilerDriver
open StandardKernals
open System.Threading.Tasks
open System.Collections.Concurrent
open System.IO

let standardOCLCompilerOptions = @"-cl-strict-aliasing -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -cl-no-signed-zeros -cl-fast-relaxed-math"



let renderBatchLocalSize = [|64L;1L|]
let renderBatchGlobalSize = [|32L*64L;32L|]
let pointPoolSize = 1024L



(*type flam4Device (platformID,deviceID) =
    let mutable _platform = ComputePlatform.Platforms.[platformID]
    let contextProps = new ComputeContextPropertyList(_platform)
    let mutable _device = _platform.Devices.[deviceID]
    let devList = new System.Collections.Generic.List<ComputeDevice>()
    do devList.Add _device
    let mutable _context = new ComputeContext(devList,contextProps,null,System.IntPtr 0)
    let mutable _queue = new ComputeCommandQueue(_context,_device,ComputeCommandQueueFlags.None)
    interface IDisposable with
        member s.Dispose() = 
            try _queue.Dispose()    with | _ -> ()
            try _context.Dispose()  with | _ -> ()
    member s.platformID = platformID
    member s.deviceID = deviceID
    member s.resetDevice = 
        //Since resetDevice is called when something fails, who knows what state these are in - eat any exceptions
        try _queue.Dispose()    with | _ -> ()
        try _context.Dispose()  with | _ -> ()
        _platform <- ComputePlatform.Platforms.[s.platformID]
        let contextProps = new ComputeContextPropertyList(_platform)
        _device <- _platform.Devices.[s.deviceID]
        let devList = new System.Collections.Generic.List<ComputeDevice>()
        devList.Add _device
        _context <- new ComputeContext(devList,contextProps,null,System.IntPtr 0)
        _queue <- new ComputeCommandQueue(_context,_device,ComputeCommandQueueFlags.None)*)


type Flam4BackEnd (clDevices,clRandSeeds,clDiscardStates,clStateStacks,clStateStackIndices,previewMode) =
    let mutable bufferManager:BufferManager = null
    let mutable bufferPageRedirectionList:ComputeBuffer<int> list = []
    let mutable pointBuffer:ComputeBuffer<float32> list = []
    let mutable flatFlame:ComputeBuffer<float32> list = []
    let mutable xdimension = 0L
    let mutable ydimension = 0L
    let mutable numXformNodes = 0
    let mutable paletteImage:ComputeMemory list = []
    let mutable paletteLength = 0
    let mutable clRenderBatchKernel:ComputeKernel list = []
    let mutable clClearBufferProgram:ComputeProgram []=[||]
    let mutable clPostProcessProgram:ComputeProgram []=[||]
    let mutable clRenderBatchProgram:ComputeProgram [] = [||]

    member this.clDevices = clDevices
    member private this.clRandSeeds = clRandSeeds
    member private this.clDiscardStates = clDiscardStates
    member private this.clStateStacks = clStateStacks
    member private this.clStateStackIndices = clStateStackIndices
    member public this.previewMode = previewMode
    new (deviceOverride,previewMode) =
        let clPlatforms = [for platform in ComputePlatform.Platforms -> platform]
        let clContextProps = clPlatforms |> List.map (fun platform -> new ComputeContextPropertyList(platform))
        let clPlatformContextProps = List.zip clPlatforms clContextProps
        let clPlatsDevsContextProps = clPlatformContextProps |> List.map (fun (platform,contextProps) -> [for device in platform.Devices -> (platform,device,contextProps)]) |> List.fold (fun last next -> last@next) []
        let deviceFilter = if deviceOverride <> [] then ComputeDeviceTypes.All else ComputeDeviceTypes.Gpu
        let clPlatsDevsContextProps = match clPlatsDevsContextProps |> List.filter (fun (plat,dev,props) -> ((int64 deviceFilter)&&&(int64 dev.Type)<>0L) && dev.Available=true) with
                                        | x when x.Length > 0 ->
                                            x
                                        | _ ->
                                            Environment.SetEnvironmentVariable("CPU_IMAGE_SUPPORT","1")
                                            clPlatsDevsContextProps |> List.filter (fun (plat,dev,props) -> dev.Type=ComputeDeviceTypes.Cpu)
        let clPlatsDevsContextProps =   if deviceOverride <> [] then
                                            deviceOverride |> List.map (fun n -> clPlatsDevsContextProps.[n])
                                        else
                                            clPlatsDevsContextProps
        let clDevices = [for platform,device,contextProps in clPlatsDevsContextProps do
                            let devList = new System.Collections.Generic.List<ComputeDevice>()
                            devList.Add device
                            let dev = new System.Collections.ObjectModel.ReadOnlyCollection<ComputeDevice>(devList)
                            let context = new ComputeContext (dev,contextProps,null,System.IntPtr 0)
                            let queueDepth = if platform.Name="ATI Stream" then 1 else 3
                            yield { clDevice = device
                                    clContext = context
                                    clQueue = new ComputeCommandQueue (context, device, ComputeCommandQueueFlags.None)
                                    queueDepth = queueDepth
                                    clEvents = Array.create queueDepth null
                                    compilerService = if Environment.isMono=true then None else Some(new CompilerService(device.Name))
                                    clPlatform = platform
                                    }]
        let randSeeds = [for device in clDevices ->
                            new ComputeBuffer<uint32>(device.clContext, ComputeMemoryFlags.ReadWrite,renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])]
        let discardStates = [for device in clDevices ->
                                new ComputeBuffer<uint32>(device.clContext, ComputeMemoryFlags.ReadWrite,(renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])>>>5)]
        let clStateStacks = [for device in clDevices ->
                                new ComputeBuffer<uint32>(device.clContext, ComputeMemoryFlags.ReadWrite, renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])]
        let clStateStackIndices = [for device in clDevices ->
                                    new ComputeBuffer<uint32>(device.clContext, ComputeMemoryFlags.ReadWrite, (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])>>>5)]
        new Flam4BackEnd(clDevices,randSeeds, discardStates, clStateStacks, clStateStackIndices,previewMode)

    interface System.IDisposable with
        member this.Dispose() =
            if bufferManager <> null then
                (bufferManager:>IDisposable).Dispose()
            for buffer in bufferPageRedirectionList do
                buffer.Dispose()
            for buffer in pointBuffer do
                buffer.Dispose()
            for palette in paletteImage do
                palette.Dispose()
            for program in clRenderBatchProgram do
                program.Dispose()
            for kernel in clRenderBatchKernel do
                kernel.Dispose()
            for buffer in clRandSeeds do
                buffer.Dispose()
            for buffer in this.clStateStacks do
                buffer.Dispose()
            for buffer in this.clStateStackIndices do
                buffer.Dispose()
            for buffer in this.clDiscardStates do
                buffer.Dispose()
            for buffer in flatFlame do
                buffer.Dispose()
            for program in clPostProcessProgram do
                program.Dispose()
            for program in clClearBufferProgram do
                program.Dispose()
            for device in this.clDevices do
                device.clQueue.Dispose()
                device.clContext.Dispose()
                for event in device.clEvents do
                    event.Dispose()
                match device.compilerService with
                | Some(service) ->
                    (service:>IDisposable).Dispose()
                | _ -> ()

///Create the render batch kernal from the flame parameter object
    member this.createKernal (flame:FlameParameter) verbose xdim ydim=
        clRenderBatchProgram <- Array.create (clDevices.Length) (null:ComputeProgram)
        clClearBufferProgram <- Array.create (clDevices.Length) (null:ComputeProgram)
        clPostProcessProgram <- Array.create (clDevices.Length) (null:ComputeProgram)

        let defines = sprintf @"#define NUM_POINTS_PER_NODE %i
#define BLOCK_SIZE 64
#define MAX_ITERATIONS 100
"                       pointPoolSize
        printfn "Compiling..."

        //clBuildProgram doesn't appear to be thread safe!  :-(
        
        bufferManager <- new BufferManager({Width=xdim;Height=ydim;NumChannels=4L;SliceRadius=0L;TotalRadius=0L},this.clDevices,clClearBufferProgram)
        bufferPageRedirectionList <- this.clDevices |> List.map (fun dev -> new ComputeBuffer<int>(dev.clContext,ComputeMemoryFlags.ReadOnly,bufferManager.numSlices*4L))
        let numPages = int bufferManager.numSlices
        if Environment.isMono=true then
            //MONO SPECIFIC CODE //////////////////////////////////////////////////////////////////////////////
            for n in 0..(clDevices.Length-1) do
                let finalCode = if this.previewMode=false then
                                    generateIterateKernel flame clDevices.[n].clDevice (bufferManager.deviceBuffer n).Length
                                else
                                    GeneratePreviewKernel.generateIterateKernel flame clDevices.[n].clDevice (bufferManager.deviceBuffer n).Length ((new FlameClass.Flame(flame,flame.allVars)).varRepository)
                let finalCode = defines + (if clDevices.[n].clPlatform.Name="ATI Stream" then
                                                "#define WARP_SIZE 64\n"
                                            else
                                                "#define WARP_SIZE 32\n") +
                                                (sprintf "#define PAGE_LOOKUP_TABLE_LENGTH %i\n" numPages) + finalCode
                let s = sprintf "Device %i of %i (%s)" (n+1) (clDevices.Length) clDevices.[n].clDevice.Name
                safePrintfn s
                let devList = new System.Collections.Generic.List<ComputeDevice>()
                devList.Add clDevices.[n].clDevice
                let device = new System.Collections.ObjectModel.ReadOnlyCollection<ComputeDevice>(devList)
                
                let compileOptions = if clDevices.[n].clPlatform.Name = "NVIDIA CUDA"
                                        then standardOCLCompilerOptions + " -D NVIDIA"
                                        else standardOCLCompilerOptions
                try
                    clRenderBatchProgram.[n] <- new ComputeProgram(clDevices.[n].clContext,finalCode)
                    clRenderBatchProgram.[n].Build(device,compileOptions,null, System.IntPtr 0)
                    if verbose=true then
                        safePrintfn finalCode
                with
                | :? Cloo.ComputeException ->
                    safePrintfn finalCode
                    safePrintfn (clRenderBatchProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
                try
                    clPostProcessProgram.[n] <- new ComputeProgram(clDevices.[n].clContext,PostProcessKernel)
                    clPostProcessProgram.[n].Build(device,compileOptions,null, System.IntPtr 0)
                with
                | :? Cloo.ComputeException ->
                    safePrintfn (clPostProcessProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
                try
                    clClearBufferProgram.[n] <- new ComputeProgram (clDevices.[n].clContext, ClearBufferKernal)
                    clClearBufferProgram.[n].Build(device,compileOptions,null, System.IntPtr 0)
                with
                | :? Cloo.ComputeException ->
                    safePrintfn (clClearBufferProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
            ///////////////////////////////////////////////////////////////////////////////////////////////////
        else
            // .NET SPECIFIC CODE /////////////////////////////////////////////////////////////////////////////
            let parallelCompile n =
                let finalCode = if this.previewMode=false then
                                    generateIterateKernel flame clDevices.[n].clDevice (bufferManager.deviceBuffer n).Length
                                else
                                    GeneratePreviewKernel.generateIterateKernel flame clDevices.[n].clDevice (bufferManager.deviceBuffer n).Length ((new FlameClass.Flame(flame,flame.allVars)).varRepository)
                let finalCode = defines + (if clDevices.[n].clPlatform.Name="ATI Stream" then
                                                "#define WARP_SIZE 64\n"
                                            else
                                                "#define WARP_SIZE 32\n") +
                                                (sprintf "#define PAGE_LOOKUP_TABLE_LENGTH %i\n" numPages) + finalCode
                let s = sprintf "Device %i of %i (%s)" (n+1) (clDevices.Length) clDevices.[n].clDevice.Name
                safePrintfn s
                let devList = new System.Collections.Generic.List<ComputeDevice>()
                devList.Add clDevices.[n].clDevice
                let device = new System.Collections.ObjectModel.ReadOnlyCollection<ComputeDevice>(devList)
                try
                    let binList = new System.Collections.Generic.List<byte[]>()
                    binList.Add (clDevices.[n].compilerService.Value.Compile(finalCode))
                    let binary = new System.Collections.ObjectModel.ReadOnlyCollection<byte[]>(binList)
                    clRenderBatchProgram.[n] <- new ComputeProgram(clDevices.[n].clContext,binList,devList)
                    clRenderBatchProgram.[n].Build(null,null,null, System.IntPtr 0)
                    
                    //File.WriteAllBytes("binDump.raw",binary.Item(0))

                    if verbose=true then
                        safePrintfn finalCode
                with
                | :? Cloo.ComputeException ->
                    safePrintfn finalCode
                    safePrintfn (clRenderBatchProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
                try
                    let binList = new System.Collections.Generic.List<byte[]>()
                    binList.Add (clDevices.[n].compilerService.Value.Compile(PostProcessKernel))
                    let binary = new System.Collections.ObjectModel.ReadOnlyCollection<byte[]>(binList)
                    clPostProcessProgram.[n] <- new ComputeProgram(clDevices.[n].clContext,binList,devList)
                    clPostProcessProgram.[n].Build(null,null,null,System.IntPtr 0)
                with
                | :? Cloo.ComputeException ->
                    safePrintfn (clPostProcessProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
                try
                    let binList = new System.Collections.Generic.List<byte[]>()
                    binList.Add (clDevices.[n].compilerService.Value.Compile(ClearBufferKernal))
                    let binary = new System.Collections.ObjectModel.ReadOnlyCollection<byte[]>(binList)
                    clClearBufferProgram.[n] <- new ComputeProgram (clDevices.[n].clContext, binary, device)
                    clClearBufferProgram.[n].Build(null,null,null, System.IntPtr 0)
                with
                | :? Cloo.ComputeException ->
                    safePrintfn (clClearBufferProgram.[n].GetBuildLog(clDevices.[n].clDevice))
                | _ ->
                    reraise()
            
            let _ = Parallel.For(0,clDevices.Length,new Action<int>(parallelCompile))
            ()
            ///////////////////////////////////////////////////////////////////////////////////////////////////
        
        ()

    
///This should resize the viewport and point buffers as needed and initialize them
    member this.startFrame flame xdim ydim=
        let flatFlm = new FlameClass.Flame(flame,flame.allVars)
        xdimension <- xdim
        ydimension <- ydim
        if (flame.xformNodeRepo.Values.Count <> numXformNodes) then do
            numXformNodes <- flame.xformNodeRepo.Values.Count
            pointBuffer <- [for device in clDevices ->
                            new ComputeBuffer<float32>(device.clContext, ComputeMemoryFlags.ReadWrite, 16L*pointPoolSize*int64 numXformNodes)]
            if this.previewMode=false then
                                flatFlame <- [for device in clDevices ->
                                                new ComputeBuffer<float32>(device.clContext, ComputeMemoryFlags.ReadOnly, int64 flame.size)]
            else
                                flatFlame <- [for device in clDevices ->
                                                    new ComputeBuffer<float32>(device.clContext, ComputeMemoryFlags.ReadOnly, int64 flatFlm.flame.Length)]
            for n in 0..clDevices.Length-1 do
                let rnd = new System.Random()
                let seedPoints = Marshal.AllocHGlobal(4*4*(int pointPoolSize)*numXformNodes)
                let seedPointsPtr = NativeInterop.NativePtr.ofNativeInt<int32> seedPoints
                for i in 0..(4*(int pointPoolSize)*numXformNodes)-1 do
                    let nextVal = if (i%4)=3 then 
                                    rnd.Next()
                                  else 
                                    (float32) (rnd.NextDouble()) |> (!*.*->*)
                    NativeInterop.NativePtr.write (NativeInterop.NativePtr.add seedPointsPtr i) nextVal
                clDevices.[n].clQueue.Write(pointBuffer.[n],
                                true,
                                0L,
                                4L*pointPoolSize*int64 numXformNodes,
                                seedPoints,
                                null)
                clDevices.[n].clQueue.Finish()
                Marshal.FreeHGlobal seedPoints
            if flame.palette.data.Length <> paletteLength then do
                for palette in paletteImage do
                    if palette <> null then do
                        palette.Dispose()
                paletteImage <- [for device in clDevices ->
                                    if device.clDevice.ImageSupport=true then
                                        new ComputeImage2D( device.clContext,
                                                            ComputeMemoryFlags.ReadOnly,
                                                            new ComputeImageFormat(ComputeImageChannelOrder.Rgba,ComputeImageChannelType.Float),
                                                            flame.palette.data.Length, 1,
                                                            0L,
                                                            nativeint 0):>ComputeMemory
                                    else
                                        new ComputeBuffer<float32>( device.clContext,
                                                                    ComputeMemoryFlags.ReadOnly,
                                                                    int64 (flame.palette.data.Length*4)):>ComputeMemory]
                paletteLength <- flame.palette.data.Length
            clRenderBatchKernel <- [for program in clRenderBatchProgram ->
                                    program.CreateKernel("renderBatch")]
            for n in 0..clDevices.Length-1 do
                let rnd = new System.Random()
                let seeds = Marshal.AllocHGlobal(4*(int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])))
                let seedsPtr = NativeInterop.NativePtr.ofNativeInt<int32> seeds
                for i in 0..int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])-1 do
                    NativeInterop.NativePtr.write (NativeInterop.NativePtr.add seedsPtr i) (rnd.Next())
                let linkOffset = uint32 flatFlm.firstLinkOffset
                let stacks = Marshal.AllocHGlobal(4*int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1]))
                let stacksPtr = NativeInterop.NativePtr.ofNativeInt<uint32> stacks
                for i in 0..int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1])-1 do
                    NativeInterop.NativePtr.write (NativeInterop.NativePtr.add stacksPtr i) (if this.previewMode=false then
                                                                                                0u
                                                                                             else
                                                                                                linkOffset)
                let states = Marshal.AllocHGlobal(4*int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1]/32L))
                let statesPtr = NativeInterop.NativePtr.ofNativeInt<uint32> states
                for i in 0..int (renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1]/32L)-1 do
                    NativeInterop.NativePtr.write (NativeInterop.NativePtr.add statesPtr i) 0u
                let flatFlameArray = if this.previewMode=false then
                                        flattenFlame flame
                                     else
                                        flatFlm.flame
                let flatFlameArrayNative = Marshal.AllocHGlobal(4*flatFlameArray.Length)
                Marshal.Copy(flatFlameArray,0,flatFlameArrayNative,flatFlameArray.Length)
                clDevices.[n].clQueue.Write(clRandSeeds.[n],
                                            true,
                                            0L,
                                            renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1],
                                            seeds,
                                            null)
                clDevices.[n].clQueue.Write(clStateStacks.[n],
                                            true,
                                            0L,
                                            renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1],
                                            stacks,
                                            null)
                clDevices.[n].clQueue.Write(clStateStackIndices.[n],
                                            true,
                                            0L,
                                            renderBatchGlobalSize.[0]*renderBatchGlobalSize.[1]/32L,
                                            states,
                                            null)
                clDevices.[n].clQueue.Write(flatFlame.[n],
                                            true,
                                            0L,
                                            int64 flatFlame.[n].Count,
                                            flatFlameArrayNative,
                                            null)
                clDevices.[n].clQueue.Finish()
                Marshal.FreeHGlobal seeds
                Marshal.FreeHGlobal stacks
                Marshal.FreeHGlobal states
                Marshal.FreeHGlobal flatFlameArrayNative
            let palette = [| for item in flame.palette.data do
                                let r,g,b,a = item
                                yield r
                                yield g
                                yield b
                                yield a |]
            let palettePtr = NativeInterop.NativePtr.stackalloc (palette.Length*sizeof<float32>)
            let mutable index = 0
            for n in palette do
                NativeInterop.NativePtr.set palettePtr index n
                index <- index+1
            for n in 0..clDevices.Length-1 do
                if paletteImage.[n] :? ComputeImage then
                    clDevices.[n].clQueue.Write((paletteImage.[n]:?>ComputeImage),false,new SysIntX3(0L,0L,0L),new SysIntX3(int64 paletteLength,1L,1L),0L,0L,NativeInterop.NativePtr.toNativeInt palettePtr,null)
                else
                    clDevices.[n].clQueue.Write((paletteImage.[n]:?>ComputeBuffer<float32>),
                                                    true,
                                                    0L,
                                                    int64 paletteLength,
                                                    NativeInterop.NativePtr.toNativeInt palettePtr,
                                                    null)
                clDevices.[n].clQueue.Finish()
            for device in clDevices do
                for n in 0..device.queueDepth-1 do
                    device.clEvents.[n] <- device.clQueue.AddMarker() :> ComputeEventBase
            for device in clDevices do
                device.clQueue.Flush()
        ()

///Perform postprocessing
    member this.finishFrame flame (numIters:float32)=
        bufferManager.resetBatchCount()
        let pages = bufferManager.getSinglePage {IsInput=true;IsOutput=true;OffLoadFunction=bufferManager.offLoadBuffersAccum} 1 0
        let pages = ref pages        
        let kernel = clPostProcessProgram.[0].CreateKernel("postProcess")
        while (!pages).Length<>0 do
            //kernel void postProcess(global float4* buffer, int xDim, int yDim, int pageSize, float xDimension, float yDimension, float brightness, float gamma, float vibrancy,float numIters)
            let t = flame.viewport.minimumCoverage
            let xr = sqrt(t.a*t.a+t.b*t.b+t.c*t.c)
            let yr = sqrt(t.e*t.e+t.f*t.f+t.g*t.g)
            kernel.SetMemoryArgument(0,(bufferManager.deviceBuffer 0).[0])
            kernel.SetValueArgument(1,int xdimension)
            kernel.SetValueArgument(2,int ydimension)
            kernel.SetValueArgument(3,int bufferManager.pageSize)
            kernel.SetValueArgument(4,xr*2.0f)
            kernel.SetValueArgument(5,yr*2.0f)
            kernel.SetValueArgument(6,flame.gamut.Brightness)
            kernel.SetValueArgument(7,flame.gamut.Gamma)
            kernel.SetValueArgument(8,flame.gamut.Vibrancy)
            kernel.SetValueArgument(9,numIters)
            let localSize = [|32L; 4L|]
            let globalSize = [| 2048L;
                                2048L|]
            printfn "Post processing page %i of %i" ((!pages).[0].AbsolutePageNumber+1) bufferManager.numSlices
            clDevices.[0].clQueue.Execute(kernel, null, globalSize,localSize,null) |> ignore
            clDevices.[0].clQueue.Finish()
            let nextpages = bufferManager.getSinglePage {IsInput=true;IsOutput=true;OffLoadFunction=bufferManager.offLoadBuffersReplace} 1 0
            pages := nextpages
        kernel.Dispose()
        ()

///Enqueue one render batch to the first available GPU
    member this.renderBatch flame batchNum (numBatches:int64) =
         
        //kernel void renderBatch(global volatile float *renderBuffer1,global unsigned int* randSeeds, global unsigned int* discardStates, global volatile float *pointPool, global int* stateStacks, global int* oldStateIndices, global int* pageLookups, constant float* flameBuffer, @palette@, int xdim, int ydim, int palLength, int numPages, int pageSize)
        let mutable accepted = -1
        let mutable pagesCount=0
        while accepted = -1 do
            for n in 0..clDevices.Length-1 do
                for m in 0..clDevices.[n].queueDepth-1 do
                    if ((accepted = -1)&&((clDevices.[n].clEvents.[m].Status=ComputeCommandExecutionStatus.Complete))) then
                        let pages,newPages = bufferManager.getPageSet {IsInput=false;IsOutput=true;OffLoadFunction=bufferManager.offLoadBuffersAccum} (int numBatches) n
                        if pages.Length <> 0 then
                            pagesCount <- pagesCount + pages.Length
                            let pageRedir = NativeInterop.NativePtr.stackalloc (4*int bufferManager.numSlices)
                            for i in 0..int (bufferManager.numSlices)-1 do
                                NativeInterop.NativePtr.write (NativeInterop.NativePtr.add pageRedir i) -1
                            for page in pages do
                                NativeInterop.NativePtr.write (NativeInterop.NativePtr.add pageRedir (page.AbsolutePageNumber)) page.MappedGpuPageSlot
                            let pos = try
                                        (System.Console.CursorLeft, System.Console.CursorTop)
                                        with
                                        | :? System.IO.IOException ->
                                        (0,0)
                            let output = sprintf "Batch %i of %i" (batchNum+pagesCount) (numBatches*bufferManager.numSlices)
                            System.Console.Write output
                            try
                                System.Console.CursorVisible <- false
                                System.Console.SetCursorPosition pos
                            with
                            | :? System.IO.IOException ->
                                printfn ""
                            try
                                if newPages=true then
                                    clDevices.[n].clQueue.Write(bufferPageRedirectionList.[n],
                                                                false,
                                                                0L,
                                                                int64 (4*int bufferManager.numSlices),
                                                                NativeInterop.NativePtr.toNativeInt pageRedir,
                                                                null)
                                let event = new Collections.ObjectModel.Collection<ComputeEventBase>()
                                clRenderBatchKernel.[n].SetMemoryArgument(0,clRandSeeds.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(1,clDiscardStates.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(2,pointBuffer.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(3,clStateStacks.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(4,clStateStackIndices.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(5,bufferPageRedirectionList.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(6,flatFlame.[n])
                                clRenderBatchKernel.[n].SetMemoryArgument(7,paletteImage.[n])
                                clRenderBatchKernel.[n].SetValueArgument(8,int xdimension)
                                clRenderBatchKernel.[n].SetValueArgument(9,int ydimension)
                                clRenderBatchKernel.[n].SetValueArgument(10,int paletteImage.[n].Size/16)
                                clRenderBatchKernel.[n].SetValueArgument(11,int bufferManager.numSlices)
                                clRenderBatchKernel.[n].SetValueArgument(12,int bufferManager.pageSize)
                                for bufferIndex in 0..(bufferManager.deviceBuffer n).Length-1 do
                                    clRenderBatchKernel.[n].SetMemoryArgument(13+bufferIndex,(bufferManager.deviceBuffer n).[bufferIndex])
                                clDevices.[n].clQueue.Execute(clRenderBatchKernel.[n],null,renderBatchGlobalSize,renderBatchLocalSize,event)
                                if clDevices.[n].clEvents.[m]<>null then
                                    clDevices.[n].clEvents.[m].Dispose()    //Avoid memory leak
                                clDevices.[n].clEvents.[m] <- event.Item(0)
                                accepted<-n
                                //let output = sprintf "rendering on device %i of %i (%s)\n" (n+1) clDevices.Length clDevices.[n].clDevice.Name
                                //System.Console.Write output
                                try
                                    clDevices.[accepted].clQueue.Flush()                  
                                with
                                :? Cloo.OutOfResourcesComputeException ->
                                    printfn "Out of resources error on device %i (%s) during clFlush!" accepted clDevices.[accepted].clDevice.Name
                                    reraise()
                            with
                            :? Cloo.OutOfResourcesComputeException ->
                                printfn "Out of resources error on device %i (%s) during renderBatch" n clDevices.[n].clDevice.Name
                                reraise()
                        else
                            accepted <- n
        pagesCount

    member this.flushPages() =
        for device in 0..clDevices.Length-1 do
            clDevices.[device].clQueue.Finish()
            let _ =  bufferManager.offLoadBuffersAccum device
            ()
        for device in 0..clDevices.Length-1 do
            bufferManager.finishAsyncTasks device

///Extract the image from GPU memory and write it to a PNG
    member this.writePng fileName =    
        printfn "%A" fileName
        ExportPNG.writeImage (bufferManager.outputBuffers ()) xdimension ydimension fileName