﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Helper

open System
open System.Xml
open System.Threading

///as_float(int x)
let (!*->*.*) (x:int) =
    (BitConverter.GetBytes x,0) |> BitConverter.ToSingle

///as_int(float x)
let (!*.*->*) (x:float32) =
    (BitConverter.GetBytes x,0) |> BitConverter.ToInt32

type ('a,'b) dict = System.Collections.Generic.Dictionary<'a,'b>

let (<+) (parent:XmlNode) (child:XmlNode) =
    match child with
    | :? XmlAttribute as child->
        parent.Attributes.Append child :> XmlNode
    | _ ->
        parent.AppendChild child

let (@?>) (node:XmlNode) (name,defaultVal) =
    match node.Attributes.GetNamedItem(name) with
    | null ->
        defaultVal
    | attr ->
        attr.Value

let (@!>) (node:XmlNode) (name) =
    match node.Attributes.GetNamedItem(name) with
    | null ->
        failwithf "Missing required attribute: %s" name
    | attr ->
        attr.Value

let (@?) (node:XmlNode) (name) =
    match node.Attributes.GetNamedItem(name) with
    | null ->
        false
    | _ ->
        true

type TaskUnion =
    | Task of (unit->obj)
    | Done

type Serial() =
    let taskPool = new System.Collections.Concurrent.ConcurrentQueue<TaskUnion>()
    let workerThread = new Thread(fun () ->
                                            let mutable finished = false
                                            while finished = false do
                                                match taskPool.TryDequeue() with
                                                | true,Task(task) -> 
                                                    let _ = task()
                                                    ()
                                                | _,Done ->
                                                    finished <- true
                                                | _ ->
                                                    Thread.Sleep(1)
                                            )
    do workerThread.Start()
    member s.Execute func =
        taskPool.Enqueue (Task (func))
    interface System.IDisposable with
        member s.Dispose() =
            taskPool.Enqueue(Done)

let serial = new Serial()

let safePrintfn output =
    serial.Execute (fun () -> (printfn "%s" output):>obj)

type AfterwordBuilder() =
    member this.Bind(x,rest) = rest();x()
    member this.Delay(f) = f()
    member this.Return(x) = x
    member this.Zero() = ()
