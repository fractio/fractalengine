﻿
//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module LoadFlam4
open System
open System.IO
open System.Globalization
open System.Xml
open System.Text.RegularExpressions
open FlameData
open Helper

let MangleName (name:string)=
    let oddCharRegex=new Regex("[^a-zA-Z_]")
    let rec pad (str:string) = 
        if str.Length < 4 then
            pad ("0"+str)
        else
            str
    let converter = new MatchEvaluator(fun mat -> (mat.Value |> char |> int16,16) |> Convert.ToString |> pad |> ( fun s-> "$"+s))
    oddCharRegex.Replace(name,converter)

let UnMangleName (name:string)=
    let escapeRX = new Regex("\$....")
    let converter = new MatchEvaluator(fun mat -> (mat.Value.Substring(1),16) |> Convert.ToInt16 |> char |> string)
    escapeRX.Replace(name,converter)
    
/// This reads an affine parameter from the XML and returns (cr, ctheta, xr, xtheta, yr, ytheta)
let ReadAffineParams (affineXML:XmlNode) =
    let q x = float32 <| affineXML @?> (x,"0")        
    let polarToAffine (xr,xtheta,xphi,yr,ytheta,yphi,zr,ztheta,zphi,cr,ctheta,cphi) =
        {
            a=xr*cos(xtheta)*cos(xphi);
            b=xr*sin(xtheta)*cos(xphi);
            c=xr*sin(xphi);

            d=cr*cos(ctheta)*cos(cphi);

            e=yr*cos(ytheta)*cos(yphi);
            f=yr*sin(ytheta)*cos(yphi);
            g=yr*sin(yphi);

            h=cr*sin(ctheta)*cos(cphi)

            i=zr*cos(ztheta)*cos(zphi)
            j=zr*sin(ztheta)*cos(zphi)
            k=zr*sin(zphi)

            l=cr*sin(zphi)
        }
    match affineXML.Attributes.Item(0).Name with
    | "abcdef" | "adbecf" | "abcdefghijkl" | "aeibfjcgkdhl" | "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" ->
        /// This converts from affine coefs a, b, c, d, e, f to polar coefs cr, ctheta, xr, xtheta, yr, ytheta
        (*let AffineToPolar (a,b,c,d,e,f,g,h,i,j,k,l) =
            {   cr = sqrt(d*d+h*h+l*l); 
                ctheta = atan2 h d; 
                cphi = atan2 l (sqrt(d*d+h*h));
                xr = sqrt(a*a+b*b+c*c);
                xtheta = atan2 b a;
                xphi = atan2 c (sqrt(b*b+a*a));
                yr = sqrt(e*e+f*f+g*g);
                ytheta = atan2 f e;
                yphi = atan2 g (sqrt(e*e+f*f));
                zr = sqrt(i*i+j*j+k*k);
                ztheta = atan2 j i;
                zphi = atan2 k (sqrt(i*i+j*j));   }*)
        match affineXML.Attributes.Item(0).Name with
        | "abcdef" ->
            let coefs =
                (affineXML @!> "abcdef").Split ' '
                |> Array.map float32
            let a,b,c,d,e,f = (coefs.[0], coefs.[1], coefs.[2], coefs.[3], coefs.[4], coefs.[5])
            {a=a;b=b;c=0.0f;d=c;e=d;f=e;g=0.0f;h=f;i=0.0f;j=0.0f;k=1.0f;l=0.0f}
        | "adbecf" ->
            let coefs =
                (affineXML @!> "adbecf").Split ' '
                |> Array.map float32
            let a,d,b,e,c,f = (coefs.[0], coefs.[1], coefs.[2], coefs.[3], coefs.[4], coefs.[5])
            {a=a;b=b;c=0.0f;d=c;e=d;f=e;g=0.0f;h=f;i=0.0f;j=0.0f;k=1.0f;l=0.0f}
        | "abcdefghijkl" ->
            let coefs = 
                (affineXML @!> "abcdefghijkl").Split ' '
                |> Array.map float32
            let a,b,c,d,e,f,g,h,i,j,k,l = (coefs.[0], coefs.[1], coefs.[2], coefs.[3], coefs.[4], coefs.[5], coefs.[6], coefs.[7], coefs.[8], coefs.[9], coefs.[10], coefs.[11])
            {a=a;b=b;c=c;d=d;e=e;f=f;g=g;h=h;i=i;j=j;k=k;l=l}
         | "aeibfjcgkdhl" ->
            let coefs = 
                (affineXML @!> "aeibfjcgkdhl").Split ' '
                |> Array.map float32
            let a,e,i,b,f,j,c,g,k,d,h,l = (coefs.[0], coefs.[1], coefs.[2], coefs.[3], coefs.[4], coefs.[5], coefs.[6], coefs.[7], coefs.[8], coefs.[9], coefs.[10], coefs.[11])
            {a=a;b=b;c=c;d=d;e=e;f=f;g=g;h=h;i=i;j=j;k=k;l=l}
        | "a" | "b" | "c" | "d" | "e" | "f" | "g" | "h" | "i" | "j" | "k" | "l" ->
            if affineXML.Attributes.GetNamedItem("g")=null then
                let a,b,c,d,e,f = (q"a",q"b",q"c",q"d",q"e",q"f")
                {a=a;b=b;c=0.0f;d=c;e=d;f=e;g=0.0f;h=f;i=0.0f;j=0.0f;k=1.0f;l=0.0f}
            else
                let a,b,c,d,e,f,g,h,i,j,k,l = (q"a",q"b",q"c",q"d",q"e",q"f",q"g",q"h",q"i",q"j",q"k",q"l")
                {a=a;b=b;c=c;d=d;e=e;f=f;g=g;h=h;i=i;j=j;k=k;l=l}

        |  _ ->
            failwith("Invalid affine parameters: \n" + affineXML.InnerXml)
    | "cr" | "xr" | "yr" | "zr" | "ctheta" | "xtheta" | "ytheta" | "ztheta" | "cphi" | "xphi" | "yphi" | "zphi"->
        let cr = q"cr"
        let ctheta = q"ctheta"*float32(Math.PI/180.)
        let cphi = q"cphi"*float32(Math.PI/180.)
        let xr = q"xr"
        let xtheta = q"xtheta"*float32(Math.PI/180.)
        let xphi = q"xphi"*float32(Math.PI/180.)
        let yr = q"yr"
        let ytheta = q"ytheta"*float32(Math.PI/180.)
        let yphi = q"yphi"*float32(Math.PI/180.)
        let zr = q"zr"
        let ztheta = q"ztheta"*float32(Math.PI/180.)
        let zphi = q"zphi"*float32(Math.PI/180.)
        polarToAffine (xr,xtheta,xphi,yr,ytheta,yphi,zr,ztheta,zphi,cr,ctheta,cphi)
    | "center" | "size" | "rotation" | "skew" ->
        let c = 
            (affineXML @!> "center").Split ' '
            |> Array.map float32
        let x,y = (c.[0],c.[1])
        let d =
            (affineXML @!> "size").Split ' '
            |> Array.map float32
        let dx,dy = (d.[0],d.[1])
        let rot = float32(Math.PI/180.)*float32(affineXML @!> "rotation")
        let skew = float32(Math.PI/2.)*(float32(affineXML @!> "skew")+1.0f)
        let cr = sqrt(x*x+y*y)
        let ctheta = atan2 y x
        let cphi = 0.0f
        let xr = dx/2.f
        let xtheta = rot
        let xphi = 0.0f
        let yr = dy/2.f
        let ytheta = rot+skew
        let yphi = 0.0f
        let zr = 1.0f
        let ztheta = 0.0f
        let zphi = (float32 Math.PI)/2.0f
        polarToAffine (xr,xtheta,xphi,yr,ytheta,yphi,zr,ztheta,zphi,cr,ctheta,cphi)
    | "position" | "lookat" | "roll"->
        let position = (affineXML @!> "position").Split ' ' |> Array.map float32
        let lookat = (affineXML @!> "lookat").Split ' ' |> Array.map float32
        let getangles x y z =
            (atan2 y x,atan2 z (sqrt(x*x+y*y)))
        let direction = Array.map2 (fun pos look -> look-pos) position lookat
        let theta,phi = getangles direction.[0] direction.[1] direction.[2]
        let rotation = affineXML @!> "roll" |> float32 |> (*)(float32 Math.PI / 180.0f)
        let cr = (Array.map (fun x -> x*x) position |> Array.sum |> sqrt);
        let ctheta = atan2 position.[1] position.[0]
        let cphi = atan2 position.[2] (sqrt(position.[0]*position.[0]+position.[1]*position.[1]))
        let xr = 1.0f
        let xtheta = theta-float32 Math.PI/2.0f
        let xphi = 0.0f
        let yr = 1.0f
        let ytheta = theta
        let yphi = phi+float32 Math.PI/2.0f
        let zr = 1.0f
        let ztheta = theta
        let zphi = phi
        polarToAffine (xr,xtheta,xphi,yr,ytheta,yphi,zr,ztheta,zphi,cr,ctheta,cphi)
    |  _ ->
        failwith("Invalid affine parameters: \n" + affineXML.OuterXml)
    
/// This preprocesses and optimizes a code segment for incorporating into the rendering kernal(s)
let ProcessCode (code:String) =
    let MathFuncrx = new Regex @"(?<![\w_$])(?>(?<Op>(cos|exp|exp2|exp10|log|log2|powr|rsqrt|sin|sqrt|tan)))(?!([A-Za-z0-9_$]))" //Find math functions with native equivalents
    let mutable Floatrx = new Regex @"(?i:(?<![\w_$.])(?<Num>(\.\d+|\d+\.\d*)e?-?\d*|\d*e-?\d+)(?![fe\d]))" //Find floating point literals not in single precision
    Floatrx.Replace(MathFuncrx.Replace(code,@"native_${Op}"),@"${Num}f")


/// This builds the code repository based on the inputed var nodes
let readVarCodeRepo(codeXML:XmlNodeList) = 
    let dict = new System.Collections.Generic.Dictionary<string,VariationDef>()
    for outerNode:XmlNode in codeXML do
        for node:XmlNode in outerNode.SelectNodes("child::var") do
            let name = node @!> "name"
            dict.Add (name,{
                    name = name |> MangleName;
                    code = ProcessCode node.InnerText;
                    parameters = Array.toList ((node@!>"parameters").Split(separator = [|' '|],options = StringSplitOptions.RemoveEmptyEntries))
                    fromInclude = false;
                })
    dict  
        
///This reads a single xform        
let readXform (xformXML:XmlNode) (codeRepo: System.Collections.Generic.Dictionary<string,VariationDef>) (includedXML:XmlDocument list) (inOffset:int)=
    let importVar name=
        let matches = List.collect (fun (inc:XmlDocument) -> [for node:XmlNode in inc.SelectNodes("//code/var") do
                                                                if node @!> "name"=name then
                                                                    yield node]) includedXML
        match matches with
        | [] ->
            failwith ("Missing code: Variation "+name)
        | nodes ->
            let node = nodes.Head
            codeRepo.Add (name,{
                            name = name |> MangleName;
                            code = ProcessCode node.InnerText;
                            parameters = Array.toList ((node@!>"parameters").Split(separator = [|' '|], options = StringSplitOptions.RemoveEmptyEntries));
                            fromInclude= true
                            })
    let mutable ofst = inOffset
    let offset = ref ofst
    {name = xformXML @!> "name" |> MangleName;
    offset = !offset;
    coefs =( xformXML.SelectSingleNode "child::coefs"
           |> ReadAffineParams);
    colorIndex = float32 <| xformXML @!> "color_index";
    colorSpeed = float32 <| xformXML @!> "color_speed";
    vars = [for node:XmlNode in xformXML.SelectNodes("child::var") do
                let name = node @!> "name"
                let test,vd = codeRepo.TryGetValue(name);
                let vd =if not test then
                            importVar name
                            let _,vd = codeRepo.TryGetValue(name)
                            vd
                        else
                            vd
                offset:=(!offset)+1+vd.parameters.Length
                yield {
                        vardef = vd;
                        weight = float32 <| node @!> "weight";
                        parameters = [ for paramName in vd.parameters ->
                                        float32 <| node @!> paramName
                                        ];    }
            ];},!offset+14

///This builds the xform repository
let readXforms (xformXML:XmlNode) includedXML (inOffset:int) (codeRepo:System.Collections.Generic.Dictionary<string,VariationDef>)=
    let xFormRepo = new System.Collections.Generic.Dictionary<string,Xform>()
    let mutable index = 0
    let mutable offset = inOffset
    for (node:XmlNode) in xformXML.SelectNodes("descendant::xform") do
        if node.HasChildNodes then
            let xform,newOffset = readXform node codeRepo includedXML offset
            offset <- newOffset
            xFormRepo.Add(node @!> "name", xform)
            index <- index+1
    xFormRepo,offset


///This builds the xform node repository
let readXformNodes (xFormNodeBaseXML:XmlNode) (xFormRepo:System.Collections.Generic.Dictionary<string,Xform>)=
    let readNode (node:XmlNode) (ind:int)= 
        let name = node.SelectSingleNode("child::xform") @!> "name"
        let _,xform = xFormRepo.TryGetValue(name)
        {
            name = node @!> "name" |> MangleName;
            self = xform;
            index = ind;
            next = [];
        }
    let nodeRepo = new System.Collections.Generic.Dictionary<string,XformNode>()
    let mutable ind = 0
    for node in xFormNodeBaseXML.SelectNodes("descendant::xform_node") do
        match node.FirstChild with
        | null ->
            ()
        | _ ->
            let data = readNode(node)(ind)
            nodeRepo.Add(node @!> "name",data)
            ind<-ind+1
    nodeRepo

///This links together the xform nodes
let rec linkXformNodes (xFormNodeBaseXML:XmlNode) (inOffset:int) (xFormNodeRepo:System.Collections.Generic.Dictionary<string,XformNode>)=
    let mutable offset = inOffset
    let name = xFormNodeBaseXML @!> "name"
    let _,xformnode = xFormNodeRepo.TryGetValue name
    for node:XmlNode in xFormNodeBaseXML.SelectNodes("child::xform_node") do
        let newNode,newOffset = linkXformNodes node offset xFormNodeRepo
        offset<-newOffset
        xformnode.next <- newNode::xformnode.next
    let weight = float32 <| xFormNodeBaseXML @?> ("weight","0")
    let opacity = float32 <| xFormNodeBaseXML @?> ("opacity","1")
    let state,discard = match (xFormNodeBaseXML @?> ("state","hold")).Split(' ') with
                        | [|"push"|] ->
                            1,0
                        | [|"hold"|] ->
                            0,0
                        | [|"pop"|] ->
                            -1,1
                        | [|"pop";x|] ->
                            -int x,1
                        | [|"push";x|] ->
                            int x,0
                        | badValue ->
                            failwith "Invalid state attribute: %A" badValue
    if weight<>0.0f then
        offset <- offset+4
    {next = xformnode;offset=offset-3;weight = weight;opacity = opacity;state = state;discard=discard},offset

let normalizeNodeWeights (xformNodeRepo:System.Collections.Generic.Dictionary<string,XformNode>) =
    for xformnode in xformNodeRepo.Values do
        let mutable weightSum = 0.0f
        for node in xformnode.next do
            weightSum <- weightSum+node.weight
        let weightSum = weightSum
        xformnode.next <- List.map (fun (node) -> {next = node.next; offset=node.offset; weight = node.weight/weightSum; opacity = node.opacity; state = node.state; discard=node.discard}) xformnode.next

let loadPalette (paletteNode:XmlNode) =
    let rawPalette = (
        match paletteNode @!> "format" with
        | "rgb8_hex" ->
            let rx = new Regex @"(?<digit1>[0-9]|[A-F])\s*?(?<digit2>[0-9]|[A-F])"
            let matches = paletteNode.InnerText.ToUpper() |> (rx.Matches)
            [ for mat in matches ->
                (mat.Result "${digit1}${digit2}",16) |> Convert.ToByte |> float32 |> fun x -> x/255.0f]
        | "rgb32_unorm_hex" ->
            let rx = new Regex @"(?<digit1>[0-9]|[A-F])\s*?(?<digit2>[0-9]|[A-F])\s*?(?<digit3>[0-9]|[A-F])\s*?(?<digit4>[0-9]|[A-F])\s*?(?<digit5>[0-9]|[A-F])\s*?(?<digit6>[0-9]|[A-F])\s*?(?<digit7>[0-9]|[A-F])\s*?(?<digit8>[0-9]|[A-F])"
            let matches = paletteNode.InnerText.ToUpper() |> rx.Matches
            [ for mat in matches ->
                (mat.Result "${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}",16) |> Convert.ToInt32 |> (!*->*.*) ]
        | "rgb32_255norm_hex" ->
            let rx = new Regex @"(?<digit1>[0-9]|[A-F])\s*?(?<digit2>[0-9]|[A-F])\s*?(?<digit3>[0-9]|[A-F])\s*?(?<digit4>[0-9]|[A-F])\s*?(?<digit5>[0-9]|[A-F])\s*?(?<digit6>[0-9]|[A-F])\s*?(?<digit7>[0-9]|[A-F])\s*?(?<digit8>[0-9]|[A-F])"
            let matches = paletteNode.InnerText.ToUpper() |> rx.Matches
            [ for mat in matches ->
                (mat.Result "${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}${digit7}${digit8}",16) |> Convert.ToInt32 |> (!*->*.*) |> fun x -> x/255.0f]
        | "rgb_unorm" ->
            paletteNode.InnerText.Split((null:string[]),StringSplitOptions.RemoveEmptyEntries) |> Array.map float32 |> List.ofArray
        | "rgb_255norm"->
            paletteNode.InnerText.Split((null:string[]),StringSplitOptions.RemoveEmptyEntries) |> Array.map float32 |> Array.map (fun x -> x/255.0f) |> List.ofArray
        | other ->
            failwith("Invalid palette format: " + other))
    let rec processPalette (rawPalette:float32 list) acc=
        match rawPalette with
        | [] ->
            acc
        | r::g::b::tail ->
            processPalette tail ((r,g,b,1.0f)::acc)
        | _ ->
            failwith "Invalid palette"
    { data = processPalette rawPalette [] |> List.rev}

let loadGamut (paletteNode:XmlNode) =
    { Brightness = float32 <| paletteNode @!>"brightness";
        Gamma = float32 <| paletteNode @!>"gamma";
        Vibrancy = float32 <| paletteNode @!>"vibrancy" }

let loadViewport (viewportNode:XmlNode) =
    { minimumCoverage = ReadAffineParams <| viewportNode.SelectSingleNode "coefs";
        aspectRatio = float32 <| viewportNode @!>"aspect_ratio" }


let loadFlame (flameNode:XmlNode) (includedXML:XmlDocument list) allVars= 
    let CodeNodes:XmlNodeList = flameNode.SelectNodes("descendant::code")
    let varRep = readVarCodeRepo CodeNodes
    let mutable offset = 0
    let xformRep,newOffset = readXforms flameNode includedXML offset varRep
    offset <- newOffset
    let xformNodeRep = 
        xformRep
        |> readXformNodes flameNode
    let xformNodes = [for node in xformNodeRep.Values -> node]
    for node in flameNode.SelectNodes "child::xform_node" do
        let _,newOffset = xformNodeRep |> linkXformNodes (node) offset
        offset<-newOffset
        ()
    xformNodeRep |> normalizeNodeWeights
    {   viewport = loadViewport (flameNode.SelectSingleNode("child::viewport"):XmlNode);
        size=offset+12+1;                       //Offset starts at 0, but size starts at 1
        gamut = loadGamut (flameNode.SelectSingleNode("child::gamut"):XmlNode);
        xforms = xformNodes;
        palette = loadPalette (flameNode.SelectSingleNode("child::palette"):XmlNode); 
        xformNodeRepo = xformNodeRep;
        xformRepo = xformRep;
        varRepo = [for item in varRep -> item.Value ]
        allVars = allVars}



///This function creates a VariationDef list containing all vars in an XML file stream and its includes
let OpenLibrary (inputFile:Stream) =
    let numFormat = new NumberFormatInfo()
    numFormat.NumberDecimalSeparator <- "."
    numFormat.NumberGroupSeparator <- ","
    let settings = new XmlReaderSettings()
    settings.ConformanceLevel <- ConformanceLevel.Fragment
    use FlameReader = XmlReader.Create(inputFile,settings)
    let FlameXML = new System.Xml.XmlDocument()
    FlameXML.Load FlameReader
    let includes = FlameXML.SelectNodes("descendant::include")
    let allXML = FlameXML::[for inc in includes do
                                    use incReader = XmlReader.Create (inc @!>"library",settings)
                                    let incXML = new System.Xml.XmlDocument()
                                    incXML.Load incReader
                                    yield incXML ]
    let varList = List.collect (fun (xml:XmlDocument) -> [
                                                            for codeNode:XmlNode in xml.SelectNodes("descendant::code") do
                                                                for varNode in codeNode.SelectNodes("child::var") do
                                                                    yield {
                                                                        name = (varNode @!> "name") |> MangleName;
                                                                        code = varNode.InnerText;
                                                                        parameters = Array.toList ((varNode@!>"parameters").Split(separator = [|' '|],options = StringSplitOptions.RemoveEmptyEntries));
                                                                        fromInclude = true
                                                                    }
                                            ]) allXML
    new Map<VariationDef,int>(Seq.zip (List.toSeq(varList)) (seq{0..varList.Length-1}))
///This function creates a flame object from an XML flame Stream
let OpenFlam4 (inputFile:Stream) =
    let allVars = OpenLibrary inputFile
    inputFile.Position <- 0L
    let numFormat = new NumberFormatInfo()
    numFormat.NumberDecimalSeparator <- "."
    numFormat.NumberGroupSeparator <- ","
    let settings = new XmlReaderSettings()
    settings.ConformanceLevel <- ConformanceLevel.Fragment
    use FlameReader = XmlReader.Create(inputFile,settings)
    let FlameXML = new System.Xml.XmlDocument()
    FlameXML.Load FlameReader
    let includes = FlameXML.SelectNodes("descendant::include")
    let includedXML = [for inc in includes do
                        use incReader = XmlReader.Create (inc @!>"library",settings)
                        let incXML = new System.Xml.XmlDocument()
                        incXML.Load incReader
                        yield incXML ]
    let FlameNode:XmlNode = FlameXML.SelectSingleNode "descendant::flame"
    loadFlame FlameNode includedXML allVars