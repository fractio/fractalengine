﻿//Copyright 2011 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module CommandLine

open System
open System.IO
open System.Text

type Commands = {
    conversions : (String*String) list
    inputs : String list
    outputs : String list
    quality : float32
    resolution : int*int
    serverMode : bool
    previewMode : bool
    verbose : bool
    help : bool
    deviceOverride : int list
}



let readCommandLine (args:String list) =
    let rec groupList result current (lst:String list) =
        match lst with
        | str::tail when str.StartsWith("-") -> groupList ((List.rev current)::result) [str] tail
        | str::tail -> groupList result (str::current) tail
        | _ -> List.rev ((List.rev current)::result) |> List.filter (fun item -> item.Length<>0)
    let groups = args
                |> groupList [[]] []
                |> List.map (fun group -> (group.Head.ToLower()::group.Tail))
    let rec accumulateCommands (commands:Commands) groups =
        match groups with
        | head::tail ->    
            match head with
            | "-convert"::input::output::_ | "-c"::input::output::_ ->
                accumulateCommands {commands with conversions=(input,output)::commands.conversions} groups.Tail
            | "-render"::inputs | "-r"::inputs ->
                accumulateCommands {commands with inputs = commands.inputs@inputs} groups.Tail
            | "-output"::outs | "-o"::outs ->
                accumulateCommands {commands with outputs = commands.outputs@outs} groups.Tail
            | "-quality"::number::_ | "-q"::number::_ ->
                accumulateCommands {commands with quality=float32 number} groups.Tail
            | "-resolution"::xdim::ydim::_ | "-res"::xdim::ydim::_->
                accumulateCommands {commands with resolution=(int xdim, int ydim)} groups.Tail
            | "-server"::_ ->
                accumulateCommands {commands with serverMode=true} groups.Tail   
            | "-verbose"::_ ->
                accumulateCommands {commands with verbose=true} groups.Tail
            | "-help"::_ | "--help"::_ |"-?"::_ ->
                accumulateCommands {commands with help=true} groups.Tail
            | "-device"::numbers ->
                let deviceNums = numbers |> List.map (int >> (+) -1)
                accumulateCommands {commands with deviceOverride=deviceNums} groups.Tail
            | "-preview"::_ ->
                accumulateCommands {commands with previewMode=true} groups.Tail
            | _ -> commands
        | _ -> commands
    let commands = {
                    conversions=[]
                    inputs=[]
                    outputs=[]
                    quality=2000.0f
                    resolution=(800,600)
                    serverMode=false
                    previewMode=false
                    verbose=false
                    help=false
                    deviceOverride = []
                    }
    if groups = [] 
    then 
        {commands with help=true} 
    else
        accumulateCommands commands groups