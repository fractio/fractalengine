﻿//Copyright 2010 Steven Brodhead
//
//This file is part of flam4.
//
//flam4 is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//flam4 is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with flam4.  If not, see <http://www.gnu.org/licenses/>.

module Program

open System
open System.IO
open System.IO.Pipes
open System.Text
open Cloo

let standardOCLCompilerOptions = @"-cl-strict-aliasing -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -cl-no-signed-zeros -cl-fast-relaxed-math"

[<STAThread>]
[<EntryPoint>]
let main args =
    //Connect to the pipe
    use pipe = new NamedPipeClientStream(args.[0])
    pipe.Connect()
    pipe.ReadMode <- PipeTransmissionMode.Message
    try
        let readMessage (pipe:NamedPipeClientStream)=
            async {
                let! length = (pipe:>Stream).AsyncRead(sizeof<int>)
                let buf = Array.create(BitConverter.ToInt32 (length,0)) 0uy
                let! msg = (pipe:>Stream).AsyncRead(buf)
                return buf
            } |> Async.RunSynchronously
        let writeMessage (pipe:NamedPipeClientStream) (msg:byte[])=
            let lengthBuf = BitConverter.GetBytes (msg.Length)
            pipe.Write (Array.append lengthBuf msg,0,msg.Length+sizeof<int>)

        let msgToStr buffer =
            buffer |> Encoding.UTF8.GetString

    //set up the OpenCL compiler for the selected device
        let clPlatforms = [for platform in ComputePlatform.Platforms -> platform]
        let clContextProps = clPlatforms |> List.map (fun platform -> new ComputeContextPropertyList(platform))
        let clPlatformContextProps = List.zip clPlatforms clContextProps
        let clDevsContextProps = clPlatformContextProps |> List.map (fun (platform,contextProps) -> [for device in platform.Devices -> (device,contextProps)]) |> List.fold (fun last next -> last@next) []

        let clDevice,clContextProps = (clDevsContextProps |>
                                        (fun devs -> [for (device,ctx) in devs -> (device,ctx)]) |>
                                        List.filter (fun (device,ctx) -> device.Name=args.[1])).[0]
        let devList = new System.Collections.Generic.List<ComputeDevice>()
        devList.Add clDevice
        let dev = new System.Collections.ObjectModel.ReadOnlyCollection<ComputeDevice>(devList)
        let clContext = new ComputeContext(dev,clContextProps,null,System.IntPtr 0)

    //Listen to pipe and compile recieved kernels until the message "END" is recieved
        let rec readLoop() =
            let msg = readMessage pipe |> msgToStr
            if (msg.Length>0)&&(msg<>"END") then
                let clProgram = new ComputeProgram(clContext,msg)
                try
                    let compileOptions = if clContext.Platform.Name = "NVIDIA CUDA"
                                            then standardOCLCompilerOptions + " -D NVIDIA"
                                            else standardOCLCompilerOptions
                    clProgram.Build(dev,compileOptions,null,System.IntPtr 0)
                with
                | :? Cloo.ComputeException ->
                    printfn "%s" (clProgram.GetBuildLog(clDevice))
                | _ ->
                    reraise()
                writeMessage pipe (clProgram.Binaries.Item(0))
                pipe.WaitForPipeDrain()
            if (msg<>"END") then readLoop() else () //Tailcall to simulate while loop

            
        readLoop()
    with
    | :? IOException ->
        ()
    | _ ->
        reraise()
    pipe.Close()
    0
