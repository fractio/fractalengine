//
//  FlameParse.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//
#import "FlameParse.h"

#define COPY_XFORM_ATTR(FOO) dest->FOO = [self attrAsFloat:attrs key:@#FOO defaultValue:0.f]

@implementation FlameParse

// enforce the requirement for unique frame names by appending a counter value to the non-unique frame names
+ (void)uniqueFrameNames:(flameParameterList *)flameData
{
	NSMutableDictionary *nameDict = [NSMutableDictionary dictionaryWithCapacity:60];
	NSMutableDictionary *useDict  = [NSMutableDictionary dictionaryWithCapacity:60];
	
	// build frame name dictionary with count of name usage
	flameParameterList * link = flameData;
	while (link) {
		NSString *name = link->flameMeta->name;
		NSNumber *count = [nameDict objectForKey:name];
		if (count) {
			count = [NSNumber numberWithUnsignedInt:[count unsignedIntValue] + 1];
			[nameDict setObject:count forKey:name];
		}
		else {
			count = [NSNumber numberWithUnsignedInt:1];
			[nameDict setObject:count forKey:name];
			[useDict setObject:count forKey:name];			
		}
		link = link->nextFlame;
	}
	
	// modify the names for non-unique names by appending a index for the non-unique names
	link = flameData;
	while (link) {
		NSString *name = link->flameMeta->name;
		int count = [[nameDict objectForKey:name] intValue];
		if (count > 1) {
			unsigned use = [[useDict objectForKey:name] unsignedIntValue];
			[useDict setObject:[NSNumber numberWithUnsignedInt:use + 1] forKey:name];
			NSString *newName = [NSString stringWithFormat:@"%@_%u", name, use];
			[name release];
			link->flameMeta->name = [newName copy];
		}
		link = link->nextFlame;
	}
}

// get the <palette>'s number attribute value
+ (int)getPalIndex:(NSArray *)palette index:(int)i
{
	NSXMLElement *paletteEle = [palette objectAtIndex:i];
	NSXMLNode *attr = [paletteEle attributeForName:@"number"];
	return [[attr objectValue] integerValue];
}

// convert <flame> attributes to a dictionary format
+ (NSDictionary *)attrsAsDict:(NSXMLElement *) ele
{
	NSArray * attrs = [ele attributes];
	int attrCount = [attrs count];
	NSMutableArray *keys = [[NSMutableArray alloc] initWithCapacity:30];
	for (int i = 0; i < attrCount; i++) {
		NSXMLNode *attr = [attrs objectAtIndex:i];
		NSString *name = [attr name];
		[keys addObject:name];
	}
	
	NSDictionary *attrDict = [NSDictionary dictionaryWithObjects:attrs forKeys:keys];
	[keys release];
	return attrDict;
}

// split a string using its embedded whites space - drops out any zero length strings
+ (NSMutableArray *)splitOnSpaces: (NSString *)str
{
	NSArray *arr         = [str componentsSeparatedByString:@" "];
	NSMutableArray *copy = [[NSMutableArray alloc] initWithCapacity: [arr count]];
	int i = 0;
	for (NSString * str in arr) {
		if ([str length] > 0) {
			[copy addObject:[arr objectAtIndex:i++]];
		}
	}
	return [copy autorelease];
}

// get the attribute's value as a int given the attributes name
+ (int)attrAsInt:(NSDictionary *)attrDict key:(NSString *)key
{
	return [[[attrDict objectForKey:key] objectValue] integerValue];
}

// get the attribute's value as a int given the attributes name, if not found return the defaultValue
+ (int)attrAsInt:(NSDictionary *)attrDict key:(NSString *)key defaultValue:(int)val
{
	NSXMLNode *node = [attrDict objectForKey:key];
	return node ? [[node objectValue] integerValue] : val;
}

// get the attribute's value as a float given the attributes name
+ (float)attrAsFloat:(NSDictionary *)attrDict key:(NSString *)key
{
	return [[[attrDict objectForKey:key] objectValue] floatValue];
}

// get the attribute's value as a float given the attributes name, if not found return the defaultValue
+ (float)attrAsFloat:(NSDictionary *)attrDict key:(NSString *)key defaultValue:(float)val
{
	NSXMLNode *node = [attrDict objectForKey:key];
	return node ? [[node objectValue] floatValue] : val;
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
+ (NSString *)attrAsString:(NSDictionary *)attrDict key:(NSString *)key
{
	return [[attrDict objectForKey:key] objectValue];
}

// get the attribute's value as a string given the attributes name, if not found return the defaultValue
+ (NSString *)attrAsString:(NSDictionary *)attrDict key:(NSString *)key defaultValue:(NSString *)val
{
	NSXMLNode *node = [attrDict objectForKey:key];
	return node ? [node objectValue] : val;
}

//This is called by OpenFlame to parse xforms.
+ (void) extractXform:(xForm *) dest form:(unAnimatedxForm *) dest2 dict:(NSDictionary *)attrs
{
	NSMutableArray *coefs = [self splitOnSpaces:[[attrs objectForKey:@"coefs"] objectValue]];
	dest->a = [[coefs objectAtIndex:0] floatValue];
	dest->d = [[coefs objectAtIndex:1] floatValue];
	dest->b = [[coefs objectAtIndex:2] floatValue];
	dest->e = [[coefs objectAtIndex:3] floatValue];
	dest->c = [[coefs objectAtIndex:4] floatValue];
	dest->f = [[coefs objectAtIndex:5] floatValue];
	
	if (dest2 != nullptr)
	{
		dest2->a = dest->a;
		dest2->b = dest->b;
		dest2->d = dest->d;
		dest2->e = dest->e;
	}
	
	dest->weight = [self attrAsFloat:attrs key:@"weight" defaultValue:0.f];
	NSMutableArray *color = [self splitOnSpaces:[[attrs objectForKey:@"color"] objectValue]];
	dest->color = [[color objectAtIndex:0] floatValue];
	dest->symmetry = [self attrAsFloat:attrs key:@"symmetry" defaultValue:0.f];
	if (dest2 != nullptr)
	{
		if (dest->symmetry > 0.0f)
			dest2->rotates = 0;
		else
			dest2->rotates = 1;
	}
	dest->symmetry = [attrs objectForKey:@"color_speed"] ? 0.5f - 0.5f*[[[attrs objectForKey:@"color_speed"] objectValue] floatValue] : dest->symmetry;
	if (dest2 != nullptr)
	{
		dest2->rotates = [self attrAsFloat:attrs key:@"animate" defaultValue:dest2->rotates];
	}
	dest->opacity = [self attrAsFloat:attrs key:@"opacity" defaultValue:1.f];
	
	dest->preblur = [self attrAsFloat:attrs key:@"preblur" defaultValue:0.f];
	dest->linear = [self attrAsFloat:attrs key:@"linear" defaultValue:0.f];
	dest->sinusoidal = [self attrAsFloat:attrs key:@"sinusoidal" defaultValue:0.f];
	dest->spherical = [self attrAsFloat:attrs key:@"spherical" defaultValue:0.f];
	dest->swirl = [self attrAsFloat:attrs key:@"swirl" defaultValue:0.f];
	dest->horseshoe = [self attrAsFloat:attrs key:@"horseshoe" defaultValue:0.f];
	dest->polar = [self attrAsFloat:attrs key:@"polar" defaultValue:0.f];
	dest->handkerchief = [self attrAsFloat:attrs key:@"handkerchief" defaultValue:0.f];
	dest->heart = [self attrAsFloat:attrs key:@"heart" defaultValue:0.f];
	dest->disc = [self attrAsFloat:attrs key:@"disc" defaultValue:0.f];
	dest->spiral = [self attrAsFloat:attrs key:@"spiral" defaultValue:0.f];
	dest->hyperbolic = [self attrAsFloat:attrs key:@"hyperbolic" defaultValue:0.f];
	dest->diamond = [self attrAsFloat:attrs key:@"diamond" defaultValue:0.f];
	dest->ex = [self attrAsFloat:attrs key:@"ex" defaultValue:0.f];
	dest->julia = [self attrAsFloat:attrs key:@"julia" defaultValue:0.f];
	dest->bent = [self attrAsFloat:attrs key:@"bent" defaultValue:0.f];
	dest->waves = [self attrAsFloat:attrs key:@"waves" defaultValue:0.f];
	dest->fisheye = [self attrAsFloat:attrs key:@"fisheye" defaultValue:0.f];
	dest->popcorn = [self attrAsFloat:attrs key:@"popcorn" defaultValue:0.f];
	dest->exponential = [self attrAsFloat:attrs key:@"exponential" defaultValue:0.f];
	dest->power = [self attrAsFloat:attrs key:@"power" defaultValue:0.f];
	dest->cosine = [self attrAsFloat:attrs key:@"cosine" defaultValue:0.f];
	dest->rings = [self attrAsFloat:attrs key:@"rings" defaultValue:0.f];
	dest->fan = [self attrAsFloat:attrs key:@"fan" defaultValue:0.f];
	dest->blob = [self attrAsFloat:attrs key:@"blob" defaultValue:0.f];
	dest->blob_high = [self attrAsFloat:attrs key:@"blob_high" defaultValue:0.f];
	dest->blob_low = [self attrAsFloat:attrs key:@"blob_low" defaultValue:0.f];
	dest->blob_waves = [self attrAsFloat:attrs key:@"blob_waves" defaultValue:0.f];
	dest->pdj = [self attrAsFloat:attrs key:@"pdj" defaultValue:0.f];
	dest->pdj_a = [self attrAsFloat:attrs key:@"pdj_a" defaultValue:0.f];
	dest->pdj_b = [self attrAsFloat:attrs key:@"pdj_b" defaultValue:0.f];
	dest->pdj_c = [self attrAsFloat:attrs key:@"pdj_c" defaultValue:0.f];
	dest->pdj_d = [self attrAsFloat:attrs key:@"pdj_d" defaultValue:0.f];
	dest->fan2 = [self attrAsFloat:attrs key:@"fan2" defaultValue:0.f];
	dest->fan2_x = [self attrAsFloat:attrs key:@"fan2_x" defaultValue:0.f];
	dest->fan2_y = [self attrAsFloat:attrs key:@"fan2_y" defaultValue:0.f];
	dest->rings2 = [self attrAsFloat:attrs key:@"rings2" defaultValue:0.f];
	dest->rings2_val = [self attrAsFloat:attrs key:@"rings2_val" defaultValue:0.f];
	dest->eyefish = [self attrAsFloat:attrs key:@"eyefish" defaultValue:0.f];
	dest->bubble = [self attrAsFloat:attrs key:@"bubble" defaultValue:0.f];
	dest->cylinder = [self attrAsFloat:attrs key:@"cylinder" defaultValue:0.f];
	dest->perspective = [self attrAsFloat:attrs key:@"perspective" defaultValue:0.f];
	dest->perspective_angle = [self attrAsFloat:attrs key:@"perspective_angle" defaultValue:0.f];
	dest->perspective_dist = [self attrAsFloat:attrs key:@"perspective_dist" defaultValue:0.f];
	dest->noise = [self attrAsFloat:attrs key:@"noise" defaultValue:0.f];
	dest->julian = [self attrAsFloat:attrs key:@"julian" defaultValue:0.f];
	dest->julian_power = [self attrAsFloat:attrs key:@"julian_power" defaultValue:0.f];
	dest->julian_dist = [self attrAsFloat:attrs key:@"julian_dist" defaultValue:0.f];
	dest->juliascope = [self attrAsFloat:attrs key:@"juliascope" defaultValue:0.f];
	dest->juliascope_power = [self attrAsFloat:attrs key:@"juliascope_power" defaultValue:0.f];
	dest->juliascope_dist = [self attrAsFloat:attrs key:@"juliascope_dist" defaultValue:0.f];
	dest->blur = [self attrAsFloat:attrs key:@"blur" defaultValue:0.f];
	dest->gaussian_blur = [self attrAsFloat:attrs key:@"gaussian_blur" defaultValue:0.f];
	dest->radial_blur = [self attrAsFloat:attrs key:@"radial_blur" defaultValue:0.f];
	dest->radial_blur_angle = [self attrAsFloat:attrs key:@"radial_blur_angle" defaultValue:0.f];
	dest->pie = [self attrAsFloat:attrs key:@"pie" defaultValue:0.f];
	dest->pie_slices = [self attrAsFloat:attrs key:@"pie_slices" defaultValue:0.f];
	dest->pie_rotation = [self attrAsFloat:attrs key:@"pie_rotation" defaultValue:0.f];
	dest->pie_thickness = [self attrAsFloat:attrs key:@"pie_thickness" defaultValue:0.f];
	dest->ngon = [self attrAsFloat:attrs key:@"ngon" defaultValue:0.f];
	dest->ngon_sides = [self attrAsFloat:attrs key:@"ngon_sides" defaultValue:0.f];
	dest->ngon_power = [self attrAsFloat:attrs key:@"ngon_power" defaultValue:0.f];
	dest->ngon_circle = [self attrAsFloat:attrs key:@"ngon_circle" defaultValue:0.f];
	dest->ngon_corners = [self attrAsFloat:attrs key:@"ngon_corners" defaultValue:0.f];
	dest->curl = [self attrAsFloat:attrs key:@"curl" defaultValue:0.f];
	dest->curl_c1 = [self attrAsFloat:attrs key:@"curl_c1" defaultValue:0.f];
	dest->curl_c2 = [self attrAsFloat:attrs key:@"curl_c2" defaultValue:0.f];
	dest->rectangles = [self attrAsFloat:attrs key:@"rectangles" defaultValue:0.f];
	dest->rectangles_x = [self attrAsFloat:attrs key:@"rectangles_x" defaultValue:0.f];
	dest->rectangles_y = [self attrAsFloat:attrs key:@"rectangles_y" defaultValue:0.f];
	dest->arch = [self attrAsFloat:attrs key:@"arch" defaultValue:0.f];
	dest->tangent = [self attrAsFloat:attrs key:@"tangent" defaultValue:0.f];
	dest->square = [self attrAsFloat:attrs key:@"square" defaultValue:0.f];
	dest->rays = [self attrAsFloat:attrs key:@"rays" defaultValue:0.f];
	dest->blade = [self attrAsFloat:attrs key:@"blade" defaultValue:0.f];
	dest->secant = [self attrAsFloat:attrs key:@"secant" defaultValue:0.f];
	dest->twintrian = [self attrAsFloat:attrs key:@"twintrian" defaultValue:0.f];
	dest->cross = [self attrAsFloat:attrs key:@"cross" defaultValue:0.f];
	dest->disc2 = [self attrAsFloat:attrs key:@"disc2" defaultValue:0.f];
	dest->disc2_rot = [self attrAsFloat:attrs key:@"disc2_rot" defaultValue:0.f];
	dest->disc2_twist = [self attrAsFloat:attrs key:@"disc2_twist" defaultValue:0.f];
	dest->supershape = [self attrAsFloat:attrs key:@"super_shape" defaultValue:0.f];
	dest->supershape_m = [self attrAsFloat:attrs key:@"super_shape_m" defaultValue:0.f];
	dest->supershape_n1 = [self attrAsFloat:attrs key:@"super_shape_n1" defaultValue:0.f];
	dest->supershape_n2 = [self attrAsFloat:attrs key:@"super_shape_n2" defaultValue:0.f];
	dest->supershape_n3 = [self attrAsFloat:attrs key:@"super_shape_n3" defaultValue:0.f];
	dest->supershape_holes = [self attrAsFloat:attrs key:@"super_shape_holes" defaultValue:0.f];
	dest->supershape_rnd = [self attrAsFloat:attrs key:@"super_shape_rnd" defaultValue:0.f];
	COPY_XFORM_ATTR(flower);
	COPY_XFORM_ATTR(flower_holes);
	COPY_XFORM_ATTR(flower_petals);
	COPY_XFORM_ATTR(conic);
	COPY_XFORM_ATTR(conic_holes);
	dest->conic_eccen = [self attrAsFloat:attrs key:@"conic_eccentricity" defaultValue:0.f];
	COPY_XFORM_ATTR(parabola);
	COPY_XFORM_ATTR(parabola_height);
	COPY_XFORM_ATTR(parabola_width);
	COPY_XFORM_ATTR(bent2);
	COPY_XFORM_ATTR(bent2_x);
	COPY_XFORM_ATTR(bent2_y);
	COPY_XFORM_ATTR(bipolar);
	COPY_XFORM_ATTR(bipolar_shift);
	COPY_XFORM_ATTR(boarders);
	COPY_XFORM_ATTR(butterfly);
	COPY_XFORM_ATTR(cell);
	COPY_XFORM_ATTR(cell_size);
	COPY_XFORM_ATTR(cpow);
	COPY_XFORM_ATTR(cpow_i);
	COPY_XFORM_ATTR(cpow_r);
	COPY_XFORM_ATTR(cpow_power);
	COPY_XFORM_ATTR(curve);
	COPY_XFORM_ATTR(curve_xamp);
	COPY_XFORM_ATTR(curve_yamp);
	COPY_XFORM_ATTR(curve_xlength);
	COPY_XFORM_ATTR(curve_ylength);
	COPY_XFORM_ATTR(edisc);
	COPY_XFORM_ATTR(elliptic);
	COPY_XFORM_ATTR(escher);
	COPY_XFORM_ATTR(escher_beta);
	COPY_XFORM_ATTR(foci);
	COPY_XFORM_ATTR(lazysusan);
	COPY_XFORM_ATTR(lazysusan_x);
	COPY_XFORM_ATTR(lazysusan_y);
	COPY_XFORM_ATTR(lazysusan_spin);
	COPY_XFORM_ATTR(lazysusan_space);
	COPY_XFORM_ATTR(lazysusan_twist);
	COPY_XFORM_ATTR(loonie);
	COPY_XFORM_ATTR(pre_blur);
	COPY_XFORM_ATTR(modulus);
	COPY_XFORM_ATTR(modulus_x);
	COPY_XFORM_ATTR(modulus_y);
	COPY_XFORM_ATTR(oscilloscope);
	COPY_XFORM_ATTR(oscilloscope_separation);
	COPY_XFORM_ATTR(oscilloscope_frequency);
	COPY_XFORM_ATTR(oscilloscope_amplitude);
	COPY_XFORM_ATTR(oscilloscope_damping);
	COPY_XFORM_ATTR(polar2);
	COPY_XFORM_ATTR(popcorn2);
	COPY_XFORM_ATTR(popcorn2_x);
	COPY_XFORM_ATTR(popcorn2_y);
	COPY_XFORM_ATTR(popcorn2_c);
	COPY_XFORM_ATTR(scry);
	COPY_XFORM_ATTR(separation);
	COPY_XFORM_ATTR(separation_x);
	COPY_XFORM_ATTR(separation_xinside);
	COPY_XFORM_ATTR(separation_y);
	COPY_XFORM_ATTR(separation_yinside);
	COPY_XFORM_ATTR(split);
	COPY_XFORM_ATTR(split_xsize);
	COPY_XFORM_ATTR(split_ysize);
	COPY_XFORM_ATTR(splits);
	COPY_XFORM_ATTR(splits_x);
	COPY_XFORM_ATTR(splits_y);
	COPY_XFORM_ATTR(stripes);
	COPY_XFORM_ATTR(stripes_space);
	COPY_XFORM_ATTR(stripes_warp);
	COPY_XFORM_ATTR(wedge);
	COPY_XFORM_ATTR(wedge_angle);
	COPY_XFORM_ATTR(wedge_hole);
	COPY_XFORM_ATTR(wedge_count);
	COPY_XFORM_ATTR(wedge_swirl);
	COPY_XFORM_ATTR(wedge_julia);
	COPY_XFORM_ATTR(wedge_julia_angle);
	COPY_XFORM_ATTR(wedge_julia_count);
	COPY_XFORM_ATTR(wedge_julia_power);
	COPY_XFORM_ATTR(wedge_julia_dist);
	COPY_XFORM_ATTR(wedge_sph);
	COPY_XFORM_ATTR(wedge_sph_angle);
	COPY_XFORM_ATTR(wedge_sph_hole);
	COPY_XFORM_ATTR(wedge_sph_count);
	COPY_XFORM_ATTR(wedge_sph_swirl);
	COPY_XFORM_ATTR(whorl);
	COPY_XFORM_ATTR(whorl_inside);
	COPY_XFORM_ATTR(whorl_outside);
	COPY_XFORM_ATTR(waves2);
	COPY_XFORM_ATTR(waves2_scalex);
	COPY_XFORM_ATTR(waves2_scaley);
	COPY_XFORM_ATTR(waves2_freqx);
	COPY_XFORM_ATTR(waves2_freqy);
	dest->v_exp = [self attrAsFloat:attrs key:@"exp" defaultValue:0.f];
	dest->v_log = [self attrAsFloat:attrs key:@"log" defaultValue:0.f];
	dest->v_sin = [self attrAsFloat:attrs key:@"sin" defaultValue:0.f];
	dest->v_cos = [self attrAsFloat:attrs key:@"cos" defaultValue:0.f];
	dest->v_tan = [self attrAsFloat:attrs key:@"tan" defaultValue:0.f];
	dest->v_sec = [self attrAsFloat:attrs key:@"sec" defaultValue:0.f];
	dest->v_csc = [self attrAsFloat:attrs key:@"csc" defaultValue:0.f];
	dest->v_cot = [self attrAsFloat:attrs key:@"cot" defaultValue:0.f];
	dest->v_sinh = [self attrAsFloat:attrs key:@"sinh" defaultValue:0.f];
	dest->v_cosh = [self attrAsFloat:attrs key:@"cosh" defaultValue:0.f];
	dest->v_tanh = [self attrAsFloat:attrs key:@"tanh" defaultValue:0.f];
	dest->v_sech = [self attrAsFloat:attrs key:@"sech" defaultValue:0.f];
	dest->v_csch = [self attrAsFloat:attrs key:@"csch" defaultValue:0.f];
	dest->v_coth = [self attrAsFloat:attrs key:@"coth" defaultValue:0.f];
	COPY_XFORM_ATTR(auger);
	COPY_XFORM_ATTR(auger_freq);
	COPY_XFORM_ATTR(auger_scale);
	COPY_XFORM_ATTR(auger_weight);
	COPY_XFORM_ATTR(auger_sym);
	
	// older deprecated way of setting variations
	if ([self attrAsString:attrs key:@"var"]) {
		NSMutableArray *vars = [self splitOnSpaces:[[attrs objectForKey:@"var"] objectValue]];
		dest->linear		= [[vars objectAtIndex:0] floatValue];
		dest->sinusoidal	= [[vars objectAtIndex:1] floatValue];
		dest->spherical		= [[vars objectAtIndex:2] floatValue];
		dest->swirl			= [[vars objectAtIndex:3] floatValue];
		dest->horseshoe		= [[vars objectAtIndex:4] floatValue];
		dest->polar			= [[vars objectAtIndex:5] floatValue];
		dest->handkerchief	= [[vars objectAtIndex:6] floatValue];
		dest->heart			= [[vars objectAtIndex:7] floatValue];
		dest->disc			= [[vars objectAtIndex:8] floatValue];
		dest->spiral		= [[vars objectAtIndex:9] floatValue];
		dest->hyperbolic	= [[vars objectAtIndex:10] floatValue];
		dest->diamond		= [[vars objectAtIndex:11] floatValue];
		dest->ex			= [[vars objectAtIndex:12] floatValue];
		dest->julia			= [[vars objectAtIndex:13] floatValue];
		dest->bent			= [[vars objectAtIndex:14] floatValue];
	}
	
	// another older deprecated way of setting variations
	if ([attrs objectForKey:@"var1"]) {
		int idx = [self attrAsInt:attrs key:@"var1"];
		switch (idx) {
			case 0:
				dest->linear		= 1.f;
				break;
			case 1:
				dest->sinusoidal	= 1.f;
				break;
			case 2:
				dest->spherical		= 1.f;
				break;
			case 3:
				dest->swirl			= 1.f;
				break;
			case 4:
				dest->horseshoe		= 1.f;
				break;
			case 5:
				dest->polar			= 1.f;
				break;
			case 6:
				dest->handkerchief	= 1.f;
				break;
			case 7:
				dest->heart			= 1.f;
				break;
			case 8:
				dest->disc			= 1.f;
				break;
			case 9:
				dest->spiral		= 1.f;
				break;
			case 10:
				dest->hyperbolic	= 1.f;
				break;
			case 11:
				dest->diamond		= 1.f;
				break;
			case 12:
				dest->ex			= 1.f;
				break;
			case 13:
				dest->julia			= 1.f;
				break;
			case 14:
				dest->bent			= 1.f;
				break;
			default:
				break;
		}
	}
	
	
	NSString *str = [self attrAsString:attrs key:@"post" defaultValue:@"1 0 0 1 0 0"];
	NSMutableArray *post = [self splitOnSpaces:str];
	
	dest->pa = [[post objectAtIndex:0] floatValue];
	dest->pd = [[post objectAtIndex:1] floatValue];
	dest->pb = [[post objectAtIndex:2] floatValue];
	dest->pe = [[post objectAtIndex:3] floatValue];
	dest->pc = [[post objectAtIndex:4] floatValue];
	dest->pf = [[post objectAtIndex:5] floatValue];	
}	

// read the packed RGB hex string and assign them to the rgba color struct instance
void readHexRGBString(const char *rawHex, struct rgba *color, int *_index, int paletteType)
{
	int index = *_index;
	//Yay, we get to read in a bunch of ugly hex values!
	unsigned char i = 0;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (paletteType == 1) // case 1: RGBA with unpopulated A, we ignore their values
		index += 2;  //skip over unused alpha value
	else if (paletteType == 3) { // case 3: RGBA with populated A, we ignore their values
		index++;  //skip over unused alpha value
		while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
			index++;
		index++;
		while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
			index++;		
	}
	// case 2: RGB - no alpha values to read or skip over 
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	i = i<<4;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	color->r = ((float)i)/255.0f;
	i = 0;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	i = i<<4;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	color->g = ((float)i)/255.0f;
	i = 0;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	i = i<<4;
	while (!(((rawHex[index]<='F') && (rawHex[index] >= 'A')) || ((rawHex[index] <= '9') && (rawHex[index] >= '0'))))
		index++;
	if (rawHex[index] <= '9')
		i+=rawHex[index]-'0';
	else
		i+=rawHex[index]-'A'+10;
	index++;
	color->b = ((float)i)/255.0f;
	color->a = 1.0f;
	*_index = index;
}

// parse the Flame XML document
+ (Flame *)parseFlame:(NSXMLElement *)flameEle  metaData:(FlameMetaData *)flameMeta  flameCount:(int)flameCount palettePath:(NSString *)palettePath
{
	NSXMLDocument *paletteDoc = nil;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	Flame *flm;
	float hue;
	int count       = 0;
	int paletteType = 0;
	int standardPaletteIndex = 0;
	
	// get attrs as a dictionary for <flame>
	NSDictionary *flameAttrDict = [self attrsAsDict:flameEle];
	
	NSArray *xForms = [flameEle elementsForName:@"xform"];
	NSArray *palette = [flameEle elementsForName:@"color"];
	
	// get palette info
	if (![flameAttrDict objectForKey:@"palette"]) {
		if ([palette count] == 0) {
			palette = [flameEle elementsForName:@"palette"];
			if ([palette count] == 0) {
				palette = [flameEle elementsForName:@"colors"];
				paletteType = 1;
			}
			else {
				paletteType = 2;
			}
			count = [[[[palette objectAtIndex:0] attributeForName:@"count"] objectValue] integerValue];
		}
		else {
			count = [palette count];
		}
	}
	else {
		int palIndex = [[[flameAttrDict objectForKey:@"palette"] objectValue] integerValue];
		
		NSError *error = nil;
		paletteDoc     = [[NSXMLDocument alloc]initWithContentsOfURL:[NSURL fileURLWithPath:palettePath] options:0 error: &error];
		NSXMLElement *palettes = [paletteDoc rootElement];
		palette = [palettes elementsForName:@"palette"];
		
		while (palIndex != [self getPalIndex:palette index:standardPaletteIndex]) {
			standardPaletteIndex++;
		}
		paletteType = 3;
		count = 256;
	}		
	
	int xformCount = [xForms count]; 
	if (xformCount > MAX_XFORMS) {  // check if we can fit into the GPU and if not truncate the xforms
		xformCount = MAX_XFORMS;
		NSAlert *a = [NSAlert alertWithMessageText:nil
									 defaultButton:nil
								   alternateButton:nil
									   otherButton:nil
						 informativeTextWithFormat:@"Fractal #%u exceeds the limit of %u xforms - truncating those beyond limit", flameCount + 1, MAX_XFORMS];
		[a runModal];
	}
	flm = new Flame(xformCount, count);
	
	// get symmetryKind
	NSArray *symmetries = [flameEle elementsForName:@"symmetry"];
	if ([symmetries count] > 0) {
		NSXMLElement *symmetryEle = [symmetries objectAtIndex:0];
		flm->symmetryKind =  [[[symmetryEle attributeForName:@"kind"] objectValue] floatValue];
	}
	else {
		flm->symmetryKind = 0.0f;
	}
	if ([flameAttrDict objectForKey:@"name"])
		flameMeta->name = [[[flameAttrDict objectForKey:@"name"] objectValue] copy];
	else
		flameMeta->name = [[NSString stringWithFormat:@"%u", flameCount] copy];
	
	// get hue
	if ([flameAttrDict objectForKey:@"hue"])
		hue = [[[flameAttrDict objectForKey:@"hue"] objectValue] floatValue];
	else 
		hue = 0.0f;
	
	// Get <flame center="??"
	if ([flameAttrDict objectForKey:@"center"]) {
		NSMutableArray *center = [self splitOnSpaces:[[flameAttrDict objectForKey:@"center"] objectValue]];			
		flm->center[0] =  [[center objectAtIndex:0] floatValue];
		flm->center[1] =  [[center objectAtIndex:1] floatValue];
	}
	else
	{
		flm->center[0]=0;
		flm->center[1]=0;
	}
	
	// Get <flame size="??"
	float x,y;
	if ([flameAttrDict objectForKey:@"size"]) {
		NSMutableArray *size = [self splitOnSpaces:[[flameAttrDict objectForKey:@"size"] objectValue]];			
		x =  [[size objectAtIndex:0] floatValue];
		y =  [[size objectAtIndex:1] floatValue];
	}
	else
	{
		x = 100;
		y = 100;
	}
	float scale = [self attrAsFloat:flameAttrDict key:@"scale" defaultValue:1.f];
	flm->scale   = scale;
	float zoom = [self attrAsFloat:flameAttrDict key:@"zoom" defaultValue:0.f];
	flm->zoom   = zoom;
	flm->size[0] = x/scale/powf(2.f, zoom);
	flm->size[1] = y/scale/powf(2.f, zoom);
	
	flm->params.quality = [self attrAsFloat:flameAttrDict key:@"quality"];
	
	// Get <flame background="??"
	if ([flameAttrDict objectForKey:@"background"]) {
		NSMutableArray *background = [self splitOnSpaces:[[flameAttrDict objectForKey:@"background"] objectValue]];
		
		flm->background.r =  [[background objectAtIndex:0] floatValue];
		flm->background.g =  [[background objectAtIndex:1] floatValue];
		flm->background.b =  [[background objectAtIndex:2] floatValue];
	}
	else
	{
		flm->background.r = 0;
		flm->background.g = 0;
		flm->background.b = 0;
	}
	flm->background.a = 0;
	
	flm->brightness = [self attrAsFloat:flameAttrDict key:@"brightness" defaultValue:4.f];
	flm->gamma = [self attrAsFloat:flameAttrDict key:@"gamma" defaultValue:4.f];
	flm->vibrancy = [self attrAsFloat:flameAttrDict key:@"vibrancy" defaultValue:1.f];
	flm->rotation = [flameAttrDict objectForKey:@"rotate"]       ? [[[flameAttrDict objectForKey:@"rotate"] objectValue] floatValue]*(2.0*M_PI/360.0) : 0.f;
	
	//grab the xForms
	for (int n = 0; n < xformCount; n++)	 {
		NSDictionary *xformAttrDict = [self attrsAsDict:[xForms objectAtIndex:n]];
		[self extractXform:&flm->trans[n] form:&flm->transAff[n] dict:xformAttrDict];
		
	}
	NSArray *finalXform = [flameEle elementsForName:@"finalxform"];
	if ([finalXform count] != 0)
	{
		flm->isFinalXform = 1;
		NSDictionary *xformAttrDict = [self attrsAsDict:[finalXform objectAtIndex:0]];
		[self extractXform:&flm->params.finalXform form:nil dict:xformAttrDict];
	}
	else
	{
		flm->isFinalXform = 0;
	}
	
	//Now read in the palette
	switch (paletteType)
	{
		case 0:
		{
			for (int n = 0; n < [palette count]; n++)
			{
				NSDictionary *paletteAttrDict = [self attrsAsDict:[palette objectAtIndex:n]];
				int i = [self attrAsInt:paletteAttrDict key:@"index"];
				NSMutableArray *colorIndices = [self splitOnSpaces:[[paletteAttrDict objectForKey:@"rgb"] objectValue]];
				flm->colorIndex[i].r = [[colorIndices objectAtIndex:0] floatValue] /255.0f;
				flm->colorIndex[i].g = [[colorIndices objectAtIndex:1] floatValue] /255.0f;
				flm->colorIndex[i].b = [[colorIndices objectAtIndex:2] floatValue] /255.0f;
				flm->colorIndex[i].a = 1.0f;
			}
		}break;
		case 1:
		{
			NSDictionary *paletteAttrDict = [self attrsAsDict:[palette objectAtIndex:0]];
			NSString *paletteHex = [self attrAsString:paletteAttrDict key:@"data"];
			paletteHex = [paletteHex uppercaseString];
			
			const char *rawHex = [paletteHex UTF8String];
			
			// unichar *buffer = (unichar *)calloc([paletteHex length], sizeof( unichar ) );
			// [ paletteHex getCharacters:buffer range:NSMakeRange(0, [paletteHex length])];
			
			int index = 0;
			for (int j = 0; j < count; j++)
			{
				readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
			}
		}break;				
		case 2:
		{
			NSXMLNode *node = [palette objectAtIndex:0];
			NSString *paletteHex = [node objectValue];
			paletteHex = [paletteHex uppercaseString];
			
			const char *rawHex = [paletteHex UTF8String];
			
			int index = 0;
			for (int j = 0; j < count; j++)
			{
				readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
			}
		}break;				
		case 3:
		{
			NSDictionary *paletteAttrDict = [self attrsAsDict:[palette objectAtIndex:standardPaletteIndex]];
			NSString *paletteHex = [self attrAsString:paletteAttrDict key:@"data"];
			paletteHex = [paletteHex uppercaseString];
			
			const char *rawHex = [paletteHex UTF8String];
			
			int index = 0;
			for (int j = 0; j < count; j++)
			{
				readHexRGBString(rawHex, &flm->colorIndex[j], &index, paletteType);
			}
		}break;				
	}
	
	//Apply the hue transformation, if present, to the palette
	if (hue != 0)
	{
		for (int n = 0; n < count; n++)
		{
			float r = flm->colorIndex[n].r;
			float g = flm->colorIndex[n].g;
			float b = flm->colorIndex[n].b;
			float max = fmaxf(fmaxf(r,g),b);
			float min = fminf(fminf(r,g),b);
			float h,s,v;
			if (max==min)
				h = 0.0f;
			else if (max == r)
				h = .16666666666f*(g-b)/(max-min);
			else if (max == g)
				h = .16666666666f*(b-r)/(max-min)+.33333333f;
			else
				h = .16666666666f*(r-g)/(max-min)+.66666666f;
			if (max == 0.0f)
				s = 0.0f;
			else
				s = (max-min)/(max);
			v = max;
			h += hue;
			h = h-floorf(h);
			int hi = ((int)floorf(h*6.0f))%6;
			float f = h*6.0f-floorf(h*6.0f);
			float p = v*(1.0f-s);
			float q = v*(1.0f-f*s);
			float t = v*(1.0f-(1.0f-f)*s);
			switch (hi)
			{
				case 0:
				{
					r = v;
					g = t;
					b = p;
				}break;
				case 1:
				{
					r = q;
					g = v;
					b = p;
				}break;
				case 2:
				{
					r = p;
					g = v;
					b = t;
				}break;
				case 3:
				{
					r = p;
					g = q;
					b = v;
				}break;
				case 4:
				{
					r = t;
					g = p;
					b = v;
				}break;
				case 5:
				{
					r = v;
					g = p;
					b = q;
				}break;
			}
			flm->colorIndex[n].r = r;
			flm->colorIndex[n].g = g;
			flm->colorIndex[n].b = b;
		}
	}
	
	//Now, normalize the weights and put them into intervals on [0,1]
	float sum = 0.0f;
	for (int n = 0; n < flm->numTrans; n++)
	{
		sum+=flm->trans[n].weight;
	}
	float sum2 = 0.0f;
	for (int n = 0; n < flm->numTrans; n++)
	{
		sum2 += flm->trans[n].weight/sum;
		flm->trans[n].weight = sum2;
	}
	[pool release];
	[paletteDoc release];
	return flm;
}

// parse the Flame XML document
+ (flameParameterList*)parseDoc:(NSXMLDocument *)doc frameCount:(int *)frameCount palettePath:(NSString *)palettePath
{
	flameParameterList* fAnimation   = new flameParameterList();
	flameParameterList* currentFlame = fAnimation;
	int                 flameCount   = 0;
	
	NSXMLNode *aNode = [doc rootElement];
	
	// look for first <flame>
	do {
		if ([aNode kind] == NSXMLElementKind && [[aNode name]isEqualToString:@"flame"]) {
			currentFlame->flameMeta = new FlameMetaData();
			currentFlame->flame = [self parseFlame:(NSXMLElement *)aNode metaData:currentFlame->flameMeta flameCount:flameCount palettePath:palettePath];
			currentFlame->originalQuality = currentFlame->flame->params.quality;
			flameCount++;
			break;
		}
	} while (aNode = [aNode nextNode]);
	
	while (aNode = [aNode nextNode]) {
		if ([aNode kind] == NSXMLElementKind && [[aNode name]isEqualToString:@"flame"]) {
			currentFlame = currentFlame->nextFlame = new flameParameterList();
			currentFlame->flameMeta = new FlameMetaData();
			currentFlame->flame = [self parseFlame:(NSXMLElement *)aNode metaData:currentFlame->flameMeta flameCount:flameCount palettePath:palettePath];
			currentFlame->originalQuality = currentFlame->flame->params.quality;
			flameCount++;
		}
	}
	*frameCount = flameCount;
	return fAnimation;
}

@end
