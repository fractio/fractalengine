/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pthread.h>
#import "PngBatchSettingsController.h"
#import "AppController.h"
#import "QualityUtils.h"
#import "HelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

@implementation PngBatchSettingsController

- (NSString *)nibName {
	return @"PNGBatchSettings";
}


- (id)init
{
	if (! [super init])
		return nil;
	return self;
}	

- (void)windowDidLoad
{
	[super windowDidLoad];
	frameNumber = 0;
	[self reset];
}

// set things that need to reset every time the window is opened
- (void)reset
{
	extern volatile int progress;
	progress = 0;
	[progressBar setDoubleValue: (double)progress];
	frameNumber = 0;
	[frameBar setDoubleValue:(double)frameNumber];
	NSString *framePlural = [doc frameCount] > 1 ? @"frames" : @"frame";
	[frameCounter setStringValue:[NSString stringWithFormat:@"0 of %u %@", [doc frameCount], framePlural]];
	[super formatActualQuality];

	if (!timer)
		timer = [[NSTimer scheduledTimerWithTimeInterval:0.5 
												  target:self 
												selector:@selector(tick:) 
												userInfo:nil 
												 repeats:YES] retain];
}

- (IBAction)renderAsPNGBatch:(id)sender
{
	// get the values of the Controls here, as any final typing changes made to them might not have been captured via keypair messaging
	[widthField validateEditing];
	[heightField validateEditing];
	[qualityField validateEditing];
	outputWidth  = [widthField integerValue];
	outputHeight = [heightField integerValue];
	quality      = [qualityField integerValue];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSURL *initialDir     = [NSURL fileURLWithPath:[defaults objectForKey:Flam4PngBatchDirectory]];

	if (doc) {
		NSMutableString *flameName = [NSMutableString stringWithString:[[doc displayName] stringByDeletingPathExtension]];
		NSOpenPanel *sPanel = [NSOpenPanel openPanel];		
		
		if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
			[sPanel setDirectory:[initialDir path]];
		else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
			[sPanel setDirectoryURL:initialDir];
#endif
		}
		[sPanel setCanCreateDirectories:YES];
		[sPanel setCanChooseDirectories:YES];
		[sPanel setCanChooseFiles:NO];
		[sPanel setMessage:[NSString stringWithFormat:@"Choose a parent directory\nwhere the PNG Batch's \"%@\" subdirectory will be placed.", flameName]];
		[sPanel setTitle:@"Choose PNG File Batch's Parent Directory"];
				
		int result = [sPanel runModal];
		// if successful, save file under designated name 
		if (result != NSOKButton) 
			return;

		[[self window] makeKeyWindow];

		NSURL *pickedDir = nil;
		if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
			NSString *path = [sPanel directory];
			pickedDir      = [NSURL fileURLWithPath:path];
		}
		else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
			pickedDir = [sPanel directoryURL];
#endif
		}
		
		if (! [initialDir isEqual:pickedDir]) { // remember this directory as the new default
			[defaults setObject:[pickedDir path] forKey:Flam4PngBatchDirectory];
		}
				
		// append the flame document's basename to this parent directory that the user chose
		NSURL *url = [sPanel URL];
		NSString *finalDirectory = [[url path] stringByAppendingPathComponent:flameName];
		
		BOOL isDir = false;
		// handle case where directory already exists
		if ([[NSFileManager defaultManager] fileExistsAtPath:finalDirectory isDirectory:&isDir]) {
			NSAlert *a = [NSAlert alertWithMessageText:nil
										 defaultButton:@"Replace"
									   alternateButton:@"Cancel"
										   otherButton:nil
							 informativeTextWithFormat:@"Directory %@ already exists. Do you want to replace it?", finalDirectory];
			if ([a runModal] == NSAlertDefaultReturn) {
				// remove the directory
				NSError *error = nil;
				[[NSFileManager defaultManager] removeItemAtPath:finalDirectory error:&error];
				if (error) {
					NSAlert *a = [NSAlert alertWithError:error];
					[a runModal];
					return;
				}
			}
			else // user does not want to reuse the directory
				return;
		}
		// finally create the new directory
		NSError *error = nil;
		[[NSFileManager defaultManager] createDirectoryAtPath:finalDirectory withIntermediateDirectories:YES attributes:nil error:&error];
		if (error) {
			NSAlert *a = [NSAlert alertWithError:error];
			[a runModal];
			return;
		}
		
		url = [NSURL fileURLWithPath:finalDirectory];
		
		thread = new RenderThread([doc flameData], outputWidth, outputHeight, transparent, 
								  nil, nil, [url retain], self);
		int frameNum = 0;
		flameParameterList* f = [doc flameData];
		do
		{
			thread->setFlameQuality(f->flame, (double)quality, [tiles count]);
			f = f->nextFlame;
			frameNum++;
		} while (f != nullptr);
		
		[frameBar    setMaxValue: frameNum];
		
		pthread_t threadHandle;										
		pthread_create(&threadHandle, NULL, RenderThread::RenderPNGBatch, thread);
	}
}

// timer tick procedure
- (void)tick:(NSTimer *)aTimer
{
	extern volatile bool isDone;
	extern volatile int progress;
	[super tick:aTimer];
	if (! isDone) {
		[progressBar setDoubleValue:progress];
		[frameBar setDoubleValue: (double)frameNumber];
		NSString *framePlural = [doc frameCount] > 1 ? @"frames" : @"frame";
		[frameCounter setStringValue:[NSString stringWithFormat:@"%u of %u %@", frameNumber, [doc frameCount], framePlural]];
	}
}

// Cancel button handler
- (IBAction)cancel:(id)sender
{	
	[super cancel:sender];
}

- (IBAction)help:(id)sender {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"howtoPNGBatch" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"RenderPNGBatch"  inBook:locBookName];
	}
}



@end
