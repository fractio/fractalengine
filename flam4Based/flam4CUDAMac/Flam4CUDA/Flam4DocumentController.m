//
//  Flam4DocumentController.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/8/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "Flam4DocumentController.h"


@implementation Flam4DocumentController

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem {
	SEL theAction = [anItem action];
    if (theAction == @selector(newDocument:))
    {
		return NO;
    }
	
	// subclass of NSDocumentController, so invoke super's implementation
	return [super validateUserInterfaceItem:anItem];
}

@end

