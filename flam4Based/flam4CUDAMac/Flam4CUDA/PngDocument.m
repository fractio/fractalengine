/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "PngDocument.h"
#import "PngView.h"



@implementation PngDocument
@synthesize image;
@synthesize view;

- (id)init
{
    self = [super init];
	
	image = nil;
    return self;
}

- (void)dealloc
{
	[image release];
	[super dealloc];
}

- (NSViewController *)pageSetupController {
	return pageSetupController;
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    // Insert code here to write your document to data of the specified type. If the given outError != NULL, ensure that you set *outError when returning nil.

    // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.

    // For applications targeted for Panther or earlier systems, you should use the deprecated API -dataRepresentationOfType:. In this case you can also choose to override -fileWrapperRepresentationOfType: or -writeToFile:ofType: instead.

    return nil;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
	image = [[NSImage alloc] initWithData:data];
    return YES;
}

- (NSString *)windowNibName {
	return @"PngDocument";
}

// called right after the Nib file is loaded
- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
	
	[aController setShouldCloseDocument:YES];
}

// handle print requests
- (void)printShowingPrintPanel:(BOOL)showPanels {
	// calculate the optimal printed dimensions in points
	NSRect previewRect     = [view bounds];
	float aspectRatio      = NSWidth(previewRect)/NSHeight(previewRect);
	NSPrintInfo *printInfo = [self printInfo];
	NSSize paperSize       = [printInfo paperSize];
	NSSize printableSize   = NSMakeSize(paperSize.width - [printInfo leftMargin] - [printInfo rightMargin],
										paperSize.height - [printInfo topMargin] - [printInfo bottomMargin]);
	
	// calculate the dimensions in points of the area to hold the image
	float maxWidth  = printableSize.width;
	float maxHeight = printableSize.height;
	if (printableSize.width/aspectRatio < printableSize.height)
		maxHeight = printableSize.width/aspectRatio;
	if (printableSize.height * aspectRatio < printableSize.width)
		maxWidth  = printableSize.height * aspectRatio;
	
	NSRect viewSize = NSMakeRect(0.f, 0.f, maxWidth, maxHeight);
	
	// Obtain a custom view that will be printed
	PngView *printView = [[PngView alloc] initWithFrame:viewSize];
	[printView setDocument:self];
	
    // Construct the print operation and setup Print panel
    NSPrintOperation *op = [NSPrintOperation
							printOperationWithView:printView
							printInfo:[self printInfo]];
    [op setShowsPrintPanel:showPanels];
	
    // Run operation, which shows the Print panel if showPanels was YES
    [self runModalPrintOperation:op
						delegate:nil
				  didRunSelector:NULL
					 contextInfo:NULL];
	[printView release];
}

// standard place to hook our custom page setup
- (BOOL)preparePageLayout:(NSPageLayout *)pageLayout {
	[pageLayout addAccessoryController:[self pageSetupController]];
	return YES;
}


@end
