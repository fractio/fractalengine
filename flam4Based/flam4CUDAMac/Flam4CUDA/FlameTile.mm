//
//  Flame.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import "FlameTile.h"
#import "FlameTileArray.h"


@implementation FlameTile
@synthesize flame;
@synthesize row;
@synthesize column;
@synthesize width;
@synthesize height;

- (id)initWithFlame:(Flame *)_flame 
				row:(unsigned)_row 
			 column:(unsigned)_column 
			  width:(float)_width 
			 height:(float)_height {
	self = [super init];
	if (self) {
		row     = _row;
		column  = _column;
		width   = _width;
		height  = _height;
		if (_flame) 
			_flame->Clone(&flame);
		else
			flame = nil;
	}
	return self;
}

- (void) dealloc {
	if (flame)
		delete flame;
	flame = nil;
	[super dealloc];
}

-(NSSize)size
{
	//float effScale = flame->scale*powf(2.f, flame->zoom);
	///return NSMakeSize(effScale * flame->size[0], effScale * flame->size[1]);
	return NSMakeSize(width, height);
}

+ (float)overlap {
	return 8.f;
}

@end
