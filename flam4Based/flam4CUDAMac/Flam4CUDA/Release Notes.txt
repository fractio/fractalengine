v.0.80
-Updated to CUDA 3.0 beta.  You'll need to download the latest beta drivers.
-Greatly improved saturation/hue correction for overbright areas.
-Added a whole bunch of new variations:
--bent2
--bipolar
--boarders
--butterfly
--cell
--cpow
--curve
--edisc
--elliptic
--escher
--foci
--lazysusan
--loonie
--pre_blur
--modulus
--oscilloscope
--polar2
--popcorn2
--scry
--separation
--split
--splits
--stripes
--wedge
--wedge_julia
--wedge_sph
--whorl
--waves
--exp
--log
--sin
--cos
--tan
--sec
--csc
--cot
--sinh
--cosh
--tanh
--sech
--csch
--coth
--auger
-We are now flam3 2.8 complient!
v.0.75
-Replaced the random number generater with a Combined Tausworthe generator.  This improves image quality for flames with large use of the various blur vars.
-Added opacity setting to xforms.
-Various changes to the render library.
v.0.74
-Massive performance increase, nearly doubling previous performance for iterations.
--Broke 1 BILLION iterations/sec!
-Fiddled with the threading code some more - flam4 no longer slows down the entire system while running.
v.0.72
-Updated to CUDA 2.3 - you'll probably have to update your drivers.
--Cuda 2.3 now supports multi-gpu with SLI enabled at the same time - no more twiddling settings!
-Fixed issue with frames becoming darker over the course of a long animation.
-Moved the flam3 XML loader to Flam4CudaX.dll, so it's now accessible to third parties.
v.0.71
-Added transparency option for PNG sequences.
-Fixed issue with the PNG sequence folder browser - you can now save to any folder, not just folders in My Pictures.
v.0.70
-Added support for rendering animations to PNG sequences.
v.0.69
-Fixed bug in loading flames with the hue parameter used which was introduced when I switched the palettes to floating point (v.0.63)
-Added support for png transparency
-Greatly improved saturation correction, most noticable in bright red and orange areas
-Set *.flam3 to the default file option, at least until *.flam4 is implemented
-More performance tweaks for multi-GPU
v.0.66
-Major performance improvements for multi-GPU systems
--These also will likely speed up single-GPU systems a bit
-Important:
--This version contians a partial loader for *.flam4 files, but these WILL NOT RENDER at this time.
--Make sure you set *.flame (apophysis flame files) in the load menu, as *.flam4 WILL NOT WORK AT THIS TIME.
v.0.64
-Interpolation is now rotational
-Fixed problem interpolating between flames with differing numbers of xforms
v.0.63
-Added interpolation for rendering sheep edges.
-Changed palette to floating point - you can now do some wild things, like negative colors and full HDR.
v.0.61
-Multi-GPU now performs as fast as it should.  Threads now yield the CPU when the next dispatch isn't ready, or when the worker threads are busy, rather than spinning.  This makes for a very big performance increase.
-Added hue correction for overbright areas.  Bright places no longer distort color.
v.0.60
-Multi-GPU support added!
--Rendering now scales across all GPUs that CUDA can detect.
---Make sure you disable SLI, or else CUDA will only see one GPU
---There are also issues with CUDA not seeing GPUs without monitors plugged in.  This is a Windows driver issue.
--Known issues:
---Resizing the window causes strange results if done after the preview render.
---Merging frames takes way longer than it should.
-Fixed bug where fan and fan2 disregarded their weights.
--There are no longer any known flames that render incorrectly from the electric sheep archives
v.0.55
-Fixed a nasty bug with the random number generator not reseeding itself properly under certian circumstances.
--This fixes a number of aliasing issues with supersampling blur being uneven, as well as some rendering errors with xforms using rand in their code.
--There's a chance that something got broken while I was tinkering with the RNG - poke around and see if you find anything.
v.0.53
-Fixed a number of minor GUI related bugs
--Render loop to file no longer causes a crash if no flame parameters are loaded.
--Render image to file no longer causes a crash if no flame parameters are loaded.
--Cancel buttons in both render to image and render loop dialogs now properly abort renders in progress.
--Fixed invalid parameter error from cuda.
---This also fixes a possible memory leak, where the palette is not properly freed when switching between rendering thread contexts.
---The code was attempting to free the palette from the previous context, which caused the invalid parameter error - the palette is now freed properly in StopCUDA()
-Render button in the render to image/loop now deactivates once you hit it so that it doesn't end up rendering twice by accident.
-Changed default render to loop settings to 160 frames at 30 frames per second, same as standard sheep loops.
-Exported PNGS are no longer upside down.  This was actually fixed a couple versions ago, but omitted from the release notes.
v.0.51
-The migration to Visual Studio 2008 has been successful, make sure to install the Visual Studio 2008 redist.
-Fixed a few minor typos in the dialogs.
-Tweaked density estimation a bit.
v.0.50
-First beta version!
-Added dialog for export AVI settings.
--Width
--Height
--Quality
--Number of Frames
--Framerate
--Bitrate
--Progress bars!
v.0.46
-Removed restriction on max image size.
--Now only limited by available graphics memory
--Note: for some reason, attempting to render images larger than your memory will cause your system to CRASH AND REBOOT.  Be sure to save all work before rendering PNGs until I figure out how to fix this.
---A handy formula for memory footprint:  width*height*32 bytes
--Also, there may be a memory leak in there, so restart the program each time you render an image to PNG.
-The PNG options window now closes properly when the image is finished rendering.
v.0.45
-Added PNG export option, with settings for:
--Width
--Height
--Quality
-Also has a progress bar.
-Note: there are still a few bugs concerning exporting PNGs
--Dimensions larger than 4096 cause a crash (this is an opengl issue)
--Sporadic invalid parameter error from CUDA.  Can be ignored.
v.0.41
-Fixed all remaining known problems with variations, this fixes the following:
--Var 49 Disc2
--Var 50 Supershape
--Var 51 Flower
--Var 52 Conic
v.0.40
-Fixed problems with the following variations:
--Var. 11 Ex
--Var. 33 Juliascope
-All of the variations listed in the flame paper are now believed to work completely.
-Added support for background colors
-Hopefully fixed issue regarding international number formats and loading flame files
v.0.38
-Added suppord for symmetry kind parameter
-Fixed crash bug when rendering a loop to file after maximizing the window
v.0.36
-Added a handful of nice features to the GUI:
--The title bar now displays the name of the currently open file
--Maximize/restore now resizes the rendering target correctly
--Added save file dialog for render loop to file
-Set the render loop to file to render a .AVI with a playback speed of 70 FPS.  Number of frames is adjusted accordingly.
v.0.35
-Added support for the 701 standard palettes from flam3!
--Now you can render many of the really old loops without problems
--Also, support for the hue parameter has been added
-Fixed parsing for palettes in hexidecimal format
-Fixed a few variations
--Var. 23 Blob
--Var. 30 Perspective
--Var. 31 Noise
-Note: the following variations still have problems:
--Var. 12 Ex
--Var. 33 Juliascope
-In addition, the final few variations are untested:
--Var. 49 Disc2
--Var. 50 Supershape
--Var. 51 Flower
--Var. 52 Conic
--Var. 53 Move
v.0.33
-Updated to new internal API
-Fixed bug with final xform coloration
-Added motion blur to real time renders
v.0.32
-Added motion blur
-Shrank batch size in order to:
--Perform better on low end systems
--Increase motion blur sample number
v.0.30
-Major update!
--Added render animation to file
--Fixed bluescreen bug from 0.21 or so
v.0.23
-Updated version of cudart.dll packaged with the release.  Hopefully this will solve some poeples' problems getting flam4 to render.
v.0.22
-Fixed problem with glitchiness in 0.21, all performance increases retained!
v.0.21
-30% performance increase!
-But also some glitchiness at low quality settings
v.0.20
-Fixed minor problem with image scaling using dimensions of the entire window instead of just the render area
-Tweaked density estimation some more
-Added option to turn off animation and simple display the first frame in a loop, as per what flam3-render renders
v.0.18
-Made some improvements to the density estimation quality
-Improved antialiasing
-Edges now load without crashing the program
v.0.15
-Added post transformations
v.0.14
-Fixed a handful of variations where theta and phi had gotten mixed up.
v.0.13
-Added antialiasing
-Performance optimizations
-Added correct cudart.dll to release.
v.0.11
-Added quality slider
v.0.10
-Post processing and gamma code seems to work finally
-Added rotate to the supported flame parameters
v.0.09
-Corrected a couple xforms
-Corrected the palette code
v.0.08 
-Corrected issue with sheep animating incorrectly.
-Added density estimation!
v.0.06
-First official CUDA release!
