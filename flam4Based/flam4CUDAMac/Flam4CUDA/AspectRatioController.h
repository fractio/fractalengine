//
//  AspectRatioController.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>

enum CropOrientation {
	horizontalCrop = NSOnState,
	verticalCrop   = NSOffState
};

enum AspectRatioMode {
	StandardSizes     = 0,
	CustomSize        = 1,
	CustomAspectRatio = 2,
};

@interface AspectRatioController : NSWindowController {
	IBOutlet NSTextField *widthField;
	IBOutlet NSTextField *heightField;
	IBOutlet NSTextField *aspectRatioField;
	
	NSInteger             selectedMonitor;
	enum AspectRatioMode  arMode;
	NSNumber             *width;
	NSNumber             *height;
	NSNumber             *aspectRatio;
	float                *aspectRatios;
	enum CropOrientation  orientation;
}

@property (assign) NSInteger             selectedMonitor;
@property (assign) enum AspectRatioMode  arMode;
@property (retain) NSNumber             *width;
@property (retain) NSNumber             *height;
@property (retain) NSNumber             *aspectRatio;
@property (assign) enum CropOrientation  orientation;

- (IBAction)ok:(id)sender;
- (IBAction)cancel:(id)sender;

+ (NSString *)stringForAspectRatio:(NSNumber *)aspectRatio orientation:(NSNumber *)_orientation;
+ (float)aspectRatioForString:(NSString *)label orientation:(NSNumber *)_orientation;
+ (float)arFromWidthXHeight:(NSString *)label;

- (id)initWithOrientation:(NSInteger)_orientation;
- (NSString *)aspectRatioAsString;
- (NSArray *)getMonitorSizes;

@end
