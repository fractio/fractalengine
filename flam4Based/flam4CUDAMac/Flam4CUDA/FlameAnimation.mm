/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FlameAnimation.h"
#include <math.h>

flameParameterList::flameParameterList() {
	flame     = (Flame *)nullptr;
	originalQuality = 0;
	nextFlame = (flameParameterList *)nullptr;
}

flameParameterList::flameParameterList(Flame* _flame) {
	flame     = _flame;
	originalQuality = 0;
	nextFlame = (flameParameterList *)nullptr;
}


flameAnimation::flameAnimation()
{
	for (int n = 0; n < sizeof(xForm)/sizeof(float); n++)
	{
		this->emptyXform.xFormVars[n] = 0.0f;
	}
	this->emptyXform.a = 1.0f;
	this->emptyXform.e = 1.0f;
	this->emptyXform.linear = 1.0f;
	this->emptyXform.symmetry = 1.0f;
	this->emptyXform.weight = 1.0f;
	this->emptyXform.pa = 1.0f;
	this->emptyXform.pe = 1.0f;
}

void flameAnimation::Interpolate(float time, Flame* startFlame, Flame* endFlame, Flame** result)
{
	float t = 0.5f*cos(3.141592653589793*time)+0.5f;
	/////////////Something's not working right here...//////////////////
	if (!(*result))
		*result = new Flame((startFlame->numTrans > endFlame->numTrans) ? startFlame->numTrans : endFlame->numTrans,
			(startFlame->numColors > endFlame->isFinalXform) ? startFlame->numColors : endFlame->numColors);
	if (((*result)->numTrans < startFlame->numTrans)||((*result)->numTrans < endFlame->numTrans) || ((*result)->numColors < startFlame->numColors) || ((*result)->numColors < endFlame->numColors))
	{
		delete *result;
		*result = new Flame((startFlame->numTrans > endFlame->numTrans) ? startFlame->numTrans : endFlame->numTrans,
			(startFlame->numColors > endFlame->isFinalXform) ? startFlame->numColors : endFlame->numColors);
	}
	/////////////////////////////////////////////////////////////////////
	//interpolate parameters for each xform
	for (int x = 0; x < (*result)->numTrans; x++)
	{
		xForm* left = (startFlame->numTrans > x) ? &(startFlame->trans[x]) : &emptyXform;
		xForm* right = (endFlame->numTrans > x) ? &(endFlame->trans[x]) : &emptyXform;
		for (int n = 0; n < sizeof(xForm)/sizeof(float); n++)
		{
			(*result)->trans[x].xFormVars[n] = t*left->xFormVars[n]+(1.0f-t)*right->xFormVars[n];
		}
		//rotationally interpolate linear coefs
		//first, compute angles and radii for the 6 sets of input vectors
		float theta1, theta2, theta3, theta11, theta12, theta13, theta21, theta22, theta23, r1, r2, r3, r11, r12, r13, r21, r22, r23;
		theta11 = atan2(left->b,left->a+.000001f);
		theta12 = atan2(left->e,left->d+.000001f);
		theta13 = atan2(left->f,left->c+.000001f);
		theta21 = atan2(right->b,right->a+.000001f);
		theta22 = atan2(right->e,right->d+.000001f);
		theta23 = atan2(right->f,right->c+.000001f);
		r11 = sqrt(left->a*left->a+left->b*left->b);
		r12 = sqrt(left->d*left->d+left->e*left->e);
		r13 = sqrt(left->c*left->c+left->f*left->f);
		r21 = sqrt(right->a*right->a+right->b*right->b);
		r22 = sqrt(right->d*right->d+right->e*right->e);
		r23 = sqrt(right->c*right->c+right->f*right->f);
		//interpolate them down to the 3 output vectors
		r1 = t*r11+(1.0f-t)*r21;
		r2 = t*r12+(1.0f-t)*r22;
		r3 = t*r13+(1.0f-t)*r23;
		if ((theta21-theta11)>3.1415926535)
			theta11+=6.283185f;
		else if ((theta11-theta21)>3.1415926535)
			theta21+=6.283185f;
		theta1 = t*theta11+(1.0f-t)*theta21;
		if ((theta22-theta12)>3.1415926535)
			theta12+=6.283185f;
		else if ((theta12-theta22)>3.1415926535)
			theta22+=6.283185f;
		theta2 = t*theta12+(1.0f-t)*theta22;
		if ((theta23-theta13)>3.1415926535)
			theta13+=6.283185f;
		else if ((theta13-theta23)>3.1415926535)
			theta23+=6.283185f;
		theta3 = t*theta13+(1.0f-t)*theta23;
		//And finally go from polar back to standard coordinates
		(*result)->trans[x].a = r1*cos(theta1);
		(*result)->trans[x].b = r1*sin(theta1);
		(*result)->trans[x].c = r3*cos(theta3);
		(*result)->trans[x].d = r2*cos(theta2);
		(*result)->trans[x].e = r2*sin(theta2);
		(*result)->trans[x].f = r3*sin(theta3);
		//repeat for post transformations
		//first, compute angles and radii for the 6 sets of input vectors
		theta11 = atan2(left->pb,left->pa+.000001f);
		theta12 = atan2(left->pe,left->pd+.000001f);
		theta13 = atan2(left->pf,left->pc+.000001f);
		theta21 = atan2(right->pb,right->pa+.000001f);
		theta22 = atan2(right->pe,right->pd+.000001f);
		theta23 = atan2(right->pf,right->pc+.000001f);
		r11 = sqrt(left->pa*left->pa+left->pb*left->pb);
		r12 = sqrt(left->pd*left->pd+left->pe*left->pe);
		r13 = sqrt(left->pc*left->pc+left->pf*left->pf);
		r21 = sqrt(right->pa*right->pa+right->pb*right->pb);
		r22 = sqrt(right->pd*right->pd+right->pe*right->pe);
		r23 = sqrt(right->pc*right->pc+right->pf*right->pf);
		//interpolate them down to the 3 output vectors
		r1 = t*r11+(1.0f-t)*r21;
		r2 = t*r12+(1.0f-t)*r22;
		r3 = t*r13+(1.0f-t)*r23;
		if ((theta21-theta11)>3.1415926535)
			theta11+=6.283185f;
		else if ((theta11-theta21)>3.1415926535)
			theta21+=6.283185f;
		theta1 = t*theta11+(1.0f-t)*theta21;
		if ((theta22-theta12)>3.1415926535)
			theta12+=6.283185f;
		else if ((theta12-theta22)>3.1415926535)
			theta22+=6.283185f;
		theta2 = t*theta12+(1.0f-t)*theta22;
		if ((theta23-theta13)>3.1415926535)
			theta13+=6.283185f;
		else if ((theta13-theta23)>3.1415926535)
			theta23+=6.283185f;
		theta3 = t*theta13+(1.0f-t)*theta23;
		//And finally go from polar back to standard coordinates
		(*result)->trans[x].pa = r1*cos(theta1);
		(*result)->trans[x].pb = r1*sin(theta1);
		(*result)->trans[x].pc = r3*cos(theta3);
		(*result)->trans[x].pd = r2*cos(theta2);
		(*result)->trans[x].pe = r2*sin(theta2);
		(*result)->trans[x].pf = r3*sin(theta3);
		//lastly, load up the buffer so that the animation code can rotate the xform without losing data
		(*result)->transAff[x].a = (*result)->trans[x].a;
		(*result)->transAff[x].b = (*result)->trans[x].b;
		(*result)->transAff[x].d = (*result)->trans[x].d;
		(*result)->transAff[x].e = (*result)->trans[x].e;
	}
	if ((startFlame->isFinalXform!=0) || (endFlame->isFinalXform!=0))
	{
		(*result)->isFinalXform = 1;
		xForm* left = (startFlame->isFinalXform != 0) ? &(startFlame->finalXform) : &emptyXform;
		xForm* right = (endFlame->isFinalXform != 0) ? &(endFlame->finalXform) : &emptyXform;
		for (int n = 0; n < sizeof(xForm)/sizeof(float); n++)
			(*result)->finalXform.xFormVars[n] = t*left->xFormVars[n]+(1.0f-t)*right->xFormVars[n];
		//rotationally interpolate linear coefs
		//first, compute angles and radii for the 6 sets of input vectors
		float theta1, theta2, theta3, theta11, theta12, theta13, theta21, theta22, theta23, r1, r2, r3, r11, r12, r13, r21, r22, r23;
		theta11 = atan2(left->b,left->a+.000001f);
		theta12 = atan2(left->e,left->d+.000001f);
		theta13 = atan2(left->f,left->c+.000001f);
		theta21 = atan2(right->b,right->a+.000001f);
		theta22 = atan2(right->e,right->d+.000001f);
		theta23 = atan2(right->f,right->c+.000001f);
		r11 = sqrt(left->a*left->a+left->b*left->b);
		r12 = sqrt(left->d*left->d+left->e*left->e);
		r13 = sqrt(left->c*left->c+left->f*left->f);
		r21 = sqrt(right->a*right->a+right->b*right->b);
		r22 = sqrt(right->d*right->d+right->e*right->e);
		r23 = sqrt(right->c*right->c+right->f*right->f);
		//interpolate them down to the 3 output vectors
		r1 = t*r11+(1.0f-t)*r21;
		r2 = t*r12+(1.0f-t)*r22;
		r3 = t*r13+(1.0f-t)*r23;
		if ((theta21-theta11)>3.1415926535)
			theta11+=6.283185f;
		else if ((theta11-theta21)>3.1415926535)
			theta21+=6.283185f;
		theta1 = t*theta11+(1.0f-t)*theta21;
		if ((theta22-theta12)>3.1415926535)
			theta12+=6.283185f;
		else if ((theta12-theta22)>3.1415926535)
			theta22+=6.283185f;
		theta2 = t*theta12+(1.0f-t)*theta22;
		if ((theta23-theta13)>3.1415926535)
			theta13+=6.283185f;
		else if ((theta13-theta23)>3.1415926535)
			theta23+=6.283185f;
		theta3 = t*theta13+(1.0f-t)*theta23;
		//And finally go from polar back to standard coordinates
		(*result)->finalXform.a = r1*cos(theta1);
		(*result)->finalXform.b = r1*sin(theta1);
		(*result)->finalXform.c = r3*cos(theta3);
		(*result)->finalXform.d = r2*cos(theta2);
		(*result)->finalXform.e = r2*sin(theta2);
		(*result)->finalXform.f = r3*sin(theta3);
		//repeat for post transformations
		//first, compute angles and radii for the 6 sets of input vectors
		theta11 = atan2(left->pb,left->pa+.000001f);
		theta12 = atan2(left->pe,left->pd+.000001f);
		theta13 = atan2(left->pf,left->pc+.000001f);
		theta21 = atan2(right->pb,right->pa+.000001f);
		theta22 = atan2(right->pe,right->pd+.000001f);
		theta23 = atan2(right->pf,right->pc+.000001f);
		r11 = sqrt(left->pa*left->pa+left->pb*left->pb);
		r12 = sqrt(left->pd*left->pd+left->pe*left->pe);
		r13 = sqrt(left->pc*left->pc+left->pf*left->pf);
		r21 = sqrt(right->pa*right->pa+right->pb*right->pb);
		r22 = sqrt(right->pd*right->pd+right->pe*right->pe);
		r23 = sqrt(right->pc*right->pc+right->pf*right->pf);
		//interpolate them down to the 3 output vectors
		r1 = t*r11+(1.0f-t)*r21;
		r2 = t*r12+(1.0f-t)*r22;
		r3 = t*r13+(1.0f-t)*r23;
		if ((theta21-theta11)>3.1415926535)
			theta11+=6.283185f;
		else if ((theta11-theta21)>3.1415926535)
			theta21+=6.283185f;
		theta1 = t*theta11+(1.0f-t)*theta21;
		if ((theta22-theta12)>3.1415926535)
			theta12+=6.283185f;
		else if ((theta12-theta22)>3.1415926535)
			theta22+=6.283185f;
		theta2 = t*theta12+(1.0f-t)*theta22;
		if ((theta23-theta13)>3.1415926535)
			theta13+=6.283185f;
		else if ((theta13-theta23)>3.1415926535)
			theta23+=6.283185f;
		theta3 = t*theta13+(1.0f-t)*theta23;
		//And finally go from polar back to standard coordinates
		(*result)->finalXform.pa = r1*cos(theta1);
		(*result)->finalXform.pb = r1*sin(theta1);
		(*result)->finalXform.pc = r3*cos(theta3);
		(*result)->finalXform.pd = r2*cos(theta2);
		(*result)->finalXform.pe = r2*sin(theta2);
		(*result)->finalXform.pf = r3*sin(theta3);
	}
	else
	{
		(*result)->isFinalXform = 0;
	}
	//interpolate palettes
	if (startFlame->numColors > endFlame->numColors)
	{
		for (int n = 0; n < startFlame->numColors; n++)
		{
			int otherIndex = n*startFlame->numColors/endFlame->numColors;  //for palettes of unequal size, we're just doing nearest point sampling on the smaller palette
			(*result)->colorIndex[n].r = t*startFlame->colorIndex[n].r+(1.0f-t)*endFlame->colorIndex[otherIndex].r;
			(*result)->colorIndex[n].g = t*startFlame->colorIndex[n].g+(1.0f-t)*endFlame->colorIndex[otherIndex].g;
			(*result)->colorIndex[n].b = t*startFlame->colorIndex[n].b+(1.0f-t)*endFlame->colorIndex[otherIndex].b;
		}
	}
	else
	{
		for (int n = 0; n < endFlame->numColors; n++)
		{
			int otherIndex = n*startFlame->numColors/endFlame->numColors;  //for palettes of unequal size, we're just doing nearest point sampling on the smaller palette
			(*result)->colorIndex[n].r = t*startFlame->colorIndex[otherIndex].r+(1.0f-t)*endFlame->colorIndex[n].r;
			(*result)->colorIndex[n].g = t*startFlame->colorIndex[otherIndex].g+(1.0f-t)*endFlame->colorIndex[n].g;
			(*result)->colorIndex[n].b = t*startFlame->colorIndex[otherIndex].b+(1.0f-t)*endFlame->colorIndex[n].b;
		}
	}
	//interpolate other parameters
	(*result)->background.r = t*startFlame->background.r+(1.0f-t)*endFlame->background.r;
	(*result)->background.g = t*startFlame->background.g+(1.0f-t)*endFlame->background.g;
	(*result)->background.b = t*startFlame->background.b+(1.0f-t)*endFlame->background.b;
	(*result)->brightness = t*startFlame->brightness+(1.0f-t)*endFlame->brightness;
	(*result)->center[0] = t*startFlame->center[0]+(1.0f-t)*endFlame->center[0];
	(*result)->center[1] = t*startFlame->center[1]+(1.0f-t)*endFlame->center[1];
	(*result)->gamma = t*startFlame->gamma+(1.0f-t)*endFlame->gamma;
	(*result)->hue = t*startFlame->hue+(1.0f-t)*endFlame->hue;
//	(*result)->quality = t*startFlame->quality+(1.0f-t)*endFlame->quality;
	(*result)->rotation = t*startFlame->rotation+(1.0f-t)*endFlame->rotation;
	(*result)->size[0] = t*startFlame->size[0]+(1.0f-t)*endFlame->size[0];
	(*result)->size[1] = t*startFlame->size[1]+(1.0f-t)*endFlame->size[1];
	(*result)->symmetryKind = t*startFlame->symmetryKind+(1.0f-t)*endFlame->symmetryKind;
	(*result)->vibrancy = t*startFlame->vibrancy+(1.0f-t)*endFlame->vibrancy;
//	(*result)->quality = t*startFlame->quality + (1.0f-t)*endFlame->quality;
	return;
}