/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KEYFRAME_H
#define KEYFRAME_H

#include <string>
#include <vector>
using std::string;
using std::vector;
#include "rgba.h"

struct coefs
{
	float ctheta;
	float cr;
	float xtheta;
	float xr;
	float ytheta;
	float yr;
};

class KeyframeVarDesc
{
public:
	string           name;
	vector<string> parameters;
	string           code;				//reserved
};

class KeyframeVariation
{
public:
	int descIndex;
	float weight;
	vector<float> parameters;
};

class KeyframeXform
{
public:
	float                                  opacity;
	float                                  weight;
	coefs *                                affine;
	vector<KeyframeVariation *>          vars;
	vector<KeyframeXform *>              post;
//	Dictionary<string,KeyframeXform *> * postXformDict;
};

class Keyframe
{
public:
	
	float                                  brightness;
	float                                  gamma;
	float                                  vibrancy;
	coefs *                                window;
	vector<KeyframeXform *>              Xforms;
//	Dictionary<string,KeyframeXform *> * xformDict;
	vector<KeyframeXform *>              FinalXforms;
//	Dictionary<string,KeyframeXform *> * finalXformDict;
//	Palette *                              palette;
};
#endif
