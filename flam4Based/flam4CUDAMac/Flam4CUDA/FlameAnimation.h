/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FLAMEANIMATION_H
#define FLAMEANIMATION_H

#include "FlameData.h"
#include "FlameMetaData.h"

#define nullptr (void *)0

struct flameAnimation
{
	xForm emptyXform;
	flameAnimation();
	void Interpolate(float time, Flame* startFlame, Flame* endFlame, Flame** result);
};

struct flameParameterList
{
	Flame* flame;				// the flame definition
	int originalQuality;		// the original quality from the file
	FlameMetaData* flameMeta;	// Flame metadata
	flameParameterList* nextFlame;
	
	flameParameterList();	
	flameParameterList(Flame* _flame);
};
#endif
