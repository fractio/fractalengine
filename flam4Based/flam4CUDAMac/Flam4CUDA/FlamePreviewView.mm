/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "GLUT/glut.h"

#import "FlamePreviewView.h"
#import "FlameAnimation.h"
#import "PreviewController.h"
#import "PreferenceController.h"
#import "FlameDocument.h"

extern volatile bool isRendering;

@implementation FlamePreviewView

@synthesize imageRep;
@synthesize backgroundColor;
@synthesize previousBounds;
@synthesize accumScroll;
@synthesize normCropRect;
@synthesize selectedSpot;
@synthesize cropAspectRatio;

static float red[4]   = { 1.0f, 0.0f, 0.0f, 1.0f };
static float black[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
static float blue[4]  = { 0.0f, 0.0f, 1.0f, 1.0f };
static float white[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
static float yellow[4] = { 1.0f, 1.0f, 0.0f, 1.0f };
static float blackAlpha[4] = { 0.0f, 0.0f, 0.0f, 0.5f };

const float scrollWheelFactor = 1.f/30.f;
const float magnifyFactor     = 1.f/2.f;

static NSCursor *shiftHorizCursor;
static NSCursor *shiftVertCursor;
static NSCursor *shiftNWCursor;
static NSCursor *shiftSWCursor;

+ (void)initCursors {
	NSImage *shiftHoriz = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shiftHoriz" ofType:@"png"]];
	NSImage *shiftVert  = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shiftVert" ofType:@"png"]];
	NSImage *shiftNW    = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shiftNW" ofType:@"png"]];
	NSImage *shiftSW    = [[NSImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shiftSW" ofType:@"png"]];

	shiftHorizCursor = [[NSCursor alloc] initWithImage:shiftHoriz hotSpot:NSMakePoint(12.f, 12.f)];
	shiftVertCursor  = [[NSCursor alloc] initWithImage:shiftVert  hotSpot:NSMakePoint(12.f, 12.f)];
	shiftNWCursor    = [[NSCursor alloc] initWithImage:shiftNW    hotSpot:NSMakePoint(12.f, 12.f)];
	shiftSWCursor    = [[NSCursor alloc] initWithImage:shiftSW    hotSpot:NSMakePoint(12.f, 12.f)];
	
	[shiftHoriz release];
	[shiftVert  release];
	[shiftNW    release];
	[shiftSW    release];
}

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		imageRep = nil;
		previousBounds = NSMakeRect(0, 0, -1, -1); // create a bogus bounds so reshape will happen on first go round
		scrollTimer = nil;
		textureName = 0;
    }
    return self;
}

- (void)dealloc {
	if (glIsTexture(textureName))
		glDeleteTextures(1, &textureName);
	[scrollTimer invalidate];
	[scrollTimer release];
	[imageRep release];
	[accumScrollArray release];
	[accumRotateArray release];
	[accumMagnifyArray release];
	[super dealloc];
}

// place to do initialization for View after its state has been restored from Nib file
- (void) awakeFromNib
{
	if ([super respondsToSelector:@selector(awakeFromNib:)]) // on Leopard this is not true
		[super awakeFromNib];
	
	dragging = NO;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSData *colorAsData = [defaults objectForKey:Flam4ViewBgColorKey];
	[self setBackgroundColor:[NSKeyedUnarchiver unarchiveObjectWithData:colorAsData]];
	
	previousBounds = NSMakeRect(0, 0, -1, -1); // create a bogus bounds so reshape will happen on first go round
	
	PreviewController *ctrl = [[self window] windowController];
	[ctrl viewInit];
	
	lastScrollWheel = [[NSDate date] timeIntervalSince1970]; // initialize last scroll wheel time to now
	requestTime     = lastScrollWheel;
	accumScroll     = 0.0f;
	accumBaseline   = accumScroll;
	accumScrollArray  = [[NSMutableArray arrayWithCapacity:16] retain];
	accumRotateArray  = [[NSMutableArray arrayWithCapacity:16] retain];
	accumMagnifyArray = [[NSMutableArray arrayWithCapacity:16] retain];
	accumMagnify    = 0.0f;
	magnifyBaseline = 0.0f;
	accumRotate    = 0.0f;
	rotateBaseline = 0.0f;
	
	scrollTimer = [[NSTimer scheduledTimerWithTimeInterval:0.2 
													target:self 
												  selector:@selector(tick:)  
												  userInfo:nil 
												   repeats:YES] retain];
	normCropRect  = NSMakeRect(-1.f, -1.f, -1.f, -1.f);
	selectedSpot = none;
	cropAspectRatio = [self bounds].size.width/[self bounds].size.height;
	
	if (! shiftNWCursor)
		[FlamePreviewView initCursors];
	trackingArea = [[NSTrackingArea alloc] initWithRect:[self bounds]
												options: (NSTrackingMouseEnteredAndExited | NSTrackingActiveWhenFirstResponder )
												  owner:self userInfo:nil];
	[self addTrackingArea:trackingArea];
	textureDirty = YES; // we want to load a texture the first time we can
	[ ctrl setOrigContentViewSize:[[[self window] contentView] bounds].size];
}


- (BOOL)translateEditStart:(NSEvent *)theEvent {
	unsigned modifiers = [theEvent modifierFlags];
	modifiers &= (NSShiftKeyMask | NSControlKeyMask);
	
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:TranslateEditMode] || 
		([ctrl isEditMode:CombinedEditMode] && (modifiers == 0));
}

- (BOOL)scaleEditStart:(NSEvent *)theEvent {
	unsigned modifiers = [theEvent modifierFlags];
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:ScaleEditMode] || ([ctrl isEditMode:CombinedEditMode] && modifiers & NSShiftKeyMask);
}

- (BOOL)rotateEditStart:(NSEvent *)theEvent {
	unsigned modifiers = [theEvent modifierFlags];
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:RotateEditMode] || ([ctrl isEditMode:CombinedEditMode] && modifiers & NSControlKeyMask);
}

- (BOOL)doTranslateEdit {
	unsigned modifiers = [[NSApp currentEvent] modifierFlags];
	//unsigned modifiers = [NSEvent modifierFlags];
	modifiers &= (NSShiftKeyMask | NSControlKeyMask);
	
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:TranslateEditMode] || ([ctrl isEditMode:CombinedEditMode] && (modifiers == 0));
}

- (BOOL)doScaleEdit {
	unsigned modifiers = [[NSApp currentEvent] modifierFlags];
	//unsigned modifiers = [NSEvent modifierFlags];
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:ScaleEditMode] || ([ctrl isEditMode:CombinedEditMode] && modifiers & NSShiftKeyMask);
}

- (BOOL)doRotateEdit {
	unsigned modifiers = [[NSApp currentEvent] modifierFlags];
	//unsigned modifiers = [NSEvent modifierFlags];
	PreviewController *ctrl = [[self window] windowController];
	return [ctrl isEditMode:RotateEditMode] || ([ctrl isEditMode:CombinedEditMode] && modifiers & NSControlKeyMask);
}

- (void)drawCirclePoint:(NSPoint)point {
	// anti-alias settings
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_POINT_SMOOTH);
	
	// draw a red point at point
	glColor4fv(white);
	glPointSize(10.0f);
	
	glBegin(GL_POINTS);
	glVertex3f(point.x, point.y, 0.f);
	glEnd();

	// draw a red point at point
	glColor4fv(red);
	glPointSize(8.0f);
	
	glBegin(GL_POINTS);
	glVertex3f(point.x, point.y, 0.f);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_POINT_SMOOTH);
}

- (void)drawCrossPoint:(NSPoint)point {
	// anti-alias settings
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	
	// draw a diagonal blue cross at point
	glColor4fv(white);
	glLineWidth(4.f);
	
	glBegin(GL_LINES);
	glVertex3f(point.x + 22.f, point.y + 22.f, 0.f);
	glVertex3f(point.x - 22.f, point.y - 22.f, 0.f);
	glVertex3f(point.x + 22.f, point.y - 22.f, 0.f);
	glVertex3f(point.x - 22.f, point.y + 22.f, 0.f);
	glEnd();
	
	// draw a diagonal blue cross at point
	glColor4fv(blue);
	glLineWidth(2.f);
	
	glBegin(GL_LINES);
	glVertex3f(point.x + 22.f, point.y + 22.f, 0.f);
	glVertex3f(point.x - 22.f, point.y - 22.f, 0.f);
	glVertex3f(point.x + 22.f, point.y - 22.f, 0.f);
	glVertex3f(point.x - 22.f, point.y + 22.f, 0.f);
	glEnd();
	
	glDisable(GL_BLEND);
	glDisable(GL_LINE_SMOOTH);
}

- (void)drawDragFrom:(NSPoint)from to:(NSPoint)to color:(float *)color {
	// anti-alias settings
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	
	// draw a white line
	glColor4fv(white);
	glLineWidth(4.f);
	
	glBegin(GL_LINES);
	glVertex3f(from.x, from.y, 0.f);
	glVertex3f(to.x, to.y, 0.f);
	glEnd();
	
	// draw a colored line
	glColor4fv(color);
	glLineWidth(2.f);
	
	glBegin(GL_LINES);
	glVertex3f(from.x, from.y, 0.f);
	glVertex3f(to.x, to.y, 0.f);
	glEnd();

	glDisable(GL_BLEND);
	glDisable(GL_LINE_SMOOTH);
}

- (bool)intersectRayWithRect:(NSRect)rect from:(NSPoint)from past:(NSPoint)past at:(NSPoint *)at {
	// degenerate case
	if (NSEqualPoints(from, past))
		return NO;
	if (rect.size.width == 0.f || rect.size.height == 0.f)
		return NO;
	if (!NSPointInRect(from, rect)) 
		return NO;
	
	//vertical ray case
	if (from.x == past.x) {
		if(past.y > from.y)
			*at = NSMakePoint(from.x, rect.origin.y + rect.size.height);
		else
			*at = NSMakePoint(from.x, rect.origin.y);
		return YES;
	}
	//horizontal ray case
	else if (from.y == past.y) {
		if(past.x > from.x)
			*at = NSMakePoint(rect.origin.x + rect.size.width, from.y);
		else
			*at = NSMakePoint(rect.origin.x, from.y);
		return YES;
	}
	float bottomY = rect.origin.y;
	float topY    = rect.origin.y + rect.size.height;
	float leftX   = rect.origin.x;
	float rightX  = rect.origin.x + rect.size.width;
	// ray slope for: y = ax + b
	float a = (from.y - past.y)/(from.x - past.x);
	float b = from.y - a *from.x;
	
	NSPoint topI    = NSMakePoint((topY - b)/a, topY);
	NSPoint rightI  = NSMakePoint(rightX, a*rightX + b);
	NSPoint bottomI = NSMakePoint((bottomY - b)/a, bottomY);
	NSPoint leftI   = NSMakePoint(leftX, a*leftX + b);

	if (topI.x >= leftX && topI.x <= rightX && past.y > from.y) {
		*at = topI;
		return YES;
	}
	else if (bottomI.x >= leftX && bottomI.x <= rightX && past.y < from.y) {
		*at = bottomI;
		return YES;
	}
	else if (leftI.y >= bottomY && leftI.y <= topY && past.x < from.x) {
		*at = leftI;
		return YES;
	}
	else {
		*at = rightI;
		return YES;
	}
}

// draw a ray from the "from" point through the "past" point to rectangle edge
- (void)drawDragFrom:(NSPoint)from toEdgePast:(NSPoint)past color:(float *)color {
	
	NSPoint at;	
	if ([self intersectRayWithRect:[self bounds] from:from past:past at:&at]) {
		// anti-alias settings
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_LINE_SMOOTH);
		
		// draw a white line
		glColor4fv(white);
		glLineWidth(4.f);
		
		glBegin(GL_LINES);
		glVertex3f(from.x, from.y, 0.f);
		glVertex3f(at.x, at.y, 0.f);
		glEnd();
		
		// draw a colored line
		glColor4fv(color);
		glLineWidth(2.f);
		
		glBegin(GL_LINES);
		glVertex3f(from.x, from.y, 0.f);
		glVertex3f(at.x, at.y, 0.f);
		glEnd();
		
		glDisable(GL_BLEND);
		glDisable(GL_LINE_SMOOTH);
	}
}

// draw the hotspot and hightlight if selected
- (void)drawHotSpot:(eHotSpot) hotSpot x:(float)x y:(float)y {
	if (selectedSpot == hotSpot)
		glColor4fv(red);
	else
		glColor4fv(blue);
	glVertex3f(x, y, 0.f);
}

// convert view relative rectangle to normalized
NSRect normalizeRect(NSRect rect, NSRect bounds) {
	return NSMakeRect(rect.origin.x / bounds.size.width, 
					  rect.origin.y / bounds.size.height, 
					  rect.size.width / bounds.size.width, 
					  rect.size.height / bounds.size.height);
}

// convert normalized rectangle to view relative dimensions
NSRect deNormalizeRect(NSRect normalizedRect, NSRect bounds) {
	return NSMakeRect(normalizedRect.origin.x * bounds.size.width, 
					  normalizedRect.origin.y * bounds.size.height, 
					  normalizedRect.size.width * bounds.size.width, 
					  normalizedRect.size.height * bounds.size.height);
}

// return the crop rectangle in view coordinates
- (NSRect)cropRect {
	return deNormalizeRect(normCropRect, [self bounds]);
}

- (void)drawCropMaskWithColor:(float *)color rect:(NSRect)rect {
	// anti-alias settings
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glEnable(GL_POLYGON_SMOOTH);
	NSRect b = rect;
	NSRect c = deNormalizeRect(normCropRect, b);
	
	// draw a white line
	glColor4fv(color);
	
	glBegin(GL_QUADS);
	glVertex3f(0.f,               0.f,        0.f);  //bottom
	glVertex3f(0.f,               c.origin.y, 0.f);	
	glVertex3f(b.size.width,      c.origin.y, 0.f);
	glVertex3f(b.size.width,      0.f,        0.f);

	glVertex3f(0.f,               c.origin.y + c.size.height, 0.f); // top
	glVertex3f(0.f,               b.size.height,              0.f);
	glVertex3f(b.size.width,      b.size.height,              0.f);
	glVertex3f(b.size.width,      c.origin.y + c.size.height, 0.f);

	glVertex3f(0.f,               c.origin.y,                 0.f); // mid-left
	glVertex3f(0.f,               c.origin.y + c.size.height, 0.f);
	glVertex3f(c.origin.x,        c.origin.y + c.size.height, 0.f);
	glVertex3f(c.origin.x,        c.origin.y,                 0.f);
	
	glVertex3f(c.origin.x + c.size.width, c.origin.y,                 0.f); // mid-left
	glVertex3f(c.origin.x + c.size.width, c.origin.y + c.size.height, 0.f);
	glVertex3f(b.size.width,              c.origin.y + c.size.height, 0.f);
	glVertex3f(b.size.width,              c.origin.y,                 0.f);	
	glEnd();
	
	glDisable(GL_BLEND);
	//glDisable(GL_POLYGON_SMOOTH);
	
	//glEnable(GL_LINE_SMOOTH);
	
	// draw a cropping lines
	glColor4fv(black);
	glLineWidth(2.f);
	
	glBegin(GL_LINES);
	glVertex3f(c.origin.x,                0.f,           0.f);
	glVertex3f(c.origin.x,                b.size.height, 0.f);
	glVertex3f(c.origin.x + c.size.width, 0.f,           0.f);
	glVertex3f(c.origin.x + c.size.width, b.size.height, 0.f);
	glVertex3f(0.f,                       c.origin.y,    0.f);
	glVertex3f(b.size.width,              c.origin.y,    0.f);
	glVertex3f(0.f,                       c.origin.y + c.size.height, 0.f);
	glVertex3f(b.size.width,              c.origin.y + c.size.height, 0.f);
	glEnd();
	//glDisable(GL_LINE_SMOOTH);
	
	glEnable(GL_POINT_SMOOTH);
	
	glColor4fv(blue);
	glPointSize(7.0f);
	
	glBegin(GL_POINTS);
	// 4 corners
	[self drawHotSpot:atLL x:c.origin.x                y:c.origin.y];
	[self drawHotSpot:atLR x:c.origin.x + c.size.width y:c.origin.y];
	[self drawHotSpot:atUR x:c.origin.x + c.size.width y:c.origin.y + c.size.height];
	[self drawHotSpot:atUL x:c.origin.x                y:c.origin.y + c.size.height];
	
	// 4 mid-points
	[self drawHotSpot:atS x:c.origin.x + c.size.width/2.f y:c.origin.y];
	[self drawHotSpot:atW x:c.origin.x                    y:c.origin.y + c.size.height/2.f];
	[self drawHotSpot:atE x:c.origin.x + c.size.width     y:c.origin.y + c.size.height/2.f];
	[self drawHotSpot:atN x:c.origin.x + c.size.width/2.f y:c.origin.y + c.size.height];
	glEnd();
	glDisable(GL_POINT_SMOOTH);
}

- (void)drawText:(NSString *)text color:(float *)color rasterPos:(NSPoint)rasterPos {
	int charLength = [text length];
	glShadeModel(GL_FLAT);
	glColor4fv(black);
	glBegin(GL_QUADS);
	glVertex3f(rasterPos.x,                   rasterPos.y -  4.f, 0.f);
	glVertex3f(rasterPos.x + charLength*12.f, rasterPos.y -  4.f, 0.f);
	glVertex3f(rasterPos.x + charLength*12.f, rasterPos.y + 12.f, 0.f);
	glVertex3f(rasterPos.x,                   rasterPos.y + 12.f, 0.f);
	glEnd();
	
	const char *str = [text UTF8String];
	glColor4fv(color);
	glRasterPos3f(rasterPos.x, rasterPos.y, 0.f);
	for (int i = 0; i < strlen(str); i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, str[i]);
}

// handle mouse wheel movement
// deltaX positive => moved to left,  deltaY positive => moved up
- (void)scrollWheel:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	if ([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:CombinedEditMode]) {
		accumScroll     += [theEvent deltaY];

		lastScrollWheel  = [[NSDate date] timeIntervalSince1970];
		if ([theEvent deltaY] != 0.f && ![self needsDisplay]) { // filter out unneeded display repeats
			[self setNeedsDisplay:YES];
		}
	}
}

#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
// handle magnify trackpad gesture
- (void)magnifyWithEvent:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	if ([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:CombinedEditMode]) {
		accumMagnify     += [theEvent magnification];
		lastScrollWheel = [[NSDate date] timeIntervalSince1970];
		if ([theEvent magnification] != 0.f && ![self needsDisplay]) { // filter out unneeded display repeats
			[self setNeedsDisplay:YES];
		}
	}
}

// handle swipe events
- (void)swipeWithEvent:(NSEvent *)event {
	PreviewController *ctrl = [[self window] windowController];
	float deltaX = [event deltaX];
	[ctrl changeFrameBy:-(int)deltaX];
}

// handle rotate trackpad gesture
- (void)rotateWithEvent:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	if ([ctrl isEditMode:RotateEditMode] || [ctrl isEditMode:CombinedEditMode]) {
		accumRotate     += [theEvent rotation];
		lastScrollWheel  = [[NSDate date] timeIntervalSince1970];
		if ([theEvent rotation] != 0.f && ![self needsDisplay]) { // filter out unneeded display repeats
			[self setNeedsDisplay:YES];
		}
	}
}
#endif

// handle scroll event check timer ticks
- (void)tick:(NSTimer *)aTimer
{
	NSTimeInterval now = [[NSDate date] timeIntervalSince1970];

	// if at least 1/2 second has passed since last scroll wheel movement and we have accumulated some scroll lets fire off a rescale
	if (now > lastScrollWheel + 0.5f) {
		PreviewController *ctrl = [[self window] windowController];
		if ([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:RotateEditMode] || [ctrl isEditMode:CombinedEditMode]) {
			FlameDocument *doc = [[[self window] windowController] document];
			if (lastScrollWheel > requestTime + 1.0) {
				BOOL rerenderNeeded = NO;
				if (accumScroll != accumBaseline) {
					float scaleFactor = expf((accumScroll - accumBaseline) * -scrollWheelFactor);
					[accumScrollArray addObject:[NSNumber numberWithFloat:accumScroll]];
					requestTime       = [[NSDate date] timeIntervalSince1970];			
					[doc zoomByWheel:scaleFactor referencePoint:[ctrl referencePoint]];
					rerenderNeeded = YES;
				}
				if (accumMagnify != magnifyBaseline) {
					float scaleFactor = 1.f/expf((accumMagnify - magnifyBaseline) * -magnifyFactor);
					[accumMagnifyArray addObject:[NSNumber numberWithFloat:accumMagnify]];
					requestTime       = [[NSDate date] timeIntervalSince1970];			
					[doc zoomByWheel:scaleFactor referencePoint:[ctrl referencePoint]];
					rerenderNeeded = YES;
				}
				if (accumRotate != rotateBaseline) {
					float rotation    = accumRotate - rotateBaseline;
					[accumRotateArray addObject:[NSNumber numberWithFloat:accumRotate]];
					requestTime       = [[NSDate date] timeIntervalSince1970];
					[doc rotateDegrees:rotation referencePoint:[ctrl referencePoint]];
					rerenderNeeded = YES;
				}
				if (rerenderNeeded) 
					[ctrl renderAsPreview:YES];
			}
		}
	}
}

// set the bitmap image - reset scrollwheel and gesture state
- (void)setImageRep:(NSBitmapImageRep *)_imageRep {
	imageRep = [_imageRep retain];
	textureDirty = YES;
	
	if ([accumScrollArray count] > 0) {
		accumBaseline = [[accumScrollArray objectAtIndex:0] floatValue];
		[accumScrollArray removeObjectAtIndex:0];
		
		// force a rerender from tick - so we capture any event dribble at end of a scroll
		if (accumScroll != accumBaseline) {
			lastScrollWheel = requestTime + 1.5;
		}
	}
	if ([accumMagnifyArray count] > 0) {
		magnifyBaseline = [[accumMagnifyArray objectAtIndex:0] floatValue];
		[accumMagnifyArray removeObjectAtIndex:0];
		
		// force a rerender from tick - so we capture any event dribble at end of a scroll
		if (accumMagnify != magnifyBaseline) {
			lastScrollWheel = requestTime + 1.5;
		}
		if (accumRotate != rotateBaseline) {
			lastScrollWheel = requestTime + 1.5;
		}
	}
	if ([accumRotateArray count] > 0) {
		rotateBaseline = [[accumRotateArray objectAtIndex:0] floatValue];
		[accumRotateArray removeObjectAtIndex:0];
		
		// force a rerender from tick - so we capture any event dribble at end of a scroll
		if (accumRotate != rotateBaseline) {
			lastScrollWheel = requestTime + 1.5;
		}
		if (accumMagnify != magnifyBaseline) {
			lastScrollWheel = requestTime + 1.5;
		}
	}
}

// return the current scaleFactor based on mouse dragging in Scale edit mode
- (float)calcScaleFactor {
	PreviewController *ctrl = [[self window] windowController];
	NSRect bounds = [self bounds];
	NSPoint refPoint = NSMakePoint([ctrl referencePoint].x * bounds.size.width/2.f  + bounds.size.width/2.f, 
								   [ctrl referencePoint].y * bounds.size.height/2.f + bounds.size.height/2.f);
	
	float distStart = sqrtf((dragStartPoint.x - refPoint.x)*(dragStartPoint.x - refPoint.x) + 
							(dragStartPoint.y - refPoint.y)*(dragStartPoint.y - refPoint.y));
	float distCurrent = sqrtf((currentDragPoint.x - refPoint.x)*(currentDragPoint.x - refPoint.x) + 
							  (currentDragPoint.y - refPoint.y)*(currentDragPoint.y - refPoint.y));
	// draw the current scale factor
	return distCurrent/distStart;
	
}

// Generate texture 'texName' from 'theView' in current OpenGL context
-(void)textureFromImageRep:(NSBitmapImageRep *)bitmap textureName:(GLuint*)texName
{
    // Generate new texture object if none passed in
    if (*texName == 0)
		glGenTextures (1, texName);

	glEnable(GL_TEXTURE_RECTANGLE_EXT); // for non-power of two texture support
	glBindTexture (GL_TEXTURE_RECTANGLE_EXT, *texName);

	// Set proper unpacking row length for bitmap
    glPixelStorei(GL_UNPACK_ROW_LENGTH, [bitmap pixelsWide]);
	
    // Set byte aligned unpacking (needed for 3 byte per pixel bitmaps)
    glPixelStorei (GL_UNPACK_ALIGNMENT, 1);
	
	// Non-mipmap filtering (redundant for texture_rectangle)
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
    glTexParameteri(GL_TEXTURE_RECTANGLE_EXT, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    int samplesPerPixel = [bitmap samplesPerPixel];
	
    // Non-planar, RGB 24 bit bitmap, or RGBA 32 bit bitmap
    if(![bitmap isPlanar] && 
       (samplesPerPixel == 3 || samplesPerPixel == 4)) { 
		glTexImage2D(GL_TEXTURE_RECTANGLE_EXT, 
					 0, 
					 samplesPerPixel == 4 ? GL_RGBA8 : GL_RGB8,
					 [bitmap pixelsWide], 
					 [bitmap pixelsHigh], 
					 0, 
					 samplesPerPixel == 4 ? GL_RGBA : GL_RGB,
					 GL_UNSIGNED_BYTE, 
					 [bitmap bitmapData]);
    } else {
		/*
		 Error condition...
		 The above code handles 2 cases (24 bit RGB and 32 bit RGBA),
		 it is possible to support other bitmap formats if desired.
		 
		 So we'll log out some useful information.
		 */
        NSLog (@"-textureFromView: Unsupported bitmap data format: isPlanar:%d, samplesPerPixel:%d, bitsPerPixel:%d, bytesPerRow:%d, bytesPerPlane:%d",
			   [bitmap isPlanar], 
			   [bitmap samplesPerPixel], 
			   [bitmap bitsPerPixel], 
			   [bitmap bytesPerRow], 
			   [bitmap bytesPerPlane]);
    }
}

// file to view squash factor for files that are displayed at different aspect ratio from the one in the file
- (NSPoint)squashFactor {
	FlameDocument *doc = [[[self window] windowController] document];
	Flame *flame = [doc getCurrentFlame];
	
	NSRect bounds       = [self bounds];
	float docViewWidth  =  flame->size[0] * flame->scale * powf(2.f, flame->zoom);
	float docViewHeight =  flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	float viewScaleX    = bounds.size.width  / docViewWidth;
	float viewScaleY    = bounds.size.height / docViewHeight;
	return NSMakePoint(viewScaleX, viewScaleY);
}

- (void) scaleMatrixScaleFactor:(float)scaleFactor rect:(NSRect)rect rasterPos:(NSPoint)rasterPos size:(NSSize)size {
	PreviewController *ctrl = [[self window] windowController];
	NSPoint refPoint = NSMakePoint([ctrl referencePoint].x * rect.size.width/2.f  + rect.size.width/2.f, 
								   [ctrl referencePoint].y * rect.size.height/2.f + rect.size.height/2.f);
	NSPoint center   = NSMakePoint(rasterPos.x + size.width/2.f, rasterPos.y + size.height/2.f);
	NSPoint delta    = NSMakePoint(refPoint.x - center.x, refPoint.y - center.y);
	glTranslatef(center.x + delta.x, center.y + delta.y, 0.f); // translate as needed
	glScalef(scaleFactor, scaleFactor, scaleFactor);
	glTranslatef(-(center.x + delta.x), -(center.y + delta.y), 0.f); // translate as needed
}

- (void) rotateMatrixRotation:(float)degrees rect:(NSRect)rect rasterPos:(NSPoint)rasterPos size:(NSSize)size {
	PreviewController *ctrl = [[self window] windowController];
	NSPoint refPoint  = NSMakePoint([ctrl referencePoint].x * rect.size.width/2.f  + rect.size.width/2.f, 
									[ctrl referencePoint].y * rect.size.height/2.f + rect.size.height/2.f);
	NSPoint center    = NSMakePoint(rasterPos.x + size.width/2.f, rasterPos.y + size.height/2.f);
	NSPoint delta     = NSMakePoint(refPoint.x - center.x, refPoint.y - center.y);
	
	
	//NSPoint viewScale = [self squashFactor];
	//float radians     = degrees * (2.0*M_PI/360.0);
	//float xScale      = viewScale.x * cosf(radians) - viewScale.y * sinf(radians);
	//float yScale      = viewScale.x * sinf(radians) + viewScale.y * cosf(radians);
	
	glTranslatef(center.x + delta.x, center.y + delta.y, 0.f); // translate as needed
	glRotatef(degrees, 0.f, 0.f, 1.f);
	glTranslatef(-(center.x + delta.x), -(center.y + delta.y), 0.f); // translate as needed
	
	/* glTranslatef(center.x, center.y, 0.f);
	 glScalef(xScale, yScale, 1.f);
	 glRotatef(degrees, 0.f, 0.f, 1.f);
	 glScalef(1.f/xScale, 1.f/yScale, 1.f);
	 glTranslatef(-center.x, -center.y, 0.f); */
}

// draw the view's contents
- (void)drawRect:(NSRect)rect {
	PreviewController *ctrl = [[self window] windowController];

	// blackColor and whiteColor are not returned as RGB colorspace colors - must convert them first
	NSColor *bkgColor = [backgroundColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace]; 
	if (! imageRep) {
		glClearColor([bkgColor redComponent], [bkgColor greenComponent], [bkgColor blueComponent], [bkgColor alphaComponent]);
		glClear(GL_COLOR_BUFFER_BIT);
	}
	else {
		glClearColor([bkgColor redComponent], [bkgColor greenComponent], [bkgColor blueComponent], 0.5f);
		glClear(GL_COLOR_BUFFER_BIT);
		
		NSSize  size = [imageRep size];
		NSPoint rasterPos;
		if (size.width == rect.size.width && size.height == rect.size.height) {
			rasterPos = NSMakePoint(0.f, 0.f);
		}
		else if ([self inLiveResize] && previousBounds.size.width != -1) {
			NSSize maxSize = NSMakeSize(size.width  <= previousBounds.size.width  ? size.width  : previousBounds.size.width, 
										size.height <= previousBounds.size.height ? size.height : previousBounds.size.height);
			rasterPos = NSMakePoint((previousBounds.size.width - maxSize.width)/2, (previousBounds.size.height - maxSize.height)/2);
		}
		else {
			NSSize maxSize = NSMakeSize(size.width  <= rect.size.width  ? size.width  : rect.size.width, 
										size.height <= rect.size.height ? size.height : rect.size.height);
			rasterPos = NSMakePoint((rect.size.width - maxSize.width)/2, (rect.size.height - maxSize.height)/2);
		}
		
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glPushMatrix(); // we want to just apply these transformation to our quad
		
		// handle user interaction that adjusts the image transformation in real time
		if (dragging && [self doTranslateEdit]) {
			float dx = dragStartPoint.x - currentDragPoint.x;
			float dy = dragStartPoint.y - currentDragPoint.y;	// flip y coordinate
			glTranslatef(-dx, -dy, 0.f); // translate as needed
		}
		
		if (dragging && [self doRotateEdit]) {
			NSPoint refPoint  = NSMakePoint([ctrl referencePoint].x * rect.size.width/2.f  + rect.size.width/2.f, 
											[ctrl referencePoint].y * rect.size.height/2.f + rect.size.height/2.f);
			float thetaStart = atan2f(dragStartPoint.y   - refPoint.y, dragStartPoint.x   - refPoint.x);
			float thetaEnd   = atan2f(currentDragPoint.y - refPoint.y, currentDragPoint.x - refPoint.x);
			float degrees = (thetaEnd - thetaStart)*180.f/M_PI;
			
			[self rotateMatrixRotation:degrees rect:rect rasterPos:rasterPos size:size];
		}
		if (dragging && [self doScaleEdit]) {
			[self scaleMatrixScaleFactor:[self calcScaleFactor] rect:rect rasterPos:rasterPos size:size];
		}
		if (!dragging && ([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:CombinedEditMode]) && accumScroll != accumBaseline) {
			float scaleFactor = expf((accumScroll - accumBaseline) * -scrollWheelFactor);
			[self scaleMatrixScaleFactor:scaleFactor rect:rect rasterPos:rasterPos size:size];
		}
		if (([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:CombinedEditMode]) && accumMagnify != magnifyBaseline) {
			float scaleFactor = 1.f/expf((accumMagnify - magnifyBaseline) * -magnifyFactor);
			[self scaleMatrixScaleFactor:scaleFactor rect:rect rasterPos:rasterPos size:size];
		}
		if (([ctrl isEditMode:RotateEditMode] || [ctrl isEditMode:CombinedEditMode]) && accumRotate != rotateBaseline) {
			float degrees = accumRotate - rotateBaseline;
			[self rotateMatrixRotation:degrees rect:rect rasterPos:rasterPos size:size];
		}
		
		// this is the texture rendering code version
		if (textureDirty) {
			[self textureFromImageRep:imageRep textureName:&textureName];
			textureDirty = NO;
		}
		
		glEnable(GL_TEXTURE_RECTANGLE_EXT);
		
		glBegin(GL_QUADS);
		glTexCoord2f(rasterPos.x,              rasterPos.y); 
		glVertex3f(rasterPos.x,                rasterPos.y,               0.f);
		glTexCoord2f(rasterPos.x,              rasterPos.y + size.height); 
		glVertex3f(rasterPos.x,                rasterPos.y + size.height, 0.f);
		glTexCoord2f(rasterPos.x + size.width, rasterPos.y + size.height); 
		glVertex3f(rasterPos.x + size.width,   rasterPos.y + size.height, 0.f);
		glTexCoord2f(rasterPos.x + size.width, rasterPos.y); 
		glVertex3f(rasterPos.x + size.width,   rasterPos.y,               0.f);
		glEnd();
		glDisable(GL_TEXTURE_RECTANGLE_EXT);  // without this the texture affects the line drawing
		
		glPopMatrix();
		
		// draw text and graphics - untransformed on top of the image
		if ([ctrl isEditMode:TranslateEditMode] || 
			[ctrl isEditMode:ScaleEditMode]     || 
			[ctrl isEditMode:RotateEditMode]    || 
			[ctrl isEditMode:CombinedEditMode]) {
			[self drawCirclePoint:NSMakePoint([ctrl referencePoint].x * rect.size.width/2.f  + rect.size.width/2.f, 
											  [ctrl referencePoint].y * rect.size.height/2.f + rect.size.height/2.f)];
			if (dragging) {
				NSPoint refPoint = NSMakePoint([ctrl referencePoint].x * rect.size.width/2.f  + rect.size.width/2.f, 
											   [ctrl referencePoint].y * rect.size.height/2.f + rect.size.height/2.f);
				
				if ([self doScaleEdit]) {
					float scaleFactor = [self calcScaleFactor];
					float *color = scaleFactor > 1.0f ? red : yellow;
					[self drawDragFrom:refPoint to:currentDragPoint color:color];

					// draw the current scale factor
					NSString *label = [NSString stringWithFormat:@"%-6.3fX", scaleFactor];
					[self drawText:label color:color rasterPos:NSMakePoint([self bounds].size.width - 70.f, 20.f)];
				}
				else if ([self doRotateEdit]) {
					[self drawDragFrom:refPoint toEdgePast:dragStartPoint color:blue];
					[self drawDragFrom:refPoint toEdgePast:currentDragPoint color:red];

					float thetaStart = atan2f(dragStartPoint.y   - refPoint.y, dragStartPoint.x   - refPoint.x);
					float thetaEnd   = atan2f(currentDragPoint.y - refPoint.y, currentDragPoint.x - refPoint.x);
					float degrees = (thetaEnd - thetaStart)*180.f/M_PI;
					
					NSString *label = [NSString stringWithFormat:@"%-6.1f Degrees", degrees];
					[self drawText:label color:red rasterPos:NSMakePoint(rect.size.width - 90.f, 20.f)];
				}
				else if ([self doTranslateEdit]) {
					float dx = dragStartPoint.x - currentDragPoint.x;
					float dy = dragStartPoint.y - currentDragPoint.y;	// flip y coordinate
					NSString *label = [NSString stringWithFormat:@"(%.0f, %.0f)", dx, dy];
					[self drawText:label color:white rasterPos:NSMakePoint(rect.size.width - 80.f, 20.f)];
				}
			}
			else if (([ctrl isEditMode:RotateEditMode] || [ctrl isEditMode:CombinedEditMode]) && accumRotate != rotateBaseline) {
				float degrees = accumRotate - rotateBaseline;
				NSString *label = [NSString stringWithFormat:@"%-6.1f Degrees", degrees];
				[self drawText:label color:red rasterPos:NSMakePoint(rect.size.width - 90.f, 20.f)];
			}
			else if (([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:CombinedEditMode]) && accumMagnify != magnifyBaseline) {
				float scaleFactor = 1.f/expf((accumMagnify - magnifyBaseline) * -magnifyFactor);
				NSString *label = [NSString stringWithFormat:@"%-6.3fX", scaleFactor];
				float *color = scaleFactor > 1.0f ? red : yellow;
				[self drawText:label color:color rasterPos:NSMakePoint([self bounds].size.width - 70.f, 20.f)];
			}
		}
		if ([ctrl isEditMode:CropEditMode]) {
			if (normCropRect.size.width > 0.f) {
				[self drawCropMaskWithColor:blackAlpha rect:rect];
				NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
				NSString *label = [NSString stringWithFormat:@"(%4.0f, %4.0f)-(%4.0fx%4.0f)", 
								   cropRect.origin.x, cropRect.origin.y,
								   cropRect.size.width, cropRect.size.height];
				[self drawText:label color:white rasterPos:NSMakePoint(rect.size.width - 150.f, 20.f)];
			}			
		}
		if ([ctrl showLocation]) {
			NSPoint mouseLoc = [self convertPoint:[[self window] mouseLocationOutsideOfEventStream] fromView:nil];
			if (NSPointInRect(mouseLoc, rect)) {
				NSString *label = [NSString stringWithFormat:@"{%.0f, %.0f}", mouseLoc.x, mouseLoc.y];
				[self drawText:label color:red rasterPos:NSMakePoint(rect.size.width - 80.f, 20.f)];
			}
		}		
	}
	[[self openGLContext] flushBuffer];

}

// window resizes and visibility change from overlapping windows
- (void) reshape 
{
	NSRect rect = [self bounds];	
	GLsizei w = rect.size.width;
	GLsizei h = rect.size.height;
	
	GLsizei oldW = previousBounds.size.width;
	GLsizei oldH = previousBounds.size.height;
	
	// set viewport to NSView bounds
	glViewport( 0, 0, w, h);
	
	// reset coordinate system
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// establish clipping volume 2d
	glOrtho(0.0, w, 0.0, h, 1.0, -1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	PreviewController *ctrl = [[self window] windowController];
	if ([self inLiveResize]) {
		[ctrl logRenderStats:w height:h];
		
		if (oldW != -1 && [ctrl aspectRatioLocked]) { // continuously adjust the aspect ratio for our lower border height
			float statusBarHeight = NSHeight([[[self window] contentView] bounds]) - NSHeight([self bounds]);
			[[self window] setContentAspectRatio:NSMakeSize([ctrl aspectRatio] * h, h + statusBarHeight)];
		}
		if (![ctrl statusBarItemsAreHidden]) {
			[ctrl hideStatusBarItems:YES];
		}
	}
	
	// only rerender if size has actually changed (often it does not)
	if (! [self inLiveResize] && (w != oldW || h != oldH)) {
		if ([ctrl statusBarItemsAreHidden])
			[ctrl hideStatusBarItems:NO];
		
		FlameDocument *doc = [[[self window] windowController] document];
		if ([[ctrl drawer] state] == NSDrawerClosedState && [[ctrl drawerRight] state] == NSDrawerClosedState) {
			[ctrl setOrigContentViewSize:[[[self window] contentView] bounds].size];
		}
		
		if ([ctrl isEditMode:CropResizeMode]) {
			NSUndoManager *undoManager = [self undoManager];
			if (! [undoManager isUndoing])
				[doc cropResizeWithRect:rect bounds:previousBounds.size];
		}
		previousBounds = rect;
		
		[ctrl renderAsPreview:YES];
	}
}

// handle dragging of reference point
- (BOOL)dragReferencePoint:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	NSPoint clickPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	NSRect bounds      = [self bounds];
	NSRect trackRect   = NSMakeRect([ctrl referencePoint].x * bounds.size.width/2.f + bounds.size.width/2.f - 3.f, 
								  [ctrl referencePoint].y * bounds.size.height/2.f + bounds.size.height/2.f - 3.f, 
								  6.f, 6.f);
	
	bool refPointDragging = NO;
	if (! NSPointInRect(clickPoint, trackRect))
		return NO;
	
	refPointDragging = YES;
	[[NSCursor crosshairCursor] push];
	while (refPointDragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		clickPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		BOOL inView = NSPointInRect(clickPoint, bounds);
		if (inView) {
			float halfW = bounds.size.width/2.f;
			float halfH = bounds.size.height/2.f;
			[ctrl setReferencePoint:NSMakePoint((clickPoint.x  - halfW)/halfW, 
												(clickPoint.y - halfH)/halfH)];
		}
		
		switch ([theEvent type]) {
			case NSLeftMouseUp:
				[NSCursor pop];
				refPointDragging = NO;
				[[self window] invalidateCursorRectsForView:self];
				[self setNeedsDisplay:YES];
				break;
			case NSLeftMouseDragged:
				if (inView)
					[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
	return YES;
}

// handle dragging of point to represent translation
- (void)dragXlatePoint:(NSEvent *)theEvent {
	dragStartPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	currentDragPoint = dragStartPoint;
	dragging = YES;
	
	[[NSCursor crosshairCursor] push];
	while (dragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		currentDragPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		switch ([theEvent type]) {
			case NSLeftMouseUp:
			{
				[NSCursor pop];
				dragging = NO;
				if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
					FlameDocument *doc = [[[self window] windowController] document];
					
					float dx = dragStartPoint.x - currentDragPoint.x;
					float dy = -(dragStartPoint.y - currentDragPoint.y);	// flip y coordinate
					[doc translateByViewX:dx viewY:dy];
				}
			}
				break;
			case NSLeftMouseDragged:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
}	

// handle dragging of point to represent scaling
- (void)dragScalePoint:(NSEvent *)theEvent {
	dragStartPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	currentDragPoint = dragStartPoint;
	dragging = YES;
	
	[[NSCursor crosshairCursor] push];
	while (dragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		currentDragPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		switch ([theEvent type]) {
			case NSLeftMouseUp:
				{
					[NSCursor pop];
					dragging = NO;
					if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
						FlameDocument *doc = [[[self window] windowController] document];

						PreviewController *ctrl = [[self window] windowController];
						[doc zoomByWheel:[self calcScaleFactor] referencePoint:[ctrl referencePoint]];
						[ctrl renderAsPreview:YES];
					}
				}
				break;
			case NSLeftMouseDragged:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
}	

// handle dragging of point to represent rotation
- (void)dragRotatePoint:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	NSRect bounds      = [self bounds];
	dragStartPoint     = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	currentDragPoint   = dragStartPoint;
	dragging = YES;
	
	[[NSCursor crosshairCursor] push];
	while (dragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		currentDragPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		switch ([theEvent type]) {
			case NSLeftMouseUp:
			{
				[NSCursor pop];
				dragging = NO;
				if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
					FlameDocument *doc = [[[self window] windowController] document];
										
					NSPoint refPoint = NSMakePoint([ctrl referencePoint].x * bounds.size.width/2.f  + bounds.size.width/2.f, 
												   [ctrl referencePoint].y * bounds.size.height/2.f + bounds.size.height/2.f);

					float thetaStart = atan2f(dragStartPoint.y   - refPoint.y, dragStartPoint.x   - refPoint.x);
					float thetaEnd   = atan2f(currentDragPoint.y - refPoint.y, currentDragPoint.x - refPoint.x);
					
					[doc rotateRadians:thetaEnd - thetaStart referencePoint:[ctrl referencePoint]];
				}
			}
				break;
			case NSLeftMouseDragged:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
}	

// handle dragging of rectangle to represent cropping rectangle
- (BOOL)dragCropRectangleAround:(NSEvent *)theEvent {
	// only move a crop rectangle if one does exists
	if (normCropRect.size.width < 0.f)
		return NO;
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);

	dragStartPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	if (! NSPointInRect(dragStartPoint, cropRect))
		return NO;
	
	NSRect origCropRect = cropRect;
	currentDragPoint = dragStartPoint;
	dragging = YES;
	
	[[NSCursor closedHandCursor] push];
	while (dragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		currentDragPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
			float dx = currentDragPoint.x - dragStartPoint.x;
			float dy = currentDragPoint.y - dragStartPoint.y;
			if (dx < 0)
				cropRect.origin.x = origCropRect.origin.x + dx > 0.f ? origCropRect.origin.x + dx : 0.f;
			else {
				float newOriginX  = origCropRect.origin.x + dx;
				float width       =  [self bounds].size.width;
				cropRect.origin.x = newOriginX  + origCropRect.size.width < width ? newOriginX : width - origCropRect.size.width;
			}
			if (dy < 0)
				cropRect.origin.y = origCropRect.origin.y + dy > 0.f ? origCropRect.origin.y + dy : 0.f;
			else {
				float newOriginY  = origCropRect.origin.y + dy;
				float height      =  [self bounds].size.height;
				cropRect.origin.y = newOriginY  + origCropRect.size.height < height ? newOriginY : height - origCropRect.size.height;
			}
			normCropRect = normalizeRect(cropRect, [self bounds]);
			selectedSpot = none;
			cropAspectRatio = cropRect.size.width / cropRect.size.height;
		}
		switch ([theEvent type]) {
			case NSLeftMouseUp:
			{
				[NSCursor pop];
				dragging = NO;
				if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
					[self setNeedsDisplay:YES];
					[[self window] invalidateCursorRectsForView:self];
				}
				else
					return NO;
			}
				break;
			case NSLeftMouseDragged:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
	return YES;
}

// handle dragging of rectangle to represent cropping rectangle
- (BOOL)dragCropRectangle:(NSEvent *)theEvent {
	// only create a crop rectangle if one does not exist
	if (normCropRect.size.width > 0.f)
		return [self dragCropRectangleAround:theEvent];
	
	dragStartPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	currentDragPoint = dragStartPoint;
	dragging = YES;
	
	[[NSCursor crosshairCursor] push];
	while (dragging) {
		theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
		currentDragPoint = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		
		BOOL inView = NSPointInRect(currentDragPoint, [self bounds]);
		if (! inView) { // clamp click point to edge
			NSRect b = [self bounds];
			if (currentDragPoint.x < 0.f)
				currentDragPoint.x = 0.f;
			if (currentDragPoint.x > b.origin.x + b.size.width)
				currentDragPoint.x = b.origin.x + b.size.width;
			if (currentDragPoint.y < 0.f)
				currentDragPoint.y = 0.f;
			if (currentDragPoint.y > b.origin.y + b.size.height)
				currentDragPoint.y = b.origin.y + b.size.height;
		}
		
		if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
			NSPoint ll = NSMakePoint(dragStartPoint.x <= currentDragPoint.x ? dragStartPoint.x : currentDragPoint.x, 
									 dragStartPoint.y <= currentDragPoint.y ? dragStartPoint.y : currentDragPoint.y);
			NSPoint ur = NSMakePoint(dragStartPoint.x >  currentDragPoint.x ? dragStartPoint.x : currentDragPoint.x, 
									 dragStartPoint.y >  currentDragPoint.y ? dragStartPoint.y : currentDragPoint.y);
			NSRect newCropRect = NSMakeRect(ll.x, ll.y, ur.x - ll.x, ur.y - ll.y);
			normCropRect = normalizeRect(newCropRect, [self bounds]);
			selectedSpot = none;
			cropAspectRatio = newCropRect.size.width / newCropRect.size.height;
			
			PreviewController *ctrl = [[self window] windowController];
			[ctrl changeAspectRatioButton:cropAspectRatio];
		}
		switch ([theEvent type]) {
			case NSLeftMouseUp:
			{
				[NSCursor pop];
				dragging = NO;
				if (! NSEqualPoints(dragStartPoint, currentDragPoint)) {
					[self setNeedsDisplay:YES];
					[[self window] invalidateCursorRectsForView:self];
				}
			}
				break;
			case NSLeftMouseDragged:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
	return YES;
}

// see if point is inside of rect and also figure the offset from the center
BOOL pointInRect(NSPoint point, NSRect rect, NSPoint *offset) {
	BOOL inside = NSPointInRect(point, rect);
	if (inside) {
		offset->x = point.x - rect.origin.x - rect.size.width/2.f;
		offset->y = point.y - rect.origin.y - rect.size.height/2.f;
	}
	return inside;
}

- (eHotSpot)pointAtHotSpot:(NSPoint)point offset:(NSPoint *)offset{
	NSRect c = deNormalizeRect(normCropRect, [self bounds]);
	
	NSPoint ll = NSMakePoint(c.origin.x,                c.origin.y);
	NSPoint lr = NSMakePoint(c.origin.x + c.size.width, c.origin.y);
	NSPoint ur = NSMakePoint(c.origin.x + c.size.width, c.origin.y +c.size.height);
	NSPoint ul = NSMakePoint(c.origin.x,                c.origin.y +c.size.height);
	
	NSPoint s = NSMakePoint(c.origin.x + c.size.width/2.f, c.origin.y);
	NSPoint w = NSMakePoint(c.origin.x,                    c.origin.y + c.size.height/2.f);
	NSPoint e = NSMakePoint(c.origin.x + c.size.width,     c.origin.y + c.size.height/2.f);
	NSPoint n = NSMakePoint(c.origin.x + c.size.width/2.f, c.origin.y + c.size.height);
		
	NSRect trackRectLL = NSMakeRect(ll.x-12.f, ll.y-12.f, 24.f, 24.f); 
	NSRect trackRectLR = NSMakeRect(lr.x-12.f, lr.y-12.f, 24.f, 24.f); 
	NSRect trackRectUR = NSMakeRect(ur.x-12.f, ur.y-12.f, 24.f, 24.f); 
	NSRect trackRectUL = NSMakeRect(ul.x-12.f, ul.y-12.f, 24.f, 24.f); 
	
	NSRect trackRectS = NSMakeRect(s.x-12.f, s.y-12.f, 24.f, 24.f); 
	NSRect trackRectW = NSMakeRect(w.x-12.f, w.y-12.f, 24.f, 24.f); 
	NSRect trackRectE = NSMakeRect(e.x-12.f, e.y-12.f, 24.f, 24.f); 
	NSRect trackRectN = NSMakeRect(n.x-12.f, n.y-12.f, 24.f, 24.f);
	
	if (pointInRect(point, trackRectLL, offset))
		return atLL;
	else if (pointInRect(point, trackRectLR, offset))
		return atLR;
	else if (pointInRect(point, trackRectUR, offset))
		return atUR;
	else if (pointInRect(point, trackRectUL, offset))
		return atUL;
	else if (pointInRect(point, trackRectS, offset))
		return atS;
	else if (pointInRect(point, trackRectW, offset))
		return atW;
	else if (pointInRect(point, trackRectE, offset))
		return atE;
	else if (pointInRect(point, trackRectN, offset))
		return atN;
	return none;
}

// take the current crop rectangle and transform it to abide by the aspect ratio
- (void)enforcePreserveCenterAspectRatio:(float)aspectRatio cropRect:(NSRect)cropRect {	
	float maxDim = cropRect.size.width >= cropRect.size.height ? cropRect.size.width : cropRect.size.height;
	float minDim = cropRect.size.width <  cropRect.size.height ? cropRect.size.width : cropRect.size.height;

	NSSize viewSize = [self bounds].size;
	NSPoint center	= NSMakePoint(cropRect.origin.x + cropRect.size.width/2.f, cropRect.origin.y + cropRect.size.height/2.f);
	
	NSRect newCropRect;
	// create a new crop rectangle that uses the same center as the old one, constrained to fit in the view
	if (aspectRatio >= 1.0f) {		
		if (maxDim > viewSize.width)
			maxDim = viewSize.width;
		if (center.x + maxDim/2.f > viewSize.width)
			maxDim = 2.f *(viewSize.width - center.x);
		if (center.x - maxDim/2.f < 0)
			maxDim = 2.f * center.x;		
		minDim = maxDim/aspectRatio;
		
		if (minDim > viewSize.height)
			minDim = viewSize.height;
		if (center.y + minDim/2.f > viewSize.height)
			minDim = 2.f *(viewSize.height - center.y);
		if (center.y - minDim/2.f < 0)
			minDim = 2.f * center.y;
		maxDim = minDim*aspectRatio;
		
		newCropRect = NSMakeRect(center.x - maxDim/2.f, center.y - minDim/2.f, maxDim, minDim);
	}
	else {
		if (maxDim > viewSize.height)
			maxDim = viewSize.height;
		if (center.y + maxDim/2.f > viewSize.height)
			maxDim = 2.f *(viewSize.height - center.y);
		if (center.y - maxDim/2.f < 0)
			maxDim = 2.f * center.y;
		minDim = maxDim*aspectRatio;
		
		if (minDim > viewSize.width)
			minDim = viewSize.width;
		if (center.x + minDim/2.f > viewSize.width)
			minDim = 2.f *(viewSize.width - center.x);
		if (center.x - minDim/2.f < 0)
			minDim = 2.f * center.x;		
		maxDim = minDim/aspectRatio;
		
		newCropRect = NSMakeRect(center.x - minDim/2.f, center.y - maxDim/2.f, minDim, maxDim);
	}
	
	normCropRect    = normalizeRect(newCropRect, [self bounds]);
	cropAspectRatio = newCropRect.size.width / newCropRect.size.height;
	[[self window] invalidateCursorRectsForView:self];
	[self setNeedsDisplay:YES];
}


// take the current crop rectangle and transform it to abide by the aspect ratio
- (void)enforceNoFlipPreserveCenterAspectRatio:(float)aspectRatio cropRect:(NSRect)cropRect longest:(BOOL)longest {
	float maxDim, minDim;
	if (aspectRatio >= 1.f) {
		maxDim = cropRect.size.width;
		minDim = cropRect.size.height;
	}
	else {
		maxDim = cropRect.size.height;
		minDim = cropRect.size.width;
	}	
	
	NSSize viewSize = [self bounds].size;
	NSPoint center	= NSMakePoint(cropRect.origin.x + cropRect.size.width/2.f, cropRect.origin.y + cropRect.size.height/2.f);
	
	NSRect newCropRect;
	// create a new crop rectangle that uses the same center as the old one, constrained to fit in the view
	if (aspectRatio >= 1.0f) {		
		if (longest) {
			if (minDim > viewSize.height)
				minDim = viewSize.height;
			if (center.y + minDim/2.f > viewSize.height)
				minDim = 2.f *(viewSize.height - center.y);
			if (center.y - minDim/2.f < 0)
				minDim = 2.f * center.y;
			maxDim = minDim*aspectRatio;
			
			if (maxDim > viewSize.width)
				maxDim = viewSize.width;
			if (center.x + maxDim/2.f > viewSize.width)
				maxDim = 2.f *(viewSize.width - center.x);
			if (center.x - maxDim/2.f < 0)
				maxDim = 2.f * center.x;		
			minDim = maxDim/aspectRatio;
		}
		else {
			if (maxDim > viewSize.width)
				maxDim = viewSize.width;
			if (center.x + maxDim/2.f > viewSize.width)
				maxDim = 2.f *(viewSize.width - center.x);
			if (center.x - maxDim/2.f < 0)
				maxDim = 2.f * center.x;		
			minDim = maxDim/aspectRatio;
			
			if (minDim > viewSize.height)
				minDim = viewSize.height;
			if (center.y + minDim/2.f > viewSize.height)
				minDim = 2.f *(viewSize.height - center.y);
			if (center.y - minDim/2.f < 0)
				minDim = 2.f * center.y;
			maxDim = minDim*aspectRatio;
			
		}
		newCropRect = NSMakeRect(center.x - maxDim/2.f, center.y - minDim/2.f, maxDim, minDim);
	}
	else {
		if (longest) {
			if (minDim > viewSize.width)
				minDim = viewSize.width;
			if (center.x + minDim/2.f > viewSize.width)
				minDim = 2.f *(viewSize.width - center.x);
			if (center.x - minDim/2.f < 0)
				minDim = 2.f * center.x;		
			maxDim = minDim/aspectRatio;
			
			if (maxDim > viewSize.height)
				maxDim = viewSize.height;
			if (center.y + maxDim/2.f > viewSize.height)
				maxDim = 2.f *(viewSize.height - center.y);
			if (center.y - maxDim/2.f < 0)
				maxDim = 2.f * center.y;
			minDim = maxDim*aspectRatio;
		}
		else {
			if (maxDim > viewSize.height)
				maxDim = viewSize.height;
			if (center.y + maxDim/2.f > viewSize.height)
				maxDim = 2.f *(viewSize.height - center.y);
			if (center.y - maxDim/2.f < 0)
				maxDim = 2.f * center.y;
			minDim = maxDim*aspectRatio;
			
			if (minDim > viewSize.width)
				minDim = viewSize.width;
			if (center.x + minDim/2.f > viewSize.width)
				minDim = 2.f *(viewSize.width - center.x);
			if (center.x - minDim/2.f < 0)
				minDim = 2.f * center.x;		
			maxDim = minDim/aspectRatio;
			
		}
		newCropRect = NSMakeRect(center.x - minDim/2.f, center.y - maxDim/2.f, minDim, maxDim);
	}
	
	normCropRect    = normalizeRect(newCropRect, [self bounds]);
	cropAspectRatio = newCropRect.size.width / newCropRect.size.height;
	[[self window] invalidateCursorRectsForView:self];
	[self setNeedsDisplay:YES];
}



// handle dragging of reference point
- (BOOL)dragCropResizers:(NSEvent *)theEvent {
	PreviewController *ctrl = [[self window] windowController];
	NSPoint clickPoint      = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	bool resizerDragging    = NO;	
	eHotSpot lastAt         = selectedSpot;
	
	NSPoint offset;
	eHotSpot at = [self pointAtHotSpot:clickPoint offset:&offset];
	
	selectedSpot = at;
	if (selectedSpot != lastAt)
		[self setNeedsDisplay:YES];
	
	if (at == none)
		return NO;
	
	resizerDragging = YES;
	switch (at) {
		case atLL:
			[shiftSWCursor push];
			break;
		case atLR:
			[shiftNWCursor push];
			break;
		case atUR:
			[shiftSWCursor push];
			break;
		case atUL:
			[shiftNWCursor push];
			break;
		case atS:
			[shiftVertCursor push];
			break;
		case atW:
			[shiftHorizCursor push];
			break;
		case atE:
			[shiftHorizCursor push];
			break;
		case atN:
			[shiftVertCursor push];
			break;
		default:
			break;
	}
	
	while (resizerDragging) {
		theEvent          = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask |NSMouseExitedMask];
		NSPoint lastPoint = clickPoint;
		clickPoint        = [self convertPoint:[theEvent locationInWindow] fromView:nil];
		NSPoint delta     = NSMakePoint(clickPoint.x - lastPoint.x, clickPoint.y - lastPoint.y);
		NSPoint hotSpot   = NSMakePoint(clickPoint.x - offset.x, clickPoint.y - offset.y);

		BOOL inView = NSPointInRect(hotSpot, [self bounds]);
		if (! inView) { // clamp click point to edge
			NSRect b = [self bounds];
			if (hotSpot.x < 0.f)
				hotSpot.x = 0.f;
			if (hotSpot.x > b.origin.x + b.size.width)
				hotSpot.x = b.origin.x + b.size.width;
			if (hotSpot.y < 0.f)
				hotSpot.y = 0.f;
			if (hotSpot.y > b.origin.y + b.size.height)
				hotSpot.y = b.origin.y + b.size.height;
		}
		NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
		NSPoint ll = cropRect.origin;
		NSPoint ur = NSMakePoint(cropRect.origin.x + cropRect.size.width, cropRect.origin.y + cropRect.size.height);
		bool longest    = NO;
		float ar        = cropRect.size.width / cropRect.size.height;
		switch (at) {
			case atLL:
				ll = hotSpot;
				longest = (ar >= 1.f && delta.y != 0) || (ar < 1.f && delta.x != 0) ? YES : NO;
				break;
			case atLR:
				ll = NSMakePoint(cropRect.origin.x, hotSpot.y);
				ur = NSMakePoint(hotSpot.x, cropRect.origin.y + cropRect.size.height);
				longest = (ar >= 1.f && delta.y != 0) || (ar < 1.f && delta.x != 0) ? YES : NO;
				break;
			case atUR:
				ur = NSMakePoint(hotSpot.x, hotSpot.y);
				longest = (ar >= 1.f && delta.y != 0) || (ar < 1.f && delta.x != 0) ? YES : NO;
				break;
			case atUL:
				ll = NSMakePoint(hotSpot.x, cropRect.origin.y);
				ur = NSMakePoint(cropRect.origin.x + cropRect.size.width, hotSpot.y);
				longest = (ar >= 1.f && delta.y != 0) || (ar < 1.f && delta.x != 0) ? YES : NO;
				break;
			case atS:
				ll = NSMakePoint(cropRect.origin.x, hotSpot.y);
				longest = ar >= 1.f ? YES : NO;
				break;
			case atW:
				ll = NSMakePoint(hotSpot.x, cropRect.origin.y);
				longest = ar < 1.f ? YES : NO;
				break;
			case atE:
				ur = NSMakePoint(hotSpot.x, cropRect.origin.y + cropRect.size.height);
				longest = ar < 1.f ? YES : NO;
				break;
			case atN:
				ur = NSMakePoint(cropRect.origin.x + cropRect.size.width, hotSpot.y);
				longest = ar >= 1.f ? YES : NO;
				break;
		}
		cropRect = NSMakeRect(ll.x, ll.y, ur.x - ll.x, ur.y - ll.y);
		if (cropRect.size.width <= 0.f)
			cropRect.size.width = 1.f;
		if (cropRect.size.height <= 0.f)
			cropRect.size.height = 1.f;

		if ([[ctrl lockCropAR] intValue] == NSOnState) {  // locked aspect ratio
			[self enforceNoFlipPreserveCenterAspectRatio:cropAspectRatio cropRect:cropRect longest:longest];
		}
		else {
			normCropRect = normalizeRect(cropRect, [self bounds]);
			cropAspectRatio = cropRect.size.width / cropRect.size.height;
		}
		
		PreviewController *ctrl = [[self window] windowController];
		[ctrl changeAspectRatioButton:cropAspectRatio];
		
		switch ([theEvent type]) {
			case NSLeftMouseUp:
				[NSCursor pop];
				resizerDragging = NO;
				[[self window] invalidateCursorRectsForView:self];
				[self setNeedsDisplay:YES];
				break;
			case NSLeftMouseDragged:
				if (inView) {
					[[self window] invalidateCursorRectsForView:self];
					[self setNeedsDisplay:YES];
				}
				break;
			case NSMouseExited:
				[self setNeedsDisplay:YES];
				break;
			default:
				break;
		}
	}
	return YES;
}

- (void) changeCropAspectRatio:(float)aspectRatio {
	if (cropAspectRatio == aspectRatio) 
		return;
	[self enforcePreserveCenterAspectRatio: aspectRatio cropRect:deNormalizeRect(normCropRect, [self bounds])];
}

// handle dynamic scrolling from mouse dragging
- (void)mouseDown:(NSEvent *)theEvent
{
	PreviewController *ctrl = [[self window] windowController];
	if ([self translateEditStart:theEvent]) {
		if (! [self dragReferencePoint:theEvent]) {
			[self dragXlatePoint:theEvent];
		}
	}
	else if ([self scaleEditStart:theEvent]) {
		// handle dragging of the reference point
		if (! [self dragReferencePoint:theEvent]) {
			[self dragScalePoint:theEvent];
		}
	}
	else if ([self rotateEditStart:theEvent]) {
		// handle dragging of the reference point
		if (! [self dragReferencePoint:theEvent]) {
			[self dragRotatePoint:theEvent];
		}
	}
	else if ([ctrl isEditMode:CropEditMode]) {
		if ([theEvent clickCount] > 1 && normCropRect.size.width > 0.f) { // double click so rop
			FlameDocument *doc = [[[self window] windowController] document];
			[doc cropWithRect:[self cropRect] bounds:[self bounds].size];
		}
		// handle dragging of one of the crop area resizer points
		else if (! [self dragCropResizers:theEvent]) {
			if (! [self dragCropRectangle:theEvent]) {
				if (selectedSpot != none) {
					selectedSpot = none; // reset selected hotspot
					[self setNeedsDisplay:YES];
				}
			}
		}
	}
}

// handle the mouse has moved event
- (void)mouseMoved:(NSEvent *)theEvent	{
	PreviewController *ctrl = [[self window] windowController];
	if (![ctrl showLocation])
		return;
	[self setNeedsDisplay:YES];
}

// handle Carriage return press during crop mode
- (BOOL)performKeyEquivalent:(NSEvent *)theEvent {
	unichar key = [[theEvent charactersIgnoringModifiers] characterAtIndex:0];
	if (key == NSCarriageReturnCharacter) {
		FlameDocument *doc = [[[self window] windowController] document];
		[doc cropWithRect:[self cropRect] bounds:[self bounds].size];
		return YES;
	}
	switch ([[theEvent characters] characterAtIndex:0]) {
		case '1':
			selectedSpot = atLL;
			[self setNeedsDisplay:YES];
			return YES;
		case '2':
			selectedSpot = atS;
			[self setNeedsDisplay:YES];
			return YES;
		case '3':
			selectedSpot = atLR;
			[self setNeedsDisplay:YES];
			return YES;
		case '4':
			selectedSpot = atW;
			[self setNeedsDisplay:YES];
			return YES;
		case '0':
		case '5':
			selectedSpot = none;
			[self setNeedsDisplay:YES];
			return YES;
		case '6':
			selectedSpot = atE;
			[self setNeedsDisplay:YES];
			return YES;
		case '7':
			selectedSpot = atUL;
			[self setNeedsDisplay:YES];
			return YES;
		case '8':
			selectedSpot = atN;
			[self setNeedsDisplay:YES];
			return YES;
		case '9':
			selectedSpot = atUR;
			[self setNeedsDisplay:YES];
			return YES;
	}
	return NO;
}

// handle Escape key press during crop mode
- (void)cancelOperation:(id)sender {
	normCropRect  = NSMakeRect(-1.f, -1.f, -1.f, -1.f);
	selectedSpot = none;
	[[self window] invalidateCursorRectsForView:self];
	[self setNeedsDisplay:YES];
}


- (float)adjustDeltaMinX:(float)delta {
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if (delta < 0) {
		return cropRect.origin.x + delta > 0.f ? delta : -cropRect.origin.x;
	}
	else {
		return cropRect.size.width - delta > 0.f ? delta : cropRect.size.width - 1.f;
	}
}

- (float)adjustDeltaMaxX:(float)delta {
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if (delta < 0) {
		return cropRect.size.width + delta > 0.f ? delta : 1.f - cropRect.size.width;
	}
	else {
		float width = [self bounds].size.width;
		return cropRect.origin.x + delta  + cropRect.size.width < width ? delta : width - cropRect.origin.x - cropRect.size.width;
	}
}

- (float)adjustDeltaMinY:(float)delta {
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if (delta < 0) {
		return cropRect.origin.y + delta > 0.f ? delta : -cropRect.origin.y;
	}
	else {
		return cropRect.size.height - delta > 0.f ? delta : cropRect.size.height - 1.f;
	}
}

- (float)adjustDeltaMaxY:(float)delta {
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if (delta < 0) {
		return cropRect.size.height + delta > 0.f ? delta : 1.f - cropRect.size.height;
	}
	else {
		float height = [self bounds].size.height;
		return cropRect.origin.y + delta  + cropRect.size.height < height ? delta : height - cropRect.origin.y - cropRect.size.height;
	}
}

// handler for arrow and shift arrow key events - note only x OR y will be moved at a time
- (void)moveCropByX:(float)x Y:(float)y {
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if (cropRect.size.width <= 0) // no crop rectangle in effect
		return;
	
	BOOL shiftKeyDown = ([[NSApp currentEvent] modifierFlags] &
						 (NSShiftKeyMask | NSAlphaShiftKeyMask)) !=0;
	float delta  = shiftKeyDown ? 1.0f : 10.f;
	bool longest = NO;
	
	// if no hotspot is selected move the crop rectangle
	if (selectedSpot == none) {
		if (x < 0)
			cropRect.origin.x = cropRect.origin.x - delta > 0.f ? cropRect.origin.x - delta : 0.f;
		else if (y < 0)
			cropRect.origin.y = cropRect.origin.y - delta > 0.f ? cropRect.origin.y - delta : 0.f;
		else if (x > 0) {
			float newOriginX  = cropRect.origin.x + delta;
			float width       =  [self bounds].size.width;
			cropRect.origin.x = newOriginX  + cropRect.size.width < width ? newOriginX : width - cropRect.size.width;
		}
		else {
			float newOriginY  = cropRect.origin.y + delta;
			float height      =  [self bounds].size.height;
			cropRect.origin.y = newOriginY  + cropRect.size.height < height ? newOriginY : height - cropRect.size.height;
		}
	}
	else { // move the selected hotspot - adjust for view bounds
		float ar     = cropRect.size.width / cropRect.size.height;
		switch (selectedSpot) {
			case atLL:
				cropRect.origin.x    += [self adjustDeltaMinX:x * delta];
				cropRect.size.width  -= [self adjustDeltaMinX:x * delta];
				cropRect.origin.y    += [self adjustDeltaMinY:y * delta];
				cropRect.size.height -= [self adjustDeltaMinY:y * delta];
				longest = (ar >= 1.f && y != 0) || (ar < 1.f && x != 0) ? YES : NO;
				break;
			case atLR:
				cropRect.size.width  += [self adjustDeltaMaxX:x * delta];
				cropRect.origin.y    += [self adjustDeltaMinY:y * delta];
				cropRect.size.height -= [self adjustDeltaMinY:y * delta];
				longest = (ar >= 1.f && y != 0) || (ar < 1.f && x != 0) ? YES : NO;
				break;
			case atUR:
				cropRect.size.width  += [self adjustDeltaMaxX:x * delta];
				cropRect.size.height += [self adjustDeltaMaxY:y * delta];
				longest = (ar >= 1.f && y != 0) || (ar < 1.f && x != 0) ? YES : NO;
				break;
			case atUL:
				cropRect.origin.x    += [self adjustDeltaMinX:x * delta];
				cropRect.size.width  -= [self adjustDeltaMinX:x * delta];
				cropRect.size.height += [self adjustDeltaMaxY:y * delta];
				longest = (ar >= 1.f && y != 0) || (ar < 1.f && x != 0) ? YES : NO;
				break;
			case atS:
				cropRect.origin.y    += [self adjustDeltaMinY:y * delta];
				cropRect.size.height -= [self adjustDeltaMinY:y * delta];
				longest = ar >= 1.f ? YES : NO;
				break;
			case atW:
				cropRect.origin.x    += [self adjustDeltaMinX:x * delta];
				cropRect.size.width  -= [self adjustDeltaMinX:x * delta];
				longest = ar < 1.f ? YES : NO;
				break;
			case atE:
				cropRect.size.width  += [self adjustDeltaMaxX:x * delta];
				longest = ar < 1.f ? YES : NO;
				break;
			case atN:
				cropRect.size.height += [self adjustDeltaMaxY:y * delta];
				longest = ar >= 1.f ? YES : NO;
				break;
		}
	}
	
	PreviewController *ctrl = [[self window] windowController];
	if ([[ctrl lockCropAR] intValue] == NSOnState) {  // locked aspect ratio
		[self enforceNoFlipPreserveCenterAspectRatio:cropAspectRatio cropRect:cropRect longest:longest];
	}
	else {
		normCropRect = normalizeRect(cropRect, [self bounds]);
		cropAspectRatio = cropRect.size.width / cropRect.size.height;
	}
	
	//normCropRect = normalizeRect(cropRect, [self bounds]);
	//cropAspectRatio = cropRect.size.width / cropRect.size.height;

	[ctrl changeAspectRatioButton:cropAspectRatio];
	
	[self setNeedsDisplay:YES];
}

- (void)moveLeft:(id)sender {
	[self moveCropByX:-1.f Y:0.f];
}

- (void)moveRight:(id)sender {
	[self moveCropByX:1.f Y:0.f];
}

- (void)moveUp:(id)sender {
	[self moveCropByX:0.f Y:1.f];
}

- (void)moveDown:(id)sender {
	[self moveCropByX:0.f Y:-1.f];
}

- (BOOL) acceptsFirstResponder
{
	PreviewController *ctrl = [[self window] windowController];
	[[self window] invalidateCursorRectsForView:self];
	if ([ctrl isEditMode:CropEditMode]) {
		return YES;
	}
    return [ctrl showLocation] ? YES : NO;	
}

// establish mouse tracking rectangles when in scale or rotate edit modes
-(void)resetCursorRects
{
	PreviewController *ctrl = [[self window] windowController];
	NSRect cropRect = deNormalizeRect(normCropRect, [self bounds]);
	if ([ctrl isEditMode:ScaleEditMode] || [ctrl isEditMode:RotateEditMode] || [ctrl isEditMode:CombinedEditMode]) {
		NSRect bounds = [self bounds];
		NSRect trackRect = NSMakeRect([ctrl referencePoint].x * bounds.size.width/2.f + bounds.size.width/2.f - 2.f, 
									  [ctrl referencePoint].y * bounds.size.height/2.f + bounds.size.height/2.f - 2.f, 
									  4.f, 4.f);
		
		[self addCursorRect:trackRect cursor:[NSCursor crosshairCursor]];
	}
	if ([ctrl isEditMode:CropEditMode] && cropRect.size.width > 0) {
		NSRect c = cropRect;
		
		NSPoint ll = NSMakePoint(c.origin.x,                c.origin.y);
		NSPoint lr = NSMakePoint(c.origin.x + c.size.width, c.origin.y);
		NSPoint ur = NSMakePoint(c.origin.x + c.size.width, c.origin.y +c.size.height);
		NSPoint ul = NSMakePoint(c.origin.x,                c.origin.y +c.size.height);
		
		NSPoint s = NSMakePoint(c.origin.x + c.size.width/2.f, c.origin.y);
		NSPoint w = NSMakePoint(c.origin.x,                    c.origin.y + c.size.height/2.f);
		NSPoint e = NSMakePoint(c.origin.x + c.size.width,     c.origin.y + c.size.height/2.f);
		NSPoint n = NSMakePoint(c.origin.x + c.size.width/2.f, c.origin.y + c.size.height);
		
		NSRect trackRectLL = NSMakeRect(ll.x-12.f, ll.y-12.f, 24.f, 24.f); 
		NSRect trackRectLR = NSMakeRect(lr.x-12.f, lr.y-12.f, 24.f, 24.f); 
		NSRect trackRectUR = NSMakeRect(ur.x-12.f, ur.y-12.f, 24.f, 24.f); 
		NSRect trackRectUL = NSMakeRect(ul.x-12.f, ul.y-12.f, 24.f, 24.f); 

		NSRect trackRectS = NSMakeRect(s.x-12.f, s.y-12.f, 24.f, 24.f); 
		NSRect trackRectW = NSMakeRect(w.x-12.f, w.y-12.f, 24.f, 24.f); 
		NSRect trackRectE = NSMakeRect(e.x-12.f, e.y-12.f, 24.f, 24.f); 
		NSRect trackRectN = NSMakeRect(n.x-12.f, n.y-12.f, 24.f, 24.f); 
		
		// higher priority tracking areas are added first (in case they overlap)

		[self addCursorRect:trackRectLL cursor:shiftSWCursor];
		[self addCursorRect:trackRectUR cursor:shiftSWCursor];
		[self addCursorRect:trackRectLR cursor:shiftNWCursor];
		[self addCursorRect:trackRectUL cursor:shiftNWCursor];
		[shiftSWCursor setOnMouseEntered:YES];
		[shiftNWCursor setOnMouseEntered:YES];

		[self addCursorRect:trackRectW cursor:shiftHorizCursor];
		[self addCursorRect:trackRectE cursor:shiftHorizCursor];
		[self addCursorRect:trackRectN cursor:shiftVertCursor];
		[self addCursorRect:trackRectS cursor:shiftVertCursor];
		
		[shiftHorizCursor setOnMouseEntered:YES];
		[shiftVertCursor setOnMouseEntered:YES];
		
		[self addCursorRect:cropRect cursor:[NSCursor openHandCursor]];
	}
}


@end
