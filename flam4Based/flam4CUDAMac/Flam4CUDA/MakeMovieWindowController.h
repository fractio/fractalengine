/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Cocoa/Cocoa.h>

@class MovieDocument;

@interface MakeMovieWindowController : NSWindowController {

	IBOutlet NSWindow          *window;
	IBOutlet NSArrayController *framerateCtrl;
	IBOutlet NSArrayController *codecCtrl;
	
	NSString      *selectedFrameRate;
	MovieDocument *movieDoc;
	int            selectedCodecIndex;
	
	NSArray			*framerateKeys;	// collection of frame rate labels
	NSMutableArray	*codecs;		// collection of codec names
}
- (IBAction)makeMovie:(id)sender;
- (IBAction)cancel:(id)sender;

@property (nonatomic, copy) NSString *selectedFrameRate;
@property (assign)			int selectedCodecIndex;
@property(retain) NSWindow *window;

- (float)frameRateForLabel:(NSString *)key;

- (NSArray *)frameRateKeys;
- (NSArray *)codecs;

- (IBAction)help:(id)sender;

@end
