//
//  Flam4CMDAppDelegate.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/14/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "Flam4CMDAppDelegate.h"


@implementation Flam4CMDAppDelegate

- (id)init
{
    self = [super init];
    if (!self)
		return nil;
	[NSApp setDelegate:self]; // become the application delegate
    return self;
}

- (void)dealloc
{
	[super dealloc];
}

// append a error text message to the Flam4 Log - sender should be the text to be logged
- (IBAction)logError:(id)sender {
	[self logInfo:sender];
}

// append a informational text message to the Flam4 Log - sender should be the text to be logged
- (IBAction)logInfo:(id)sender {
	NSString *text = sender;
	NSLog(@"%@", text);
}

@end
