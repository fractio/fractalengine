//
//  WebhelpController.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/20/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "WebhelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

@implementation WebhelpController
@synthesize helpURL;

- (id)initWithHelpResource:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir
{
	self = [super initWithWindowNibName:@"Help"];
	
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
		NSString *path = [[NSBundle mainBundle] pathForResource:helpDir ofType:@"help"];
		path           = [[NSBundle bundleWithPath:path] pathForResource:name ofType:extension];		
		helpURL        = [[NSURL fileURLWithPath:path] retain];
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		NSURL *url = [[NSBundle mainBundle] URLForResource:helpDir withExtension:@"help"];
		helpURL    = [[[NSBundle bundleWithURL:url] URLForResource:name withExtension:extension] retain];
#endif
	}
    return self;
}

- (void)dealloc
{
	[helpURL release];
	[super dealloc];
}

- (void)windowDidLoad {
	//NSLog(@"%@", helpURL);
	[[webView mainFrame] loadRequest:[NSURLRequest requestWithURL:helpURL]];
}

- (void)webView:(WebView *)sender didFailProvisionalLoadWithError:(NSError *)error forFrame:(WebFrame *)frame {
	NSLog(@"%@", [error localizedDescription], [error localizedFailureReason]);	 
 }

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame {
	 NSLog(@"%@", [error localizedDescription], [error localizedFailureReason]);	 
 }

@end
