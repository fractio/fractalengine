/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Globals.h"
#include "RenderThread.h"
#import "FlamePreviewView.h"
#import "FlamePrintView.h"

#import "PreviewController.h"
#import "PngBatchSettingsController.h"
#import "QualityUtils.h"
#import "AppController.h"

void Render(flameParameterList* flameParams, int width, int height, FlamePreviewView *view);
//void RenderFrame(flameParameterList* flameParams, int x, int y, NSURL* outFile, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl);
void RenderFrameTileSet(flameParameterList *flameParams, int width, int height, NSURL* outFile, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl);
void RenderPreview(flameParameterList *flameParams, int width, int height, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl);
void RenderPreviewTileSet(flameParameterList *flameParams, int width, int height, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl);
void RenderIterationLoop(FlamePreviewView *view, FlameDocument *doc, RenderThread *threadObj);
void RenderFrameBatch(flameParameterList *flameParams, int width, int height, NSURL* directory, FlamePreviewView *view, bool useAlpha, NSObject *ctrl);
void RenderForPrint(Flame* flame, int width, int height, FlamePrintView *view, unsigned quality);
void StartCuda(unsigned int hwnd, int x, int y);

RenderThread::RenderThread(flameParameterList* flames, 
						   int width,
						   int height,
						   BOOL useAlpha,
						   FlamePreviewView *view,
						   FlamePrintView *printView,
						   NSURL *outFile,
						   NSObject *ctrl)
{
	this->flames   = flames;
	this->width    = width;
	this->height   = height;
	this->useAlpha = useAlpha;
	this->view     = [view retain];
	this->printView = [printView retain];
	this->outFile  = [outFile copy];
	this->ctrl     = [ctrl retain];
	abortThread    = false;
}

RenderThread::~RenderThread()
{
	[outFile release];
	[view release];
	[printView release];
	[ctrl release];
}

void RenderThread::setFlameQuality(Flame *flame, double quality, int tiles) {
	flame->numBatches = [QualityUtils batchesForDesired:quality width:width height:height tiles:tiles];
}

void *RenderThread::RenderPNG(void *_t)
{
	RenderThread *t = (RenderThread *)_t;
	t->RenderPNGProc();
	return NULL;
}

void *RenderThread::RenderToPreview(void *_t)
{
	RenderThread *t = (RenderThread *)_t;
	t->RenderPreviewProc();
	return NULL;
}

void *RenderThread::Render(void *_t)
{
	RenderThread *t = (RenderThread *)_t;
	t->RenderProc();
	return NULL;
}

void *RenderThread::RenderPNGBatch(void *_t)
{
	RenderThread *t = (RenderThread *)_t;
	t->RenderPNGBatchProc();
	return NULL;
}


void *RenderThread::RenderPrint(void *_t)
{
	RenderThread *t = (RenderThread *)_t;
	t->RenderPrintProc();
	return NULL;
}

void RenderThread::RenderPNGProc()
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

	isRendering = true;
	RenderFrameTileSet(flames, width, height, outFile, view, useAlpha, ctrl);
	isRendering = false;
	abortRender = false;
	
	[pool release];
	[(PngSettingsController *)ctrl threadDone];
}

void RenderThread::RenderPreviewProc()
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	isRendering = true;
	RenderPreviewTileSet(flames, width, height, (FlamePreviewView *)view, useAlpha, ctrl);
	isRendering = false;
	
	[pool release];
}

void RenderThread::RenderPNGBatchProc()
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	isRendering = true;
	RenderFrameBatch(flames, width, height, outFile, view, useAlpha, ctrl);
	isRendering = false;
	abortRender = false;
	[pool release];
	[(PngBatchSettingsController *)ctrl threadDone];
}

void RenderThread::RenderPrintProc()
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	if (renderingActive)
		[NSApp warnStoppingAnimation:@"Print generation"];
	isRendering = true;
	RenderForPrint(flames->flame, width, height, printView, [printView quality]);
	isRendering = false;
	
	[pool release];
	[(PreviewController *)ctrl printThreadDone]; // remove thread when stopping
}

void RenderThread::RenderProc()
{
	RenderIterationLoop(view, [(PreviewController *)ctrl document], this);
	[(PreviewController *)ctrl workerThreadDone]; // remove thread when stopping
}



