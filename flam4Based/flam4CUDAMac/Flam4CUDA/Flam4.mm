/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
//#include <GL/glew.h>
#include <math.h>

#if defined(_WIN32)
#include "FWMInterop.h"
#include "Encoder.h"
#include "image.h"
#include <windows.h>
#define DLLIMPORT __declspec(dllimport)

HINSTANCE	g_hInst;

#elif defined(__APPLE__)
#include <mach/mach_time.h>

#include "FlameAnimation.h"
#include "Defines.h"
#include <Cocoa/Cocoa.h>
#import "PngSettingsController.h"
#import "PngBatchSettingsController.h"
#import "FlamePreviewView.h"
#import "PreviewController.h"
#import "FlameDocument.h"
#import "RenderThread.h"
#import "FlamePrintView.h"
#import "QualityUtils.h"
#import "AppController.h"
#import "FlameTileArray.h"
#import "ImageRepTileArray.h"
#import "FlameTile.h"
#import "FlameTileArray.h"
#import "ImageUtil.h"

#define DLLIMPORT

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif
#endif

Flame* g_pFlame = NULL;

#if defined(_WIN32)
extern "C" DLLIMPORT void cuStartCuda(unsigned int hwnd, int x, int y);
extern "C" DLLIMPORT void cuStartFrame(unsigned int hwnd, int x, int y, Flame* flame);
#elif defined(__APPLE__)
extern "C" DLLIMPORT void cuStartCuda(NSOpenGLView *view, int x, int y);
extern "C" DLLIMPORT void cuStartFrame(NSOpenGLView *view, int x, int y, Flame* flame);
#endif
extern "C" DLLIMPORT void cuStopCuda();
extern "C" DLLIMPORT void cuRunFuse(Flame* flame);
extern "C" DLLIMPORT void cuRenderBatch(Flame* flame);
extern "C" DLLIMPORT void cuFinishFrame(void* image, bool useAlpha);
extern "C" DLLIMPORT bool cuCudaActive();

// Global variables ================================
int quality = 30;
bool          isAnimating = true;
volatile int  progress = 0;
volatile int  frameNumber = 0;
volatile bool isDone = false;
volatile bool abortRender = false;
int           g_NumFrames = NUM_FRAMES;

bool          renderingActive = false;
bool          cudaActive = false;
bool          optionsOpen = false;
volatile bool isRendering = false;
NSObject     *lockObj = nil; // the global synchronization object



#if defined(_WIN32)
unsigned int oldtime;
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
	oldtime = GetTickCount();
	OpenWindow();
	return 0;
}

#elif defined(__APPLE__)
// Get number of milliseconds that have elapsed since the system was started
unsigned long GetTickCount(void)
{
    static mach_timebase_info_data_t    sTimebaseInfo;
	
    // If this is the first time we've run, get the timebase.
    // We can use denom == 0 to indicate that sTimebaseInfo is
    // uninitialised because it makes no sense to have a zero
    // denominator is a fraction.
	
    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }
	
    // Convert to nanoseconds.
	
    // Do the maths.  We hope that the multiplication doesn't
    // overflow; the price you pay for working in fixed point.
	
    return (long)(mach_absolute_time()/1000000) * sTimebaseInfo.numer / sTimebaseInfo.denom;
}

unsigned long oldtime  = GetTickCount();

#endif

// log the render batch performance globally
void logBatchPerformance(float seconds, float batches)
{
	AppController *appCtrl = (AppController *)[NSApp delegate];
	[appCtrl renderBatchPerformance:seconds numBatches:batches];
}

//This function renders a single frame.
NSBitmapImageRep *RenderTile(Flame* flame, int width, int height, NSView *view, BOOL useAlpha, BOOL flip, double *duration)
{
	if (abortRender)
		return nil;
	
	if (flame == NULL)
		return nil;
	if (! lockObj)  // create our shared lock object if needed
		lockObj = [[NSObject alloc] init];
	
	NSBitmapImageRep *rep;
	@synchronized(lockObj) {
		unsigned long now = GetTickCount();
		
		cuStartCuda(view,width,height);
		cuRunFuse(flame);
		flame->Clone(&g_pFlame);
		cuStartFrame(view, width, height, g_pFlame);
		for (int subSample = 0; subSample < flame->numBatches; subSample++)
		{
			cuRenderBatch(flame);
			progress++;
			if (abortRender)
				break; // dont submit anymore render batches - just finish prematurely
		}
		unsigned char* img = new unsigned char[4*width*height];
		cuFinishFrame(img,useAlpha);
		cuStopCuda();
		NSData *data = [NSData dataWithBytes:img length:4*width*height];
		delete img;
		
		rep = [ImageUtil imageRepFromData:data hasAlpha:useAlpha rect:NSMakeRect(0, 0, width, height) flip:flip];
		*duration = (double)(GetTickCount() - now)/1000.;
	}
	logBatchPerformance(*duration, flame->numBatches);
	return rep;
}

//This function renders a frame directly to the view window
void Render(FlamePreviewView *view, int width, int height, flameParameterList *flameParams)
{
	unsigned long newtime = GetTickCount();
	unsigned long startTime = GetTickCount();
	double timeElapsed = (double)(newtime-oldtime);
	double rotationSpeed = .0002;
	if (flameParams == NULL)
		return;
	flameAnimation* animation = new flameAnimation();
	if (flameParams->nextFlame)
		if (flameParams->nextFlame->flame->numTrans > flameParams->flame->numTrans)
			flameParams->nextFlame->flame->Clone(&g_pFlame);
		else
			flameParams->flame->Clone(&g_pFlame);
	else
		flameParams->flame->Clone(&g_pFlame);
	if (flameParams->nextFlame)
		animation->Interpolate(fmod(timeElapsed*rotationSpeed/(2.0*3.141592653589793),1.0), flameParams->flame, flameParams->nextFlame->flame, &g_pFlame);
	double sinTheta = 0.0;
	double cosTheta = 1.0;
	cuStartFrame(view, width, height, g_pFlame);
	for (int n = 0; n < flameParams->flame->numBatches; n++)
	{
		
		newtime = GetTickCount();
		timeElapsed = (double)(newtime-oldtime);
		if (flameParams->nextFlame)
			animation->Interpolate(fmod(timeElapsed*rotationSpeed/(2.0*3.141592653589793),1.0), flameParams->flame, flameParams->nextFlame->flame, &g_pFlame);
		if (isAnimating)
		{
			sinTheta = sin(rotationSpeed*timeElapsed);
			cosTheta = cos(rotationSpeed*timeElapsed);
		}
		for (int n = 0; n < g_pFlame->numTrans; n++)
		{
			if (g_pFlame->transAff[n].rotates != 0)
			{
				double a = g_pFlame->transAff[n].a;
				double b = g_pFlame->transAff[n].b;
				double d = g_pFlame->transAff[n].d;
				double e = g_pFlame->transAff[n].e;
				g_pFlame->trans[n].a=(float)(cosTheta*a-sinTheta*d);
				g_pFlame->trans[n].b=(float)(cosTheta*b-sinTheta*e);
				g_pFlame->trans[n].d=(float)(sinTheta*a+cosTheta*d);
				g_pFlame->trans[n].e=(float)(sinTheta*b+cosTheta*e);
			}
		}
		cuRenderBatch(g_pFlame);
	}	
	unsigned char* img = new unsigned char[4*width*height];
	cuFinishFrame(img, false);
	NSData *data          = [NSData dataWithBytes:img length:4*width*height];
	
	// create an image from the data for painting to view
	NSBitmapImageRep *rep = [ImageUtil imageRepFromData:data hasAlpha:NO rect:NSMakeRect(0, 0, width, height) flip:NO];
	
	[view setImageRep:rep];
	[view setNeedsDisplay:YES];
	delete img;
	delete animation;
	logBatchPerformance(GetTickCount() - startTime, flameParams->flame->numBatches);
}

//This function renders a single frame and saves it to outFile.
//hwnd should be NULL if you're rendering a frame larger than 4096x4096
//since openGL cannot handle textures that large.
#if defined(_WIN32)
void RenderFrameTileSet(flameParameterList *flameParams, int width, int height, wchar_t* outFile, unsigned int hwnd,bool useAlpha)
#elif defined(__APPLE__)
void RenderFrameTileSet(flameParameterList *flameParams, int width, int height, NSURL* outFile, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl)
#endif
{
	PngSettingsController *pngCtrl = (PngSettingsController *)ctrl;
	ImageRepTileArray *imageArray  = [[pngCtrl tiles] makeImageRepTileArray:YES];
	
	double duration = 0.;
	double totalDuration = 0.;
	unsigned numBatches = 0;
	
	// iterate through the tiles do each in turn
	for (FlameTile * tile in [[pngCtrl tiles] array]) {
		numBatches   += [tile flame]->numBatches;	
		NSBitmapImageRep *rep = RenderTile([tile flame], [tile width], [tile height], view, useAlpha, YES, &duration);
		if (abortRender) 
			return;
		totalDuration += duration;
		[imageArray setTileAtRow:[tile row] col:[tile column] withObject:rep];
	}
	
	NSBitmapImageRep *combinedRep = [imageArray combinedImageRep];
	
	[ImageUtil exportImageToPNG:outFile rep:combinedRep useAlpha:useAlpha rect:NSMakeRect(0, 0, width, height)];
	
	isDone = true;

	FlameTileArray *tiles = [pngCtrl tiles];
	int tileCount = [tiles count];
	double actualQuality = [QualityUtils actualQualityWith:[tiles numBatchesPerTile] width:width height:height tiles:tileCount];
	NSString *msg = [NSString stringWithFormat:@"PNG Render: [%dX%d]  %0.3f sec  Quality:%.0f  Tiles:%u Batches Total:%u", 
					 width, height, totalDuration, actualQuality, tileCount, [tiles numBatches]];
	[NSApp sendAction:@selector(logInfo:) to:nil from:msg];
	
	[NSApp sendAction:@selector(openPngDocument:) to:nil from:outFile];  // open the new PNG file as a document
#if defined(_WIN32)	
	MessageBeep(MB_ICONASTERISK);
#else
	NSBeep();
#endif
}

//This function renders a single frame and saves it to outFile.
//hwnd should be NULL if you're rendering a frame larger than 4096x4096
//since openGL cannot handle textures that large.
#if defined(_WIN32)
void RenderPreviewTileSet(flameParameterList *flameParams, int width, int height, unsigned int hwnd,bool useAlpha)
#elif defined(__APPLE__)
void RenderPreviewTileSet(flameParameterList *flameParams, int width, int height, FlamePreviewView *view, BOOL useAlpha, NSObject *ctrl)
#endif
{
	PreviewController *previewCtrl = (PreviewController *)ctrl;
	[[view window] orderFront:nil];
			
	ImageRepTileArray *imageArray = [[previewCtrl tiles] makeImageRepTileArray:NO];
	
	double duration = 0.;
	double totalDuration = 0.;
	unsigned numBatches = 0;
	
	// iterate through the tiles do each in turn
	for (FlameTile * tile in [[previewCtrl tiles] array]) {
		numBatches   += [tile flame]->numBatches;	
		NSBitmapImageRep *rep = RenderTile([tile flame], [tile width], [tile height], view, useAlpha, NO, &duration);
		if (abortRender) 
			return;
		totalDuration += duration;
		[imageArray setTileAtRow:[tile row] col:[tile column] withObject:rep];			 
	}
	
	NSBitmapImageRep *combinedRep = [imageArray combinedImageRep];
	
	[view setImageRep:combinedRep];
	[view setNeedsDisplay:YES];
	
	double actualQuality = [QualityUtils actualQualityWith:[[previewCtrl tiles] numBatchesPerTile]
													 width:width
													height:height
													 tiles:[[previewCtrl tiles] count]];
	
	[previewCtrl logRenderStats:totalDuration 
						  width:width 
						 height:height 
						quality:actualQuality 
					 numBatches:numBatches 
						  tiles:[imageArray labelDescription]];
	[previewCtrl threadDone];
}


// the background rendering loop
#if defined(_WIN32)
void RenderIterationLoop(unsigned int hwnd, FlameDocument *doc, RenderThread *threadObj)
#elif defined(__APPLE__)
void RenderIterationLoop(FlamePreviewView *view, FlameDocument *doc, RenderThread *threadObj)
#endif
{
	if (! lockObj)  // create our shared lock object if needed
		lockObj = [[NSObject alloc] init];
	static long const iterationTime = 2000L; // 2 milliseconds
	
	long iterationCount = 0;
	while (1) {
		unsigned long now = GetTickCount();
		unsigned long lastTime = now;
		
		if (threadObj->abortThread) { // dont stop Cuda as it is possible in use by other documents
			return;
		}
		if (renderingActive)
		{
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
			
			NSRect rect = [view bounds];	
			int w = rect.size.width;
			int h = rect.size.height;
			
#if defined(_WIN32)
			@synchronized(lockObj) {
				cuStartCuda(view, w, h);
				Render(hwnd, w, h, [doc flameData]);
				cuStopCuda();
			}
#elif defined(__APPLE__)
			@synchronized(lockObj) {
				cuStartCuda(view, w, h);
				Render(view, w, h, [doc flameData]);
				cuStopCuda();
			}
#endif
			
			now = GetTickCount();
			
			[pool release];
			iterationCount++;
		}
		if (threadObj->abortThread) { // dont stop Cuda as it is possible in use by other documents
			return;
		}
		long timeToSleep = iterationTime - (now - lastTime);
		if (timeToSleep > 0) // want 2 millisecond duration per iteration
			usleep(timeToSleep);
	}
}

#if defined(_WIN32)
// export this frame to a file in the batch directory
char * pngURL(char *filename, char_t* outFile) {
	static wchar_t outFilePath[1024];
	outFilePath[0] = 0;
	wcscat(outFilePath,outFile);
	wcscat(outFilePath,L"\\");
	wcscat(outFilePath,filename);
	wcscat(outFilePath,L".png");
	return outFilePath
}

#elif defined(__APPLE__)
// export this frame to a file in the batch directory
NSURL *pngURL(NSString *filename, NSURL* directory) {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)  {// Leopard
		NSString *directoryPath = [directory path];
		return [NSURL fileURLWithPath:[[directoryPath stringByAppendingPathComponent:filename] 
									  stringByAppendingPathExtension:@"png"]];
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		return [[directory URLByAppendingPathComponent:filename] URLByAppendingPathExtension:@"png"];
#endif
	}
	return nil;
}
#endif

#if defined(_WIN32)
void RenderFrameBatch(flameParameterList *flameParams, int width, int height, wchar_t* outFile, unsigned int hwnd, bool useAlpha)
#elif defined(__APPLE__)
void RenderFrameBatch(flameParameterList *flameParams, int width, int height, NSURL* directory, FlamePreviewView *view, bool useAlpha, NSObject *ctrl)
#endif
{
	PngBatchSettingsController *pngCtrl = (PngBatchSettingsController *)ctrl;
	unsigned firstNumBatches = flameParams->flame->numBatches;
	double duration      = 0.;
	double totalDuration = 0.;
	
	do // for each frame - process the frame's PNG file
	{
		[pngCtrl setTiles:[AppController splitAreaIntoTiles:width * height 
												width:width 
											   height:height 
													  flame:flameParams->flame 
													flipped:YES]];
		int batchesPerTile = ceil([QualityUtils batchesForDesired:[pngCtrl quality] width:width height:height tiles:[[pngCtrl tiles] count]]);
		[[pngCtrl tiles] setNumBatchesPerTile:batchesPerTile];
		[[pngCtrl progressBar] setMaxValue: batchesPerTile];
		
		ImageRepTileArray *imageArray = [[pngCtrl tiles] makeImageRepTileArray:YES];
		
		progress = 0;
		// iterate through the tiles do each in turn
		for (FlameTile * tile in [[pngCtrl tiles] array]) {
			NSBitmapImageRep *rep = RenderTile([tile flame], [tile width], [tile height], view, useAlpha, YES, &duration);
			if (abortRender)
				return;
			totalDuration += duration;
			[imageArray setTileAtRow:[tile row] col:[tile column] withObject:rep];			 
		}
		
		NSBitmapImageRep *combinedRep = [imageArray combinedImageRep];
		NSURL * url = pngURL(flameParams->flameMeta->name, directory);
		[ImageUtil exportImageToPNG:url rep:combinedRep useAlpha:useAlpha rect:NSMakeRect(0, 0, width, height)];
		
		flameParams = flameParams->nextFlame;
		frameNumber++;
		
	} while  (flameParams!= NULL);
	isDone = true;
	
	double actualQuality = [QualityUtils actualQualityWith:[[pngCtrl tiles] numBatchesPerTile] width:width height:height tiles:[[pngCtrl tiles]count]];
	NSString *msg = [NSString stringWithFormat:@"PNGBatchRender: %d frames [%dX%d]  %0.3f sec  Quality:%.0f  BatchesPerFrame:%.0f", 
					 frameNumber, width, height, totalDuration, actualQuality, firstNumBatches];
	[NSApp sendAction:@selector(logInfo:) to:nil from:msg];
#if defined(_WIN32)
	MessageBeep(MB_ICONASTERISK);
#elif defined(__APPLE__)
	NSBeep();
#endif
}


//This function renders a single frame for printing
#if defined(_WIN32)
void RenderForPrint(Flame* flame, int width, int height, unsigned int hwnd, unsigned quality)
#elif defined(__APPLE__)
void RenderForPrint(Flame* flame, int width, int height, FlamePrintView *view, unsigned quality)
#endif
{
	double duration      = 0.;
	double totalDuration = 0.;
	FlameTileArray *tiles = [[AppController splitAreaIntoTiles:width * height 
														 width:width 
														height:height 
														 flame:flame 
													   flipped:YES] retain];
	int batchesPerTile = ceil([QualityUtils batchesForDesired:quality width:width height:height tiles:[tiles count]]);
	[tiles setNumBatchesPerTile:batchesPerTile];
	
	ImageRepTileArray *imageArray = [tiles makeImageRepTileArray:YES];
	
	progress = 0;
	// iterate through the tiles do each in turn
	for (FlameTile * tile in [tiles array]) {
		NSBitmapImageRep *rep = RenderTile([tile flame], [tile width], [tile height], NULL, NO, YES, &duration);
		totalDuration += duration;
		[imageArray setTileAtRow:[tile row] col:[tile column] withObject:rep];			 
	}
	
	NSBitmapImageRep *combinedRep = [imageArray combinedImageRep];
	[view setImageRep:combinedRep];

	double actualQuality = [QualityUtils actualQualityWith:[tiles numBatchesPerTile] width:width height:height tiles:[tiles count]];
	NSString *msg = [NSString stringWithFormat:@"PrintRender: [%dX%d]  %0.3f sec  Quality:%.0f  Batches:%.0f Tiles:%u", 
					 width, height, duration, actualQuality, flame->numBatches, [tiles count]];
	
	[[NSApp delegate] logInfo:msg];
	[tiles release];
}


