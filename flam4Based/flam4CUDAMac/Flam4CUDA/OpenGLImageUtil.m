//
//  OpenGLImageUtil.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "OpenGLImageUtil.h"


@implementation OpenGLImageUtil

// capture the pixel data from the view and flip it so its not upside down
+ (NSImage*) imageFromOpenGLViewAlpha:(NSOpenGLView*)myOpenGLView {
	NSRect rect = [myOpenGLView bounds];
	int height=NSHeight(rect);
	int width=NSWidth(rect);
	NSBitmapImageRep* imageRep;
	NSBitmapImageRep* imageRep2;
	NSImage* finalImage;
	
	imageRep=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													  pixelsWide:width
													  pixelsHigh:height
												   bitsPerSample:8
												 samplesPerPixel:4
														hasAlpha:YES
														isPlanar:NO
												  colorSpaceName:NSCalibratedRGBColorSpace
													 bytesPerRow:width*4
													bitsPerPixel:32] 
			  autorelease];
	// we need another copy so we can flip the image data
	imageRep2=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													   pixelsWide:width
													   pixelsHigh:height
													bitsPerSample:8
												  samplesPerPixel:4
														 hasAlpha:YES
														 isPlanar:NO
												   colorSpaceName:NSCalibratedRGBColorSpace
													  bytesPerRow:width*4
													 bitsPerPixel:32] 
			   autorelease];
	
	[[myOpenGLView openGLContext] makeCurrentContext];
	
	glPixelStorei(GL_PACK_ALIGNMENT,   1);
	glPixelStorei(GL_PACK_ROW_LENGTH,  0);
	glPixelStorei(GL_PACK_SKIP_ROWS,   0);
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
	GLenum lastBuffer; // get the current read buffer
	glGetIntegerv(GL_READ_BUFFER, (GLint*)&lastBuffer);
	glReadBuffer(GL_FRONT); // we want to read from front buffer
	
	const char *buf  = (const char *)[imageRep bitmapData];
	const char *dest = (const char *)[imageRep2 bitmapData];
	
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, [imageRep bitmapData]); // get the pixels
	glReadBuffer(lastBuffer); // restore the original read buffer
	
	// flip the image around the x-axis so it is not saved upside down
	int rowWidth = width*4;
	for (int si = height - 1; si > 0; si--) {
		int di = height -1 - si;
		const char *d = dest + di * rowWidth;
		const char *s = buf  + si * rowWidth;
		memcpy((void *)d, (void *)s, rowWidth);
	}
	
	[NSOpenGLContext clearCurrentContext];
	finalImage   = [[[NSImage alloc] initWithSize:NSMakeSize(width, height)] autorelease];
	[finalImage addRepresentation:imageRep2];
	
	return finalImage;
}

// capture the pixel data from the view and flip it so its not upside down
+ (NSImage*) imageFromOpenGLViewNoAlpha:(NSOpenGLView*)myOpenGLView {
	NSRect rect = [myOpenGLView bounds];
	int height=NSHeight(rect);
	int width=NSWidth(rect);
	NSBitmapImageRep* imageRep;
	NSBitmapImageRep* imageRep2;
	NSImage* finalImage;
	
	imageRep=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													  pixelsWide:width
													  pixelsHigh:height
												   bitsPerSample:8
												 samplesPerPixel:3
														hasAlpha:NO
														isPlanar:NO
												  colorSpaceName:NSCalibratedRGBColorSpace
													 bytesPerRow:width*3
													bitsPerPixel:24] 
			  autorelease];
	// we need another copy so we can flip the image data
	imageRep2=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													   pixelsWide:width
													   pixelsHigh:height
													bitsPerSample:8
												  samplesPerPixel:3
														 hasAlpha:NO
														 isPlanar:NO
												   colorSpaceName:NSCalibratedRGBColorSpace
													  bytesPerRow:width*3
													 bitsPerPixel:24] 
			   autorelease];
	
	[[myOpenGLView openGLContext] makeCurrentContext];
	
	glPixelStorei(GL_PACK_ALIGNMENT,   1);
	glPixelStorei(GL_PACK_ROW_LENGTH,  0);
	glPixelStorei(GL_PACK_SKIP_ROWS,   0);
	glPixelStorei(GL_PACK_SKIP_PIXELS, 0);
	GLenum lastBuffer; // get the current read buffer
	glGetIntegerv(GL_READ_BUFFER, (GLint*)&lastBuffer);
	glReadBuffer(GL_FRONT); // we want to read from front buffer
	
	const char *buf  = (const char *)[imageRep bitmapData];
	const char *dest = (const char *)[imageRep2 bitmapData];
	
	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, [imageRep bitmapData]); // get the pixels
	glReadBuffer(lastBuffer); // restore the original read buffer
	
	// flip the image around the x-axis so it is not saved upside down
	int rowWidth = width*3;
	for (int si = height - 1; si >= 0; si--) {
		int di = height -1 - si;
		const char *d = dest + di * rowWidth;
		const char *s = buf  + si * rowWidth;
		memcpy((void *)d, (void *)s, rowWidth);
	}
	
	[NSOpenGLContext clearCurrentContext];
	finalImage   = [[[NSImage alloc] initWithSize:NSMakeSize(width, height)] autorelease];
	[finalImage addRepresentation:imageRep2];
	
	return finalImage;
}

// capture the pixel data from the view and flip it so its not upside down
+ (NSImage*) imageFromOpenGLView:(NSOpenGLView*)myOpenGLView hasAlpha:(BOOL)hasAlpha {
	if (hasAlpha)
		return [self imageFromOpenGLViewAlpha:myOpenGLView];
	else
		return [self imageFromOpenGLViewNoAlpha:myOpenGLView];	
}

// capture the view's image then export it as a PNG file
+ (void)exportImageToPNG:(NSURL *)url 
					view:(NSOpenGLView *)view 
{
	if ([view lockFocusIfCanDraw]) {
		NSImage *image = [OpenGLImageUtil imageFromOpenGLView:view hasAlpha:transparent];
		
		NSBitmapImageRep* rep = [[image representations] objectAtIndex:0];
		
		[view unlockFocus];
		
		NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
		NSData *data = [rep representationUsingType:NSPNGFileType properties:properties];
		
		NSError *error = nil;
		if (! [data writeToURL:url options:0 error:&error]) {
			NSAlert *a = [NSAlert alertWithError:error];
			[a runModal];
			return;
		}
	}
}


@end
