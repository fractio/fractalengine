//
//  OpenGLImageUtil.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface OpenGLImageUtil : NSObject {

}
+ (NSImage*) imageFromOpenGLView:(NSOpenGLView*)myOpenGLView hasAlpha:(BOOL)hasAlpha;

+ (void)exportImageToPNG:(NSURL *)fileNameStr 
					view:(NSOpenGLView *)view; 

@end
