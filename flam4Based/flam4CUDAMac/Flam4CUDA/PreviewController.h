/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Cocoa/Cocoa.h>
#import <pthread.h>

@class FlamePreviewView;
@class FlamePrintView;
@class FlameTileArray;

// used for radio button selection
enum EditMode {
	NoEditMode        = 0,
	CombinedEditMode  = 1,
	TranslateEditMode = 2,
	ScaleEditMode     = 3,
	RotateEditMode    = 4,
	CropEditMode      = 5,
	CropResizeMode    = 6,
};

enum AspectRatioSetting {
	AnyAR         = 0,
	AR_1x1		  = 569,
	AR_3x2		  = 570,
	AR_4x3		  = 571,
	AR_5x4		  = 572,
	AR_16x9		  = 573,
	AR_16x10	  = 574,
	CustomAR      = 575,
};


@interface PreviewController : NSWindowController {
	IBOutlet FlamePreviewView    *view;					// the view I control
	IBOutlet NSViewController    *pageSetupController;
	IBOutlet NSWindow            *printProgressWindow;
	IBOutlet NSButton            *aspectRatioLockBtn;
	IBOutlet NSButton            *goBackBtn;
	IBOutlet NSButton            *goForwardBtn;
	IBOutlet NSButton            *infoBtn;
	IBOutlet NSTextField         *currentFrameField;
	IBOutlet NSTextField         *statsLabelField;
	IBOutlet NSPanel             *infoPanel;
	IBOutlet NSPanel             *helpPanel;
	IBOutlet NSView              *trayView;
	IBOutlet NSView              *trayViewRight;
	IBOutlet NSMatrix            *editModeBtns;
	IBOutlet NSMatrix            *editModeRightBtns;
	IBOutlet NSButton            *trayUndoBtn;
	IBOutlet NSButton            *trayRedoBtn;
	IBOutlet NSButton            *trayUndoAllBtn;
	IBOutlet NSButton            *trayUndoRightBtn;
	IBOutlet NSButton            *trayRedoRightBtn;
	IBOutlet NSButton            *trayUndoAllRightBtn;
	IBOutlet NSButton            *trayOpenCloseBtn;
	IBOutlet NSPopUpButton       *aspectRatioPopUpBtn;
	IBOutlet NSPopUpButton       *aspectRatioPopUpRightBtn;
		
	BOOL      transparent;				// do we want transparency?
	int       quality;					// render quality
	NSString *statsLabel;
	NSString *infoLabel;
	NSString *printStatsLabel;
	NSString *printStatsLabel2;
	NSString *printStatsLabel3;
	BOOL      aspectRatioLocked;
	float     aspectRatio;
	FlameTileArray *tiles;
	NSNumber  *editMode;
	NSDrawer *drawer;
	NSDrawer *drawerRight;
	NSPoint   referencePoint;			// normalized relative to viewport so viewport center is {0, 0} range is -1 to 1
	BOOL      showLocation;				// show mouse location as it is moved
	NSNumber *lockCropAR;				// lock crop aspect ratio
	NSSize    origContentViewSize;      // window content view at point of opening edit drawer
	NSInteger currentAspectRatioTag;    // tag value of currently selected aspect ratio menuitem
	NSNumber *orientation;				// crop rectangle orientation

@private
	class RenderThread *thread;			// preview rendering thread
	class RenderThread *workerThread;	// background Rendering thread
	class RenderThread *printThread;	// print rendering thread
}

@property BOOL transparent;
@property int  quality;
@property (assign) float    aspectRatio;
@property (copy) NSString  *statsLabel;
@property (copy) NSString  *infoLabel;
@property (copy) NSString  *printStatsLabel;
@property (copy) NSString  *printStatsLabel2;
@property (copy) NSString  *printStatsLabel3;
@property (assign) BOOL     aspectRatioLocked;
@property (retain) FlameTileArray *tiles;
@property (retain) NSNumber *editMode;
@property (assign) NSPoint  referencePoint;
@property (assign) BOOL     showLocation;
@property (retain) NSNumber *lockCropAR;
@property (retain) NSDrawer *drawer;
@property (retain) NSDrawer *drawerRight;
@property (assign) NSSize    origContentViewSize;
@property (retain) NSNumber *orientation;

- (void) threadDone;
- (void) workerThreadDone;
- (void) printThreadDone;
- (void) renderAsPreview:(BOOL)useDefaults;
- (FlamePreviewView *)view; // return the NSOpenGLView
- (NSViewController *)pageSetupController;
- (void)logRenderStats:(double)width height:(double)height;
- (void)logRenderStats:(NSTimeInterval)duration 
				 width:(double)width 
				height:(double)height 
			   quality:(double)actualQuality 
			numBatches:(double)numBatches 
				 tiles:(NSString *)tiles;
- (void)startPrintProgress:(float)quality pointSize:(NSSize)size renderSize:(NSSize)renderSize resolution:(float)resolution;
- (void)stopPrintProgress;
- (pthread_t) invokeRenderPrintThread:(NSRect)rect printView:(FlamePrintView *)printView;
- (void)desiredQualityChanged;
- (unsigned)tileCount;
- (void)viewInit;
- (BOOL)statusBarItemsAreHidden;
- (void)hideStatusBarItems:(BOOL)show;
- (void)checkTrayUndoRedoBtns:(NSDocument *)doc;
- (BOOL)isEditMode:(EditMode)editMode;
- (void)changeFrameBy:(int)increment;

- (IBAction)aspectRatioLockClicked:(id)sender;
- (IBAction)goBackClicked:(id)sender;
- (IBAction)goForwardClicked:(id)sender;
- (IBAction)showInfo:(id)sender;
- (IBAction)editModeChange:(id)sender;
- (IBAction)editModeRightChange:(id)sender;
- (IBAction)toggleDrawer:(id)sender;
- (IBAction)resetCameraChanges:(id)sender;
- (IBAction)toggleShowMouseLocation:(id)sender;
- (IBAction)rotate90Left:(id)sender;
- (IBAction)rotate90Right:(id)sender;
- (IBAction)radioAction:(id)sender;
- (IBAction)customAspectRatio:(id)sender;
- (void)changeAspectRatioButton:(float)aspectRatio;
- (IBAction)flipCropOrientation:(id)sender;
- (IBAction)help:(id)sender;

- (IBAction)RP_center:(id)sender;
@end
