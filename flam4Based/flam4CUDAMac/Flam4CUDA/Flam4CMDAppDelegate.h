//
//  Flam4CMDAppDelegate.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/14/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface Flam4CMDAppDelegate : NSObject {

}
- (IBAction)logError:(id)sender;
- (IBAction)logInfo:(id)sender;
	
@end
