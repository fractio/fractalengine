//
//  Transformers.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/10/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "Transformers.h"
#import "PreviewController.h"
#import "AspectRatioController.h"


@implementation NotCropEditModeTransformer

+ (Class)transformedValueClass
{
    return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value
{
	EditMode editMode;
    if (value == nil) return nil;
	
    // Attempt to get a reasonable value from the
    // value object.
    if ([value respondsToSelector: @selector(intValue)]) {
		// handles NSString and NSNumber
        editMode = (EditMode)[value intValue];
    }
	return [NSNumber numberWithBool: editMode != CropEditMode];
}
@end

@implementation NotCropResizeModeTransformer

+ (Class)transformedValueClass
{
    return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value
{
	EditMode editMode;
    if (value == nil) return nil;
	
    // Attempt to get a reasonable value from the
    // value object.
    if ([value respondsToSelector: @selector(intValue)]) {
		// handles NSString and NSNumber
        editMode = (EditMode)[value intValue];
    }
	return [NSNumber numberWithBool: editMode != CropResizeMode];
}
@end

@implementation NotRotateEditModeTransformer

+ (Class)transformedValueClass
{
    return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value
{
	EditMode editMode;
    if (value == nil) return nil;
	
    // Attempt to get a reasonable value from the
    // value object.
    if ([value respondsToSelector: @selector(intValue)]) {
		// handles NSString and NSNumber
        editMode = (EditMode)[value intValue];
    }
	return [NSNumber numberWithBool: editMode != RotateEditMode];
}
@end

@implementation NotScaleEditModeTransformer

+ (Class)transformedValueClass
{
    return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value
{
	EditMode editMode;
    if (value == nil) return nil;
	
    // Attempt to get a reasonable value from the
    // value object.
    if ([value respondsToSelector: @selector(intValue)]) {
		// handles NSString and NSNumber
        editMode = (EditMode)[value intValue];
    }
	return [NSNumber numberWithBool: editMode != ScaleEditMode];
}
@end

@implementation NotRotateScaleEditModeTransformer

+ (Class)transformedValueClass
{
    return [NSNumber class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value
{
	EditMode editMode;
    if (value == nil) return nil;
	
    // Attempt to get a reasonable value from the
    // value object.
    if ([value respondsToSelector: @selector(intValue)]) {
		// handles NSString and NSNumber
        editMode = (EditMode)[value intValue];
    }
	return [NSNumber numberWithBool: editMode != ScaleEditMode && editMode != RotateEditMode && editMode != CombinedEditMode];
}
@end


