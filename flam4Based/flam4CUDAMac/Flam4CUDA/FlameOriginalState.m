//
//  FlameOriginalState.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/4/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "FlameOriginalState.h"


@implementation FlameOriginalState
@synthesize size;
@synthesize center;
@synthesize rotation;
@synthesize scale;

- (id)initWithSize:(NSSize)_size center:(NSPoint)_center rotation:(float)_rotation scale:(float)_scale {
	self = [super init];
    if (!self)
		return nil;
	size     = _size;
	center   = _center;
	rotation = _rotation;
	scale    = _scale;
	
	return self;
}


@end
