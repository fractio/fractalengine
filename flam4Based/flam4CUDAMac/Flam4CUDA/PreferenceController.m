/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */
#import "PreferenceController.h"
NSString * const Flam4ViewBgColorKey         = @"ViewBackgroundColor";
NSString * const Flam4PreviewDimensionSource = @"PreviewDimensionSource";
NSString * const Flam4PreviewQualityKey      = @"PreviewQuality";
NSString * const Flam4PreviewWidthKey        = @"PreviewWidth";
NSString * const Flam4PreviewHeightKey       = @"PreviewHeight";
NSString * const Flam4PreviewTransparencyKey = @"PreviewTransparency";
NSString * const Flam4PreviewLinkStateKey    = @"PreviewLinkState";
NSString * const Flam4PngQualityKey          = @"PngQuality";
NSString * const Flam4PngWidthKey            = @"PngWidth";
NSString * const Flam4PngHeightKey           = @"PngHeight";
NSString * const Flam4PngTransparencyKey     = @"PngTransparency";
NSString * const Flam4PngLinkStateKey        = @"PngLinkState";
NSString * const Flam4PrintQualityKey        = @"PrintQuality";


@implementation PreferenceController
@synthesize prevQuality;

- (id)init {
	if (! [super initWithWindowNibName:@"Preferences"])
		return nil;
	return self;
}

- (void)dealloc {
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];	
	[nc removeObserver:self];
	
	[super dealloc];
}

- (IBAction)changeBackgroundColor:(id)sender
{
	NSColor *color = [previewColorWell color];
	NSData *colorAsData = [NSKeyedArchiver archivedDataWithRootObject:color];

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:colorAsData forKey:Flam4ViewBgColorKey];
}

- (IBAction)changePreviewQuality:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	int quality = [previewQualityField integerValue];
	[defaults setInteger:quality forKey:Flam4PreviewQualityKey];
	
	[self setPrevQuality:[NSNumber numberWithInt:quality]]; // want to be able to report changes to preview quality
}

- (IBAction)changePreviewWidth:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	int width = [previewWidthField integerValue];
	[defaults setInteger:width forKey:Flam4PreviewWidthKey];

	// set linked height field if applicable
	if ([defaults boolForKey:Flam4PreviewLinkStateKey]) {
		float aspectRatio = (previewAspectRatio == 0.0f) ? 4.0f/3.0f : previewAspectRatio; // check for zero aspectRatio
		int newHeight = (int)((float)width/aspectRatio);
		if ([previewHeightField integerValue] != newHeight) {
			[defaults setInteger:newHeight forKey:Flam4PreviewHeightKey];
			[previewHeightField setIntegerValue:newHeight];
		}
	}
	else { // update aspect ratio
		int height = [PreferenceController previewHeight];
		if (height != 0) 
			previewAspectRatio = (float)width/(float)height;
		else
			previewAspectRatio = 4.0f/3.0f;		
	}
}

- (IBAction)changePreviewHeight:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	int height = [previewHeightField integerValue];
	[defaults setInteger:height forKey:Flam4PreviewHeightKey];
	
	// set linked width field if applicable
	if ([defaults boolForKey:Flam4PreviewLinkStateKey]) {
		int newWidth = (int)((float)height*previewAspectRatio);
		if ([previewWidthField integerValue] != newWidth) {
			[defaults setInteger:newWidth forKey:Flam4PreviewWidthKey];
			[previewWidthField setIntegerValue:newWidth];

		}
	}
	else { // update aspect ratio
		int width = [PreferenceController previewWidth];
		if (height != 0) 
			previewAspectRatio = (float)width/(float)height;
		else
			previewAspectRatio = 4.0f/3.0f;		
	}
}

- (IBAction)changePreviewTransparency:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:([previewTransparentCB state] == NSOnState) forKey:Flam4PreviewTransparencyKey];
}


- (IBAction)changePngQuality:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:[pngQualityField integerValue] forKey:Flam4PngQualityKey];
}

- (IBAction)changePngWidth:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	int width = [pngWidthField integerValue];
	[defaults setInteger:width forKey:Flam4PngWidthKey];
	
	// set linked height field if applicable
	if ([defaults boolForKey:Flam4PngLinkStateKey]) {
		float aspectRatio = (pngAspectRatio == 0.0f) ? 4.0f/3.0f : pngAspectRatio; // check for zero aspectRatio
		int newHeight = (int)((float)width/aspectRatio);
		if ([pngHeightField integerValue] != newHeight) {
			[defaults setInteger:newHeight forKey:Flam4PngHeightKey];
			[pngHeightField setIntegerValue:newHeight];
			
		}
	}
	else { // update aspect ratio
		int height = [PreferenceController pngHeight];
		if (height != 0) 
			pngAspectRatio = (float)width/(float)height;
		else
			pngAspectRatio = 4.0f/3.0f;		
	}
}

- (IBAction)changePngHeight:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	int height = [pngHeightField integerValue];
	[defaults setInteger:height forKey:Flam4PngHeightKey];
	
	// set linked width field if applicable
	if ([defaults boolForKey:Flam4PngLinkStateKey]) {
		int newWidth = (int)((float)height * pngAspectRatio);
		if ([pngWidthField integerValue] != newWidth) {
			[defaults setInteger:newWidth forKey:Flam4PngWidthKey];
			[pngWidthField setIntegerValue:newWidth];
			
		}
	}
	else { // update aspect ratio
		int width = [PreferenceController pngWidth];
		if (height != 0) 
			pngAspectRatio = (float)width/(float)height;
		else
			pngAspectRatio = 4.0f/3.0f;		
	}
}

- (IBAction)changePngTransparency:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:([pngTransparentCB state] == NSOnState) forKey:Flam4PngTransparencyKey];
}

- (IBAction)changePrintQuality:(id)sender {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setInteger:[printQualityField integerValue] forKey:Flam4PrintQualityKey];
}



+ (NSColor *)bgViewColor {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSData *colorAsData = [defaults objectForKey:Flam4ViewBgColorKey];
	return [NSKeyedUnarchiver unarchiveObjectWithData:colorAsData];
}

+ (enum DimensionSource)previewDimensionSource {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PreviewDimensionSource];
}

+ (NSInteger)previewQuality {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PreviewQualityKey];
}

+ (NSInteger)previewWidth {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PreviewWidthKey];
}

+ (NSInteger)previewHeight {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PreviewHeightKey];
}

+ (BOOL)previewTransparency {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults boolForKey:Flam4PreviewTransparencyKey];
}

+ (NSUInteger)previewLinkState {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return (NSUInteger)[defaults integerForKey:Flam4PreviewLinkStateKey];
}

+ (NSInteger)pngQuality {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PngQualityKey];
}

+ (NSInteger)pngWidth {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PngWidthKey];
}

+ (NSInteger)pngHeight {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PngHeightKey];
}

+ (BOOL)pngTransparency {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults boolForKey:Flam4PngTransparencyKey];
}

+ (NSUInteger)pngLinkState {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return (NSUInteger)[defaults integerForKey:Flam4PngLinkStateKey];
}

+ (NSInteger)printQuality {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults integerForKey:Flam4PrintQualityKey];
}

- (void)textDidChange:(NSNotification *)aNotification {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if ([aNotification object] == previewWidthField) {
		int width = [previewWidthField integerValue];
		
		// set linked height field if applicable
		if ([defaults boolForKey:Flam4PreviewLinkStateKey]) {
			float aspectRatio = (previewAspectRatio == 0.0f) ? 4.0f/3.0f : previewAspectRatio; // check for zero aspectRatio
			int newHeight = (int)((float)width/aspectRatio);
			if ([previewHeightField integerValue] != newHeight)
				[previewHeightField setIntegerValue:newHeight];
		}
	}
	else if ([aNotification object] == previewHeightField) {
		int height = [previewHeightField integerValue];
		// set linked width field if applicable
		if ([defaults boolForKey:Flam4PreviewLinkStateKey]) {
			int newWidth = (int)((float)height*previewAspectRatio);
			if ([previewWidthField integerValue] != newWidth)
				[previewWidthField setIntegerValue:newWidth];
		}
	}
	else if ([aNotification object] == pngHeightField) {
		int width = [pngWidthField integerValue];
		
		// set linked height field if applicable
		if ([defaults boolForKey:Flam4PngLinkStateKey]) {
			float aspectRatio = (pngAspectRatio == 0.0f) ? 4.0f/3.0f : pngAspectRatio; // check for zero aspectRatio
			int newHeight = (int)((float)width/aspectRatio);
			if ([pngHeightField integerValue] != newHeight) {
				[pngHeightField setIntegerValue:newHeight];
			}
		}
	}
	else if ([aNotification object] == pngHeightField) {
		int height = [pngHeightField integerValue];
		
		// set linked width field if applicable
		if ([defaults boolForKey:Flam4PngLinkStateKey]) {
			int newWidth = (int)((float)height * pngAspectRatio);
			if ([pngWidthField integerValue] != newWidth) {
				[pngWidthField setIntegerValue:newWidth];
			}
		}
	}
}

- (void)windowDidLoad
{
	[window setLevel: NSModalPanelWindowLevel];

	[previewColorWell setColor:[PreferenceController bgViewColor]];
	[previewQualityField  setIntegerValue:[PreferenceController previewQuality]];
	[previewWidthField    setIntegerValue:[PreferenceController previewWidth]];
	[previewHeightField   setIntegerValue:[PreferenceController previewHeight]];
	[previewTransparentCB setState:[PreferenceController previewTransparency] ? NSOnState : NSOffState];
	
	[pngQualityField  setIntegerValue:[PreferenceController pngQuality]];
	[pngWidthField    setIntegerValue:[PreferenceController pngWidth]];
	[pngHeightField   setIntegerValue:[PreferenceController pngHeight]];
	[pngTransparentCB setState:[PreferenceController pngTransparency] ? NSOnState : NSOffState];
	
	[printQualityField  setIntegerValue:[PreferenceController printQuality]];

	// check for garbage height and if == 0, set aspect ratio to 4/3
	if ([PreferenceController previewHeight] != 0) 
		previewAspectRatio = ((float)[PreferenceController previewWidth])/((float)[PreferenceController previewHeight]);
	else
		previewAspectRatio = 4.0f/3.0f;
	if ([PreferenceController pngHeight] != 0) 
		pngAspectRatio = ((float)[PreferenceController pngWidth])/((float)[PreferenceController pngHeight]);
	else
		pngAspectRatio = 4.0f/3.0f;
	
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter]; // we want these 4 fields to respond to each keystroke when linked
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:previewWidthField];
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:previewHeightField];
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:pngWidthField];
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:pngHeightField];
}

@end
