//
//  FlameOriginalState.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/4/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FlameOriginalState : NSObject {
	NSSize  size;
	NSPoint center;
	float   rotation;
	float   scale;
}

- (id)initWithSize:(NSSize)_size center:(NSPoint)_center rotation:(float)_rotation scale:(float)_scale;

@property (assign) NSSize  size;
@property (assign) NSPoint center;
@property (assign) float   rotation;	
@property (assign) float   scale;

@end
