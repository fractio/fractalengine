/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */
#import <Cocoa/Cocoa.h>

enum eHotSpot { // crop rectangle hotspot
	none = 0,
	atLL = 1,
	atLR = 2,
	atUR = 3,
	atUL = 4,
	atS  = 5,
	atW  = 6,
	atE  = 7,
	atN  = 8
};

@interface FlamePreviewView : NSOpenGLView {
	NSBitmapImageRep *imageRep;			// current frame's image representation
	NSColor          *backgroundColor;  // color to paint when our image is smaller than the window
	NSRect            previousBounds;	// size of view from previous reshape() event
	BOOL              dragging;			// is a mouse rag operation in progress?
	NSPoint           dragStartPoint;   // point where drag started - in view coordinates
	NSPoint           currentDragPoint; // curent mouse location during drag
	NSTimer          *scrollTimer;      // timer for flushing accumulated scroll wheel amounts
	NSRect            normCropRect;     // crop rectangle - normalized coords from 0 to 1.f
	NSTrackingArea   *trackingArea;     // view tracking area so we know mouse exited window - needed during crop drags
	eHotSpot          selectedSpot;     // the current selected hot spot
	GLuint            textureName;      // the allocated texture's name
	BOOL              textureDirty;     // does the loaded texture need updating?
	float             cropAspectRatio;  // crop rectangle aspect ratio
	NSTimeInterval    requestTime;      // time that a rescroll zoom rerender was requested
	NSTimeInterval    lastScrollWheel;  // last scroll wheel event time
	float             accumScroll;      // accumulated scroll wheel amount
	NSMutableArray   *accumScrollArray; // accumulated scroll wheel amount since last rerender request per queued request
	float             accumBaseline;    // accumulated scroll wheel amount for last rendered image
	float             accumMagnify;     // accumulated magnify touch gesture amount
	float             magnifyBaseline;  // accumulated magnify gesture amount for last rendered image
	NSMutableArray   *accumMagnifyArray; // accumulated magnify touch gesture amount since last rerender request per queued request
	float             accumRotate;      // accumulated rotate touch gesture amount
	float             rotateBaseline;   // accumulated rotate gesture amount for last rendered image
	NSMutableArray   *accumRotateArray; // accumulated rotate touch gesture amount since last rerender request per queued request
}
@property (retain) NSBitmapImageRep *imageRep;
@property (retain) NSColor          *backgroundColor;
@property (assign) NSRect            previousBounds;
@property (assign) float             accumScroll;
@property (assign) NSRect            normCropRect;
@property (assign) eHotSpot          selectedSpot;
@property (assign) float             cropAspectRatio;

- (void)awakeFromNib;
- (NSRect)cropRect;
- (void) changeCropAspectRatio:(float)aspectRatio;

@end
