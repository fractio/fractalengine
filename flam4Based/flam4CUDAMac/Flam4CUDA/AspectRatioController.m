//
//  AspectRatioController.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "AspectRatioController.h"


@implementation AspectRatioController
@synthesize selectedMonitor;
@synthesize arMode;
@synthesize width;
@synthesize height;
@synthesize aspectRatio;
@synthesize orientation;

static float _aspectRatios[]  = {
	16.f/9.f,
	1.6f,
	1.6f,
	16.f/9.f,
	1.6f,
	4.f/3.f,
	1600.f/1024.f,
	1.6f,
	1280.f/1024.f,
	1.5f,
	4.f/3.f,
	1.5f,
	4.f/3.f,
	4.f/3.f,
	4.f/3.f,
	4.f/3.f,
};

static 	NSArray      *monitorSizes;
static 	NSDictionary *standardAspectRatios;
static 	NSArray      *flippedMonitorSizes;
static 	NSDictionary *flippedStandardAspectRatios;


+ (void)initialize
{
	monitorSizes = [[NSArray arrayWithObjects:
					 @"2560x1440 : 16x9", 
					 @"2048x1280 : 16x10", 
					 @"1920x1200 : 16x10", 
					 @"1920x1080 : 16x9", 
					 @"1680x1050 : 16x10", 
					 @"1600x1200 :  4x3", 
					 @"1600x1024 : 25x16",
					 @"1440x900  : 16x10", 
					 @"1280x1024 :  5x4",
					 @"1280x854  :  3x2",
					 @"1152x870  :  4x3",
					 @"1152x768  :  3x2",
					 @"1024x768  :  4x3",
					 @"832x624   :  4x3",
					 @"800x600   :  4x3",
					 @"640x480   :  4x3",
					 nil] retain];
	
	standardAspectRatios = [[NSDictionary dictionaryWithObjectsAndKeys:
							 @"1x1",
							 [NSNumber numberWithFloat:1.f],
							 @"16x10",
							 [NSNumber numberWithFloat:1.6f],
							 @"16x9",
							 [NSNumber numberWithFloat:16.f/9.f],
							 @"5x4",
							 [NSNumber numberWithFloat:1.25f],
							 @"4x3",
							 [NSNumber numberWithFloat:4.f/3.f],
							 @"3x2",
							 [NSNumber numberWithFloat:1.5f],
							 @"25x16",
							 [NSNumber numberWithFloat:1.5625f],
							 nil] retain];
	flippedMonitorSizes = [[NSArray arrayWithObjects:
					 @"2560x1440 : 9x16", 
					 @"2048x1280 : 10x16", 
					 @"1920x1200 : 10x16", 
					 @"1920x1080 : 9x16", 
					 @"1680x1050 : 10x16", 
					 @"1600x1200 :  4x3", 
					 @"1600x1024 : 25x16",
					 @"1440x900  : 10x16", 
					 @"1280x1024 :  4x5",
					 @"1280x854  :  2x3",
					 @"1152x870  :  3x4",
					 @"1152x768  :  2x3",
					 @"1024x768  :  3x4",
					 @"832x624   :  3x4",
					 @"800x600   :  3x4",
					 @"640x480   :  3x4",
					 nil] retain];
	
	flippedStandardAspectRatios = [[NSDictionary dictionaryWithObjectsAndKeys:
							 @"1x1",
							 [NSNumber numberWithFloat:1.f],
							 @"10x16",
							 [NSNumber numberWithFloat:1.f/1.6f],
							 @"9x16",
							 [NSNumber numberWithFloat:9.f/16.f],
							 @"4x5",
							 [NSNumber numberWithFloat:1.f/1.25f],
							 @"3x4",
							 [NSNumber numberWithFloat:3.f/4.f],
							 @"2x3",
							 [NSNumber numberWithFloat:1.f/1.5f],
							 @"16x25",
							 [NSNumber numberWithFloat:1.f/1.5625f],
							 nil] retain];
}

- (id)initWithOrientation:(NSInteger)_orientation
{
	self = [super initWithWindowNibName:@"AspectRatioSelect"];
	
	orientation       = _orientation;
	aspectRatios      = _aspectRatios;
	arMode            = StandardSizes;
	NSRect screenRect = [[NSScreen mainScreen] frame];
	width             = [[NSNumber numberWithFloat:screenRect.size.width] retain];
	height            = [[NSNumber numberWithFloat:screenRect.size.height] retain];
	aspectRatio       = [[NSNumber numberWithFloat:screenRect.size.width/screenRect.size.height] retain];
	
	selectedMonitor   = 0;
    return self;
}

- (void)windowDidLoad {
	[self setSelectedMonitor:0];
	NSString *arLabel = [NSString stringWithFormat:@"%ux%u", width, height];
	int i = 0;
	for (NSString *size in [self getMonitorSizes]) {
		if ([size hasPrefix:arLabel]) {
			[self setSelectedMonitor:i];
			break;
		}
		i++;
	}	
}

- (void)dealloc
{
	[width release];
	[height release];
	[aspectRatio release];
	[super dealloc];
}

- (NSArray *)getMonitorSizes {
	if (orientation == horizontalCrop)
		return monitorSizes;
	else
		return flippedMonitorSizes;
}

// create a pretty floating point value string by deleting trailing zeroes and "."
+ (NSString*)prettyFloat:(float)aValue
{
	NSString*	theString = [NSString stringWithFormat:@"A%0.4lf", aValue];
	
	theString = [theString stringByTrimmingCharactersInSet:
				 [NSCharacterSet characterSetWithCharactersInString:@"0"]];
	
	theString = [theString stringByTrimmingCharactersInSet:
				 [NSCharacterSet characterSetWithCharactersInString:@"."]];
	return [theString substringWithRange:NSMakeRange(1, [theString length]-1)];
}

// convert the numeric aspect ratio to first either one of the standard aspect ratio strings or to just the aspect ratio
+ (NSString *)stringForAspectRatio:(NSNumber *)_aspectRatio orientation:(NSNumber *)_orientation {
	enum CropOrientation o = [_orientation intValue];
	NSDictionary *ratios = o == horizontalCrop ? standardAspectRatios : flippedStandardAspectRatios;
	NSString *str = [ratios objectForKey:_aspectRatio];
	if (str == nil) {
		return [NSString stringWithFormat:@"%@", [AspectRatioController prettyFloat:[_aspectRatio floatValue]]];
	}
	return str;
}

// get the aspect ratio in string label form
- (NSString *)aspectRatioAsString {
	return [AspectRatioController stringForAspectRatio:aspectRatio orientation:[NSNumber numberWithInt:orientation]];
}

// get aspect ratio from a string with syntax "widthxheight" i.e. "800x600"
+ (float)arFromWidthXHeight:(NSString *)label {
	NSRange range;
	if ([label rangeOfString:@"x"].location != NSNotFound)
		range = [label rangeOfString:@"x"];
	else if ([label rangeOfString:@"X"].location != NSNotFound)
		range = [label rangeOfString:@"X"];
	else
		return 0.0f;
	
	NSString *part1 = [label substringToIndex:range.location];
	NSString *part2 = [label substringToIndex:range.location + 1];
	return [part1 floatValue] / [part2 floatValue];
}

// convert the aspect ratio label to the aspect ratio float value
+ (float)aspectRatioForString:(NSString *)label  orientation:(NSNumber *)_orientation {
	enum CropOrientation o = [_orientation intValue];
	NSDictionary *ratios = o == horizontalCrop ? standardAspectRatios : flippedStandardAspectRatios;
	NSArray *keys = [ratios allKeysForObject:label];
	if (keys && [keys count] > 0)
		return [[keys objectAtIndex:0] floatValue];
	
	float value = [self arFromWidthXHeight:label];
	if (value != 0.0f)
		return value;
	return [label floatValue];
}


- (IBAction)ok:(id)sender {
	[widthField validateEditing];
	[heightField validateEditing];
	[aspectRatioField validateEditing];
	// get the values of the Controls here, as any final typing changes made to them might not have been captured via keypair messaging
	width       = [[NSNumber numberWithFloat:[widthField floatValue]] retain];
	height      = [[NSNumber numberWithFloat:[heightField floatValue]] retain];
	aspectRatio = [[NSNumber numberWithFloat:[aspectRatioField floatValue]] retain];

	switch (arMode) {
		case StandardSizes:
			{
			float _ar = orientation == horizontalCrop ? aspectRatios[selectedMonitor] : 1.f/aspectRatios[selectedMonitor];
			aspectRatio = [[NSNumber numberWithFloat:_ar] retain];
			}
			break;
		case CustomSize:
			if (width == nil || [width intValue] == 0 || height == nil)
				aspectRatio = [[NSNumber numberWithFloat:1.f] retain];
			else
				aspectRatio = [[NSNumber numberWithFloat:[width floatValue]/[height floatValue]] retain];
			break;
		case CustomAspectRatio:
			if (aspectRatio == nil)
				aspectRatio = [[NSNumber numberWithFloat:1.f] retain];
			break;
	}
	
	[NSApp stopModal];
	[[self window] close];
}

// Cancel button handler
- (IBAction)cancel:(id)sender
{	
	[NSApp abortModal];
	[[self window] close];
}


@end
