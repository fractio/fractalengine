//
//  ImageUtil.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ImageUtil : NSObject {

}
+ (NSBitmapImageRep *) imageRepFromData:(NSData*)myData hasAlpha:(BOOL)hasAlpha rect:(NSRect)rect flip:(BOOL)flip;

+ (void)exportImageToPNG:(NSURL *)fileNameStr 
					 rep:(NSBitmapImageRep *)rep
				useAlpha:(BOOL)useAlpha
					rect:(NSRect)rect;

+ (void)exportImageToPNG:(NSURL *)url 
					data:(NSData *)data 
				useAlpha:(BOOL)useAlpha
					rect:(NSRect)rect;
@end
