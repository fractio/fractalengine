//
//  Transformers.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/10/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NotCropEditModeTransformer : NSValueTransformer {
}
@end

@interface NotCropResizeModeTransformer : NSValueTransformer {
}
@end

@interface NotRotateEditModeTransformer : NSValueTransformer {
}
@end

@interface NotScaleEditModeTransformer : NSValueTransformer {
}
@end

@interface NotRotateScaleEditModeTransformer : NSValueTransformer {
}
@end

