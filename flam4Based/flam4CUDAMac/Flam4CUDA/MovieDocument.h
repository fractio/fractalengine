/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <QTKit/QTKit.h>
#import <Cocoa/Cocoa.h>
#import <QuickTime/QuickTime.h>

#import "MovieWindowController.h"

@interface MovieDocument : NSDocument
{
	QTMovie               *mMovie;
	MovieWindowController *mWinController;
	DataHandler            mDataHandlerRef;
	NSSize				   contentSize;
}

- (id)init;
- (QTMovie *)docMovie;
- (void)setMovie:(QTMovie *)movie;
- (NSString *)windowNibName;
- (BOOL)panel:(id)sender shouldShowFilename:(NSString *)filename ;
- (void)openDocument:(id)sender ;
- (id)openDocumentWithContentsOfURL:(NSURL *)aDocumentURL display:(BOOL)displayFlag;
- (BOOL)readFromURL:(NSURL *)url ofType:(NSString *)type error:(NSError **)outError;
- (BOOL)application:(NSApplication *)theApplication openFile:(NSString *)filename;
- (void)makeWindowControllers;
- (BOOL)validateMenuItem:(NSMenuItem *)item ;
- (Movie)quicktimeMovieFromTempFile:(DataHandler *)outDataHandler error:(OSErr *)outErr;
-(QTMovie *)buildQTKitMovie;

- (NSSize)contentSize;
- (void)setContentSize:(NSSize)size;

@property (retain) MovieWindowController *mWinController;

@end
