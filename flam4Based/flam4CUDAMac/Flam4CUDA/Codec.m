/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "Codec.h"
#import <QuickTime/ImageCompression.h>
#import <QTKit/QTUtilities.h>


@implementation Codec
@synthesize osType;
@synthesize name;
@synthesize details;

- (id)init
{
	self = [super init];
	if (self) {
		osType      = nil;
		name        = nil;
		details = nil;
	}
	return self;
}

- (void)dealloc
{
	[osType release];
	[name release];
	[details release];
	[super dealloc];
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"<Codec: osType=%@ name=\"%@\" details=\"%@\">", osType, name, details];
}

+ (Codec *)codecWithCodecNameSpec:(CodecNameSpec *)spec
{
	Codec *info = [[Codec alloc] init];
	[info setOsType:QTStringForOSType(spec->cType)];
	
	unsigned char* _typeName = spec->typeName;
	[info setName:[[NSString alloc] initWithBytes:&_typeName[1] length:(unsigned)_typeName[0] encoding:[NSString defaultCStringEncoding]]];
	
	unsigned char * _name = (unsigned char *)*(spec->name);
	[info setDetails:[[NSString alloc] initWithBytes:&_name[1] length:(unsigned)_name[0] encoding:[NSString defaultCStringEncoding]]];
	return [info autorelease];
}

// return an informational array of Codec objects populated with all Codecs on the system
+ (NSArray *)getCodecs {
	CodecNameSpecListPtr list;
	OSErr err = GetCodecNameList(&list, 1);
	
	if (!err) {
		CodecNameSpec *l = list->list;
		NSMutableArray *array = [NSMutableArray arrayWithCapacity:list->count];
		for (int i = 0; i < list->count; i++) {
			Codec *info = [Codec codecWithCodecNameSpec:l];
			[array addObject:info];
			l = l++;
		}
		DisposeCodecNameList(list);
		return array;
	}
	return nil;
}

@end
