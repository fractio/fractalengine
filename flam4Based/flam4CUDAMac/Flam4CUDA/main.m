//
//  main.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 11/11/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
