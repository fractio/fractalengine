//
//  WebhelpController.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/20/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>


@interface WebhelpController : NSWindowController {
	NSURL            *helpURL;
	IBOutlet WebView *webView;
}

@property (retain) NSURL *helpURL;

- (id)initWithHelpResource:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir;
@end
