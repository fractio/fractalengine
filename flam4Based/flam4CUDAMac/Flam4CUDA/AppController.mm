/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cuda_runtime_api.h>
#include <limits.h>
#import "AppController.h"
#import "FlameAnimation.h"
#import "PreferenceController.h"
#import "MakeMovieWindowController.h"
#import "InchesToPointsTransformer.h"
#import "PreviewController.h"
#import "FlameTileArray.h"
#import "FlameTile.h"
#import "Transformers.h"
#import "HelpController.h"

#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#define NSFoundationVersionNumber10_5 677
#endif

#if defined(_WIN32)
#define DLLIMPORT __declspec(dllimport)
#elif defined(__APPLE__)
#define DLLIMPORT
#endif

extern "C" DLLIMPORT int cuCudaDeviceCount();
extern "C" DLLIMPORT int cuCudaDeviceProps(int deviceNumber, struct cudaDeviceProp *prop);
extern "C" DLLIMPORT bool cuMemCheck(unsigned deviceNumber, unsigned area, unsigned numColors, unsigned *amountAlloced);

NSString * const Flam4CumRenderSeconds   = @"CumRenderSeconds";
NSString * const Flam4CumBatches         = @"CumBatches";
NSString * const Flam4PngDirectory		 = @"PngDirectory";
NSString * const Flam4PngBatchDirectory  = @"PngBatchDirectory";
NSString * const Flam4MovieDirectory	 = @"MovieDirectory";


extern bool renderingActive;
extern volatile bool isRendering;
float    reductionFactor; // amount memory is reduced each pass for GPU memory consumption test


@implementation AppController

- (id)init
{
    self = [super init];
    if (!self)
		return nil;
	showRenderSteps  = false;
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setBool:YES forKey:@"NSDisabledCharacterPaletteMenuItem"]; // we dont want the automatic "Special Characters" menu item

	cumRenderSeconds = (double)[defaults doubleForKey:Flam4CumRenderSeconds];
	cumBatches       = (double)[defaults doubleForKey:Flam4CumBatches];
	reductionFactor = 0.8f;
    return self;
}

+ (void)initialize
{
	InchesToPointsTransformer    *iToPTransformer     = [[[InchesToPointsTransformer alloc] init] autorelease];
	NotCropEditModeTransformer   *cropEMTransformer   = [[[NotCropEditModeTransformer alloc] init] autorelease];
	NotRotateEditModeTransformer *rotateEMTransformer = [[[NotRotateEditModeTransformer alloc] init] autorelease];
	NotScaleEditModeTransformer  *scaleEMTransformer  = [[[NotScaleEditModeTransformer alloc] init] autorelease];
	NotRotateScaleEditModeTransformer  *rotScaleEMTransformer
		= [[[NotRotateScaleEditModeTransformer alloc] init] autorelease];
	
	
	[NSValueTransformer setValueTransformer:iToPTransformer
									forName:@"InchesToPointsTransformer"];
	[NSValueTransformer setValueTransformer:cropEMTransformer
									forName:@"NotCropEditModeTransformer"];
	[NSValueTransformer setValueTransformer:rotateEMTransformer
									forName:@"NotRotateEditModeTransformer"];
	[NSValueTransformer setValueTransformer:scaleEMTransformer
									forName:@"NotScaleEditModeTransformer"];
	[NSValueTransformer setValueTransformer:rotScaleEMTransformer
									forName:@"NotRotateScaleEditModeTransformer"];
	
	
	NSMutableDictionary *defaultValues = [NSMutableDictionary dictionary];
	// archive the color object
	NSData *colorAsData = [NSKeyedArchiver archivedDataWithRootObject:[NSColor whiteColor]];
	
	// put defaults into dictionary
	[defaultValues setObject:colorAsData forKey:Flam4ViewBgColorKey];
	[defaultValues setObject:[NSNumber numberWithDouble:0]  forKey:Flam4CumRenderSeconds];
	[defaultValues setObject:[NSNumber numberWithDouble:0]  forKey:Flam4CumBatches];
	
	[defaultValues setObject:[NSNumber numberWithInt:OverrideDimensionsKeepAspectRatio] forKey:Flam4PreviewDimensionSource];
	[defaultValues setObject:[NSNumber numberWithInt:55]  forKey:Flam4PreviewQualityKey];
	[defaultValues setObject:[NSNumber numberWithInt:400] forKey:Flam4PreviewWidthKey];
	[defaultValues setObject:[NSNumber numberWithInt:300] forKey:Flam4PreviewHeightKey];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:Flam4PreviewTransparencyKey];
	[defaultValues setObject:[NSNumber numberWithInt:NSOnState]  forKey:Flam4PreviewLinkStateKey];
	
	[defaultValues setObject:[NSNumber numberWithInt:300] forKey:Flam4PngQualityKey];
	[defaultValues setObject:[NSNumber numberWithInt:800] forKey:Flam4PngWidthKey];
	[defaultValues setObject:[NSNumber numberWithInt:600] forKey:Flam4PngHeightKey];
	[defaultValues setObject:[NSNumber numberWithBool:NO] forKey:Flam4PngTransparencyKey];
	[defaultValues setObject:[NSNumber numberWithInt:NSOnState]  forKey:Flam4PngLinkStateKey];
	
	[defaultValues setObject:[NSNumber numberWithInt:2000] forKey:Flam4PrintQualityKey];
	
	if (floor(NSFoundationVersionNumber) == NSFoundationVersionNumber10_5) {  // Leopard
		NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES) objectAtIndex:0];
		//NSURL *documentURL     = [NSURL fileURLWithPath:documentPath isDirectory:YES];
		[defaultValues setObject:documentPath forKey:Flam4PngDirectory];
		[defaultValues setObject:documentPath forKey:Flam4PngBatchDirectory];
		[defaultValues setObject:documentPath forKey:Flam4MovieDirectory];		
	}
	else { // Snow Leopard and beyond
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		NSString *picturesPath = [NSSearchPathForDirectoriesInDomains(NSPicturesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
		NSString *moviesPath   = [NSSearchPathForDirectoriesInDomains(NSMoviesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
		//NSURL *picturesURL     = [NSURL fileURLWithPath:picturesPath isDirectory:YES];
		//NSURL *moviesURL       = [NSURL fileURLWithPath:moviesPath isDirectory:YES];
		[defaultValues setObject:picturesPath forKey:Flam4PngDirectory];
		[defaultValues setObject:picturesPath forKey:Flam4PngBatchDirectory];
		[defaultValues setObject:moviesPath forKey:Flam4MovieDirectory];		
#endif
	}
	// turn off Magic mouse momentum scrolling - screws up mouse zooming
	//[defaultValues setObject:@"NO" forKey:@"AppleMomentumScrollSupported"];
	
	// Register the dictionary of defaults
	[[NSUserDefaults standardUserDefaults] registerDefaults:defaultValues];
}

// validate main menu items
- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
	extern volatile bool isRendering;
	SEL theAction = [anItem action];
	
	NSDocument *doc = [[NSDocumentController sharedDocumentController] currentDocument];
	NSString * fileType = [doc fileType];
		
	if (! fileType || [fileType isEqualToString:@"public.png"]) {
		if (theAction == @selector(openOptionsPanel:))
			return NO;
		else if (theAction == @selector(openPngSettingsPanel:))
			return NO;
		else if (theAction == @selector(startRender:))
			return NO;
		else if (theAction == @selector(openPngBatchSettingsPanel:))
			return NO;
		else if (theAction == @selector(openTextEdit:))
			return NO;
	}
	else if ([fileType isEqualToString:@"org.electricsheep.flame"]) {
		if (theAction == @selector(openOptionsPanel:))
			return isRendering ? NO :(doc ? YES : NO);
		else if (theAction == @selector(openPngSettingsPanel:))
			return isRendering ? NO :(doc ? YES : NO);
		else if (theAction == @selector(startRender:))
			return doc ? YES : NO;
		else if (theAction == @selector(openPngBatchSettingsPanel:))
			return doc ? YES : NO;
		else if (theAction == @selector(openTextEdit:))
			return doc ? YES : NO;
	}
	else if (! fileType || [fileType isEqualToString:@"com.apple.quicktime-movie"]) {
		if (theAction == @selector(openOptionsPanel:))
			return NO;
		else if (theAction == @selector(openPngSettingsPanel:))
			return NO;
		else if (theAction == @selector(startRender:))
			return NO;
		else if (theAction == @selector(openPngBatchSettingsPanel:))
			return NO;
		else if (theAction == @selector(openTextEdit:))
			return NO;
	}
	return (BOOL)[[NSDocumentController sharedDocumentController] validateUserInterfaceItem:anItem];
}

- (void)warnStoppingAnimation:(NSString *)s
{
	NSAlert *a = [NSAlert alertWithMessageText:nil
								 defaultButton:nil
							   alternateButton:nil
								   otherButton:nil
					 informativeTextWithFormat:@"Stopping fractal animation for better performance of %@", s];
	renderingActive = false;
	[a runModal];
}

// open the PNG settings panel
- (IBAction)openPngSettingsPanel:(id)sender
{
	if (isRendering)
		return;
	renderingActive = false;
	
	PngSettingsController *pngSettingsCtrl = [[PngSettingsController alloc] init];
	[pngSettingsCtrl window]; // be sure we have a window instance before we reset it
	[pngSettingsCtrl reset];
	[pngSettingsCtrl showWindow:self];
	[pngSettingsCtrl release];
}

// open the PNG Batch settings panel
- (IBAction)openPngBatchSettingsPanel:(id)sender
{
	if (isRendering)
		return;
	if (renderingActive)
		[self warnStoppingAnimation:@"PNG batch generation"];
	
	PngBatchSettingsController *pngBatchSettingsCtrl = [[PngBatchSettingsController alloc] init];
	[pngBatchSettingsCtrl window]; // be sure we have a window instance before we reset it
	[pngBatchSettingsCtrl reset];
	[pngBatchSettingsCtrl showWindow:self];
	[pngBatchSettingsCtrl release];
}

// open the render options panel
- (IBAction)openOptionsPanel:(id)sender
{
	OptionsController *optionsCtrl = [[OptionsController alloc] init];
	NSWindow * win = [optionsCtrl window];
	[win setLevel: NSModalPanelWindowLevel];
	[win makeKeyAndOrderFront:sender];
}

// open the render options panel
- (IBAction)openMakeMoviePanel:(id)sender
{
	if (renderingActive)
		[NSApp warnStoppingAnimation:@"Make a movie"];
	MakeMovieWindowController *ctrl = [[MakeMovieWindowController alloc] init];
	NSWindow * win = [ctrl window];
	[win setLevel: NSModalPanelWindowLevel];
	[win makeKeyAndOrderFront:sender];
}

// open the PNG settings panel
- (IBAction)openTextEdit:(id)sender
{
	FlameDocument *doc = [[NSDocumentController sharedDocumentController] currentDocument];
	NSURL *url = [doc fileURL];
	
	NSTask *task = [[NSTask alloc] init];
	[task setLaunchPath:@"/Applications/TextEdit.app/Contents/MacOS/TextEdit"];
	NSArray *array = [NSArray arrayWithObjects:[url path], nil];
	[task setArguments:array];
	[task launch];
	[task release];
}

// handle the Run ->Animate menu item state
- (IBAction)startRender:(id)sender
{
	if ([sender state] == NSOffState) {
		[sender setOnStateImage:nil];
		[sender setState:NSOnState];
		[sender setTitle:@"Pause Animation"];
	}
	else {
		[sender setState:NSOffState];
		[sender setTitle:@"Animate!"];
	}
	
	renderingActive = !renderingActive;
}

- (IBAction)openFlameFile:(id)sender
{
	NSArray *fileTypes = [NSArray arrayWithObjects:@"flame", @"flam3", @"flam4", nil];
	NSOpenPanel *oPanel = [NSOpenPanel openPanel];
	NSXMLDocument *xmlDoc;
	
	[oPanel setAllowsMultipleSelection:NO];
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[oPanel setDirectory:[[NSURL fileURLWithPath:NSHomeDirectory()] path]];
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		[oPanel setDirectoryURL:[NSURL fileURLWithPath:NSHomeDirectory()]];		
#endif
	}
	[oPanel setAllowedFileTypes:fileTypes];
	
	int result = [oPanel runModal];	
	if (result == NSOKButton) {
		NSArray *urlsToOpen = [oPanel URLs];
		NSURL *url = [urlsToOpen objectAtIndex:0];
		NSError *error = nil;
		xmlDoc = [[NSXMLDocument alloc] initWithContentsOfURL:url options:0 error:&error]; 
		
		if (xmlDoc == nil && error) {
		}
	}
}

// color code for errors
- (void)setLogErrorState:(BOOL)signalError {
	if (signalError) {
		NSColor * backgroundColor = [NSColor colorWithCalibratedRed:1.0f green:102.f/255.f blue:153.f/255.f alpha:1.0f]; // light red
		[logView setBackgroundColor:backgroundColor];
		[logResetBtn setHidden:NO];
	}
	else {
		[logView setBackgroundColor:[NSColor whiteColor]];
		[logResetBtn setHidden:YES];
	}
	[logView setNeedsDisplay:YES];
}

// append a error text message to the Flam4 Log - sender should be the text to be logged
- (IBAction)logError:(id)sender {
	[self logInfo:sender];
	[self setLogErrorState:YES];
}

// append a informational text message to the Flam4 Log - sender should be the text to be logged
- (IBAction)logInfo:(id)sender {
	NSString *text = sender;
	NSMutableString *s = [NSMutableString stringWithString:text];
	[s appendString:@"\n"];
	NSRange endRange;
	
	endRange.location = [[logView textStorage] length];
	endRange.length = 0;
	
	// we can call this from a background thread, so we must do this to avoid potential deadlock
	[logView lockFocus];
	[logView replaceCharactersInRange:endRange withString:s];
	endRange.length = [s length];
	[logView scrollRangeToVisible:endRange];
	[logView unlockFocus];
}

- (IBAction)logRenderSteps:(id)sender {
	if (showRenderSteps)
		[self logInfo:sender];
}

// handle the Run ->Animate menu item state
- (IBAction)showRenderSteps:(id)sender
{
	if ([sender state] == NSOffState) {
		showRenderSteps = YES;
		[sender setState:NSOnState];
	}
	else {
		showRenderSteps = NO;
		[sender setState:NSOffState];
	}
}

// reset the log's background colort to signal normal state
- (IBAction)resetLogColor:(id)sender
{
	[self setLogErrorState:NO];
}

// say no to suppress opening an empty QT movie document & window
- (BOOL)applicationShouldOpenUntitledFile:(NSApplication *)sender
{
	return NO;
}

- (IBAction)showPreferencePanel:(id)sender
{
	if (!preferenceController) {
		preferenceController = [[PreferenceController alloc] init];
		// we want to observe preview Quality changes
		[preferenceController addObserver:self forKeyPath:@"prevQuality" 
								  options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
	}
	[preferenceController showWindow:self];
}

+ (void)tileSizeForArea:(float)area x:(float *)xOptDim y:(float *)yOptDim aspectRatio:(float)aspectRatio
{
	// area = x * x * aspectRatio
	*xOptDim = floorf(sqrtf(area/aspectRatio));
	*yOptDim = floorf(*xOptDim/aspectRatio);
}

// return the largest tile size for this GPU
+ (BOOL)deviceAllocCheck:(int)deviceNum area:(float *)area amount:(unsigned *)amountAlloced
{
	// variables used to find the largest area we can support
	int numColors     = 256;
	BOOL success      = NO;
	while (!success) {
		if (!(success = cuMemCheck(deviceNum, *area, numColors, amountAlloced))) {
			*area = floorf(*area * reductionFactor);
			if (*area <= 1.0f) // the smallest possible tile size!
				return NO;
		}
	}
	return success;
}

// return the largest tile size for this GPU
+ (BOOL)deviceAllocCheck:(int)deviceNum x:(float *)xOptDim y:(float *)yOptDim amount:(unsigned *)amountAlloced
{
	// variables used to find the largest area we can support
	int numColors     = 256;
	float aspectRatio = *xOptDim / *yOptDim;
	BOOL success      = NO;
	while (!success) {
		float xDim = *xOptDim;
		if (!(success = cuMemCheck(deviceNum, xDim * *yOptDim, numColors, amountAlloced))) {
			*xOptDim = floorf(xDim * sqrtf(reductionFactor));
			*yOptDim = floorf(xDim / aspectRatio * sqrtf(reductionFactor));
			if (*xOptDim <= 1.0f || *yOptDim <= 1.0f) // the smallest possible tile size for fixed aspect ratio!
				return NO;
		}
	}
	return success;
}

// return the largest tile size for all GPUs
+ (BOOL)allDevicesAllocCheck:(unsigned)numDevices area:(float *)area amount:(unsigned *)amountAlloced niceFactor:(float)niceFactor
{
	BOOL success = NO;
	for (int i = 0; i < numDevices; i++) {
		success = [self deviceAllocCheck:i area:area amount:amountAlloced];
		if (!success)
			return NO;
	}
	*area = floorf(niceFactor * *area);
	return success;
}

// return the largest tile size for all GPUs
+ (BOOL)allDevicesAllocCheck:(unsigned)numDevices x:(float *)xOptDim y:(float *)yOptDim amount:(unsigned *)amountAlloced niceFactor:(float)niceFactor
{
	BOOL success = NO;
	for (int i = 0; i < numDevices; i++) {
		success = [self deviceAllocCheck:i x:xOptDim y:yOptDim amount:amountAlloced];
		if (!success)
			return NO;
	}
	*xOptDim = floorf(niceFactor * *xOptDim);
	*yOptDim = floorf(niceFactor * *yOptDim);
	return success;
}

// report the current largest tile size (can change every call)
+ (NSString *)getAllocSummary:(BOOL)success x:(float)xOptDim y:(float)yOptDim amount:(unsigned)amountAlloced
{
	if (success) {
		// format the max allocated amount
		NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
		[numberFormatter setHasThousandSeparators:YES];
		[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
		NSString *str = [numberFormatter stringFromNumber:[NSNumber numberWithInt:amountAlloced]];
		[numberFormatter release];
		
		return [NSString stringWithFormat:@"Current largest tile size:[%0.fX%0.f] Device Memory Available: %@ bytes",
				xOptDim, yOptDim, str];
	}
	return @"Unable to allocate enough memory for any tile size";	
}

// report the current largest tile size (can change every call)
+ (NSString *)getAllocSummary:(BOOL)success area:(float)area amount:(unsigned)amountAlloced
{
	float x = sqrtf(1.333333333f*area);
	float y = x/1.3333333f;
	return [AppController getAllocSummary:success x:x y:y amount:amountAlloced];
}

// report the current largest tile size (can change every call)
+ (NSString *)deviceAllocSummary:(unsigned)numDevices x:(float *)xOptDim y:(float *)yOptDim amount:(unsigned *)amountAlloced  niceFactor:(float)niceFactor
{
	BOOL success = [AppController allDevicesAllocCheck:numDevices x:xOptDim y:yOptDim amount:amountAlloced niceFactor:niceFactor];
	return [AppController getAllocSummary:success x:*xOptDim y:*yOptDim amount:*amountAlloced];
}

+ (BOOL)checkGPUMemAvailable:(unsigned *)memoryAvailable area:(float *)area niceFactor:(float)niceFactor
{
	unsigned deviceCount = cuCudaDeviceCount();
	struct cudaDeviceProp deviceProperties;
	unsigned maxAmount = UINT_MAX;
	float    maxArea   = float(UINT_MAX);
	bool     success   = YES;
	for (int i = 0; i < deviceCount; i++) {
		cuCudaDeviceProps(i, &deviceProperties);
		float mbTotalGlobal = deviceProperties.totalGlobalMem;
		// BOOL canMapHostMemory = deviceProperties.canMapHostMemory;
		float areaGlobal    = floorf(mbTotalGlobal/3.f); // a reasonable starting point just grab everything assuming RGB 3 bytes per pixel
		
		unsigned amountAlloced;
		success = [AppController allDevicesAllocCheck:deviceCount area:&areaGlobal amount:&amountAlloced niceFactor:niceFactor];
		
		maxArea   = (areaGlobal <= maxArea)      ? areaGlobal : maxArea;
		maxAmount = (amountAlloced <= maxAmount) ? amountAlloced : maxAmount;
	}
	*memoryAvailable = maxAmount;
	*area            = maxArea;
	return success;
}

+ (BOOL)checkGPUMemAvailable:(unsigned *)memAlloced w:(float *)w h:(float *)h niceFactor:(float)niceFactor
{
	float area;
	bool success = [AppController checkGPUMemAvailable:memAlloced area:&area niceFactor:niceFactor];
	*w = sqrtf(area/1.3333333f);
	*h = *w/1.33333333f;
	
	return success;
}

// allow overrides of the pass area reduction factor for higher accuracy at cost of more iterations
+ (BOOL)checkGPUMemAvailable:(unsigned *)memoryAvailable area:(float *)area niceFactor:(float)niceFactor reductionFactor:(float)factor
{
	float originalFactor = reductionFactor;
	reductionFactor = factor;
	BOOL success = [AppController checkGPUMemAvailable:memoryAvailable area:area niceFactor:niceFactor];
	reductionFactor = originalFactor;
	return success;
}

// determine how to split the render area into tiles
+ (FlameTileArray *)splitAreaIntoTiles:(float)area width:(float)width height:(float)height flame:(Flame *)flame flipped:(BOOL)flipped
{
	float maxArea   = area;
	float minHeight = 100.f;
	float minWidth  = 100.f;
	float overlap   = [FlameTile overlap];
	unsigned amount;
	BOOL success = [self checkGPUMemAvailable:&amount area:&maxArea niceFactor:0.8f];
	
	if (!success) {
		[[NSApp delegate] logInfo:@"Unable to allocate enough memory for any tile size"];
		return nil;
	}
	
	int rows = 1;
	int cols = 1;
	if (width >= height) { // horizontal tiling
		if (maxArea >= width * height || maxArea/height >= minWidth + 2*overlap) { // 1 row is enough
			cols = ceilf(area/maxArea);
			rows = 1;
		}
		else if (height >= minHeight + 2*overlap) {  // too narrow case - split height by multiples of minHeight
			int minTiles = ceilf(area/(maxArea));
			
			rows = ceilf((minWidth + 2*overlap)*height/maxArea); // calc number of rows needed to keep size reasonable
			cols = ceilf(minTiles/rows);
			float effHeight = height + 2 * overlap * (rows - 1);
			float effWidth  = width  + 2 * overlap * (cols - 1);
			minTiles = ceilf(effWidth * effHeight/maxArea);
			cols = ceilf(minTiles/rows);
		}
		else {			
			cols = ceilf(width/(minWidth + 2*overlap));
			rows = 1;
		}
	}
	else  {		// vertical tiling  (width < height)
		if (maxArea >= width * height || maxArea/width >= minHeight + 2*overlap) {
			cols = 1;
			rows = ceilf(area/maxArea);	
		}
		else if (width >= minWidth + 2*overlap) {  // too short case - split width by multiples of minWidth
			int minTiles = ceilf(area/maxArea);
			
			cols = ceilf((minHeight + 2*overlap)*width/maxArea); // calc number of cols needed to keep size reasonable
			rows = ceilf(minTiles/cols);
			float effHeight = height + 2 * overlap * (rows - 1);
			float effWidth  = width  + 2 * overlap * (cols - 1);
			minTiles = ceilf(effWidth * effHeight/maxArea);
			rows = ceilf(minTiles/cols);
		}
		else {			
			cols = 1;
			rows = ceilf(height/(minHeight + 2*overlap));
		}
	}
	//[self logInfo:[NSString stringWithFormat:@"[%0.f X %0.f] %u tile(s) needed", width, height, rows*cols]];
	
	return [FlameTileArray arrayWithRows:rows cols:cols flame:flame width:width height:height flipped:flipped];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	[[logView window] orderBack:nil];
	
	[self logInfo:@"Welcome to Flam4 - Mac Edition!\n"];
	
	// print out the attached CUDA devices
	unsigned deviceCount = cuCudaDeviceCount();
	struct cudaDeviceProp deviceProperties;
	if (deviceCount == 1)
		[self logInfo:[NSString stringWithFormat:@"%d CUDA Device on your Mac:", deviceCount]];
	else
		[self logInfo:[NSString stringWithFormat:@"%d CUDA Devices on your Mac:", deviceCount]];
	
	for (int i = 0; i < deviceCount; i++) {
		cuCudaDeviceProps(i, &deviceProperties);
		int major = deviceProperties.major; // determine the number of CUDA cores per multiprocessor for each generation
		unsigned mbTotalGlobal = deviceProperties.totalGlobalMem;
		NSString *memTotal = [NSString stringWithFormat:@"%uMB", mbTotalGlobal/1024/1024];
		int coresPerMultiProcessor = 8;
		if (major > 1) 
			coresPerMultiProcessor = 16;
		
		char *_name = deviceProperties.name;
		int multCount = deviceProperties.multiProcessorCount;
		NSString *name = [NSString stringWithCString:_name encoding:NSASCIIStringEncoding];
		NSString *deviceInfo = [NSString stringWithFormat:@"\tCUDA Device: %@ with %@  %d CUDA Cores",
													name, memTotal, multCount * coresPerMultiProcessor];
		[self logInfo:deviceInfo];
	}
	// find the largest width and height for 4/3 aspect ratio
	float xOptDim;
	float yOptDim;
	unsigned amountAlloced;
	bool success = [AppController checkGPUMemAvailable:&amountAlloced w:&xOptDim h:&yOptDim niceFactor:1.0f];
	NSString *summary = [AppController getAllocSummary:success x:xOptDim y:yOptDim amount:amountAlloced];
	[self logInfo:summary];
}

// cleanup App before it is closed
- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	extern volatile bool abortRender;
	abortRender = true; // tell background renderers to exit
}

// show the release notes
- (IBAction)showReleaseNotes:(id)sender
{
	HelpController *ctrl = [[HelpController alloc] initWithResource:@"ReleaseNotesMac" withExtension:@"rtf"];	
	[ctrl window];
	[ctrl showWindow:sender];
}

// show the troubleshooting notes
- (IBAction)showTroubleshooting:(id)sender
{
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"troubleshooting" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"Troubleshoot"  inBook:locBookName];
	}
}

// open this document
- (IBAction)openPngDocument:(id)sender
{
	NSURL *url = sender;
	NSDocumentController *ctrl = [NSDocumentController sharedDocumentController];
	NSDocument *doc = [ctrl documentForURL:url];
	if (doc) {
		[doc close];
	}
	NSError *error = nil;
	[ctrl openDocumentWithContentsOfURL:url display:YES error:&error];
	if (error) {
		NSAlert *a = [NSAlert alertWithError:error];
		[a runModal];
	}
}

- (void)renderBatchPerformance:(float)seconds numBatches:(float)batches
{
	cumRenderSeconds += seconds;
	cumBatches       += batches;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:[NSNumber numberWithDouble:cumRenderSeconds]	forKey:Flam4CumRenderSeconds];
	[defaults setObject:[NSNumber numberWithDouble:cumBatches]			forKey:Flam4CumBatches];
}

- (double)secondsPerBatch
{
	return cumRenderSeconds/cumBatches;
}

// notified that the Preview Quality preference has changed, lets re-render all preview windows
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"prevQuality"]) {
		for (NSDocument *doc in [[NSDocumentController sharedDocumentController] documents]) {
			if ([doc isMemberOfClass:[FlameDocument class]]) {
				PreviewController * ctrl = [[doc windowControllers] objectAtIndex:0];
				[ctrl renderAsPreview:YES];
			}
		}
	}
}

- (IBAction)showManual:(id)sender {
	HelpController *ctrl = [[HelpController alloc] initWithResource:@"UserGuide" withExtension:@"rtf"];	
	[ctrl window];
	[ctrl showWindow:sender];
}


@end
