//
//  HelpController.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/16/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "HelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

@implementation HelpController
@synthesize helpContents;

- (id)initWithResource:(NSString *)name withExtension:(NSString *)extension
{
	self = [super initWithWindowNibName:@"Help"];
	
	NSData *data;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
		NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
		data = [NSData dataWithContentsOfFile:path];
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		NSURL *url = [[NSBundle mainBundle] URLForResource:name withExtension:extension];
		data = [NSData dataWithContentsOfURL:url];
#endif
	}
	
	
	NSDictionary *attributes;
	helpContents = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:&attributes] retain];
    return self;
}

- (id)initWithHelpResource:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir
{
	self = [super initWithWindowNibName:@"Help"];
	
	NSData *data;
	NSURL *url;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
		NSString *path = [[NSBundle mainBundle] pathForResource:helpDir ofType:@"help"];
		path = [[NSBundle bundleWithPath:path] pathForResource:name ofType:extension];		
		data = [NSData dataWithContentsOfFile:path];
		url  = [NSURL fileURLWithPath:path];
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		url  = [[NSBundle mainBundle] URLForResource:helpDir withExtension:@"help"];
		url  = [[NSBundle bundleWithURL:url] URLForResource:name withExtension:extension];
		data = [NSData dataWithContentsOfURL:url];
#endif
	}
	
	
	NSDictionary *attributes;
	helpContents = [[[NSAttributedString alloc] initWithHTML:data baseURL:url documentAttributes:&attributes] retain];
    return self;
}

+ (NSString *)pathForHelpFile:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir
{
	NSString *path   = [[NSBundle mainBundle] pathForResource:helpDir ofType:@"help"];
	NSBundle *bundle = [NSBundle bundleWithPath:path];
	path = [bundle pathForResource:name ofType:extension inDirectory:@"pgs"];
	return path;
}

+ (void)launchHelpForHelpFile:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir {
	NSString *htmlPath = [HelpController pathForHelpFile:name withExtension:extension helpDirectory:helpDir];
	NSTask *task       = [[NSTask alloc] init];
	NSString *launchPath;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		launchPath = @"/System/Library/CoreServices/Help Viewer.app/Contents/MacOS/Help Viewer";
	else
		launchPath = @"/System/Library/CoreServices/HelpViewer.app/Contents/MacOS/HelpViewer";
	[task setLaunchPath:launchPath];
	NSArray *array = [NSArray arrayWithObjects:htmlPath, nil];
	[task setArguments:array];
	[task launch];
	[task release];
}

- (void)dealloc
{
	[helpContents release];
	[super dealloc];
}

- (void)windowDidLoad {
	[[helpView textStorage] setAttributedString: helpContents];
}


@end
