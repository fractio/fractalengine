/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "MovieWindowController.h"
#import "MovieDocument.h"

enum { kDefaultWidthForNonVisualMovies = 320};

@implementation MovieWindowController


- (id)init
{
    self = [super init];
    if (!self)
		return nil;
	
    return self;
}

//////////
//
// windowDidLoad
//
// Called when the window is loaded
// We'll adjust our button position relative to the movie
//
//////////

- (void)windowDidLoad
{
    // first size our window to fit the movie
	MovieDocument *doc = [self document];
	QTMovie *docMovie = [doc docMovie];
	
    // now set up the movie view
	[movieView setControllerVisible:YES];
	
	// get movie natural size for sizing our movie view
	NSSize contentSize = [[docMovie attributeForKey:QTMovieNaturalSizeAttribute] sizeValue];
	
	// check for sound-only movie, and size it appropriately
	// so movie controller can be shown
	if (NSEqualSizes(contentSize, NSZeroSize) == YES)
	{
		contentSize.width = kDefaultWidthForNonVisualMovies;
		contentSize.height = [movieView controllerBarHeight];
	}
	else
	{
		// we have video tracks, so take into account movie controller height
		contentSize.height = contentSize.height + [movieView controllerBarHeight];
	}
	
	// size the movie view
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
	[movieView setFrameSize:contentSize];
#endif
	[[movieView window] setContentSize:contentSize];
	[movieView setMovie:docMovie];
	
	[movieView gotoBeginning:docMovie];
}

- (QTMovieView *)movieView
{
	return movieView;
}

- (NSString *)windowTitleForDocumentDisplayName:(NSString *)displayName
{
	return [NSString stringWithFormat:@"Movie - %@", displayName];
}



@end
