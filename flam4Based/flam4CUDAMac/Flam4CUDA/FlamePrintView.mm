/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "FlamePrintView.h"
#import "PreviewController.h"

void RenderForPrint(Flame* flame, int width, int height, FlamePrintView *view, unsigned quality);

@class NSPrintPreviewGraphicsContext;

@implementation FlamePrintView
@synthesize imageRep;
@synthesize flame;
@synthesize scaleFactor;
@synthesize quality;
@synthesize ctrl;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        imageRep    = nil;
		quality     = 2000;
		scaleFactor = 1.0;
		flame       = nil;
		ctrl		= nil;
    }
    return self;
}

- (void)dealloc {
	[ctrl release];
	[imageRep release];
	[super dealloc];
}

// return the actual rendering dimensions
- (NSSize) renderDimensions:(NSRect)rect
{
	return NSMakeSize(ceil(scaleFactor * rect.size.width),
					  ceil(scaleFactor * rect.size.height));
}

// draw to the printer
- (void)drawRect:(NSRect)dirtyRect {
	NSRect scaledRect;
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	NSString *contextDescription = [context description];
	NSRange range = [contextDescription rangeOfString:@"NSPrintPreviewGraphicsContext"];
	
	// a kludgy way to see if this is drawing a thumbnail to the screen - check if we are a proxy object
	// standard check of GraphicsContext to see if it is drawing to screen does not work in this case
	if (! NSEqualRanges(range, NSMakeRange(NSNotFound, 0))) {
		// create low quality image for print preview
		scaledRect = dirtyRect;
		RenderForPrint(flame, NSWidth(scaledRect), NSHeight(scaledRect), self, 30);
	}
	else {
		// up-rez the image for the printer's optimal resolution
		scaledRect = NSMakeRect(floor(scaleFactor * dirtyRect.origin.x), 
								floor(scaleFactor * dirtyRect.origin.y), 
								ceil(scaleFactor * dirtyRect.size.width),
								ceil(scaleFactor * dirtyRect.size.height));
		
		// start background print rendering thread
		pthread_t threadHandle = [ctrl invokeRenderPrintThread:scaledRect printView:self];
		// wait for rendering thread to finish
		pthread_join(threadHandle, NULL);
	}

	NSImage *image = [[NSImage alloc] initWithSize:scaledRect.size];
	[image addRepresentation:imageRep];
	
	// the OS will handle the user coordinate space to device space scaling for us
	[image drawInRect:dirtyRect fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
	[image release];
}

@end
