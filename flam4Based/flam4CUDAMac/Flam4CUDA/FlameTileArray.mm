//
//  FlameTileArray.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import "FlameTile.h"
#import "FlameTileArray.h"
#import "ImageRepTileArray.h"

@implementation FlameTileArray
@synthesize rows;
@synthesize cols;
@synthesize array;
@synthesize flipped;

- (id)initWithRows:(int)_rows cols:(int)_cols flipped:(BOOL)_flipped
{
	self = [super init];
	if (self) {
		array = [[NSMutableArray arrayWithCapacity:_rows * _cols] retain];
		for (int i = 0; i < _rows * _cols; i++)
			[array insertObject:[NSNull null] atIndex:i];
		rows    = _rows;
		cols    = _cols;
		flipped = _flipped;
	}
	return self;
}

// calculate the flames for each tile 
- (id)initWithRows:(int)_rows cols:(int)_cols flame:(Flame *)flame width:(float)width height:(float)height flipped:(BOOL)_flipped
{
	self = [self initWithRows:_rows cols:_cols flipped:_flipped];
	if (! self) 
		return nil;
	
	if (rows == 1 && cols == 1) {
		FlameTile *f = [[FlameTile alloc] initWithFlame:flame row:0 column:0 width:width height:height];
		[array replaceObjectAtIndex:0 withObject:f];
		[f release];
		return self;
	}
	
	float fwidth  = roundf(flame->size[0] * flame->scale * powf(2.f, flame->zoom)); // normalized file coordinates
	float fheight = roundf(flame->size[1] * flame->scale * powf(2.f, flame->zoom));
	float tileWidth;
	float tileHeight;
	float ftileWidth;
	float ftileHeight;
	float overlap = [FlameTile overlap];
	
	// split the total window into tiles
	float cumHeight  = 0.f;
	float fcumHeight = 0.f;
	for (int i = _rows - 1; i >= 0; i--) {  // do rows in reverse order		
		tileHeight  = [self tileHeightAtRow:i height:height];
		ftileHeight = tileHeight/height *fheight;
		
		float cumWidth  = 0.f;
		float fcumWidth = 0.f;
		float x0, y0, x1, y1;
		for (int j = 0; j < _cols; j++) {			
			tileWidth   = [self tileWidthAtCol:j width:width];
			ftileWidth  = tileWidth/width *fwidth;
			
			FlameTile *f = [[FlameTile alloc] initWithFlame:flame row:i column:j width:tileWidth height:tileHeight];
			[array replaceObjectAtIndex:i * _cols + j withObject:f];
			[f release];
			
			Flame *newFlame  = [f flame];
			
			// calculate the new size and centers for each tile
			// first get the centers of each tile in view coordinates
			// then unrotate & untranslate to get the new centers in normalized fractal coordinates
			x0 = (fcumWidth  + ftileWidth/2.0f  - fwidth/2.0f)/(flame->scale * powf(2.f, flame->zoom));
			y0 = (fcumHeight + ftileHeight/2.0f - fheight/2.0f)/(flame->scale * powf(2.f, flame->zoom));
			
			x1 = x0 * cosf(-flame->rotation) - y0 * sinf(-flame->rotation);
			y1 = x0 * sinf(-flame->rotation) + y0 * cosf(-flame->rotation);
			
			newFlame->size[0] = ftileWidth/flame->scale/powf(2.f, flame->zoom);
			newFlame->size[1] = ftileHeight/flame->scale/powf(2.f, flame->zoom);
			
			newFlame->center[0] = x1 + flame->center[0];
			newFlame->center[1] = y1 + flame->center[1];
			
			cumWidth  += tileWidth  - 2*overlap;
			fcumWidth += ftileWidth - 2*overlap/width*fwidth;
		}
		cumHeight  += tileHeight  - 2*overlap;
		fcumHeight += ftileHeight - 2*overlap/height*fheight;
	}
	return self;
}



- (void)dealloc
{
	[array release];
	[super dealloc];
}

- (NSUInteger)count
{
	return [array count];
}

-(FlameTile *)tileAtIndex:(int)index
{
	return [array objectAtIndex:index];
}

-(FlameTile *)tileAtRow:(int)row col:(int)col
{
	return [array objectAtIndex:row * cols + col];
}

-(void)setTileAtIndex:(int)index withObject:(FlameTile *)object
{
	[array replaceObjectAtIndex:index withObject:object];
}

-(void)setTileAtRow:(int)row col:(int)col withObject:(FlameTile *)object
{
	[self setTileAtIndex:row * cols + col withObject:object];
}

- (int)numBatchesPerTile {
	return [[self tileAtIndex:0] flame]->numBatches;
}

- (int)numBatches {
	return [[self tileAtIndex:0] flame]->numBatches * [self count];
}

- (void)setNumBatchesPerTile:(int)batches
{
	for (FlameTile *obj in array)
		[obj flame]->numBatches = batches;
}

- (void)setNumBatches:(int)batches
{
	for (FlameTile *obj in array)
		[obj flame]->numBatches = ceilf((float)batches/[array count]);  // to guarantee no loss in quality
}

- (ImageRepTileArray *)makeImageRepTileArray:(BOOL)_flipped
{
	return [ImageRepTileArray arrayWithRows:rows cols:cols flipped:_flipped];
}

- (float)tileWidthAtCol:(int)col width:(float)width {
	float overlap = [FlameTile overlap];
	float  base = (col == cols - 1) ? (width - (cols - 1) * floorf(width/cols)) : floorf(width/cols);	
	return base + ((col > 0) ? overlap : 0.f) + ((col < cols - 1) ? overlap : 0.f);
}

- (float)tileHeightAtRow:(int)row height:(float)height {
	float overlap = [FlameTile overlap];
	float  base = (row == rows - 1) ? (height - (rows - 1) * floorf(height/rows)) : floorf(height/rows);	
	return base + ((row > 0) ? overlap : 0.f) + ((row < rows - 1) ? overlap : 0.f);
}

+ (FlameTileArray *)arrayWithRows:(int)_rows cols:(int)_cols flame:(Flame *)flame width:(float)width height:(float)height flipped:(BOOL)flipped
{
	return [[[FlameTileArray alloc] initWithRows:_rows cols:_cols flame:flame width:width height:height flipped:flipped] autorelease];
}

@end
