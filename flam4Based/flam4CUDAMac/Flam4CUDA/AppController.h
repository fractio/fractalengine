/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Cocoa/Cocoa.h>
#import "PngSettingsController.h"
#import "OptionsController.h"
#import "PngBatchSettingsController.h"

extern NSString * const Flam4CumRenderSeconds;
extern NSString * const Flam4CumBatches;
extern NSString * const Flam4PngDirectory;
extern NSString * const Flam4PngBatchDirectory;
extern NSString * const Flam4MovieDirectory;

@class FlameTileArray;

@class PreferenceController;

@interface AppController : NSObject <NSUserInterfaceValidations>  {
	IBOutlet NSTextView *logView;
	IBOutlet NSButton   *logResetBtn;

	PreferenceController       *preferenceController;
	BOOL     showRenderSteps; // if set to true, the render steps will be logged
	double   cumRenderSeconds;
	double   cumBatches;
}

- (IBAction)openOptionsPanel:(id)sender;
- (IBAction)openPngSettingsPanel:(id)sender;
- (IBAction)openPngBatchSettingsPanel:(id)sender;
- (IBAction)openMakeMoviePanel:(id)sender;
- (IBAction)startRender:(id)sender;
- (IBAction)openFlameFile:(id)sender;
- (IBAction)showPreferencePanel:(id)sender;
- (IBAction)resetLogColor:(id)sender;
- (IBAction)logRenderSteps:(id)sender;
- (IBAction)logInfo:(id)sender;
- (IBAction)logError:(id)sender;
- (IBAction)showReleaseNotes:(id)sender;
- (IBAction)showTroubleshooting:(id)sender;
- (IBAction)openPngDocument:(id)sender;
- (IBAction)openTextEdit:(id)sender;
- (IBAction)showManual:(id)sender;

- (double)secondsPerBatch;
- (void)renderBatchPerformance:(float)seconds numBatches:(float)batches;
- (IBAction)showRenderSteps:(id)sender;
- (void)warnStoppingAnimation:(NSString *)s;
+ (NSString *)getAllocSummary:(BOOL)success x:(float)xOptDim y:(float)yOptDim amount:(unsigned)amountAlloced;
+ (NSString *)getAllocSummary:(BOOL)success area:(float)area amount:(unsigned)amountAlloced;
+ (BOOL)allDevicesAllocCheck:(unsigned)numDevices area:(float *)area amount:(unsigned *)amountAlloced niceFactor:(float)niceFactor;
+ (BOOL)allDevicesAllocCheck:(unsigned)numDevices x:(float *)xOptDim y:(float *)yOptDim amount:(unsigned *)amountAlloced niceFactor:(float)niceFactor;
+ (BOOL)deviceAllocCheck:(int)deviceNum area:(float *)area amount:(unsigned *)amountAlloced;
+ (NSString *)deviceAllocSummary:(unsigned)numDevices x:(float *)xOptDim y:(float *)yOptDim amount:(unsigned *)amountAlloced  niceFactor:(float)niceFactor;
+ (FlameTileArray *)splitAreaIntoTiles:(float)area width:(float)width height:(float)height flame:(Flame *)flame flipped:(BOOL)flipped;
+ (BOOL)checkGPUMemAvailable:(unsigned *)memoryAvailable area:(float *)area niceFactor:(float)niceFactor;
+ (BOOL)checkGPUMemAvailable:(unsigned *)memoryAvailable area:(float *)area niceFactor:(float)niceFactor reductionFactor:(float)factor;

@end
