/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#include <libkern/OSAtomic.h>
#import "Defines.h"
#import "PngSettingsController.h"
#import "FlameDocument.h"
#import "RenderThread.h"
#import "Globals.h"
#import "PreferenceController.h"
#import "QualityUtils.h"
#import "AppController.h"
#import "FlameTileArray.h"
#import "FlameTile.h"
#import "ImageUtil.h"
#import "OpenGLImageUtil.h"
#import "HelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

@implementation PngSettingsController

//@synthesize window;
@synthesize outputHeight;
@synthesize outputWidth;
@synthesize transparent;
@synthesize quality;
@synthesize actualQuality;
@synthesize tiles;
@synthesize progressBar;

- (void)formatActualQuality
{
	if (outputWidth == 0 or outputHeight == 0) {
		actualQuality = @"Actual Quality: ";
		return;
	}
	[self setActualQuality: [NSString stringWithFormat:@"Actual Quality: %0.0f", 
							 [QualityUtils actualQuality:quality width:outputWidth height:outputHeight tiles:1]]];
}

- (NSString *)nibName {
	return @"PNGSettings";
}

// timer tick procedure
- (void)tick:(NSTimer *)aTimer
{
	if (isDone)
	{
		isDone = false;
		[[self window] close];
	}
	else
		[progressBar setDoubleValue: (double)progress];
}

- (id)init
{
	if (! [super initWithWindowNibName:[self nibName]])
		return nil;
	
	thread = nil;
	doc    = nil;
	tiles  = nil;

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	transparent  = [defaults integerForKey:Flam4PngTransparencyKey];
	outputWidth  = [defaults integerForKey:Flam4PngWidthKey];
	outputHeight = [defaults integerForKey:Flam4PngHeightKey];
	quality      = [defaults integerForKey:Flam4PngQualityKey];
	if (outputHeight != 0) 
		aspectRatio = ((float)outputWidth)/((float)outputHeight);
	else
		aspectRatio = 4.0f/3.0f;
	
	return self;
}

- (void)dealloc
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];	
	[nc removeObserver:self];

	if (thread) {
		delete thread;
		thread = nil;
	}
	[doc release];
	[actualQuality release];
	[tiles release];
	[super dealloc];
}

- (void) threadDone {
	delete thread;
	thread = nil;
}

- (void)willClose:(NSNotification *)note
{
	//[self autorelease];
	[timer invalidate];
	timer = nil;
}

- (void)textDidChange:(NSNotification *)aNotification {
	if ([aNotification object] == widthField) {
		outputWidth = [widthField integerValue];
		
		// set linked height field if applicable
		if ([linkBtn state] == NSOnState) {
			int newHeight = (int)((float)outputWidth/aspectRatio);
			if (outputHeight != newHeight) {
				outputHeight = newHeight;
				[heightField setIntegerValue:newHeight];
			}
		}
	}
	else if ([aNotification object] == heightField) {
		outputHeight = [heightField integerValue];
		// set linked width field if applicable
		if ([linkBtn state] == NSOnState) {
			int newWidth = (int)((float)outputHeight*aspectRatio);
			if (outputWidth != newWidth) {
				outputWidth = newWidth;
				[widthField setIntegerValue:newWidth];
			}
		}
	}
}

- (void)windowDidLoad
{
	float area;
	unsigned amountAlloced;
	// lets check the current amount available to inform the user
	bool success = [AppController checkGPUMemAvailable:&amountAlloced area:&area niceFactor:1.0f reductionFactor:0.95f];
	NSString *summary = [AppController getAllocSummary:success area:area amount:amountAlloced];
	[[NSApp delegate] logInfo:summary];
		
	[[self window] setLevel: NSModalPanelWindowLevel];

	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];	
	[nc addObserver:self
		   selector:@selector(willClose:) name:NSWindowWillCloseNotification object:[self window]];
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:widthField];
	[nc addObserver:self
		   selector:@selector(textDidChange:) name:NSControlTextDidChangeNotification object:heightField];
	
	[self reset];
	
	doc = [[[NSDocumentController sharedDocumentController] currentDocument] retain];
	
	[linkBtn setState:NSOffState]; // set this so we dont have auto aspect ration preserving while initializing
	
	[widthField   setObjectValue:[NSNumber numberWithInt:outputWidth]];
	[heightField  setObjectValue:[NSNumber numberWithInt:outputHeight]];
	[qualityField setObjectValue:[NSNumber numberWithInt:quality]];
	[fileQualityLabel setStringValue: [NSString stringWithFormat:@"Flame File Quality: %-8u",[doc flameData]->originalQuality]];

	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[linkBtn setState:[defaults integerForKey:Flam4PngLinkStateKey]];
}	

// set things that need to reset every time the window is opened
- (void)reset
{
	progress = 0;
	[progressBar setDoubleValue: (double)progress];
	[self formatActualQuality];
	if (!timer)
		timer = [[NSTimer scheduledTimerWithTimeInterval:0.5 
												  target:self 
												selector:@selector(tick:)  
												userInfo:nil 
												 repeats:YES] retain];
}

// quality changed event handler
- (IBAction)changeQuality:(id)sender {
	quality = [qualityField integerValue];
	[self formatActualQuality];
}

// width changed event handler
- (IBAction)changeWidth:(id)sender {
	outputWidth  = [widthField integerValue];
	
	// set linked height field if applicable
	if ([linkBtn state] == NSOnState) {
		int newHeight = (int)((float)outputWidth/aspectRatio);
		if (outputHeight != newHeight) {
			outputHeight = newHeight;
			[heightField setIntegerValue:newHeight];
		}
	}
	else { // update aspect ratio
		if (outputHeight != 0) 
			aspectRatio = (float)outputWidth/(float)outputHeight;
		else
			aspectRatio = 4.0f/3.0f;		
	}
	[self formatActualQuality];
}

// height changed event handler
- (IBAction)changeHeight:(id)sender {
	outputHeight = [heightField integerValue];

	// set linked width field if applicable
	if ([linkBtn state] == NSOnState) {
		int newWidth = (int)((float)outputHeight*aspectRatio);
		if (outputWidth != newWidth) {
			outputWidth = newWidth;
			[widthField setIntegerValue:newWidth];
		}
	}
	else { // update aspect ratio
		if (outputHeight != 0) 
			aspectRatio = (float)outputWidth/(float)outputHeight;
		else
			aspectRatio = 4.0f/3.0f;		
	}
	
	[self formatActualQuality];
}


- (BOOL)loadMyNibFile: (NSString *)nibName
{
	// The myNib file must be in the bundle that defines self's class.
	if (![NSBundle loadNibNamed:nibName owner:self])
	{
		NSLog(@"Warning! Could not load myNib file: %@.\n", nibName);
		return NO;
	}
	return YES;
}

// Cancel button handler
- (IBAction)cancel:(id)sender
{	
	isDone = false;
	if (isRendering)
		abortRender = true;
	while (abortRender)
	{
		usleep(50000);
	}
	[[self window] close];
}

- (IBAction)renderAsPNG:(id)sender
{
	// get the values of the Controls here, as any final typing changes made to them might not have been captured via keypair messaging
	[widthField validateEditing];
	[heightField validateEditing];
	[qualityField validateEditing];
	outputWidth  = [widthField integerValue];
	outputHeight = [heightField integerValue];
	quality      = [qualityField integerValue];
		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSURL *initialDir        = [NSURL fileURLWithPath:[defaults objectForKey:Flam4PngDirectory]];
	
	if (doc) {
		NSSavePanel *sPanel = [NSSavePanel savePanel];		
		NSArray *fileTypes = [NSArray arrayWithObjects:@"png", nil];
		
		if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) // Leopard
			[sPanel setDirectory:[initialDir path]];
		else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
			[sPanel setDirectoryURL:initialDir];
#endif
		}
		[sPanel setCanCreateDirectories:YES];
		[sPanel setAllowedFileTypes:fileTypes];
		[sPanel setMessage:@"Choose a file to save the PNG file as"];
		[sPanel setTitle:@"Save as PNG File"];
		[sPanel setExtensionHidden:NO];
		
		NSMutableString *filename = [NSMutableString stringWithString:[[doc displayName] stringByDeletingPathExtension]];
		[filename appendString: @".png"];
		// if successful, save file under designated name 
		if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
			int result = [sPanel runModalForDirectory:[initialDir path] file:filename];
			if (result != NSOKButton) 
				return;			
		}
		else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
			[sPanel setNameFieldStringValue:filename];
			
			int result = [sPanel runModal];
			if (result != NSOKButton) 
				return;
#endif
		}
		[[self window] makeKeyWindow];
		
		NSURL *url = [sPanel URL];
		NSURL *pickedDir = nil;
		if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
			NSString *path = [sPanel directory];
			pickedDir      = [NSURL fileURLWithPath:path];
		}
		else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
			pickedDir = [sPanel directoryURL];
#endif
		}		
		if (! [initialDir isEqual:pickedDir]) { // remember this directory as the new default
			[defaults setObject:[pickedDir path] forKey:Flam4PngDirectory];
		}

		[self setTiles:[AppController splitAreaIntoTiles:outputWidth * outputHeight 
											 width:outputWidth 
											height:outputHeight
												   flame:[doc getCurrentFlame] 
												 flipped:YES]];
		
		thread = new RenderThread([doc flameData], outputWidth, outputHeight, transparent, 
								  nil, nil, [url retain], self);
		int batchesPerTile = ceil([QualityUtils batchesForDesired:quality width:outputWidth height:outputHeight tiles:[tiles count]]);
		[tiles setNumBatchesPerTile:batchesPerTile];

		[progressBar setMaxValue: batchesPerTile * [tiles count]];
		OSMemoryBarrier(); // otherwise progress bar blows up ??
		
		pthread_t threadHandle;										
		pthread_create(&threadHandle, NULL, RenderThread::RenderPNG, thread);
	}
}


/* - (IBAction)renderAsPNGTest:(id)sender
{
	[[self window] close];

	[progressBar setMaxValue: [doc getCurrentFlame]->numBatches];
	
	if (doc) {
		FlameWindowController *ctrl = [[doc windowControllers] objectAtIndex:0];
		NSOpenGLView *view = [ctrl view];
									   
		if ([view lockFocusIfCanDraw]) {
			NSImage *image = [OpenGLImageUtil imageFromOpenGLView:view hasAlpha:transparent];

			NSBitmapImageRep* rep = [[image representations] objectAtIndex:0];
			
			[view unlockFocus];
			
			NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
			NSData *data = [rep representationUsingType:NSPNGFileType properties:properties];
			
			NSSavePanel *sPanel = [NSSavePanel savePanel];		
			NSArray *fileTypes = [NSArray arrayWithObjects:@"png", nil];
			
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			NSURL *initialDir        = [NSURL fileURLWithPath:[defaults objectForKey:Flam4PngDirectory]];
			
			if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
				[sPanel setDirectory:[initialDir path]];
			else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
				[sPanel setDirectoryURL:initialDir];		
#endif
			}
			[sPanel setCanCreateDirectories:YES];
			[sPanel setAllowedFileTypes:fileTypes];
			[sPanel setMessage:@"Save as PNG File"];
			[sPanel setTitle:@"Save as PNG File"];
			[sPanel setExtensionHidden:NO];
			
			NSMutableString *filename = [NSMutableString stringWithString:[[doc displayName] stringByDeletingPathExtension]];
			[filename appendString: @".png"];
			if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
				int result = [sPanel runModalForDirectory:[initialDir path] file:filename];
				if (result != NSOKButton) 
					return;			
			}
			else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
				[sPanel setNameFieldStringValue:filename];
				int result = [sPanel runModal];
				if (result != NSOKButton) 
					return;
#endif				
			}
						
			NSURL *url = [sPanel URL];
			NSError *error = nil;
			if (! [data writeToURL:url options:0 error:&error]) {
				NSAlert *a = [NSAlert alertWithError:error];
				[a runModal];
				return;
			}
		}
		else {
			NSRunAlertPanel(@"Warning", @"Could not save flame image as PNG - could not lock focus on the View", @"Ok", nil, nil);
			return;
		}
	}
}
 */
+ (NSURL *)pieceURL:(NSURL *)url tile:(FlameTile *)tile cols:(int)cols
{
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
	NSString *last = [NSString stringWithFormat:@"%@_%u.png", [[url URLByDeletingPathExtension] lastPathComponent], [tile row]*cols +[tile column]] ;
	NSURL *url2 = [url URLByDeletingLastPathComponent];
	return [url2 URLByAppendingPathComponent:last];
#else
	return nil;
#endif
}

- (IBAction)help:(id)sender {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"howtoCreatePNG" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"CreatePNG"  inBook:locBookName];
	}
}



@end
