/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */
#import <Cocoa/Cocoa.h>

extern NSString * const Flam4ViewBgColorKey;
extern NSString * const Flam4PreviewQualityKey;
extern NSString * const Flam4PreviewDimensionSource;
extern NSString * const Flam4PreviewWidthKey;
extern NSString * const Flam4PreviewHeightKey;
extern NSString * const Flam4PreviewTransparencyKey;
extern NSString * const Flam4PreviewLinkStateKey;
extern NSString * const Flam4PngQualityKey;
extern NSString * const Flam4PngWidthKey;
extern NSString * const Flam4PngHeightKey;
extern NSString * const Flam4PngTransparencyKey;
extern NSString * const Flam4PngLinkStateKey;
extern NSString * const Flam4PrintQualityKey;

// used for the radio button selection
enum DimensionSource {
	DimensionsFromFile = 0,
	OverrideDimensions = 1,
	OverrideDimensionsKeepAspectRatio = 2
};

@interface PreferenceController : NSWindowController {
	IBOutlet NSWindow    *window;

	IBOutlet NSColorWell *previewColorWell;
	IBOutlet NSTextField *previewQualityField;
	IBOutlet NSTextField *previewWidthField;
	IBOutlet NSTextField *previewHeightField;
	IBOutlet NSButton    *previewTransparentCB;
	
	IBOutlet NSTextField *pngQualityField;
	IBOutlet NSTextField *pngWidthField;
	IBOutlet NSTextField *pngHeightField;
	IBOutlet NSButton    *pngTransparentCB;
	
	IBOutlet NSTextField *printQualityField;

	float previewAspectRatio; // width/height
	float pngAspectRatio; // width/height
	
	NSNumber *prevQuality;
}
@property (copy) NSNumber *prevQuality;

- (IBAction)changeBackgroundColor:(id)sender;
- (IBAction)changePreviewQuality:(id)sender;
- (IBAction)changePreviewWidth:(id)sender;
- (IBAction)changePreviewHeight:(id)sender;
- (IBAction)changePreviewTransparency:(id)sender;
- (IBAction)changePngQuality:(id)sender;
- (IBAction)changePngWidth:(id)sender;
- (IBAction)changePngHeight:(id)sender;
- (IBAction)changePngTransparency:(id)sender;
- (IBAction)changePrintQuality:(id)sender;

+ (NSColor *) bgViewColor;
+ (enum DimensionSource)previewDimensionSource;
+ (NSInteger) previewQuality;
+ (NSInteger) previewWidth;
+ (NSInteger) previewHeight;
+ (BOOL)      previewTransparency;
+ (NSUInteger)previewLinkState;
+ (NSInteger) pngQuality;
+ (NSInteger) pngWidth;
+ (NSInteger) pngHeight;
+ (BOOL)      pngTransparency;
+ (NSUInteger)pngLinkState;
+ (NSInteger) printQuality;

@end
