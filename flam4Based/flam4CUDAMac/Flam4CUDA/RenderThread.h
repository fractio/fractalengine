/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RENDERTHREAD_H
#define RENDERTHREAD_H

#include <Cocoa/Cocoa.h>
#include "FlameAnimation.h"
#import "PngSettingsController.h"
#import "FlamePreviewView.h"
#import "FlamePrintView.h"

extern volatile bool isRendering;
extern bool renderingActive;

class RenderThread
{
public:
	flameParameterList* flames;
	int                 width;
	int                 height;
	bool                useAlpha;
	FlamePreviewView *  view;
	FlamePrintView *    printView;
	NSURL              *outFile;
	NSObject           *ctrl;
	bool				abortThread;
	
	RenderThread(flameParameterList* flames, 
				 int width,
				 int height,
				 BOOL useAlpha,
				 FlamePreviewView *view,
				 FlamePrintView *printView,
				 NSURL *outFile,
				 NSObject *ctrl);
	~RenderThread();
	
	void RenderProc();
	void RenderPNGProc();
	void RenderPreviewProc();
	void RenderPNGBatchProc();
	void RenderPrintProc();
	
	void setFlameQuality(Flame *flame, double quality, int tiles);
	
	static void *Render(void *t);
	static void *RenderPNG(void *t);
	static void *RenderToPreview(void *t);
	static void *RenderPNGBatch(void *t);
	static void *RenderPrint(void *t);
};
#endif