//
//  HelpController.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 2/16/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface HelpController : NSWindowController {
	NSAttributedString   *helpContents;
	IBOutlet NSTextView  *helpView;
}

@property (retain) NSAttributedString *helpContents;

+ (NSString *)pathForHelpFile:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir;
+ (void)launchHelpForHelpFile:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir;

- (id)initWithResource:(NSString *)name withExtension:(NSString *)extension;
- (id)initWithHelpResource:(NSString *)name withExtension:(NSString *)extension helpDirectory:(NSString *)helpDir;

@end
