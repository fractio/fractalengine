//
//  ImageRepTileArray.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ImageRepTileArray : NSObject {
	int rows;
	int cols;
	float overlap;
	BOOL flipped;          // are we rendering for flipped y coords
	NSMutableArray *array;
}

@property (assign) int rows;
@property (assign) int cols;
@property (retain) NSMutableArray *array;
@property (assign) BOOL flipped;

- (id)initWithRows:(int)_rows cols:(int)_cols flipped:(BOOL)flipped;
- (NSBitmapImageRep *)tileAtIndex:(int)index;
- (NSBitmapImageRep *)tileAtRow:(int)row col:(int)col;
- (void)setTileAtIndex:(int)index withObject:(NSBitmapImageRep *)object;
- (void)setTileAtRow:(int)row col:(int)col withObject:(NSBitmapImageRep *)object;
+ (ImageRepTileArray *)arrayWithRows:(int)_rows cols:(int)_cols flipped:(BOOL)flipped;
- (NSBitmapImageRep *)combinedImageRep;
- (NSString *)labelDescription;

@end
