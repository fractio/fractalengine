/*
 *  mainzCmd.m
 *  Flam4CUDA
 *
 *  Created by Steven Brodhead on 1/12/10.
 *  Copyright 2010 Centcom Inc.. All rights reserved.
 *
 */
#include <cuda_runtime_api.h>
#import <Foundation/Foundation.h>
#import "FlameParse.h"
#import "QualityUtils.h"
#import "FlameTileArray.h"
#import "FlameTile.h"
#import "Flam4CMDAppDelegate.h"

extern "C" int cuCudaDeviceCount();
extern "C" int cuCudaDeviceProps(int deviceNumber, struct cudaDeviceProp *prop);
extern "C" bool cuMemCheck(unsigned deviceNumber, unsigned area, unsigned numColors, unsigned *amountAlloced);
extern NSBitmapImageRep *RenderPreviewTileSetCore(flameParameterList *flameParams, BOOL useAlpha, FlameTileArray *tiles);

float reductionFactor = 0.8f; // amount memory is reduced each pass for GPU memory consumption test

// return the largest tile size for this GPU
BOOL deviceAllocCheck(int deviceNum, float *area, unsigned *amountAlloced)
{
	// variables used to find the largest area we can support
	int numColors     = 256;
	BOOL success      = NO;
	while (!success) {
		if (!(success = cuMemCheck(deviceNum, *area, numColors, amountAlloced))) {
			*area = floorf(*area * reductionFactor);
			if (*area <= 1.0f) // the smallest possible tile size!
				return NO;
		}
	}
	return success;
}

// return the largest tile size for all GPUs
BOOL allDevicesAllocCheck(unsigned numDevices, float *area, unsigned *amountAlloced, float niceFactor)
{
	BOOL success = NO;
	for (int i = 0; i < numDevices; i++) {
		success = deviceAllocCheck(i, area, amountAlloced);
		if (!success)
			return NO;
	}
	*area = floorf(niceFactor * *area);
	return success;
}

BOOL checkGPUMemAvailable(unsigned *memoryAvailable, float *area, float niceFactor)
{
	unsigned deviceCount = cuCudaDeviceCount();
	struct cudaDeviceProp deviceProperties;
	unsigned maxAmount = UINT_MAX;
	float    maxArea   = float(UINT_MAX);
	bool     success   = YES;
	for (int i = 0; i < deviceCount; i++) {
		cuCudaDeviceProps(i, &deviceProperties);
		float mbTotalGlobal = deviceProperties.totalGlobalMem;
		float areaGlobal    = floorf(mbTotalGlobal/3.f); // a reasonable starting point just grab everything assuming RGB 3 bytes per pixel
		
		unsigned amountAlloced;
		success = allDevicesAllocCheck(deviceCount, &areaGlobal, &amountAlloced, niceFactor);
		
		maxArea   = (areaGlobal <= maxArea)      ? areaGlobal : maxArea;
		maxAmount = (amountAlloced <= maxAmount) ? amountAlloced : maxAmount;
	}
	*memoryAvailable = maxAmount;
	*area            = maxArea;
	return success;
}

// determine how to split the render area into tiles
FlameTileArray *splitAreaIntoTiles(float area, float width, float height, Flame *flame, BOOL flipped)
{
	float maxArea = area;
	float minHeight = 100.f;
	float minWidth  = 100.f;
	float overlap = [FlameTile overlap];
	unsigned amount;
	BOOL success = checkGPUMemAvailable(&amount, &maxArea, 0.8f);
	
	if (!success) {
		printf("Unable to allocate enough memory for any tile size");
		return nil;
	}
	
	int rows = 1;
	int cols = 1;
	if (width >= height) { // horizontal tiling
		if (maxArea >= width * height || maxArea/height >= minWidth + 2*overlap) { // 1 row is enough
			cols = ceilf(area/maxArea);
			rows = 1;
		}
		else if (height >= minHeight + 2*overlap) {  // too narrow case - split height by multiples of minHeight
			int minTiles = ceilf(area/(maxArea));
			
			rows = ceilf((minWidth + 2*overlap)*height/maxArea); // calc number of rows needed to keep size reasonable
			cols = ceilf(minTiles/rows);
			float effHeight = height + 2 * overlap * (rows - 1);
			float effWidth  = width  + 2 * overlap * (cols - 1);
			minTiles = ceilf(effWidth * effHeight/maxArea);
			cols = ceilf(minTiles/rows);
		}
		else {			
			cols = ceilf(width/(minWidth + 2*overlap));
			rows = 1;
		}
	}
	else  {		// vertical tiling  (width < height)
		if (maxArea >= width * height || maxArea/width >= minHeight + 2*overlap) {
			cols = 1;
			rows = ceilf(area/maxArea);	
		}
		else if (width >= minWidth + 2*overlap) {  // too short case - split width by multiples of minWidth
			int minTiles = ceilf(area/maxArea);
			
			cols = ceilf((minHeight + 2*overlap)*width/maxArea); // calc number of cols needed to keep size reasonable
			rows = ceilf(minTiles/cols);
			float effHeight = height + 2 * overlap * (rows - 1);
			float effWidth  = width  + 2 * overlap * (cols - 1);
			minTiles = ceilf(effWidth * effHeight/maxArea);
			rows = ceilf(minTiles/cols);
		}
		else {			
			cols = 1;
			rows = ceilf(height/(minHeight + 2*overlap));
		}
	}
	//[self logInfo:[NSString stringWithFormat:@"[%0.f X %0.f] %u tile(s) needed", width, height, rows*cols]];
	
	return [FlameTileArray arrayWithRows:rows cols:cols flame:flame width:width height:height flipped:flipped];
}

flameParameterList *readFromData(NSData *data, NSString *palettePath, NSError **outError)
{
	if (!data)
		return nil;
	NSXMLDocument *doc = [[NSXMLDocument alloc] 
						  initWithData:data options:0 error:outError];	
	
    if (doc == nil ) {
		NSString * description = [*outError localizedDescription];
		NSLog(@"Opening flame recipe failed: %@", description);
		
		[doc release];
		return nil;
	}
	@try {
		int frameCount;
		
		flameParameterList *flameData = [FlameParse parseDoc: doc frameCount:&frameCount palettePath:palettePath];
		[doc release];
	
		[FlameParse uniqueFrameNames:flameData]; // modify frame names as needed to make them unique
		return flameData;		
	}	
	@catch (NSException * e) {
		[doc release];
		return nil;
	}
	return nil;
}

// calculate the largest preview up to the maximum of our input width or height squared (maxDim x maxDim)
void calcPreviewSize(flameParameterList *flameData, float &width, float &height) {
	Flame *flame = flameData->flame;
	float fwidth  = roundf(flame->size[0] * flame->scale * powf(2.f, flame->zoom)); // normalized file coordinates
	float fheight = roundf(flame->size[1] * flame->scale * powf(2.f, flame->zoom));
	float aspectRatio = fwidth/fheight;
	
	if (aspectRatio >= 1.0f) {
		if (height >= width/aspectRatio)
			height = width/aspectRatio;
		else
			width = height*aspectRatio;
	}
	else {
		if (width >= height*aspectRatio)
			width = height*aspectRatio;
		else 
			height = width/aspectRatio;
	}
	
	/* // we want the largest preview up to maxDim x maxDim
		float maxDim  = width >= height ? width : height;
		if (fwidth >= fheight) {
			height = fheight / fwidth * maxDim;
			width  = maxDim;
		}
		else {
			width  = fwidth/fheight*maxDim;
			height = maxDim;
		} */
}

NSBitmapImageRep *renderAsPreview(flameParameterList *flameData, NSRect rect, int quality)
{
	FlameTileArray *tiles = splitAreaIntoTiles(rect.size.width * rect.size.height, 
												rect.size.width, rect.size.height, flameData->flame, NO);
	int batches = ceil([QualityUtils batchesForDesired:quality width:rect.size.width height:rect.size.height tiles:1]);
	[tiles setNumBatches:batches];
	return RenderPreviewTileSetCore(flameData, NO, tiles);
}

// find the palettes XML file pathname starting from the executable's pathname
NSString *findPalettePath(char *_exePath) {
	NSString *exePath = [NSString stringWithCString:_exePath encoding:[NSString defaultCStringEncoding]];
	return [[[exePath stringByDeletingLastPathComponent] 
					  stringByDeletingLastPathComponent]
			stringByAppendingPathComponent:@"Resources/flam3-palettes.xml"];
}


int main(int argc, char *argv[])
{
	int quality  = 50;
	float width  = 800.f;
	float height = 600.f;
	
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	Flam4CMDAppDelegate *appDlg = [[Flam4CMDAppDelegate alloc] init];

	NSURL *url = [NSURL URLWithString:[NSString stringWithCString:argv[argc - 1] encoding:NSASCIIStringEncoding]];
	
	sscanf(argv[1], "%f", &width);
	sscanf(argv[2], "%f", &height);
    for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "-q") == 0) {
			sscanf(argv[++i], "%u", &quality);
		}
	}
	NSString *mode = [NSString stringWithCString:argv[3] encoding:NSASCIIStringEncoding];
	
	NSError *error = nil;
	
	NSData *data = [NSData dataWithContentsOfURL:url];
	flameParameterList *flameData = readFromData(data, findPalettePath(argv[0]), &error);
	if (flameData == nil) {
		NSLog(@"Reading/parsing flame recipe failed for: %@", [url path]);
		return -2;
	}
	NSFileHandle *fh = [NSFileHandle fileHandleWithStandardOutput];
	
	// adjust preview size by the file's viewport aspect ratio
	float imageWidth  = width;
	float imageHeight = height;
	if ([mode isEqualToString:@"Preview"]) {
		calcPreviewSize(flameData, imageWidth, imageHeight);
		
		NSBitmapImageRep *rep = renderAsPreview(flameData, NSMakeRect(0.f, 0.f, imageWidth, imageHeight), quality);
		if (rep == nil) {
			NSLog(@"Rendering flame recipe failed for: %@", [url path]);
			return -3;
		}
		
		// create the preview image and fill it with black
		NSImage* previewImage = [[NSImage alloc] initWithSize:NSMakeSize(width, height)];
		[previewImage lockFocus];
		[[[NSColor blackColor] colorWithAlphaComponent:0.0f] setFill];
		NSRectFill(NSMakeRect(0.f, 0.f, width, height));
		
		// draw the fractal centered in the image
		NSImage *fractalImage = [[NSImage alloc] initWithSize:NSMakeSize(imageWidth, imageHeight)];
		[fractalImage addRepresentation:rep];
		
		// Draw the image in the current context.
		[fractalImage drawAtPoint:NSMakePoint((width - imageWidth)/2.f, (height - imageHeight)/2.f)
						 fromRect: NSMakeRect(0.0, 0.0, imageWidth, imageHeight)
						operation: NSCompositeCopy
						 fraction: 1.0];
		// get the preview image bitmap
		NSBitmapImageRep* previewRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0.f, 0.f, width, height)];	
		[previewImage unlockFocus];

		NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
		NSData *pngData = [previewRep representationUsingType:NSPNGFileType properties:properties];
		if (pngData == nil) {
			NSLog(@"Creating PNG representation of image failed for: %@", [url path]);
			return -4;
		}
		[fh writeData:pngData];
		[fh closeFile];
		[previewRep release];
		[previewImage release];
		[fractalImage release];
	}
	else { // Thumbnail mode
		calcPreviewSize(flameData, imageWidth, imageHeight);
		
		NSBitmapImageRep *rep = renderAsPreview(flameData, NSMakeRect(0.f, 0.f, imageWidth, imageHeight), quality);
		if (rep == nil) {
			NSLog(@"Rendering flame recipe failed for: %@", [url path]);
			return -3;
		}
		
		NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
		NSData *pngData = [rep representationUsingType:NSPNGFileType properties:properties];
		if (pngData == nil) {
			NSLog(@"Creating PNG representation of image failed for: %@", [url path]);
			return -4;
		}
		[fh writeData:pngData];
		[fh closeFile];
	}

	[appDlg release];
	[pool release];
	return 0;
}
