//
//  FlameTileArray.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FlameTile;
@class ImageRepTileArray;

@interface FlameTileArray : NSMutableArray {
	int rows;
	int cols;
	BOOL flipped;          // are we rendering for flipped y coords
	NSMutableArray *array;
}

@property (assign) int rows;
@property (assign) int cols;
@property (retain) NSMutableArray *array;
@property (assign) BOOL flipped;

- (id)initWithRows:(int)_rows cols:(int)_cols flipped:(BOOL)flipped;
- (id)initWithRows:(int)_rows cols:(int)_cols flame:(Flame *)flame width:(float)width height:(float)height flipped:(BOOL)flipped;
- (FlameTile *)tileAtIndex:(int)index;
- (FlameTile *)tileAtRow:(int)row col:(int)col;
- (void)setTileAtIndex:(int)index withObject:(FlameTile *)object;
- (void)setTileAtRow:(int)row col:(int)col withObject:(FlameTile *)object;
- (int)numBatches;
- (void)setNumBatches:(int)batches;
- (int)numBatchesPerTile;
- (void)setNumBatchesPerTile:(int)batches;
+ (FlameTileArray *)arrayWithRows:(int)_rows cols:(int)_cols flame:(Flame *)flame width:(float)width height:(float)height flipped:(BOOL)flipped;
- (ImageRepTileArray *)makeImageRepTileArray:(BOOL)_flipped;
- (float)tileWidthAtCol:(int)col width:(float)width;
- (float)tileHeightAtRow:(int)row height:(float)height;

@end
