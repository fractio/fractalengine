/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Cocoa/Cocoa.h>
#import "FlameDocument.h"
#import "RenderThread.h"

@class FlameTileArray;
@class FlameTile;

@interface PngSettingsController : NSWindowController  {
	IBOutlet NSProgressIndicator *progressBar;
	IBOutlet NSTextField         *widthField;
	IBOutlet NSTextField         *heightField;
	IBOutlet NSTextField         *qualityField;
	IBOutlet NSTextField         *fileQualityLabel;
	IBOutlet NSButton	         *linkBtn;

@public
	NSString *actualQuality;
@protected
	BOOL				transparent;
	int					outputHeight;
	int					outputWidth;
	int					quality;
	FlameDocument      *doc;
	class RenderThread *thread;
	NSTimer            *timer;
	FlameTileArray     *tiles;

@private
	float               aspectRatio; // width/height
}

- (IBAction)renderAsPNG:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)changeQuality:(id)sender;
- (IBAction)changeWidth:(id)sender;
- (IBAction)changeHeight:(id)sender;

- (void)reset;
- (NSString *)nibName;

- (void)threadDone;
- (void)tick:(NSTimer *)aTimer;
- (void)formatActualQuality;
+ (NSURL *)pieceURL:(NSURL *)url tile:(FlameTile *)tile cols:(int)cols;

@property(retain) NSString *actualQuality;
@property         BOOL transparent;
@property         int  outputHeight;
@property         int  outputWidth;
@property         int  quality;
@property (retain) FlameTileArray *tiles;
@property (readonly) NSProgressIndicator *progressBar;


@end
