/*
 Copyright 2009 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>
#import "FlameData.h"
#import "PreviewController.h"
#import "PreferenceController.h"
#import "FlameDocument.h"
#import "FlamePreviewView.h"
#import "RenderThread.h"
#import "QualityUtils.h"
#import "AppController.h"
#import "FlameTileArray.h"
#import "AspectRatioController.h"
#import "HelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

@implementation PreviewController
@synthesize transparent;
@synthesize quality;
@synthesize statsLabel;
@synthesize infoLabel;
@synthesize printStatsLabel;
@synthesize printStatsLabel2;
@synthesize printStatsLabel3;
@synthesize aspectRatioLocked;
@synthesize aspectRatio;
@synthesize tiles;
@synthesize editMode;
@synthesize referencePoint;
@synthesize showLocation;
@synthesize lockCropAR;
@synthesize drawer;
@synthesize drawerRight;
@synthesize origContentViewSize;
@synthesize orientation;

- (id)init
{
	self = [super initWithWindowNibName:@"PreviewFlame"];
	thread = nil;
	statsLabel = nil;
	infoLabel = nil;
	printStatsLabel = nil;
	tiles = nil;
	drawer = nil;
	drawerRight = nil;
	editMode = [[NSNumber numberWithInt:NoEditMode] retain];
	referencePoint = NSMakePoint(0.f, 0.f); // center of scaling/rotation is in center of view
	showLocation = NO;
	lockCropAR = [[NSNumber numberWithInt:NSOffState] retain];
	currentAspectRatioTag = AnyAR;
	orientation           = [[NSNumber numberWithInt:horizontalCrop] retain];
    return self;
}

- (void)dealloc
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];	
	[nc removeObserver:self];
	
	FlameDocument *doc = [self document];
	[doc removeObserver:self forKeyPath:@"currentFrameIndex"];
	[self removeObserver:self forKeyPath:@"editMode"];
	
	[tiles release];
	[statsLabel release];
	[infoLabel release];
	[printStatsLabel release];
	[drawer release];
	[drawerRight release];
	[super dealloc];
}

- (void) threadDone {
	delete thread;
	thread = nil;
}

- (void) workerThreadDone {
	delete workerThread;
	workerThread = nil;
}

- (void) printThreadDone {
	delete printThread;
	printThread = nil;
}


- (NSString *)windowTitleForDocumentDisplayName:(NSString *)displayName
{
	return [NSString stringWithFormat:@"Preview - %@", displayName];
}

- (unsigned)tileCount {
	return (tiles) ? [tiles count] : 1;
}

- (void)renderAsPreview:(BOOL)useDefaults
{
	FlameDocument *doc = [self document];
	
	if (doc) {		
		NSWindow *win      = [view window];
		NSRect rect        = [view bounds];
		
		if (transparent) {
			[win setAlphaValue:0.7f];
		}

		[self setTiles:[AppController splitAreaIntoTiles:rect.size.width * rect.size.height 
											 width:rect.size.width 
											height:rect.size.height
												   flame:[doc getCurrentFlame] 
												 flipped:NO]];

		thread = new RenderThread([doc flameData], rect.size.width, rect.size.height, transparent, view, nil, nil, self);
		if (useDefaults) {
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			quality = [defaults integerForKey:Flam4PreviewQualityKey];
		}
		int batches = ceil([QualityUtils batchesForDesired:quality width:rect.size.width height:rect.size.height tiles:[tiles count]]);
		[tiles setNumBatches:batches];
		
		pthread_t threadHandle;										
		pthread_create(&threadHandle, NULL, RenderThread::RenderToPreview, thread);
	}
}

- (NSSize)sizeForARinBounds:(NSSize)bounds aspectRatio:(float)_aspectRatio {
	float w = bounds.width;
	float h = bounds.height;
	if (_aspectRatio >= 1.0f) {
		if (h >= w/_aspectRatio)
			return NSMakeSize(w, w/_aspectRatio);
		else
			return NSMakeSize(h*_aspectRatio, h);
	}
	else {
		if (w >= h*_aspectRatio)
			return NSMakeSize(h*_aspectRatio, h);
		else
			return NSMakeSize(w, w/_aspectRatio);
	}
}

- (void)viewInit {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	transparent = [defaults boolForKey:Flam4PreviewTransparencyKey];
	quality     = [defaults integerForKey:Flam4PreviewQualityKey];
	
	float desiredWidth;
	float desiredHeight;
	if ([PreferenceController previewDimensionSource] == OverrideDimensions) {
		desiredWidth    = [defaults integerForKey:Flam4PreviewWidthKey];
		desiredHeight   = [defaults integerForKey:Flam4PreviewHeightKey];
	}
	else if ([PreferenceController previewDimensionSource] == OverrideDimensionsKeepAspectRatio) {
		FlameDocument *doc = [[[self window] windowController] document];
		Flame *l = [doc getCurrentFlame];
		desiredWidth      = [defaults integerForKey:Flam4PreviewWidthKey];
		desiredHeight     = [defaults integerForKey:Flam4PreviewHeightKey];
		float _aspectRatio = l->size[0] / l->size[1];
		NSSize optimal    = [self sizeForARinBounds:NSMakeSize(desiredWidth, desiredHeight) aspectRatio:_aspectRatio];
		desiredWidth      = optimal.width;
		desiredHeight     = optimal.height;
	}
	else {
		FlameDocument *doc = [[[self window] windowController] document];
		Flame *l = [doc getCurrentFlame];
		desiredWidth  = l->size[0] * l->scale * powf(2.f, l->zoom);
		desiredHeight = l->size[1] * l->scale * powf(2.f, l->zoom);
	}
	aspectRatioLocked = [defaults boolForKey:Flam4PreviewLinkStateKey];
	[aspectRatioLockBtn setState:aspectRatioLocked];
	aspectRatio = desiredWidth/desiredHeight;
	
	// determine the height of the info bar below the image
	
	
	NSWindow *win         = [self window];
	float statusBarHeight = NSHeight([[win contentView] bounds]) - NSHeight([[self view] bounds]);
	
	[[self window] setAlphaValue:transparent ? 0.7f : 1.0f];
	[[self window] setContentSize:NSMakeSize(desiredWidth, desiredHeight + statusBarHeight)];
	
	// if we have preserve preview window aspect ratio set as a preference,
	// set aspect ratio of window to only allow window resizes that respect that ratio
	if ([defaults boolForKey:Flam4PreviewLinkStateKey])		
		[win setContentAspectRatio:NSMakeSize(desiredWidth, desiredHeight + statusBarHeight)];
	else
		[win setContentResizeIncrements:NSMakeSize(1.0f, 1.0f)];
	
	drawer = [[NSDrawer alloc] initWithContentSize:[trayView bounds].size preferredEdge:NSMinYEdge];
	[drawer setParentWindow:win];
	[drawer setContentView:trayView];
	
	drawerRight = [[NSDrawer alloc] initWithContentSize:[trayViewRight bounds].size preferredEdge:NSMaxXEdge];
	[drawerRight setParentWindow:win];
	[drawerRight setContentView:trayViewRight];
	
	[win orderFront:nil];
	
	FlameDocument *doc = [self document];
	workerThread = new RenderThread([doc flameData], desiredWidth, desiredHeight, NO, view, nil, nil, self);
	workerThread->setFlameQuality([doc flameData]->flame, (double)quality, [self tileCount]);
	
	pthread_t threadHandle;										
	pthread_create(&threadHandle, NULL, RenderThread::Render, workerThread);
}

// handle locking/unlocking of view aspectratio
- (IBAction)aspectRatioLockClicked:(id)sender {
	NSButton *btn = sender;
	
	NSWindow *win         = [self window];
	NSRect rect           = [view bounds];
	float statusBarHeight = NSHeight([[win contentView] bounds]) - NSHeight(rect);
	
	if ([btn state]  == NSOnState) {
		aspectRatioLocked = YES;
		aspectRatio = NSWidth(rect)/NSHeight(rect);
		[win setContentAspectRatio:NSMakeSize(NSWidth(rect), NSHeight(rect) + statusBarHeight)];
	}
	else {
		aspectRatioLocked = NO;
		[win setContentResizeIncrements:NSMakeSize(1.0f, 1.0f)];
	}
}

- (void)windowDidLoad {
	FlameDocument *doc = [self document];
	[goBackBtn    setHidden:[doc frameCount] == 1];
	[goForwardBtn setHidden:[doc frameCount] == 1];
	[currentFrameField setHidden:[doc frameCount] == 1];
	[goBackBtn    setEnabled:NO];
	[goForwardBtn setEnabled:YES];
	[currentFrameField setStringValue:[NSString stringWithFormat:@"%u", [doc currentFrameIndex] + 1]];
	[currentFrameField setToolTip:[NSString stringWithFormat:@"Frame %u of %u", [doc currentFrameIndex] + 1, [doc frameCount]]];

	[doc addObserver:self forKeyPath:@"currentFrameIndex" options:NSKeyValueObservingOptionNew context:nil];
	
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter]; // we want these 4 fields to respond to each keystroke when linked
	[nc addObserver:self
		   selector:@selector(textDidEndEditing:) name:NSControlTextDidEndEditingNotification object:currentFrameField];
	[nc addObserver:self 
		   selector:@selector(undoRedoNotification:) name:NSUndoManagerDidUndoChangeNotification object:[doc undoManager]];
	[nc addObserver:self 
		   selector:@selector(undoRedoNotification:) name:NSUndoManagerDidRedoChangeNotification object:[doc undoManager]];

	[self addObserver:self forKeyPath:@"editMode"       options:NSKeyValueObservingOptionNew context:nil];
}

// move the current frame
- (void)changeFrame:(int)target {
	FlameDocument *doc = [self document];
	int i = [doc currentFrameIndex];
	[doc setCurrentFrameIndex:target];
	
	int newI = [doc currentFrameIndex];
	
	if (i != newI) {
		[view setPreviousBounds:NSMakeRect(0, 0, -1, -1)]; // force a preview rerender
		[view reshape];
		
		if (newI == 0) {
			[goBackBtn    setEnabled:NO];
			[goForwardBtn setEnabled:YES];
		}
		else if (newI == [doc frameCount] - 1) {
			[goBackBtn    setEnabled:YES];
			[goForwardBtn    setEnabled:NO];
		}
		else {
			[goBackBtn    setEnabled:YES];
			[goForwardBtn setEnabled:YES];
		}
	}
}

// move the current frame
- (void)changeFrameBy:(int)increment {
	FlameDocument *doc = [self document];
	[self changeFrame:[doc currentFrameIndex] + increment];
}

- (IBAction)goBackClicked:(id)sender {
	[self changeFrameBy:-1];
}

- (IBAction)goForwardClicked:(id)sender {
	[self changeFrameBy:+1];
}

// toggle Info panel visibility
- (IBAction)showInfo:(id)sender {
	if ([infoPanel isVisible]) {
		[infoPanel close];
	}
	else {
		NSRect winFrame  = [[self window] frame];
		NSRect infoFrame = [infoPanel frame];
		NSPoint origin   = NSMakePoint(NSMaxX(winFrame) - 30.f, NSMinY(winFrame) + 30.f);
		[infoPanel setFrameOrigin:origin];
		[infoPanel orderFront:sender];
	}
}

- (void)textDidEndEditing:(NSNotification *)aNotification {
	if ([aNotification object] == currentFrameField) {
		[self changeFrame:[currentFrameField integerValue] - 1];
	}
}

- (void)undoRedoNotification:(NSNotification *)aNotification {
	FlameDocument *doc = [self document];
	if ([aNotification object] == [doc undoManager]) {
		[trayUndoBtn setEnabled:[[doc undoManager] canUndo]];
		[trayRedoBtn setEnabled:[[doc undoManager] canRedo]];
		[trayUndoRightBtn setEnabled:[[doc undoManager] canUndo]];
		[trayRedoRightBtn setEnabled:[[doc undoManager] canRedo]];
	}
}

- (FlamePreviewView *)view {
	return view;
}

- (NSViewController *)pageSetupController {
	return pageSetupController;
}

// Document controller will ask window if it should be closed, use this to shut down background thread
- (BOOL)windowShouldClose:(id)sender
{
	FlameDocument *doc = [self document];
	[doc removeObserver:self forKeyPath:@"currentFrameIndex"];
	// tell thread it should abort
	if (workerThread)
		workerThread->abortThread = true;
	
	// thread will be destroyed - wait for it to go down
	while (workerThread != nil)
	{
		usleep(100000);
	}
	return TRUE;
}

- (void)logRenderStats:(double)width height:(double)height
{
	[self setStatsLabel:[NSString stringWithFormat:@"%.0fX%.0f", width, height]];
}

- (void)logRenderStats:(NSTimeInterval)duration 
				 width:(double)width 
				height:(double)height 
			   quality:(double)actualQuality 
			numBatches:(double)numBatches 
				 tiles:(NSString *)tilesLabel
{
	if ([view frame].size.width < 400.f) 
		[self setStatsLabel:[NSString stringWithFormat:@"%.0fX%.0f %@ %0.1fs", 
						 width, height, tilesLabel, duration]];
	else
		[self setStatsLabel:[NSString stringWithFormat:@"[%.0fX%.0f]%@  %0.2f sec", 
							 width, height, tilesLabel, duration]];
	[self setInfoLabel:[NSString stringWithFormat:@"[%.0fX%.0f]%@  %0.2f sec  Quality:%.0f RenderBatches:%.0f", 
						 width, height, tilesLabel, duration, actualQuality, numBatches]];

}

// set things that need to reset every time the window is opened
- (void)startPrintProgress:(float)desiredQuality pointSize:(NSSize)size renderSize:(NSSize)renderSize resolution:(float)resolution
{
	double actualQuality = [QualityUtils actualQuality:desiredQuality width:renderSize.width height:renderSize.height tiles:[self tileCount]];
	double numBatches    = [QualityUtils batchesForDesired:desiredQuality width:renderSize.width height:renderSize.height tiles:[self tileCount]];

	[self setPrintStatsLabel:[NSString stringWithFormat:@"PrintArea:[%.2fx%.2f] inches  Resolution:%.0f ppi Quality:%.0f  Batches:%.0f", 
							  size.width/72., size.height/72., resolution, actualQuality, numBatches]];
	[self setPrintStatsLabel2:[NSString stringWithFormat:@"PrintArea:[%.0fx%.0f] pixels", renderSize.width, renderSize.height]];

	AppController *appCtrl = (AppController *)[NSApp delegate];
	double secsPerBatch    = [appCtrl secondsPerBatch];
	double seconds         = secsPerBatch * numBatches;
	double minutes         = floor(seconds/60.);
	seconds                = seconds - minutes * 60.;
	if (minutes == 0.0)
		[self setPrintStatsLabel3:[NSString stringWithFormat:@"Estimated Print Time: %.0f seconds -- may be way off first time", seconds]];
	else
		[self setPrintStatsLabel3:[NSString stringWithFormat:@"Estimated Print Time: %.0f mins  %.0f secs -- may be way off first time", minutes, seconds]];
	
	[printProgressWindow orderFront:self]; // show the progress window
}

// called when print operation is done
- (void)stopPrintProgress
{
	[printProgressWindow orderOut:self]; // hide the progress window
}

// invoke a background print rendering thread
- (pthread_t) invokeRenderPrintThread:(NSRect)rect printView:(FlamePrintView *)printView
{
	FlameDocument *doc = [self document];
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	thread = new RenderThread([doc flameData], rect.size.width, rect.size.height, transparent, nil, printView, nil, self);
	thread->setFlameQuality([doc getCurrentFlame], [defaults integerForKey:Flam4PrintQualityKey], [self tileCount]);
	pthread_t threadHandle;										
	pthread_create(&threadHandle, NULL, RenderThread::RenderPrint, thread);

	return threadHandle;
}

- (void)documentDidRunModalPrintOperation:(NSDocument *)document success:(BOOL)success contextInfo:(void *)contextInfo
{
	[self stopPrintProgress];
}

- (void)desiredQualityChanged
{
	[self renderAsPreview:YES];
}

// when current Frame Index changes, show it on window
- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
	if ([keyPath isEqual:@"currentFrameIndex"]) {
		FlameDocument *doc = [self document];
		[currentFrameField setStringValue:[NSString stringWithFormat:@"%u", [doc currentFrameIndex] + 1]];
		[currentFrameField setToolTip:[NSString stringWithFormat:@"Frame %u of %u", [doc currentFrameIndex] + 1, [doc frameCount]]];
	}
	else if ([keyPath isEqual:@"editMode"]) {
		
		if ([self isEditMode:CropEditMode]) {
			[view setNormCropRect:NSMakeRect(-1.f, -1.f, -1.f, -1.f)]; // reset the visible crop rectangle
		}
		[view setNeedsDisplay:YES];
		[[self window] invalidateCursorRectsForView:view];
		[view setAccumScroll:0.f];
	}
}

// are status bar items hidden?
- (BOOL)statusBarItemsAreHidden {
	return [aspectRatioLockBtn isHidden];
}

// show/hide the status bar items during live resizing
- (void)hideStatusBarItems:(BOOL)hide {
	FlameDocument *doc = [self document];
	// hide these only if they are originally visible
	if ([doc frameCount] > 1) {
		[goBackBtn          setHidden:hide];
		[goForwardBtn       setHidden:hide];
		[currentFrameField  setHidden:hide];
	}
	[infoBtn            setHidden:hide];
	[aspectRatioLockBtn setHidden:hide];
	[trayOpenCloseBtn   setHidden:hide];
}

- (BOOL)validateUserInterfaceItem:(id <NSValidatedUserInterfaceItem>)anItem
{
    SEL theAction = [anItem action];
    if (theAction == @selector(goForwardClicked:))
    {
		if ([[self document] currentFrameIndex] == [[self document] frameCount] - 1)
			return NO;
		else 
			return YES;
    } else if (theAction == @selector(goBackClicked:))  {
		if ([[self document] currentFrameIndex] == 0)
			return NO;
		else return YES;
	}
	// subclass of NSDocument, so invoke super's implementation
	return [NSApp validateUserInterfaceItem:anItem];
}

- (IBAction)editModeChange:(id)sender {
	editMode = [[NSNumber numberWithInt:(EditMode)[editModeBtns selectedColumn]] retain];
}

- (IBAction)editModeRightChange:(id)sender {
	editMode = [[NSNumber numberWithInt:(EditMode)[editModeRightBtns selectedColumn]] retain];
}

- (IBAction)toggleDrawer:(id)sender {
	if ([view bounds].size.width >= [view bounds].size.height) {
		NSDrawerState state = [drawer state];
		if (NSDrawerOpeningState == state || NSDrawerOpenState == state) {
			[drawer close];
			[self setEditMode:[NSNumber numberWithInt:NoEditMode]]; // turn off edit mode
		} else {
			[self checkTrayUndoRedoBtns:[self document]];
			[drawer openOnEdge:NSMinYEdge];
		}
	}
	else {
		NSDrawerState state = [drawerRight state];
		if (NSDrawerOpeningState == state || NSDrawerOpenState == state) {
			[drawerRight close];
			[self setEditMode:[NSNumber numberWithInt:NoEditMode]]; // turn off edit mode
		} else {
			[self checkTrayUndoRedoBtns:[self document]];
			[drawerRight openOnEdge:NSMaxXEdge];
		}
	}
}

- (void)windowDidResize:(NSNotification *)notification {
	if ([view bounds].size.width >= [view bounds].size.height) {
		if ([drawerRight state] == NSDrawerOpenState || [drawerRight state] == NSDrawerOpeningState) {
			[drawerRight close];
			[drawer open];
		}
	}
	else {
		if ([drawer state] == NSDrawerOpenState || [drawer state] == NSDrawerOpeningState) {
			[drawer close];
			[drawerRight open];
		}
	}
}

- (void)checkTrayUndoRedoBtns:(NSDocument *)doc {
	[trayUndoBtn setEnabled:[[doc undoManager] canUndo]];
	[trayRedoBtn setEnabled:[[doc undoManager] canRedo]];
	[trayUndoAllBtn setEnabled:[[doc undoManager] canUndo]];
	[trayUndoRightBtn setEnabled:[[doc undoManager] canUndo]];
	[trayRedoRightBtn setEnabled:[[doc undoManager] canRedo]];
	[trayUndoAllRightBtn setEnabled:[[doc undoManager] canUndo]];
}

- (IBAction)resetCameraChanges:(id)sender {
	FlameDocument *doc = [self document];
	[doc restoreOriginalFlameCoords];
	[doc updateChangeCount:NSChangeCleared];
	[self checkTrayUndoRedoBtns:doc];
	[self setReferencePoint:NSMakePoint(0.f, 0.f)];	

	[self setEditMode:[NSNumber numberWithInt:NoEditMode]]; // turn off edit mode - prevents cropping from resize
	[[self window] invalidateCursorRectsForView:view];
	
	NSSize currentContentSize  = [[[self window] contentView] bounds].size;

	if (NSEqualSizes(currentContentSize, origContentViewSize))
		[self renderAsPreview:YES];
	else
		[[view window] setContentSize:origContentViewSize];
}

- (IBAction)RP_center:(id)sender {
	referencePoint = NSMakePoint(0.f, 0.f); // center of scaling/rotation is in center of view
	[[self window] invalidateCursorRectsForView:view];
	[view setNeedsDisplay:YES];
}

// toggle whether we display mouse location info
- (IBAction)toggleShowMouseLocation:(id)sender {
	NSMenuItem *item = sender;
	if ([item state] == NSOnState) {
		[item setState:NSOffState];
		showLocation = NO;
		[[[self view] window] setAcceptsMouseMovedEvents:NO];
		[[self view] setNeedsDisplay:YES];
	}
	else {
		[item setState:NSOnState];
		showLocation = YES;
		[[[self view] window] setAcceptsMouseMovedEvents:YES];
	}
}

- (IBAction)rotate90Left:(id)sender {
	[[self document] rotateContentView90DegreesLeft:YES];
}

- (IBAction)rotate90Right:(id)sender {
	[[self document] rotateContentView90DegreesLeft:NO];
}


- (BOOL)isEditMode:(EditMode)_editMode {
	return (EditMode)[editMode intValue] == _editMode;
}

- (BOOL)validateMenuItem:(NSMenuItem *)item {
	NSInteger tag = [item tag];
	switch (tag) {
		case AnyAR:
		case AR_1x1:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			return YES;
		case AR_3x2:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			if ([orientation intValue] == horizontalCrop)
				[item setTitle:@"3x2"];
			else
				[item setTitle:@"2x3"];
			return YES;
		case AR_4x3:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			if ([orientation intValue] == horizontalCrop)
				[item setTitle:@"4x3"];
			else
				[item setTitle:@"3x4"];
			return YES;
		case AR_5x4:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			if ([orientation intValue] == horizontalCrop)
				[item setTitle:@"5x4"];
			else
				[item setTitle:@"4x5"];
			return YES;
		case AR_16x9:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			if ([orientation intValue] == horizontalCrop)
				[item setTitle:@"16x9"];
			else
				[item setTitle:@"9x16"];
			return YES;
		case AR_16x10:
			[item setState:((tag == currentAspectRatioTag) ? NSOnState : NSOffState)];
			if ([orientation intValue] == horizontalCrop)
				[item setTitle:@"16x10"];
			else
				[item setTitle:@"10x16"];
			return YES;
		default:
			return YES;
	}
	
}

- (BOOL)permanentMenuItems:(NSInteger)tag {
	switch (tag) {
		case CustomAR:
		case AR_1x1:
		case AR_3x2:
		case AR_4x3:
		case AR_5x4:
		case AR_16x9:
		case AR_16x10:
			return YES;
	}
	return NO;
}

- (IBAction)radioAction:(id)sender {
	currentAspectRatioTag = [sender tag];
	NSString *label       = [sender title];
	[view changeCropAspectRatio:[AspectRatioController aspectRatioForString:label orientation:orientation]];
}

// modify the aspectRatioButton's menu items and select this label
- (void)modifyAspectRatioButton:(NSString *)label {
	if (label != nil) {
		// select the menu item that has its label the same as ours
		// if it does not exist, add it, and assign a random number as its tag
		if ([aspectRatioPopUpBtn itemWithTitle:label] != nil) {
			[aspectRatioPopUpBtn setTitle:label];
			NSMenuItem *mi2 = [aspectRatioPopUpBtn selectedItem];
			[mi2 setState:NSOnState];
			
			[aspectRatioPopUpRightBtn setTitle:label];
			mi2 = [aspectRatioPopUpRightBtn selectedItem];
			[mi2 setState:NSOnState];
			
			currentAspectRatioTag = [mi2 tag];
		}
		else { // that aspect ratio is not in the menu right now
			// remove any temporary menu items first
			for (NSMenuItem *mi in [aspectRatioPopUpBtn itemArray]) {
				if (! [self permanentMenuItems:[mi tag]]) {
					[aspectRatioPopUpBtn removeItemAtIndex:
					 [aspectRatioPopUpBtn indexOfItemWithTag:[mi tag]]];
				}
			}
			for (NSMenuItem *mi in [aspectRatioPopUpRightBtn itemArray]) {
				if (! [self permanentMenuItems:[mi tag]]) {
					[aspectRatioPopUpRightBtn removeItemAtIndex:
					 [aspectRatioPopUpRightBtn indexOfItemWithTag:[mi tag]]];
				}
			}
			
			currentAspectRatioTag = rand();
			[aspectRatioPopUpBtn setTitle:label];
			NSMenuItem *mi2 = [aspectRatioPopUpBtn selectedItem];
			[mi2 setState:NSOnState];
			[mi2 setTag:currentAspectRatioTag];
			
			[aspectRatioPopUpRightBtn setTitle:label];
			mi2 = [aspectRatioPopUpRightBtn selectedItem];
			[mi2 setState:NSOnState];
			[mi2 setTag:currentAspectRatioTag];
		}
	}
}

// modify the aspectRatioButton's menu items and select this label
- (void)changeAspectRatioButton:(float)_aspectRatio {
	if ([orientation intValue] == verticalCrop)
		_aspectRatio = 1.f/_aspectRatio;
	NSString *label = [AspectRatioController stringForAspectRatio:[NSNumber numberWithFloat:_aspectRatio] orientation:orientation];
	[self modifyAspectRatioButton:label];
}

// handle the custom aspect ratio menu selection of the aspect ratio popup button
- (IBAction)customAspectRatio:(id)sender {
	AspectRatioController *ctrl = [[AspectRatioController alloc] initWithOrientation:[orientation intValue]];	
	[ctrl window];
	[ctrl showWindow:sender];
	
	NSInteger result = [NSApp runModalForWindow:[ctrl window]];
	if (result == NSRunStoppedResponse) {
		NSString *label = [ctrl aspectRatioAsString];
		[self modifyAspectRatioButton:label];
		if ([orientation intValue] == horizontalCrop) {
			[view changeCropAspectRatio:[AspectRatioController aspectRatioForString:label orientation:orientation]];
		}
		else {
			float _aspectRatio = 1.f/[[ctrl aspectRatio] floatValue];
			[view changeCropAspectRatio:_aspectRatio];
		}
	}
	else {
		NSString *label = [AspectRatioController stringForAspectRatio:[NSNumber numberWithFloat:[view cropAspectRatio]] orientation:orientation];
		[aspectRatioPopUpBtn setTitle:label];
		[aspectRatioPopUpRightBtn setTitle:label];
	}
	[ctrl release];
}

// modify the aspectRatioButton's menu items and select this label
- (IBAction)flipCropOrientation:(id)sender {
	[view changeCropAspectRatio:1.f/[view cropAspectRatio]];
	
	// this will change all of the menu item titles for the new orientation
	for (NSMenuItem *mi in [aspectRatioPopUpBtn itemArray])
		[self validateMenuItem:mi];
	for (NSMenuItem *mi in [aspectRatioPopUpRightBtn itemArray])
		[self validateMenuItem:mi];
	
	NSString *label = [AspectRatioController stringForAspectRatio:[NSNumber numberWithFloat:[view cropAspectRatio]] orientation:orientation];
	[self modifyAspectRatioButton:label];
}

- (IBAction)help:(id)sender {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"howtoEditView" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"EditViewCamera"  inBook:locBookName];
	}
}

@end
