//
//  ImageRepTileArray.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import "ImageRepTileArray.h"
#import "FlameTile.h"


@implementation ImageRepTileArray
@synthesize rows;
@synthesize cols;
@synthesize array;
@synthesize flipped;

- (id)initWithRows:(int)_rows cols:(int)_cols  flipped:(BOOL)_flipped
{
	self = [super init];
	if (self) {
		array = [[NSMutableArray arrayWithCapacity:_rows * _cols] retain];
		for (int i = 0; i < _rows * _cols; i++)
			[array insertObject:[NSNull null] atIndex:i];
		rows = _rows;
		cols = _cols;
		flipped = _flipped;
		overlap = [FlameTile overlap];
	}
	return self;
}

+ (ImageRepTileArray *)arrayWithRows:(int)_rows cols:(int)_cols  flipped:(BOOL)flipped
{
	return [[[ImageRepTileArray alloc] initWithRows:_rows cols:_cols flipped:flipped] autorelease];
}

- (void)dealloc
{
	[array release];
	[super dealloc];
}

- (NSBitmapImageRep *)tileAtIndex:(int)index {
	return [array objectAtIndex:index];
}

-(void)setTileAtIndex:(int)index withObject:(NSBitmapImageRep *)object
{
	[array replaceObjectAtIndex:index withObject:object];
}

-(void)setTileAtRow:(int)row col:(int)col withObject:(NSBitmapImageRep *)object
{
	[self setTileAtIndex:row * cols + col withObject:object];
}

- (float)overlapTop:(int)row {
	return row > 0 ? overlap : 0.f;
}

- (float)overlapBottom:(int)row {
	return (row < rows - 1) ? overlap : 0.f;
}

- (float)overlapLeft:(int)column {
	return column > 0 ? overlap : 0.f;
}

- (float)overlapRight:(int)column {
	return (column < cols - 1) ? overlap : 0.f;
}


-(NSBitmapImageRep *)tileAtRow:(int)row col:(int)col
{
	return [array objectAtIndex:row * cols + col];
}

- (NSString *)labelDescription {
	if (rows == 1 && cols == 1)
		return @"";
	else if (rows == 1 && cols > 1)
		return [NSString stringWithFormat:@"(%uHT)", cols];
	else if (rows > 1 && cols == 1)
		return [NSString stringWithFormat:@"(%uVT)", rows];
	else
		return [NSString stringWithFormat:@"(%uHT%uVT)", cols, rows];
}

// create a large image by combining all of the tiles
-(NSBitmapImageRep *)combinedImageRepNoOverlap
{
	for (int i = 0; i < [array count]; i++) {
		if ([array objectAtIndex:i] == [NSNull null]) {
			NSException *exception = [NSException exceptionWithName:@"MissingImagesException"
															reason:@"Not all tile images have been processed"  userInfo:nil];
			@throw exception;
		}		
	}
	if (rows == 1 && cols == 1)
		return [self tileAtRow:0 col:0];

	int combinedBytesPerRow  = 0;
	int combinedPixelsWide   = 0;
	int combinedPixelsHigh   = 0;
	for (int j = 0; j < cols; j++) {
		combinedBytesPerRow += [[self tileAtRow:0 col:j] bytesPerRow];
		combinedPixelsWide += [[self tileAtRow:0 col:j] pixelsWide];
	}
	for (int i = 0; i < rows; i++) {
		combinedPixelsHigh += [[self tileAtRow:i col:0] pixelsHigh];
	}
	
	NSBitmapImageRep *rep = [self tileAtRow:0 col:0];
	
	NSBitmapImageRep * combinedImageRep;
	combinedImageRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL											  
																pixelsWide:combinedPixelsWide
																pixelsHigh:combinedPixelsHigh
															 bitsPerSample:[rep bitsPerSample]
														   samplesPerPixel:[rep samplesPerPixel]
																  hasAlpha:[rep hasAlpha]
																  isPlanar:NO
															colorSpaceName:NSCalibratedRGBColorSpace
															   bytesPerRow:combinedBytesPerRow
															  bitsPerPixel:[rep bitsPerPixel]] retain];

	// combine them row by row
	unsigned char *pixels  = (unsigned char *)[combinedImageRep bitmapData];

	if (cols == 1) {
		// simpler case
		if (flipped) {
			for (int i = rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order
				NSBitmapImageRep *tile    = [self tileAtRow:i col:0];
				unsigned tileWidth        = [tile bytesPerRow];
				unsigned tileHeight       = [tile pixelsHigh];
				unsigned char *tilePixels = (unsigned char *)[tile bitmapData];
				memcpy((void *)pixels, tilePixels, tileWidth * tileHeight);
				pixels += tileWidth * tileHeight;
			}
		}
		else {
			for (int i = 0; i < rows; i++) {  // for each row of pixels in reverse order
				NSBitmapImageRep *tile    = [self tileAtRow:i col:0];
				unsigned tileWidth        = [tile bytesPerRow];
				unsigned tileHeight       = [tile pixelsHigh];
				unsigned char *tilePixels = (unsigned char *)[tile bitmapData];
				memcpy((void *)pixels, tilePixels, tileWidth * tileHeight);
				pixels += tileWidth * tileHeight;
			}
		}
	}
	else {
		// interleave the pixels
		for (int i = rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order
			NSBitmapImageRep *col0Tile   = [self tileAtRow:i col:0];
			unsigned      tilePixelsHigh = [col0Tile pixelsHigh];
			
			for (int k = 0; k < tilePixelsHigh; k++) { // for each row of pixels in this row of tiles
				for (int j = 0; j < cols; j++) {     // for each column of tiles
					NSBitmapImageRep *tile    = [self tileAtRow:i col:j];
					unsigned tileWidth        = [tile bytesPerRow];
					unsigned char *tilePixels = (unsigned char *)[tile bitmapData] + k * tileWidth;
					memcpy((void *)pixels, tilePixels, tileWidth);
					pixels += tileWidth;
				}
			}
		}
	}
	return combinedImageRep;
}

// blend the horizontal overlapped seam between tiles
- (void)blendHorizontalOverlapRow:(unsigned)i Column:(unsigned)j pixelRow:(unsigned)k rightPixels:(unsigned char *)rightPixels  rightTarget:(unsigned char *)rightTarget {
	NSBitmapImageRep *rightTile    = [self tileAtRow:i col:j];
	NSBitmapImageRep *leftTile     = [self tileAtRow:i col:j - 1];
	int samplesPerPixel            = [rightTile samplesPerPixel];
	unsigned overlapBytesLeft      = [self overlapLeft:j]  * [rightTile bitsPerPixel]/8; // overlap for a single strip - not whole tile
	unsigned leftTileWidth         = [leftTile bytesPerRow];
	unsigned leftOverlapBytesRight = [self overlapRight:j - 1] * [leftTile bitsPerPixel]/8;
	unsigned overlappedPixels      = [self overlapRight:j - 1] + [self overlapLeft:j];
	unsigned char *leftBytes       = (unsigned char *)[leftTile bitmapData] + k * leftTileWidth + leftTileWidth - leftOverlapBytesRight - overlapBytesLeft;
	unsigned char *rightBytes      = rightPixels - overlapBytesLeft;
	unsigned char *target          = rightTarget - leftOverlapBytesRight;
	float ramp                     = 1.f/((float)overlappedPixels + 1);
	
	for (unsigned n = 1; n <= overlappedPixels; n ++) {
		target[0] = (float)n * ramp * rightBytes[0] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[0];
		target[1] = (float)n * ramp * rightBytes[1] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[1];
		target[2] = (float)n * ramp * rightBytes[2] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[2];
		if (samplesPerPixel == 4) // do we have an alpha channel?
			target[3] = (float)n * ramp * rightBytes[3] + (float)(overlappedPixels - n + 1) * ramp * leftBytes[3];
		
		target     += samplesPerPixel;
		rightBytes += samplesPerPixel;
		leftBytes  += samplesPerPixel;
	}
}

// blend the vertical overlapped seam between tiles - we process from the bottom tile upwards flipping rows as we go
- (void)blendVerticalOverlapRow:(unsigned)i Column:(unsigned)j topTarget:(unsigned char *)topTarget {
	NSBitmapImageRep *bottomTile   = [self tileAtRow:i col:j];
	NSBitmapImageRep *topTile      = [self tileAtRow:i - 1 col:j];
	int samplesPerPixel            = [bottomTile samplesPerPixel];
	unsigned tileWidth             = [bottomTile bytesPerRow];
	unsigned tilePixelsWide        = [bottomTile pixelsWide];
	unsigned overlapBytesTop       = tileWidth * [self overlapTop:i];
	unsigned topOverlapBytesBtm    = tileWidth * [self overlapBottom:i - 1];
	unsigned overlappedPixels      = [self overlapBottom:i - 1] + [self overlapTop:i];
	unsigned char *topBytes        = (unsigned char *)[topTile bitmapData];
	unsigned char *bottomBytes     = (unsigned char *)[bottomTile bitmapData] + [bottomTile pixelsHigh]*tileWidth - overlapBytesTop - topOverlapBytesBtm;
	unsigned char *target          = topTarget - topOverlapBytesBtm;
	float ramp                     = 1.f/((float)overlappedPixels + 1);
	
	for (unsigned n = 1; n <= overlappedPixels; n ++) {
		for (unsigned m = 0; m < tilePixelsWide; m++) {
			target[0] = (float)n * ramp * topBytes[0] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[0];
						target[1] = (float)n * ramp * topBytes[1] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[1];
						target[2] = (float)n * ramp * topBytes[2] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[2];
						if (samplesPerPixel == 4) // do we have an alpha channel?
							target[3] = (float)n * ramp * topBytes[3] + (float)(overlappedPixels - n + 1) * ramp * bottomBytes[3];
			
			target      += samplesPerPixel;
			bottomBytes += samplesPerPixel;
			topBytes    += samplesPerPixel;
		}
	}
}

// blend the vertical overlapped seam between tiles - we process from the bottom tile upwards flipping rows as we go
- (void)blendVerticalOverlapRow:(unsigned)i Column:(unsigned)j bottomTarget:(unsigned char *)bottomTarget {
	NSBitmapImageRep *bottomTile = [self tileAtRow:i col:j];
	NSBitmapImageRep *topTile    = [self tileAtRow:i - 1 col:j];	
	int samplesPerPixel          = [topTile samplesPerPixel];
	unsigned tileWidth           = [topTile bytesPerRow];
	unsigned tilePixelsWide      = [topTile pixelsWide];
	
	// calculate top and bottom overlap in bytes - not pixels
	unsigned topOverlapBytesBtm  = tileWidth * [self overlapBottom:i - 1];	
	unsigned botOverlapBytesTop  = tileWidth * [self overlapTop:i];
	unsigned overlappedPixels    = [self overlapBottom:i - 1] + [self overlapTop:i];
	unsigned char *topBytes      = (unsigned char *)[topTile bitmapData] + [topTile pixelsHigh]*tileWidth - topOverlapBytesBtm - botOverlapBytesTop;
	unsigned char *bottomBytes   = (unsigned char *)[bottomTile bitmapData];
	unsigned char *target        = bottomTarget - topOverlapBytesBtm;
	float ramp                   = 1.f/((float)overlappedPixels + 1);
	
	for (unsigned n = 1; n <= overlappedPixels; n ++) {
		for (unsigned m = 0; m < tilePixelsWide; m++) {
			target[0] = (float)n * ramp * bottomBytes[0] + (float)(overlappedPixels - n + 1) * ramp * topBytes[0];
			target[1] = (float)n * ramp * bottomBytes[1] + (float)(overlappedPixels - n + 1) * ramp * topBytes[1];
			target[2] = (float)n * ramp * bottomBytes[2] + (float)(overlappedPixels - n + 1) * ramp * topBytes[2];
			if (samplesPerPixel == 4) // do we have an alpha channel?
				target[3] = (float)n * ramp * bottomBytes[3] + (float)(overlappedPixels - n + 1) * ramp * topBytes[3];
			
			// for debug visualization only
			//target[0] = 0.5f * target[0] + 0.5f;  // overlay a red on top of overlapped zone so we can see it !!
			//target[1] = 0.5f * target[1];
			//target[2] = 0.5f * target[2];
			
			target      += samplesPerPixel;
			bottomBytes += samplesPerPixel;
			topBytes    += samplesPerPixel;
		}
	}
}	

// create a large image by combining all of the tiles
-(NSBitmapImageRep *)combinedImageRep
{
	for (int i = 0; i < [array count]; i++) {
		if ([array objectAtIndex:i] == [NSNull null]) {
			NSException *exception = [NSException exceptionWithName:@"MissingImagesException"
															 reason:@"Not all tile images have been processed"  userInfo:nil];
			@throw exception;
		}		
	}
	if (rows == 1 && cols == 1)
		return [self tileAtRow:0 col:0];
	
	NSBitmapImageRep *rep = [self tileAtRow:0 col:0];
	int samplesPerPixel   = [rep samplesPerPixel];
	
	int combinedBytesPerRow  = 0;
	int combinedPixelsWide   = 0;
	int combinedPixelsHigh   = 0;
	for (int j = 0; j < cols; j++) {
		combinedBytesPerRow += [[self tileAtRow:0 col:j] bytesPerRow] - ([self overlapLeft:j] + [self overlapRight:j]) * [rep bitsPerPixel]/8;
		combinedPixelsWide += [[self tileAtRow:0 col:j] pixelsWide]   - ([self overlapLeft:j] + [self overlapRight:j]);
	}
	for (int i = 0; i < rows; i++) {
		combinedPixelsHigh += [[self tileAtRow:i col:0] pixelsHigh]   - ([self overlapTop:i] + [self overlapBottom:i]);
	}
	
	NSBitmapImageRep * combinedImageRep;
	combinedImageRep = [[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL											  
																pixelsWide:combinedPixelsWide
																pixelsHigh:combinedPixelsHigh
															 bitsPerSample:[rep bitsPerSample]
														   samplesPerPixel:samplesPerPixel
																  hasAlpha:[rep hasAlpha]
																  isPlanar:NO
															colorSpaceName:NSCalibratedRGBColorSpace
															   bytesPerRow:combinedBytesPerRow
															  bitsPerPixel:[rep bitsPerPixel]] retain];
	
	// combine them row by row
	unsigned char *pixels  = (unsigned char *)[combinedImageRep bitmapData];
	
	if (cols == 1) {
		// simpler case
		if (flipped) {
			for (int i = rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order - flips the image
				NSBitmapImageRep *tile    = [self tileAtRow:i col:0];
				unsigned tileWidth        = [tile bytesPerRow];
				unsigned tileHeight       = [tile pixelsHigh];
				// calculate top and bottom overlap in bytes - not pixels
				unsigned overlapBytesTop  = [tile pixelsWide] * [self overlapTop:i]    * [tile bitsPerPixel]/8;
				unsigned overlapBytesBtm  = [tile pixelsWide] * [self overlapBottom:i] * [tile bitsPerPixel]/8;
				
				unsigned stride           = tileWidth * tileHeight - overlapBytesTop - overlapBytesBtm;	// pointer advance amount for this tile			
				unsigned char *tilePixels = (unsigned char *)[tile bitmapData] + overlapBytesBtm;		// we dont blit the leading overlap
				
				memcpy((void *)pixels, tilePixels, stride);
				
				// blend our bottom overlap with our top neighbor's top overlap - remember we are flipping pixel rows
				if (i < rows - 1) { // top row has no bottom neighbor
					[self blendVerticalOverlapRow:i + 1 Column:0 topTarget:pixels];
				}			
				pixels += stride;
			}
		}
		else {
			for (int i = 0; i < rows; i++) {
				NSBitmapImageRep *tile    = [self tileAtRow:i col:0];
				unsigned tileWidth        = [tile bytesPerRow];
				unsigned tileHeight       = [tile pixelsHigh];
				// calculate top and bottom overlap in bytes - not pixels
				unsigned overlapBytesTop  = [tile pixelsWide] * [self overlapTop:i]    * [tile bitsPerPixel]/8;
				unsigned overlapBytesBtm  = [tile pixelsWide] * [self overlapBottom:i] * [tile bitsPerPixel]/8;
				
				unsigned stride           = tileWidth * tileHeight - overlapBytesTop - overlapBytesBtm;	// pointer advance amount for this tile			
				unsigned char *tilePixels = (unsigned char *)[tile bitmapData] + overlapBytesTop;
				
				memcpy((void *)pixels, tilePixels, stride);
				
				// blend our top overlap with our top neighbor's bottom overlap
				if (i > 0) { // top row has no top neighbor
					[self blendVerticalOverlapRow:i Column:0 bottomTarget:pixels];
				}			
				pixels += stride;
			}
		}
	}
	else {
		// interleave the pixels
		for (int i = rows - 1; i >= 0; i--) {  // for each row of pixels in reverse order - flips the image
			NSBitmapImageRep *col0Tile   = [self tileAtRow:i col:0];
			unsigned      tilePixelsHigh = [col0Tile pixelsHigh] - [self overlapBottom:i]; // we dont copy the bottom overlap
			unsigned char * pixelsForRow = pixels;
			for (int k = [self overlapBottom:i]; k < tilePixelsHigh; k++) { // for each row of pixels in this row of tiles
				for (int j = 0; j < cols; j++) {     // for each column of tiles
					NSBitmapImageRep *tile    = [self tileAtRow:i col:j];
					unsigned tileWidth        = [tile bytesPerRow];
					unsigned overlapBytesLeft = [self overlapLeft:j]  * [tile bitsPerPixel]/8; // overlap for a single strip - not whole tile
					unsigned overlapBytesRight= [self overlapRight:j] * [tile bitsPerPixel]/8;

					
					unsigned stride           = tileWidth - overlapBytesLeft - overlapBytesRight;	// pointer advance amount for this strip			
					unsigned char *tilePixels = (unsigned char *)[tile bitmapData] + k * tileWidth + overlapBytesLeft; // k is already adjusted for top overlap
					memcpy((void *)pixels, tilePixels, stride);
					
					// blend our left overlap with our left neighbor's right overlap
					if (j > 0) { // first column has no left neighbor
						[self blendHorizontalOverlapRow:i Column:j pixelRow:k rightPixels:tilePixels  rightTarget:pixels];
					}
					pixels += stride;
				}
			}
			
			// blend our bottom overlap with our top neighbor's top overlap - remember we are flipping pixel rows
			if (i < rows - 1) { // top column has no bottom neighbor
				[self blendVerticalOverlapRow:i + 1 Column:0 topTarget:pixelsForRow];
			}			
		}
	}
	return combinedImageRep;
}


@end
