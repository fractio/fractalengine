/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "QualityUtils.h"
#import "Defines.h"


@implementation QualityUtils

+ (double)actualQuality:(double)desiredQuality width:(double)width height:(double)height tiles:(double)tiles {
	double batches = ceil(width * height / tiles * desiredQuality / (1024. * 32. * NUM_ITERATIONS));
	return 1024. * 32. * NUM_ITERATIONS * batches/(width * height);
}

+ (double)batchesForDesired:(double)desiredQuality width:(double)width height:(double)height tiles:(double)tiles {
	return ceil(width * height / tiles * desiredQuality / (1024. * 32. * NUM_ITERATIONS));
}

+ (double)batchesForActual:(double)actualQuality width:(double)width height:(double)height tiles:(double)tiles {
	return floor(width * height / tiles * actualQuality / (1024. * 32. * NUM_ITERATIONS));
}

+ (double)actualQualityWith:(double)batches width:(double)width height:(double)height tiles:(double)tiles {
	return 1024. * 32. * NUM_ITERATIONS * batches/(width * height / tiles);
}

@end
