/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import <Cocoa/Cocoa.h>
#import "FlameDocument.h"
#import "Globals.h"
#import "PreferenceController.h"
#import "PreviewController.h"
#import "FlamePrintView.h"
#import "FlamePreviewView.h"
#import "FlameParse.h"
#import "FlameOriginalState.h"

extern int quality;

@implementation FlameDocument

@synthesize flameData;
@synthesize frameCount;
@synthesize frameOriginals;

- (id)init
{
    self = [super init];
	
	flameData = nil;
	currentFrameIndex = 0;
	frameOriginals = nil;
	xmlData = nil;
    return self;
}

- (void)dealloc
{
	flameParameterList * link = flameData;
	while (link) {
		flameParameterList * it = link;
		delete link->flame;
		NSString *name = link->flameMeta->name;
		[name release];
		link = link->nextFlame;
		delete it;
	}
	
	[xmlData release];
	[frameOriginals release];
	[super dealloc];
}

- (BOOL)validateUserInterfaceItem:(id < NSValidatedUserInterfaceItem >)anItem {
	SEL theAction = [anItem action];
    if (theAction == @selector(saveDocument:))
    {
		return [self isDocumentEdited];
    }
    else if (theAction == @selector(saveDocumentAs:))
    {
		return YES;
    }
    else if (theAction == @selector(revertDocumentToSaved:))
    {
		return [self isDocumentEdited];
    }
	
	// subclass of NSDocumentController, so invoke super's implementation
	return [super validateUserInterfaceItem:anItem];
}



- (int)currentFrameIndex {
	return currentFrameIndex;
}

- (void)setCurrentFrameIndex:(int)i {
	if (i < 0)				i = 0;
	if (i >= frameCount)	i = frameCount - 1;
	[self willChangeValueForKey:@"currentFrameIndex"];
	currentFrameIndex = i;
}

// get the ith frame's Flame - clamp to the number of frames
- (Flame *)getFlame:(int)i {
	if (i < 0)
		i = 0;
	else if (i >= frameCount)
		i = frameCount - 1;
	
	flameParameterList *link = flameData;
	flameParameterList *last = flameData;
	while (link) {
		last = link;
		if (i-- == 0)
			return link->flame;
		link = link->nextFlame;
	}
	return last->flame;
}

// get the current frame's Flame
- (Flame *)getCurrentFlame {
	return [self getFlame:currentFrameIndex];
}

// save the original coords for a specific frame
- (void)saveOriginalFrameCoords:(int)index {
	Flame *flame = [self getFlame:index];
	FlameOriginalState *original = [[FlameOriginalState alloc] initWithSize:NSMakeSize(flame->size[0],    flame->size[1]) 
																	 center:NSMakePoint(flame->center[0], flame->center[1]) 
																   rotation:flame->rotation 
																	  scale:flame->scale];
	[frameOriginals insertObject:original atIndex:index];
}

// reset to the original coords and reset undo/redo
- (void)restoreOriginalFrameCoords:(int)index {
	[[self undoManager] removeAllActions]; // clear out undo

	Flame *flame = [self getFlame:index];
	FlameOriginalState *original = [frameOriginals objectAtIndex:index];
	
	flame->size[0]   = original.size.width;
	flame->size[1]   = original.size.height;
	flame->center[0] = original.center.x;
	flame->center[1] = original.center.y;
	flame->rotation  = original.rotation;
	flame->scale     = original.scale;
}

// reset to the original coords and reset undo/redo
- (void)restoreOriginalFlameCoords {
	[self restoreOriginalFrameCoords:currentFrameIndex];
}

- (void)makeWindowControllers
{
	//[self addWindowController: [[FlameWindowController alloc] init]];
	[self addWindowController: [[PreviewController alloc] init]];
}

// called right after the Nib file is loaded
- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
		
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
	
	[aController setShouldCloseDocument:YES];
}

// create a pretty floating point value string by deleting trailing zeroes and "."
- (NSString*)prettyFloat:(float)aValue
{
	NSString*	theString = [NSString stringWithFormat:@"A%0.4lf", aValue];
	
	theString = [theString stringByTrimmingCharactersInSet:
				 [NSCharacterSet characterSetWithCharactersInString:@"0"]];
	
	theString = [theString stringByTrimmingCharactersInSet:
				 [NSCharacterSet characterSetWithCharactersInString:@"."]];
	return [theString substringWithRange:NSMakeRange(1, [theString length]-1)];
}

// update this attribute or add it to the element if it does not exist
- (void)updateAttribute:(NSString *)attrName forElement:(NSXMLElement *)ele value:(NSString *)value {
	NSXMLNode *attr = [ele attributeForName:attrName];
	if (attr)
		[attr setStringValue:value];
	else
		[ele addAttribute:[NSXMLNode attributeWithName:attrName stringValue:value]];
}

// update the flame element in the docs for changes to the item
- (void)updateFlameElement:(NSXMLElement *)ele flame:(Flame *)flame {
	NSString *center = [NSString stringWithFormat:@"%@ %@", 
						[self prettyFloat:flame->center[0]], 
						[self prettyFloat:flame->center[1]]];
	NSString *size   = [NSString stringWithFormat:@"%@ %@", 
						[self prettyFloat:flame->size[0]*flame->scale*powf(2.f,flame->zoom)], 
						[self prettyFloat:flame->size[1]*flame->scale*powf(2.f,flame->zoom)]];
	NSString *rotate = [self prettyFloat:360.0/2.0*M_PI*flame->rotation];
	NSString *scale  = [self prettyFloat:flame->scale];
	
	[self updateAttribute:@"center" forElement:ele value:center];
	[self updateAttribute:@"size"   forElement:ele value:size];
	[self updateAttribute:@"rotate" forElement:ele value:rotate];
	[self updateAttribute:@"scale"  forElement:ele value:scale];
}

// create an XML document from the flame
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
	NSXMLNode *aNode = [xmlData rootElement];
	int frameNum = -1;
	
	// look for first <flame>
	do {
		if ([aNode kind] == NSXMLElementKind && [[aNode name]isEqualToString:@"flame"]) {
			Flame *flame = [self getFlame:++frameNum];
			[self updateFlameElement:(NSXMLElement *)aNode flame:flame];
			break;
		}
	} while (aNode = [aNode nextNode]);
	
	while (aNode = [aNode nextNode]) {
		if ([aNode kind] == NSXMLElementKind && [[aNode name]isEqualToString:@"flame"]) {
			Flame *flame = [self getFlame:++frameNum];
			[self updateFlameElement:(NSXMLElement *)aNode flame:flame];
		}
	}
	return [xmlData XMLDataWithOptions:NSXMLNodePrettyPrint];
}

// read a flame document from its XML file contents
- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
	xmlData = [[NSXMLDocument alloc] initWithData:data options:0 error:outError];	
	
    if (*outError != NULL ) {
		*outError = [NSError errorWithDomain:NSOSStatusErrorDomain code:unimpErr userInfo:NULL];
		[xmlData release];
		xmlData = nil;
		return NO;
	}
	flameData = [FlameParse parseDoc:xmlData 
						  frameCount:&frameCount 
						 palettePath:[[NSBundle mainBundle] pathForResource:@"flam3-palettes" ofType:@"xml"]];
	currentFrameIndex = 0;
	
	[self setFrameOriginals:[NSMutableArray arrayWithCapacity:frameCount]];
	for (int i = 0; i < frameCount; i++)
		[self saveOriginalFrameCoords:i];
		
	[FlameParse uniqueFrameNames:flameData]; // modify frame names as needed to make them unique
	return YES;
}

// use recommended resolutions for photography here - typically bigger prints are viewed from further away
- (float)optimalResolutionFor:(NSSize)size orientation:(NSPrintingOrientation)orientation
{
	int w = size.width;
	int h = size.height;
	if (orientation == NSLandscapeOrientation) { // flip the dimesions in the check
		w = size.height;
		h = size.width;
	}
	if (w <= 4*72 && h <= 5*72)			// 4x5"
		return 360.f;
	else if (w <= 5*72 && h <= 7*72)	// 5x7"
		return 300.f;
	else if (w <= 8*72 && h <= 10*72)	// 8x10"
		return 240.f;
	else if (w <= 11*72 && h <= 14*72)	// 11x14"
		return 200.f;
	else if (w <= 16*72 && h <= 20*72)	// 16x20"
		return 180.f;
	return 140.f;
}

// handle print requests
- (void)printShowingPrintPanel:(BOOL)showPanels {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	FlamePreviewView * view = [ctrl view];
	
	// calculate the optimal printed dimensions in points
	NSRect previewRect     = [view bounds];
	float aspectRatio      = NSWidth(previewRect)/NSHeight(previewRect);
	NSPrintInfo *printInfo = [self printInfo];
	NSSize paperSize       = [printInfo paperSize];
	NSSize printableSize   = NSMakeSize(paperSize.width - [printInfo leftMargin] - [printInfo rightMargin],
									 paperSize.height - [printInfo topMargin] - [printInfo bottomMargin]);
	
	// calculate the dimensions in points of the area to hold the image
	float maxWidth  = printableSize.width;
	float maxHeight = printableSize.height;
	if (printableSize.width/aspectRatio < printableSize.height)
		maxHeight = printableSize.width/aspectRatio;
	if (printableSize.height * aspectRatio < printableSize.width)
		maxWidth  = printableSize.height * aspectRatio;
	
	NSRect viewSize = NSMakeRect(0.f, 0.f, maxWidth, maxHeight);
		
	// Obtain a custom view that will be printed
	FlamePrintView *printView = [[FlamePrintView alloc] initWithFrame:viewSize];
	[printView setFlame:[self getCurrentFlame]];
	[printView setCtrl:ctrl];

	float resolution = [self optimalResolutionFor:viewSize.size orientation:[printInfo orientation]];
	float ussf = [[[view window] screen] userSpaceScaleFactor];
	[printView setScaleFactor:resolution/(72.0f * ussf)];
	
	// use PNG quality for printing
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	int quality      = [defaults integerForKey:Flam4PngQualityKey];
	[printView setQuality:quality];
	
    // Construct the print operation and setup Print panel
    NSPrintOperation *op = [NSPrintOperation
							printOperationWithView:printView
							printInfo:[self printInfo]];
    [op setShowsPrintPanel:showPanels];
	
	// show the printProgress window
	NSSize renderSize = [printView renderDimensions:viewSize];
	[ctrl startPrintProgress:quality pointSize:viewSize.size renderSize:renderSize resolution:resolution];

	// Run operation, which shows the Print panel if showPanels was YES - note this call returns immediately
	[self runModalPrintOperation:op
						delegate:ctrl
				  didRunSelector:@selector(documentDidRunModalPrintOperation:success:contextInfo:)
					 contextInfo:NULL];

	
	[printView release];
}

// standard place to hook our custom page setup
- (BOOL)preparePageLayout:(NSPageLayout *)pageLayout {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	[pageLayout addAccessoryController:[ctrl pageSetupController]];
	return YES;
 }

// translate the fractal by x,y expressed in view coordinates
- (void)translateByViewX:(float)deltaX viewY:(float)deltaY {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	FlamePreviewView * view = [ctrl view];
	
	Flame *flame  = [self getCurrentFlame];
	
	float docViewWidth  =  flame->size[0] * flame->scale * powf(2.f, flame->zoom);
	float docViewHeight =  flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	float viewScaleX = [view bounds].size.width  / docViewWidth;
	float viewScaleY = [view bounds].size.height / docViewHeight;
	
	// first rescale translation delta based on view window size changes
	float sDeltaX = deltaX / viewScaleX;
	float sDeltaY = deltaY / viewScaleY;

	// scale deltaX, deltaY to normalized fractal coordinates
	// then unrotate & untranslate to get the new centers in normalized fractal coordinates
	float xDelta = sDeltaX/(flame->scale * powf(2.f, flame->zoom));
	float yDelta = sDeltaY/(flame->scale * powf(2.f, flame->zoom));
	
	float x1 = xDelta * cosf(-flame->rotation) - yDelta * sinf(-flame->rotation);
	float y1 = xDelta * sinf(-flame->rotation) + yDelta * cosf(-flame->rotation);
	
	flame->center[0] += x1;
	flame->center[1] += y1;

	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] translateByViewX:-deltaX viewY:-deltaY];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Translate"];
	}

	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
	
	// rerender the fractal
	[ctrl renderAsPreview:YES];
}

// translate the fractal by x,y expressed in view coordinates
- (void)zoomByWheel:(float)scaleFactor  referencePoint:(NSPoint)refPoint  {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	Flame *flame     = [self getCurrentFlame];
	
	// so undo/redo modifies the reference point to the one this operation needs
	[ctrl setReferencePoint:refPoint];
	
	float xDelta = refPoint.x * flame->size[0] / 2.f;  // refPoint is normalized between -1 and 1
	float yDelta = refPoint.y * flame->size[1] / 2.f;
	
	flame->scale   *= scaleFactor; // only used while parsing the flame file - the size[0] and size[1] are the ones to change
	flame->size[0] /= scaleFactor;
	flame->size[1] /= scaleFactor;

	float x1 = xDelta * cosf(flame->rotation) - yDelta * sinf(flame->rotation);
	float y1 = xDelta * sinf(flame->rotation) + yDelta * cosf(flame->rotation);
	
	//  reference point coordinates go from -1 to 1 with 0 at center
	//  S = scale factor
	// c0 = center of viewport in fractal coordinates unscaled
	// c1 = center of viewport in fractal coordinates scaled
	// r =  reference point (not scaled in this operation) about which scaling is performed
	// w0 = unscaled width in fractal coordinates corresponding to viewport
	// w1 = scaled width in fractal coordinates corresponding to viewport
	// w1 = w0/S
	// f0 = w0/2*r + c0  equation showing mapping from reference point coordinates and fractal coordinates
	// then c1 - c0 = (S - 1)/S * w0/2 * r
	
	flame->center[0] += (scaleFactor - 1.f)/scaleFactor * x1;
	flame->center[1] += (1.f - scaleFactor)/scaleFactor * y1; // adjust for flipped coordinates
		
	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] zoomByWheel:1/scaleFactor referencePoint:refPoint];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Scale"];
	}
	
	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
}

// rotate the fractal by degrees
- (void)rotateDegrees:(float)degrees  referencePoint:(NSPoint)refPoint {
	[self rotateRadians:degrees * (2.0*M_PI/360.0) referencePoint:refPoint];
}

// adjust file rotation based on difference between viewport and file aspect ratios
float aspectRatioAngleTransform(float angle, float phiOrig, float ARfrom, float ARto) {
	// phi = atan(ARfile/ARview * tan(theta))
	// theta = atan(ARview/ARfile * tan(phi))
	// where theta is rotation in viewport
	//       phi is rotation in file coords
	float thetaOrig;
	if (phiOrig == M_PI/2.f || phiOrig == -M_PI/2.f)
		thetaOrig = phiOrig;
	else
		thetaOrig  = atanf(ARfrom/ARto * tanf(phiOrig));
	float phi;
	if (thetaOrig + angle == M_PI/2.f || thetaOrig + angle == -M_PI/2.f)
		phi = thetaOrig + angle;
	else
		phi = atanf(ARto/ARfrom * tanf(thetaOrig + angle));
	
	// correct for quadrant
	//float a = (thetaOrig+angle)/M_PI*180.f;
	
	if (thetaOrig+angle > M_PI/2.f && thetaOrig+angle < 3*M_PI/2.f)
		phi -= M_PI;
	else if (thetaOrig+angle < -M_PI/2.f && thetaOrig+angle > -3*M_PI/2.f)
		phi -= M_PI;
	return phi;
}

// rotate the fractal by radians
- (void)rotateRadians:(float)radians  referencePoint:(NSPoint)refPoint {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	FlamePreviewView * view  = [ctrl view];
	NSRect bounds            = [view bounds];
	Flame *flame             = [self getCurrentFlame];
	
	// so undo/redo modifies the reference point to the one this operation needs
	[ctrl setReferencePoint:refPoint];
	
	// adjust file rotation based on difference between viewport and file aspect ratios
	// phi = atan(ARfile/ARview * tan(theta))
	// theta = atan(ARview/ARfile * tan(phi))
	// where theta is rotation in viewport
	//       phi is rotation in file coords
	float ARview = bounds.size.width/bounds.size.height;
	float ARfile = flame->size[0]/flame->size[1];
	float phi = aspectRatioAngleTransform(-radians, flame->rotation, ARview, ARfile);
	flame->rotation = phi;
	
	//flame->rotation -= radians; -- no adjustment case
	
	float xRP  = refPoint.x * bounds.size.width/2.f;  // refPoint is normalized between -1 and 1
	float yRP  = refPoint.y * bounds.size.height/2.f;
	
	// calculate the movement of the reference point by translating around the center of the viewport
	float xRP1 = xRP * cosf(radians) - yRP * sinf(radians);
	float yRP1 = xRP * sinf(radians) + yRP * cosf(radians);
	
	float deltaX = (xRP1 - xRP);	// translate viewport in reverse direction of reference point movement
	float deltaY = -(yRP1 - yRP);
	
	float docViewWidth  =  flame->size[0] * flame->scale * powf(2.f, flame->zoom);
	float docViewHeight =  flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	float viewScaleX = bounds.size.width  / docViewWidth;
	float viewScaleY = bounds.size.height / docViewHeight;
	
	// first rescale translation delta based on view window size changes
	float sDeltaX = deltaX / viewScaleX;
	float sDeltaY = deltaY / viewScaleY;
	
	// scale deltaX, deltaY to normalized fractal coordinates
	// then unrotate & untranslate to get the new centers in normalized fractal coordinates
	float xDelta = sDeltaX/(flame->scale * powf(2.f, flame->zoom));
	float yDelta = sDeltaY/(flame->scale * powf(2.f, flame->zoom));
	
	float x1 = xDelta * cosf(-flame->rotation) - yDelta * sinf(-flame->rotation);
	float y1 = xDelta * sinf(-flame->rotation) + yDelta * cosf(-flame->rotation);
	
	flame->center[0] += x1;
	flame->center[1] += y1;
	
	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] rotateRadians:-radians referencePoint:refPoint];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Rotate"];
	}
	
	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
	
	// rerender the fractal
	[ctrl renderAsPreview:YES];
}

// set the flame view parameters and resize window as needed
-(void)setViewStateforCenter:(NSPoint)center size:(NSSize)size winContentSize:(NSSize)viewSize scale:(float)scale {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	FlamePreviewView * view = [ctrl view];
	Flame *flame  = [self getCurrentFlame];
	
	NSPoint origCenter   = NSMakePoint(flame->center[0], flame->center[1]);
	NSSize  origSize     = NSMakeSize(flame->size[0],    flame->size[1]);
	NSSize  origViewSize = [[[view window] contentView] bounds].size;
	float   origScale    = flame->scale;
	
	flame->center[0] = center.x;
	flame->center[1] = center.y;
	flame->size[0]   = size.width;
	flame->size[1]   = size.height;
	flame->scale     = scale;
	
	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] setViewStateforCenter:origCenter 
																	 size:origSize 	
														   winContentSize:origViewSize 
																	scale:origScale];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Crop"];
	}
		
	float statusBarHeight = NSHeight([[[view window] contentView] bounds]) - NSHeight([view bounds]);
	viewSize.height += statusBarHeight;
	if (NSEqualSizes([[[view window] contentView] bounds].size, viewSize))
		[ctrl renderAsPreview:YES];
	else
		[[view window] setContentSize:viewSize];

	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
}

// crop the fractal by x,y expressed in view coordinates
- (void)cropWithRect:(NSRect)cropRect bounds:(NSSize)bounds {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	FlamePreviewView * view = [ctrl view];
	
	Flame *flame  = [self getCurrentFlame];
	
	NSPoint origCenter   = NSMakePoint(flame->center[0], flame->center[1]);
	NSSize  origSize     = NSMakeSize(flame->size[0],    flame->size[1]);
	NSSize  origViewSize = [[[view window] contentView] bounds].size;
	
	float statusBarHeight = NSHeight([[[view window] contentView] bounds]) - NSHeight([view bounds]);

	NSPoint viewCenter = NSMakePoint(bounds.width/2.f, bounds.height/2.f);
	NSPoint cropCenter = NSMakePoint(NSMidX(cropRect), NSMidY(cropRect));
	
	float deltaX = cropCenter.x - viewCenter.x;
	float deltaY = -(cropCenter.y - viewCenter.y); // adjust for flipped Y coordinate
	
	float docViewWidth  =  flame->size[0] * flame->scale * powf(2.f, flame->zoom);
	float docViewHeight =  flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	float viewScaleX = bounds.width  / docViewWidth;
	float viewScaleY = bounds.height / docViewHeight;
	
	// first rescale translation delta based on view window size changes
	float sDeltaX = deltaX / viewScaleX;
	float sDeltaY = deltaY / viewScaleY;
	
	// scale deltaX, deltaY to normalized fractal coordinates
	// then unrotate & untranslate to get the new centers in normalized fractal coordinates
	float xDelta = sDeltaX/(flame->scale * powf(2.f, flame->zoom));
	float yDelta = sDeltaY/(flame->scale * powf(2.f, flame->zoom));
	
	float x1 = xDelta * cosf(-flame->rotation) - yDelta * sinf(-flame->rotation);
	float y1 = xDelta * sinf(-flame->rotation) + yDelta * cosf(-flame->rotation);
	
	flame->center[0] += x1;
	flame->center[1] += y1;
	
	float fwidth  = flame->size[0] * flame->scale * powf(2.f, flame->zoom); // normalized file coordinates
	float fheight = flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	
	float ftileWidth  = roundf(cropRect.size.width/bounds.width *fwidth);
	float ftileHeight = roundf(cropRect.size.height/bounds.height *fheight);
	
	flame->size[0] = ftileWidth/flame->scale/powf(2.f, flame->zoom);
	flame->size[1] = ftileHeight/flame->scale/powf(2.f, flame->zoom);
	
	// determine the new window content view sized from crop dimensions
	NSSize viewSize;
	if (cropRect.size.width/cropRect.size.height > 1.f) {
		float viewResizeScale = bounds.width/cropRect.size.width;
		if (bounds.width >= cropRect.size.width)
			viewSize = NSMakeSize(viewResizeScale * cropRect.size.width, viewResizeScale * cropRect.size.height);
		else
			viewSize = cropRect.size;
	}
	else {
		float viewResizeScale = bounds.height/cropRect.size.height;
		if (bounds.height >= cropRect.size.height)
			viewSize = NSMakeSize(viewResizeScale * cropRect.size.width, viewResizeScale * cropRect.size.height);
		else
			viewSize = cropRect.size;
	}
	viewSize.height += statusBarHeight; // adjust new content view size by statusbar height

	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] setViewStateforCenter:origCenter 
																	 size:origSize 	
														   winContentSize:origViewSize 
																	scale:flame->scale];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Crop"];
	}
	
	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
	
	[view setNormCropRect:NSMakeRect(-1.f, -1.f, -1.f, -1.f)];
	[view setSelectedSpot:none];
	
	NSWindow * window = [view window];
	[window invalidateCursorRectsForView:view];
	
	NSSize currentContentSize = [[window contentView] bounds].size;
	if (NSEqualSizes(currentContentSize, viewSize))
		[ctrl renderAsPreview:YES];
	else
		[window setContentSize:viewSize];
}

// crop the fractal by x,y expressed in view coordinates
- (void)cropResizeWithRect:(NSRect)cropRect bounds:(NSSize)bounds {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	
	Flame *flame  = [self getCurrentFlame];
	
	NSPoint origCenter   = NSMakePoint(flame->center[0], flame->center[1]);
	NSSize  origSize     = NSMakeSize(flame->size[0],    flame->size[1]);
	NSSize  origViewSize = bounds;
	
	NSPoint viewCenter = NSMakePoint(bounds.width/2.f, bounds.height/2.f);
	NSPoint cropCenter = NSMakePoint(NSMidX(cropRect), NSMidY(cropRect));
	
	// offset the viewCenter (view will be in lower left corner of new crop without this)
	viewCenter.x += cropCenter.x - viewCenter.x;
	viewCenter.y += cropCenter.y - viewCenter.y;
	
	float deltaX = cropCenter.x - viewCenter.x;
	float deltaY = -(cropCenter.y - viewCenter.y); // adjust for flipped Y coordinate
	
	float docViewWidth  =  flame->size[0] * flame->scale * powf(2.f, flame->zoom);
	float docViewHeight =  flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	float viewScaleX = bounds.width  / docViewWidth;
	float viewScaleY = bounds.height / docViewHeight;
	
	// first rescale translation delta based on view window size changes
	float sDeltaX = deltaX / viewScaleX;
	float sDeltaY = deltaY / viewScaleY;
	
	// scale deltaX, deltaY to normalized fractal coordinates
	// then unrotate & untranslate to get the new centers in normalized fractal coordinates
	float xDelta = sDeltaX/(flame->scale * powf(2.f, flame->zoom));
	float yDelta = sDeltaY/(flame->scale * powf(2.f, flame->zoom));
	
	float x1 = xDelta * cosf(-flame->rotation) - yDelta * sinf(-flame->rotation);
	float y1 = xDelta * sinf(-flame->rotation) + yDelta * cosf(-flame->rotation);
	
	flame->center[0] += x1;
	flame->center[1] += y1;
	
	float fwidth  = flame->size[0] * flame->scale * powf(2.f, flame->zoom); // normalized file coordinates
	float fheight = flame->size[1] * flame->scale * powf(2.f, flame->zoom);
	
	float ftileWidth  = roundf(cropRect.size.width/bounds.width *fwidth);
	float ftileHeight = roundf(cropRect.size.height/bounds.height *fheight);
	
	flame->size[0] = ftileWidth/flame->scale/powf(2.f, flame->zoom);
	flame->size[1] = ftileHeight/flame->scale/powf(2.f, flame->zoom);
	
	
	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] setViewStateforCenter:origCenter 
																	 size:origSize 	
														   winContentSize:origViewSize 
																	scale:flame->scale];
	if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Crop Resize"];
	}
	
	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
}

// crop the fractal by x,y expressed in view coordinates
- (void)rotateContentView90DegreesLeft:(BOOL)left {
	PreviewController * ctrl = [[self windowControllers] objectAtIndex:0];
	
	Flame *flame  = [self getCurrentFlame];	
	flame->rotation += left ? -M_PI/2.f : M_PI/2.f;
	
	NSUndoManager *undoManager = [self undoManager];
	[[undoManager prepareWithInvocationTarget:self] rotateContentView90DegreesLeft:!left]; 

	 if (! [undoManager isUndoing]) {
		[undoManager setActionName:@"Rotate Window"];
	}
	
	[ctrl checkTrayUndoRedoBtns:self]; // enable/disable tray's undo/redo buttons
	
	FlamePreviewView * view = [ctrl view];
	float statusBarHeight = NSHeight([[[view window] contentView] bounds]) - NSHeight([view bounds]);
	
	NSSize currentContentSize = [[[view window] contentView] bounds].size;
	currentContentSize.height -= statusBarHeight;
	NSSize viewSize = currentContentSize;

	// transpose the sides
	viewSize.width   = currentContentSize.height;
	viewSize.height  = currentContentSize.width;
	viewSize.height += statusBarHeight;
	if (NSEqualSizes(currentContentSize, viewSize))
		[ctrl renderAsPreview:YES];
	else
		[[view window] setContentSize:viewSize];
	
}



@end
