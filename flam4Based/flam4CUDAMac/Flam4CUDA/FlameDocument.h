/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */


#import <Cocoa/Cocoa.h>
#import "FlameData.h"
#import "FlameAnimation.h"

@interface FlameDocument : NSDocument
{
	@public
	flameParameterList *flameData;
	int					frameCount;
	int                 currentFrameIndex;
	NSMutableArray     *frameOriginals;
	NSXMLDocument      *xmlData; 
	
	@private
}
@property (readonly, assign) flameParameterList *flameData;
@property (readonly)         int                 frameCount;
@property (retain)           NSMutableArray     *frameOriginals;

- (struct Flame *)getCurrentFlame;
- (int)currentFrameIndex;
- (void)setCurrentFrameIndex:(int)i;
- (void)translateByViewX:(float)deltaX viewY:(float)deltaY;
- (void)zoomByWheel:(float)scaleFactor  referencePoint:(NSPoint)referencePoint;
- (void)rotateDegrees:(float)degrees  referencePoint:(NSPoint)refPoint;
- (void)rotateRadians:(float)radians  referencePoint:(NSPoint)refPoint;
- (void)restoreOriginalFlameCoords;
- (void)cropWithRect:(NSRect)cropRect bounds:(NSSize)bounds;
- (void)cropResizeWithRect:(NSRect)cropRect bounds:(NSSize)bounds;
- (void)rotateContentView90DegreesLeft:(BOOL)left;
- (void)setViewStateforCenter:(NSPoint)center size:(NSSize)size winContentSize:(NSSize)viewSize scale:(float)scale;
@end
