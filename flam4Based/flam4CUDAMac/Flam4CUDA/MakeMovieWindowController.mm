/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "MakeMovieWindowController.h"
#import "MovieDocument.h"
#import "Codec.h"
#import "QTMovieExtensions.h"
#import "AppController.h"
#import "HelpController.h"

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

static NSDictionary *framerates;	// convert frame rate labels to frames per second as a float

@implementation MakeMovieWindowController
@synthesize window;
@synthesize selectedFrameRate;
@synthesize selectedCodecIndex;

- (void)initArrays
{
	// query available codecs and build the codec name array
	NSArray *_codecs = [Codec getCodecs];
	codecs = [[NSMutableArray arrayWithCapacity:[_codecs count]] retain];
	
	NSEnumerator *enumerator = [_codecs objectEnumerator];
	Codec *codec = nil;
	while (codec = [enumerator nextObject]) {
		[codecs addObject:[codec name]];
   }
	
	// look for H..264 first, if not there try MPEG-4, else just set it to the first one
	selectedCodecIndex = [codecs indexOfObject:@"H.264"];
	if (selectedCodecIndex == NSNotFound)
		selectedCodecIndex = [codecs indexOfObject:@"MPEG-4 Video"];
	if (selectedCodecIndex == NSNotFound)
		selectedCodecIndex = 0;
				   
	framerateKeys = [[NSArray arrayWithObjects:
					 @"60 frames per second", 
					 @"50 frames per second", 
					 @"30 frames per second", 
					 @"29.97 frames per second", 
					 @"25 frames per second", 
					 @"24 frames per second", 
					 @"23.976 frames per second", 
					 @"15 frames per second", 
					 @"12 frames per second", 
					 @"10 frames per second", 
					 @"6 frames per second", 
					 @"2 frames per second", 
					 @"1 frames per second",
					 @"2 seconds per frame",
					 @"3 seconds per frame",
					 @"4 seconds per frame",
					 @"5 seconds per frame",
					 @"10 seconds per frame",
					 nil] retain];
	NSArray *values = [NSArray arrayWithObjects:
					   [NSNumber numberWithFloat:60.f],
					   [NSNumber numberWithFloat:50.f],
					   [NSNumber numberWithFloat:30.f],
					   [NSNumber numberWithFloat:29.97f],
					   [NSNumber numberWithFloat:25.f],
					   [NSNumber numberWithFloat:24.f],
					   [NSNumber numberWithFloat:23.976f],
					   [NSNumber numberWithFloat:15.f],
					   [NSNumber numberWithFloat:12.f],
					   [NSNumber numberWithFloat:10.f],
					   [NSNumber numberWithFloat:6.f],
					   [NSNumber numberWithFloat:2.f],
					   [NSNumber numberWithFloat:1.f],
					   [NSNumber numberWithFloat:0.5f],
					   [NSNumber numberWithFloat:(float)1.0f/3.0f],
					   [NSNumber numberWithFloat:0.25f],
					   [NSNumber numberWithFloat:0.2f],
					   [NSNumber numberWithFloat:0.1f],
					   nil];
	framerates = [[NSDictionary dictionaryWithObjects:values forKeys:framerateKeys] retain];
}

- (float)frameRateForLabel:(NSString *)key {
	return [[framerates objectForKey:key] floatValue];
}

- (NSArray *)frameRateKeys {
	return framerateKeys;
}

- (NSArray *)codecs {
	return codecs;
}

- (id)init
{
	if (![super init])
		return nil;
	movieDoc          = nil;
	selectedFrameRate = @"15 frames per second";
	
	[self initArrays];
	
	@try {
		// The myNib file must be in the bundle that defines self's class.
		if (![NSBundle loadNibNamed:@"MakeMovie" owner:self])
		{
			NSLog(@"Warning! Could not load myNib file: %@.\n", @"MakeMovie");
			return NO;
		}
		
	}
	@catch (NSException * e) {
		return nil;
	}
	
	return self;
}

- (void)dealloc {
	[framerateKeys release];
	[codecs release];
	[movieDoc release];
	[selectedFrameRate release];
	[super dealloc];
}

- (NSString *)nibName {
	return @"MakeMovie";
}

- (IBAction)cancel:(id)sender
{
	[[self window] close];
}

// get a list of available Movie Export components
- (NSArray *)availableComponents
{
	NSMutableArray *array = [NSMutableArray array];
	
	ComponentDescription cd;
	Component c = NULL;
	
	//cd.componentType = MovieExportType;
	cd.componentType = compressorComponentType;
	cd.componentSubType = 0;
	cd.componentManufacturer = 0;
	cd.componentFlags = canMovieExportFiles;
	cd.componentFlagsMask = canMovieExportFiles;
	
	while((c = FindNextComponent(c, &cd)))
	{
		Handle name = NewHandle(4);
		ComponentDescription exportCD;
		
		if (GetComponentInfo(c, &exportCD, name, nil, nil) == noErr)
		{
			unsigned char *namePStr = (unsigned char *)*name;
			NSString *nameStr = [[NSString alloc] initWithBytes:&namePStr[1] length:namePStr[0] encoding:NSUTF8StringEncoding];
			
			NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
										nameStr, @"name",
										[NSData dataWithBytes:&c length:sizeof(c)], @"component",
										[NSNumber numberWithLong:exportCD.componentType], @"type",
										[NSNumber numberWithLong:exportCD.componentSubType], @"subtype",
										[NSNumber numberWithLong:exportCD.componentManufacturer], @"manufacturer",
										QTStringForOSType(exportCD.componentType), @"typeStr",
										QTStringForOSType(exportCD.componentSubType), @"subtypeStr",
										QTStringForOSType(exportCD.componentManufacturer), @"manufStr",
										nil];
			[array addObject:dictionary];
			[nameStr release];
		}
		
		DisposeHandle(name);
	}
	return array;
}

// used to sort string arrays in the same manner as the Finder
int finderSortWithLocale(id string1, id string2, void *locale)
{
	NSString *filePath1 = [string1 path]; // convert our urls to strings
	NSString *filePath2 = [string2 path];
	
    static NSStringCompareOptions comparisonOptions =
	NSCaseInsensitiveSearch | NSNumericSearch |
	NSWidthInsensitiveSearch | NSForcedOrderingSearch;
	
    NSRange string1Range = NSMakeRange(0, [filePath1 length]);
	
    return [filePath1 compare:filePath2
                    options:comparisonOptions
					  range:string1Range
					 locale:(NSLocale *)locale];
}

- (IBAction)makeMovie:(id)sender
{
	// get the directory that holds the PNG frames we need to read
	NSOpenPanel *oPanel = [NSOpenPanel openPanel];		
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSURL *initialBatchDir     = [NSURL fileURLWithPath:[defaults objectForKey:Flam4PngBatchDirectory]];
	NSURL *initialMovieDir     = [NSURL fileURLWithPath:[defaults objectForKey:Flam4MovieDirectory]];
	
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[oPanel setDirectory:[initialBatchDir path]];
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		[oPanel setDirectoryURL:initialBatchDir];		
#endif
	}
	[oPanel setCanCreateDirectories:NO];
	[oPanel setCanChooseDirectories:YES];
	[oPanel setCanChooseFiles:NO];
	[oPanel setMessage:@"Choose an existing PNG Batch directory as the source for the Movie's frames"];
	[oPanel setTitle:@"Choose Existing PNG File Batch Directory"];
	
	int result = [oPanel runModal];
	// if successful, save file under designated name 
	if (result != NSOKButton) 
		return;
	[[self window] makeKeyWindow];
	
	NSURL *directoryUrl = [oPanel URL];
	
	// find where to put the new movie file
	NSSavePanel *sPanel = [NSSavePanel savePanel];		
	NSArray *fileTypes = [NSArray arrayWithObjects:@"mov", nil];
	
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[sPanel setDirectory:[initialMovieDir path]];
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		[sPanel setDirectoryURL:initialMovieDir];
#endif
	}
	[sPanel setCanCreateDirectories:YES];
	[sPanel setAllowedFileTypes:fileTypes];
	[oPanel setCanChooseDirectories:NO];
	[oPanel setCanChooseFiles:YES];
	[sPanel setMessage:@"Choose a file to save the Quicktime Movie as"];
	[sPanel setTitle:@"Save as Quicktime Movie File"];
	[sPanel setExtensionHidden:NO];
	
	NSMutableString *filename = [NSMutableString stringWithString:[[directoryUrl path] lastPathComponent]];
	[filename appendString: @".mov"];

	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
		int result = [sPanel runModalForDirectory:[initialMovieDir path] file:filename];
		if (result != NSOKButton) 
			return;			
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		[sPanel setNameFieldStringValue:filename];
		
		int result = [sPanel runModal];
		if (result != NSOKButton) 
			return;
#endif
	}

	[[self window] makeKeyWindow];
	
	NSURL *url = [sPanel URL];
	NSURL *pickedDir = nil;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) {
		NSString *path = [sPanel directory];
		pickedDir      = [NSURL fileURLWithPath:path];
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		pickedDir = [sPanel directoryURL];
#endif
	}
	
	if (! [initialMovieDir isEqual:pickedDir]) { // remember this directory as the new default
		[defaults setObject:[pickedDir path] forKey:Flam4MovieDirectory];
	}
	
	// create the movie Document manually -- not using the NSDocumentController - we will handle it ourselves
	NSError *error = nil;
	movieDoc = [[MovieDocument alloc] initWithType:@"com.apple.quicktime-movie" error:&error];
	if (movieDoc == nil && error) {
		NSAlert *a = [NSAlert alertWithError:error];
		[a runModal];
		return;
	}
	[movieDoc setContentSize:NSMakeSize(400.0f, 300.0f)];
	[movieDoc makeWindowControllers];
	[movieDoc buildQTKitMovie];
	NSArray *imageArray ;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5) { // Leopard
		NSString *path = [directoryUrl path];
		NSArray *array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:&error];
		
		if (array == nil) {
			NSAlert *a = [NSAlert alertWithError:error];
			[a runModal];
			return;
		}
		// transform filenames to the full path
		NSMutableArray *mArray = [NSMutableArray arrayWithCapacity:[array count]];
		for (NSString *filename in array)
		{
			if ([[filename  pathExtension] isEqualToString:@"png"])
				[mArray addObject: [NSURL fileURLWithPath:[path stringByAppendingPathComponent:filename]]];
		}
		imageArray = mArray;
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		imageArray = [[NSFileManager defaultManager]
					  contentsOfDirectoryAtURL:directoryUrl
					  includingPropertiesForKeys:nil 
					  options:NSDirectoryEnumerationSkipsSubdirectoryDescendants 
					  error:&error];
		
		if (imageArray == nil) {
			NSAlert *a = [NSAlert alertWithError:error];
			[a runModal];
			return;
		}
#endif
	}
	// filter out non-PNG files
	NSMutableArray *filteredArray = [NSMutableArray arrayWithArray:imageArray];
	
	for (int i = [imageArray count] - 1; i >= 0; i--) {
		NSURL *url = [imageArray objectAtIndex:i];
		
		if (! [[[url path] pathExtension] isEqualToString:@"png"])
			[filteredArray removeObjectAtIndex:i];
	}
	// resort them like the finder would
	NSArray *sortedArray = [filteredArray sortedArrayUsingFunction:finderSortWithLocale
														  context:[NSLocale currentLocale]];	
	float fps = [self frameRateForLabel:selectedFrameRate];
	
	NSArray *_codecs = [Codec getCodecs];
	Codec *codec = [_codecs objectAtIndex:selectedCodecIndex];
	NSString *osType = [codec osType];
	
	// we want the movie to automatically loop
	[[movieDoc docMovie] setAttribute:[NSNumber numberWithBool:YES] forKey:QTMovieLoopsAttribute];
	
	// compress images with the selected codec
	[[movieDoc docMovie] addImagesWithCodec:sortedArray osType:osType framesPerSecond:fps];
	
	[movieDoc setFileURL:url];  // so the window title is correct

	[movieDoc showWindows];
	
	NSString *filePath = [url path];
	[[movieDoc docMovie] flattenToFilePath:filePath];
	
	[[self window] close];
}

- (IBAction)help:(id)sender {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"howtoVideo" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"RenderVideo"  inBook:locBookName];
	}
}


@end
