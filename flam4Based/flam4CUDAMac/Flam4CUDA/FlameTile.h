//
//  Flame.h
//  Flam4CUDA
//
//  Created by Steven Brodhead on 12/19/09.
//  Copyright 2009 Centcom Inc.. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FlameData.h"



@class FlameTileArray;

@interface FlameTile : NSObject {
	Flame   *flame;
	unsigned row;
	unsigned column;
	float    width;
	float    height;
}
@property (assign) Flame *flame;
@property unsigned row;
@property unsigned column;
@property float width;
@property float height;

- (id)initWithFlame:(Flame *)_flame 
				row:(unsigned)_row 
			 column:(unsigned)_column 
			  width:(float)width 
			 height:(float)height;
-(NSSize)size;
+ (float)overlap;
@end
