/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "PngView.h"
#import "PngDocument.h"


@implementation PngView
@synthesize document;

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

// handle dynamic scrolling from mouse dragging
- (void)mouseDown:(NSEvent *)theEvent
{
    BOOL dragging = YES;
    NSPoint windowPoint;
    NSPoint lastWindowPoint = [theEvent locationInWindow];
    float dx, dy;
	
	[[NSCursor closedHandCursor] push];
    while (dragging) {
        theEvent = [[self window] nextEventMatchingMask:NSLeftMouseUpMask | NSLeftMouseDraggedMask];
        windowPoint = [theEvent locationInWindow];

		NSScrollView * scrollview = [self enclosingScrollView];				
		NSClipView *clipView = [scrollview contentView];
		switch ([theEvent type]) {
            case NSLeftMouseUp:
				[NSCursor pop];
				dragging = NO;
                break;
            case NSLeftMouseDragged:
                dx = windowPoint.x - lastWindowPoint.x;
                dy = windowPoint.y - lastWindowPoint.y;
				
				// get the current scroll position of the document view
				NSPoint currentScrollPos=[[scrollview contentView] bounds].origin;
				NSPoint newScrollPos = NSMakePoint(currentScrollPos.x - dx, currentScrollPos.y - dy);
				NSPoint constrainedScrollPos = [clipView constrainScrollPoint:newScrollPos];
				
				if (! NSEqualPoints(currentScrollPos, constrainedScrollPos)) {
					//[self scrollPoint:newScrollPos];  -- does not work in vertical direction ?????
					[clipView scrollToPoint:constrainedScrollPos];
					[scrollview reflectScrolledClipView:clipView];
				}
				
				lastWindowPoint = windowPoint;
				break;
            default:
                break;
        }
    }
}

- (void)awakeFromNib {
	NSClipView *clipView = [[self enclosingScrollView] contentView];
	[clipView setDocumentCursor:[NSCursor openHandCursor]];
	
	// figure out the largest window that can be and use that as the max size if necessary
	NSWindow *myWindow = [self window];
	NSRect screenArea  = [[[self window] screen] visibleFrame]; // the usable part of the screen
	NSSize imageSize   = [[[self document] image] size];	
	NSRect scrolledImageRect   = NSMakeRect(screenArea.origin.x, screenArea.origin.y, 
											imageSize.width + [NSScroller scrollerWidth], imageSize.height + [NSScroller scrollerWidth]);
	NSRect frameRect   = [NSWindow frameRectForContentRect:scrolledImageRect styleMask:[myWindow styleMask]];
	
	NSRect biggestFrameRect = NSIntersectionRect(frameRect, screenArea);  // clip to usable screen area
	NSRect biggestRect      = [NSWindow contentRectForFrameRect:biggestFrameRect styleMask:[myWindow styleMask]];
	
	// compensate for scroller being visible if we had to clip the desired frame rectangle
	BOOL scrollerNeeded = biggestRect.size.height < imageSize.height || biggestRect.size.width < imageSize.width;
	if (scrollerNeeded) {
		[[self window] setContentSize:biggestRect.size];		// set content size to the image size
	}
	else { // if no scroll bar needed, adjust for the fact that biggest rect was sized assuming the scrollers would be visible
		NSRect optimalRect        = biggestRect;
		optimalRect.size.width   -= [NSScroller scrollerWidth];
		optimalRect.size.height  -= [NSScroller scrollerWidth];
		[[self window] setContentSize:optimalRect.size];	// set content size to the best fit for the screen
	}
	[myWindow setMaxSize:[myWindow frame].size];			// set window max size so it cant be enlarged bigger than our best size
	[self setFrameSize:imageSize];	// this will always be the image size
}

// handle both full image and clipped image redraws
- (void)drawRect:(NSRect)dirtyRect {
	NSImage *image = [[self document] image];
	NSRect bounds  = [self bounds];
	
	if (NSEqualRects(dirtyRect, bounds)) { // redrawing the whole image
		[image drawInRect:dirtyRect fromRect:NSZeroRect operation:NSCompositeCopy fraction:1.0];
	}
	else { // redrawing a sliver
		NSSize imageSize   = [[[self document] image] size];
		// convert the dirty rect to units based on 0,0 = lower left 1,1 = upper right
		NSPoint ll = NSMakePoint(dirtyRect.origin.x/bounds.size.width, dirtyRect.origin.y/bounds.size.height);
		NSPoint ur = NSMakePoint((dirtyRect.origin.x + dirtyRect.size.width)/bounds.size.width, 
								 (dirtyRect.origin.y + dirtyRect.size.height)/bounds.size.height);
		
		NSRect clipRect = NSMakeRect(ll.x * imageSize.width, ll.y *imageSize.height,
									 (ur.x - ll.x) * imageSize.width, (ur.y - ll.y) * imageSize.height);
		[image drawInRect:dirtyRect fromRect:clipRect operation:NSCompositeCopy fraction:1.0];
	}
}

@end
