/*
 *  Flam4Cmd.cpp
 *  Flam4CUDA
 *
 *  Created by Steven Brodhead on 1/13/10.
 *  Copyright 2010 Centcom Inc.. All rights reserved.
 *
 */

#import <AppKit/AppKit.h>
#include "FlameData.h"
#include "FlameAnimation.h"
#import "ImageUtil.h"
#import "FlameTile.h"
#import "FlameTileArray.h"
#import "ImageRepTileArray.h"

extern "C"  void cuStartCuda(NSOpenGLView *view, int x, int y);
extern "C"  void cuStartFrame(NSOpenGLView *view, int x, int y, Flame* flame);
extern "C"  void cuStopCuda();
extern "C"  void cuRunFuse(Flame* flame);
extern "C"  void cuRenderBatch(Flame* flame);
extern "C"  void cuFinishFrame(void* image, bool useAlpha);
extern "C"  bool cuCudaActive();

Flame* g_pFlame = NULL;


//This function renders a single frame.
NSBitmapImageRep *RenderTileCore(Flame* flame, int width, int height, NSView *view, BOOL useAlpha, BOOL flip)
{	
	cuStartCuda(view,width,height);
	cuRunFuse(flame);
	flame->Clone(&g_pFlame);
	cuStartFrame(view, width, height, g_pFlame);
	for (int subSample = 0; subSample < flame->numBatches; subSample++)
	{
		cuRenderBatch(flame);
	}
	unsigned char* img = new unsigned char[4*width*height];
	cuFinishFrame(img,useAlpha);
	cuStopCuda();
	NSData *data = [NSData dataWithBytes:img length:4*width*height];
	delete img;
	
	return [ImageUtil imageRepFromData:data hasAlpha:useAlpha rect:NSMakeRect(0, 0, width, height) flip:flip];;
}

//This function renders a single frame and saves it to outFile.
//hwnd should be NULL if you're rendering a frame larger than 4096x4096
//since openGL cannot handle textures that large.
NSBitmapImageRep *RenderPreviewTileSetCore(flameParameterList *flameParams, BOOL useAlpha, FlameTileArray *tiles)
{
	ImageRepTileArray *imageArray = [tiles makeImageRepTileArray:NO];
	
	// iterate through the tiles do each in turn
	for (FlameTile * tile in [tiles array]) {
		NSBitmapImageRep *rep = RenderTileCore([tile flame], [tile width], [tile height], nil, useAlpha, YES);
		if (rep == nil)
			return nil;
		[imageArray setTileAtRow:[tile row] col:[tile column] withObject:rep];			 
	}
	
	return [imageArray combinedImageRep];
}


