#!/bin/sh

# preinstall.sh
# Flam4CUDA
#
# Created by Steven Brodhead on 12/16/09.
# Copyright 2009 Centcom Inc.. All rights reserved.

if [ -d /Applications/Flam4CUDA.app ]; then
	sudo rm -R /Applications/Flam4CUDA.app
fi
exit 0