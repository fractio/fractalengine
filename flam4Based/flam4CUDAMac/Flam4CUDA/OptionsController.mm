/*
 Copyright 2008 Steven Brodhead
 
 This file is part of flam4.
 
 flam4 is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 flam4 is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with flam4.  If not, see <http://www.gnu.org/licenses/>.
 */

#import "OptionsController.h"
#import "Defines.h"
#import "Globals.h"
#import "FlameDocument.h"
#import "QualityUtils.h"
#import "PreviewController.h"
#import "HelpController.h"

// compensate for stuff missing on Leopard
extern int quality;  // display quality

@implementation OptionsController

@synthesize window;
@synthesize qual;

- (id)init
{
	if (![super init])
		return nil;
	qual = nil;
	originalQual = 55;

	NSDocument *doc = [[NSDocumentController sharedDocumentController] currentDocument];
	if (! doc) {
		NSRunAlertPanel(@"Error", @"No flame document is open", @"Ok", nil, nil);
		return nil;
	}
	
	[self loadMyNibFile:@"Options"];
	return self;
}

- (void)dealloc
{
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];	
	[nc removeObserver:self];
	
	[qual release];
	[super dealloc];
}

- (void)willClose:(NSNotification *)note
{
	[self autorelease];
}

// format Iterations lable
- (void)setIterationsLabel
{
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setHasThousandSeparators:YES];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	NSMutableString  *s =[NSMutableString stringWithString:@"Total Iterations: "];
	NSString *str = [numberFormatter stringFromNumber:[NSNumber numberWithLongLong:[qual longLongValue]*1024LL*32LL*NUM_ITERATIONS]];
	[s appendString:str];
	
	[iterationsLabel           setStringValue:s];
	[numberFormatter release];
}

// when qual changes, modify the Total Iterations label
- (void)observeValueForKeyPath:(NSString *)keyPath
					  ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
	if ([keyPath isEqual:@"qual"]) {
		[self setIterationsLabel];
	}
	
}

//load my Nib file
- (BOOL)loadMyNibFile: (NSString *)nibName
{
	// The myNib file must be in the bundle that defines self's class.
	if (![NSBundle loadNibNamed:nibName owner:self])
	{
		NSLog(@"Warning! Could not load myNib file: %@.\n", nibName);
		return NO;
	}
	return YES;
}

// initialize Options form after loading it from Nib file
- (void)awakeFromNib
{
	qual = [[NSNumber numberWithInt:quality] retain];
	originalQual = [qual intValue];

	[qualityLabel setObjectValue:qual];
	[qualitySlider setObjectValue:qual];
	[animateCheckBox setState:isAnimating ? NSOnState : NSOffState];
	[self setIterationsLabel];

	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self
		   selector:@selector(willClose:) name:NSWindowWillCloseNotification object:window];
	
	[self addObserver:self forKeyPath:@"qual" options:NSKeyValueObservingOptionNew context:nil];
}

- (IBAction)cancel:(id)sender
{
	[[self window] close];
}

// OK button handler
- (IBAction)processOptions:(id)sender
{
	quality = [qualitySlider intValue];
	isAnimating = [animateCheckBox state] == NSOnState;
	
	FlameDocument *doc       = [[NSDocumentController sharedDocumentController] currentDocument];
	PreviewController * ctrl = [[doc windowControllers] objectAtIndex:0];
	[ctrl setQuality:quality];
	
	FlamePreviewView  * view = [ctrl view];
	NSRect rect              = [view bounds];
	flameParameterList* temp = [doc flameData];
	
	while (temp != nullptr)
	{
		temp->flame->numBatches = [QualityUtils batchesForDesired:quality width:NSWidth(rect) height:NSHeight(rect) tiles:[ctrl tileCount]];
		temp = temp->nextFlame;
	}
	if (quality != originalQual)
		[ctrl renderAsPreview:NO];
	 	
	[[self window] close];
}

- (IBAction)help:(id)sender {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)
		[HelpController launchHelpForHelpFile:@"howtoOptionQuality" withExtension:@"html" helpDirectory:@"Flam4"];
	else {
		NSString *locBookName = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleHelpBookName"];
		[[NSHelpManager sharedHelpManager] openHelpAnchor:@"OptionQuality"  inBook:locBookName];
	}
}

@end
