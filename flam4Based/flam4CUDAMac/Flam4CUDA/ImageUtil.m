//
//  ImageUtil.m
//  Flam4CUDA
//
//  Created by Steven Brodhead on 1/13/10.
//  Copyright 2010 Centcom Inc.. All rights reserved.
//

#import "ImageUtil.h"


@implementation ImageUtil
// convert raw bitmap data to a NSBitmapImageRep instance
+ (NSBitmapImageRep *) imageRepFromDataNoAlpha:(NSData *)data rect:(NSRect)rect flip:(BOOL)flip {
	int height=NSHeight(rect);
	int width=NSWidth(rect);
	
	NSBitmapImageRep* imageRep;	
	imageRep=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													  pixelsWide:width
													  pixelsHigh:height
												   bitsPerSample:8
												 samplesPerPixel:3
														hasAlpha:NO
														isPlanar:NO
												  colorSpaceName:NSCalibratedRGBColorSpace
													 bytesPerRow:width*3
													bitsPerPixel:24] 
			  autorelease];
	
	// convert from incoming rgba array to rgb array - flip upside down
	unsigned char* rgba = (unsigned char *)[data bytes];
	unsigned char* rgb  = (unsigned char *)[imageRep bitmapData];
	
	for (unsigned int y = 0; y < height; ++y)
	{
		for (unsigned int x = 0; x < width; ++x)
		{
			if (flip) {
				rgb[3*(width*(height-y-1) + x)    ] = rgba[4*(y*width + x)    ];  
				rgb[3*(width*(height-y-1) + x) + 1] = rgba[4*(y*width + x) + 1];  
				rgb[3*(width*(height-y-1) + x) + 2] = rgba[4*(y*width + x) + 2];  
			}
			else {
				rgb[3*(y*width + x)    ] = rgba[4*(y*width + x)    ];  
				rgb[3*(y*width + x) + 1] = rgba[4*(y*width + x) + 1];  
				rgb[3*(y*width + x) + 2] = rgba[4*(y*width + x) + 2];  
			}
		}
	}
	
	return imageRep;
}

// convert raw bitmap data to a NSBitmapImageRep instance
+ (NSBitmapImageRep *) imageRepFromDataAlpha:(NSData *)data rect:(NSRect)rect flip:(BOOL)flip {
	int height=NSHeight(rect);
	int width=NSWidth(rect);
	NSBitmapImageRep* imageRep;
	
	imageRep=[[[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
													  pixelsWide:width
													  pixelsHigh:height
												   bitsPerSample:8
												 samplesPerPixel:4
														hasAlpha:YES
														isPlanar:NO
												  colorSpaceName:NSCalibratedRGBColorSpace
													 bytesPerRow:width*4
													bitsPerPixel:32] 
			  autorelease];
	
	const char *buf  = (const char *)[data bytes];
	const char *dest = (const char *)[imageRep bitmapData];
	
	// flip the image around the x-axis so it is not saved upside down
	int rowWidth = width*4;
	if (flip) {
		for (int si = height - 1; si >= 0; si--) {
			int di = height -1 - si;
			const char *d = dest + di * rowWidth;
			const char *s = buf  + si * rowWidth;
			memcpy((void *)d, (void *)s, rowWidth);
		}		
	}
	else {
		memcpy((void *)dest, (void *)buf, 4*width*height);
	}
	return imageRep;
}

// capture the pixel data from the view and flip it so its not upside down
+ (NSBitmapImageRep *) imageRepFromData:(NSData*)myData hasAlpha:(BOOL)hasAlpha rect:(NSRect)rect flip:(BOOL)flip {
	if (hasAlpha)
		return [ImageUtil imageRepFromDataAlpha:myData rect:rect flip:flip];
	else
		return [ImageUtil imageRepFromDataNoAlpha:myData rect:rect flip:flip];	
}

// export the raw bitmap data as a PNG file
+ (void)exportImageToPNG:(NSURL *)url 
					 rep:(NSBitmapImageRep *)rep 
				useAlpha:(BOOL)useAlpha
					rect:(NSRect)rect
{
	NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
	NSData *pngData = [rep representationUsingType:NSPNGFileType properties:properties];
	
	NSError *error = nil;
	if (! [pngData writeToURL:url options:0 error:&error]) {
		NSAlert *a = [NSAlert alertWithError:error];
		[a runModal];
		return;
	}
}

// export the raw bitmap data as a PNG file
+ (void)exportImageToPNG:(NSURL *)url 
					data:(NSData *)data 
				useAlpha:(BOOL)useAlpha
					rect:(NSRect)rect
{
	NSBitmapImageRep *rep = [ImageUtil imageRepFromData:data hasAlpha:useAlpha rect:rect flip:YES];
	[self exportImageToPNG:url rep:rep useAlpha:useAlpha rect:rect];
}

@end
