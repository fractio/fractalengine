#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>
#include <AppKit/AppKit.h>

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

// sort by the image reps' area (pixel *pixel)
NSInteger repSizeSort(NSBitmapImageRep *rep1, NSBitmapImageRep *rep2, void *context)
{
    int v1 = [rep1 pixelsWide] * [rep1 pixelsHigh];
    int v2 = [rep2 pixelsWide] * [rep2 pixelsHigh];
    if (v1 < v2)
        return NSOrderedAscending;
    else if (v1 > v2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}

// .icns files have multiple image sizes in it, choose the best one
NSData *getIconImageData(NSBundle *bundle, NSSize maxSize) {
	NSString* iconPath    = [bundle pathForResource:@"noRender" ofType:@"icns"];
	NSData *iconData      = [NSData dataWithContentsOfFile:iconPath];
	NSImage *iconMultReps = [[NSImage alloc] initWithData:iconData];
	NSBitmapImageRep *bestRep;
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)  { // Leopard
		NSArray *reps          = [iconMultReps representations];
		NSArray *sortedReps    = [reps sortedArrayUsingFunction:repSizeSort context:NULL];
		
		// find the rep that is the optimal size based on the icon's maximum size
		bestRep = [sortedReps lastObject]; // default in case we dont find one below
		for (NSBitmapImageRep *rep in sortedReps) {
			if (maxSize.width * maxSize.height <= [rep pixelsWide]*[rep pixelsHigh]) {
				bestRep = rep;
				break;
			}
		}
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		bestRep = (NSBitmapImageRep *)[iconMultReps bestRepresentationForRect:NSMakeRect(0, 0, maxSize.width, maxSize.height) context:nil hints:nil];
#endif
	}

	NSDictionary *properties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSImageInterlaced, nil];
	NSData *data             = [bestRep representationUsingType:NSPNGFileType properties:properties];
	[iconMultReps release];
	return data;
}

// figure out where the Flam4CMD executable is
static NSURL *getCmdURL(NSBundle *bundle) {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)  { // Leopard
		NSString *bundlePath        = [bundle bundlePath];
		NSArray *pathComponents     = [bundlePath pathComponents];
		NSUInteger i                = [pathComponents indexOfObject:@"Flam4CUDA.app"];		 // find the components that make up the Flam4CUDA app URL
		if (i == NSNotFound) { // assume the case we are beside the app package - not inside it - true for debugging
			i                       = [pathComponents indexOfObject:@"Flam4Quicklook.qlgenerator"];		 // find the components that make up the Flam4CUDA app URL
			NSIndexSet *iSet        = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i)];
			NSArray * appBundleComponents = [pathComponents objectsAtIndexes:iSet];
			NSString *appBundlePath = [NSString pathWithComponents:appBundleComponents];
			NSString *cmdPath       = [appBundlePath stringByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
			return [NSURL fileURLWithPath:cmdPath];
		}
		else { // normal install location is inside the Flam4CUDA.app package
			NSIndexSet *iSet        = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i + 1)];
			NSArray * appBundleComponents = [pathComponents objectsAtIndexes:iSet];
			NSString *appBundlePath = [NSString pathWithComponents:appBundleComponents];
			NSBundle *appBundle     = [NSBundle bundleWithPath:appBundlePath];					// get the Flam4CUDA app Bundle
			NSString *pluginsPath   = [appBundle builtInPlugInsPath];
			NSString *cmdPath       = [pluginsPath stringByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
			return [NSURL fileURLWithPath:cmdPath];
		}
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		NSString *path         = [bundle bundlePath];
		NSURL *bundleURL       = [NSURL fileURLWithPath:path];
		NSArray *urlComponents = [bundleURL pathComponents];
		NSUInteger i           = [urlComponents indexOfObject:@"Flam4CUDA.app"];		 // find the components that make up the Flam4CUDA app URL
		if (i == NSNotFound) { // assume the case we are beside the app package - not inside it - true for debugging
			i                      = [urlComponents indexOfObject:@"Flam4Quicklook.qlgenerator"];		 // find the components that make up the Flam4CUDA app URL
			NSIndexSet *iSet       = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i)];
			NSArray * appBundleComponents = [urlComponents objectsAtIndexes:iSet];
			NSURL *appBundleURL    = [NSURL fileURLWithPathComponents:appBundleComponents];
			return [appBundleURL URLByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
		}
		else { // normal install location is inside the Flam4CUDA.app package
			NSIndexSet *iSet       = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i + 1)];
			NSArray * appBundleComponents = [urlComponents objectsAtIndexes:iSet];
			NSURL *appBundleURL    = [NSURL fileURLWithPathComponents:appBundleComponents];
			NSBundle *appBundle    = [NSBundle bundleWithURL:appBundleURL];					// get the Flam4CUDA app Bundle
			NSURL *pluginsURL      = [[appBundle builtInPlugInsURL] absoluteURL];
			return [pluginsURL URLByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
		}
#endif
	}
	return nil;
}

/* -----------------------------------------------------------------------------
    Generate a thumbnail for file

   This function's job is to create thumbnail for designated file as fast as possible
   ----------------------------------------------------------------------------- */

OSStatus GenerateThumbnailForURL(void *thisInterface, QLThumbnailRequestRef thumbnail, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options, CGSize _maxSize)
{
	NSSize maxSize = NSMakeSize(_maxSize.width, _maxSize.height);
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	CFURLRef flameURL = QLThumbnailRequestCopyURL(thumbnail);
	
	// traverse from the generator location in its bundle back to the path of the Flam4CUDA executable
	CFBundleRef bundle     = QLThumbnailRequestGetGeneratorBundle(thumbnail);            // get the CF generator bundle
	CFStringRef identifier = CFBundleGetIdentifier(bundle);
	NSBundle *nsbundle     = [NSBundle bundleWithIdentifier:(NSString *)identifier]; // now get it as a NSBundle

	NSURL *cmdURL = getCmdURL(nsbundle);
		
	// fork off Flam4CMD to process a flame and read its output
	NSTask *task   = [[NSTask alloc] init];
	NSString *path = [cmdURL path];
	NSArray *array = [NSArray arrayWithObjects: 
					  [[NSNumber numberWithFloat:maxSize.width] stringValue], 
					  [[NSNumber numberWithFloat:maxSize.height] stringValue],
					  @"Thumbnail", 
					  [(NSURL *)flameURL absoluteString],
					  nil];
	[task setLaunchPath:path];
	[task setArguments:array];
	
	NSPipe *pipe = [[NSPipe alloc] init];
	[task setStandardOutput:pipe];
	[pipe release];
	
	@try {
		[task launch];
	}
	@catch (NSException * e) {
		NSData *iconData = getIconImageData(nsbundle, (NSSize)maxSize);
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: @"public.png", kCGImageSourceTypeIdentifierHint, nil];
		QLThumbnailRequestSetImageWithData(thumbnail, (CFDataRef)iconData, (CFDictionaryRef)dict);
		[task release];
		[pool release];
		return noErr;
	}
	
	NSData *data = [[pipe fileHandleForReading] readDataToEndOfFile];
	[task waitUntilExit];
	int status = [task terminationStatus];
	[task release];
	
	if (status != 0) { // render failed
		NSData *iconData = getIconImageData(nsbundle, (NSSize)maxSize);
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: @"public.png", kCGImageSourceTypeIdentifierHint, nil];
		QLThumbnailRequestSetImageWithData(thumbnail, (CFDataRef)iconData, (CFDictionaryRef)dict);
	}
	else {  // render succeeded
		NSImage *image = [[NSImage alloc] initWithData:data];
		if (image) {
			NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: @"public.png", kCGImageSourceTypeIdentifierHint, nil];
			QLThumbnailRequestSetImageWithData(thumbnail, (CFDataRef)data, (CFDictionaryRef)dict);
		}
		else {
			NSData *iconData = getIconImageData(nsbundle, (NSSize)maxSize);
			NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: @"public.png", kCGImageSourceTypeIdentifierHint, nil];
			QLThumbnailRequestSetImageWithData(thumbnail, (CFDataRef)iconData, (CFDictionaryRef)dict);
		}
		[image release];
	}
	
	[pool release];
	
	return noErr;
}

void CancelThumbnailGeneration(void* thisInterface, QLThumbnailRequestRef thumbnail)
{
    // implement only if supported
}
