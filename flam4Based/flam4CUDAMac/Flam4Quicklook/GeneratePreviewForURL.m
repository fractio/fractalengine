#include <CoreFoundation/CoreFoundation.h>
#include <CoreServices/CoreServices.h>
#include <QuickLook/QuickLook.h>
#include <AppKit/AppKit.h>

// compensate for stuff missing on Leopard
#if (MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_5)
#define NSAppKitVersionNumber10_5 949
#endif

// show an error message window if something fails
void showError(QLPreviewRequestRef preview, NSString *errMsg) {
	NSSize canvasSize = NSMakeSize(400, 200);
	CGContextRef cgContext = QLPreviewRequestCreateContext(preview, *(CGSize *)&canvasSize, false, NULL);
	if(cgContext) {
		NSGraphicsContext* context = [NSGraphicsContext graphicsContextWithGraphicsPort:(void *)cgContext flipped:NO];
		if(context) {
			[NSGraphicsContext setCurrentContext:context];
			[[NSColor lightGrayColor] setFill]; 
			NSRect aRect = NSMakeRect(0, 0, canvasSize.width, canvasSize.height);
			NSRectFill(aRect);
			
			
			NSFont *font = [NSFont fontWithName:@"Palatino-Roman" size:24.0];
			NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
			NSAttributedString *attrString =
			[[NSAttributedString alloc] initWithString:@"Could not render flame." attributes:attrsDictionary];
			[attrString drawAtPoint:NSMakePoint(100, floorf(canvasSize.height/2.f))];
			[attrString release];
		}
		QLPreviewRequestFlushContext(preview, cgContext);
		CFRelease(cgContext);
	}	
}

// figure out where the Flam4CMD executable is
static NSURL *getCmdURL(NSBundle *bundle) {
	if (floor(NSAppKitVersionNumber) == NSAppKitVersionNumber10_5)  { // Leopard
		NSString *bundlePath        = [bundle bundlePath];
		NSArray *pathComponents     = [bundlePath pathComponents];
		NSUInteger i                = [pathComponents indexOfObject:@"Flam4CUDA.app"];		 // find the components that make up the Flam4CUDA app URL
		if (i == NSNotFound) { // assume the case we are beside the app package - not inside it - true for debugging
			i                       = [pathComponents indexOfObject:@"Flam4Quicklook.qlgenerator"];		 // find the components that make up the Flam4CUDA app URL
			NSIndexSet *iSet        = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i)];
			NSArray * appBundleComponents = [pathComponents objectsAtIndexes:iSet];
			NSString *appBundlePath = [NSString pathWithComponents:appBundleComponents];
			NSString *cmdPath       = [appBundlePath stringByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
			return [NSURL fileURLWithPath:cmdPath];
		}
		else { // normal install location is inside the Flam4CUDA.app package
			NSIndexSet *iSet        = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i + 1)];
			NSArray * appBundleComponents = [pathComponents objectsAtIndexes:iSet];
			NSString *appBundlePath = [NSString pathWithComponents:appBundleComponents];
			NSBundle *appBundle     = [NSBundle bundleWithPath:appBundlePath];					// get the Flam4CUDA app Bundle
			NSString *pluginsPath   = [appBundle builtInPlugInsPath];
			NSString *cmdPath       = [pluginsPath stringByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
			return [NSURL fileURLWithPath:cmdPath];
		}
	}
	else {
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5)
		NSString *path         = [bundle bundlePath];
		NSURL *bundleURL       = [NSURL fileURLWithPath:path];
		NSArray *urlComponents = [bundleURL pathComponents];
		NSUInteger i           = [urlComponents indexOfObject:@"Flam4CUDA.app"];		 // find the components that make up the Flam4CUDA app URL
		if (i == NSNotFound) { // assume the case we are beside the app package - not inside it - true for debugging
			i                      = [urlComponents indexOfObject:@"Flam4Quicklook.qlgenerator"];		 // find the components that make up the Flam4CUDA app URL
			NSIndexSet *iSet       = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i)];
			NSArray * appBundleComponents = [urlComponents objectsAtIndexes:iSet];
			NSURL *appBundleURL    = [NSURL fileURLWithPathComponents:appBundleComponents];
			return [appBundleURL URLByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
		}
		else { // normal install location is inside the Flam4CUDA.app package
			NSIndexSet *iSet       = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, i + 1)];
			NSArray * appBundleComponents = [urlComponents objectsAtIndexes:iSet];
			NSURL *appBundleURL    = [NSURL fileURLWithPathComponents:appBundleComponents];
			NSBundle *appBundle    = [NSBundle bundleWithURL:appBundleURL];					// get the Flam4CUDA app Bundle
			NSURL *pluginsURL      = [[appBundle builtInPlugInsURL] absoluteURL];
			return [pluginsURL URLByAppendingPathComponent:@"Flam4Cmd.app/Contents/MacOS/Flam4Cmd"];
		}
#endif
	}
	return nil;
}

/* -----------------------------------------------------------------------------
   Generate a preview for file

   This function's job is to create preview for designated file
   ----------------------------------------------------------------------------- */

OSStatus GeneratePreviewForURL(void *thisInterface, QLPreviewRequestRef preview, CFURLRef url, CFStringRef contentTypeUTI, CFDictionaryRef options)
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	CFURLRef flameURL = QLPreviewRequestCopyURL(preview);
	
	// traverse from the generator location in its bundle back to the path of the Flam4CUDA executable
	CFBundleRef bundle     = QLPreviewRequestGetGeneratorBundle(preview);            // get the CF generator bundle
	CFStringRef identifier = CFBundleGetIdentifier(bundle);
	NSBundle *nsbundle     = [NSBundle bundleWithIdentifier:(NSString *)identifier]; // now get it as a NSBundle

	NSURL *cmdURL = getCmdURL(nsbundle);
	
	// fork off Flam4CMD to process a flame and read its output
	NSTask *task   = [[NSTask alloc] init];
	NSString *path = [cmdURL path];
	NSArray *array = [NSArray arrayWithObjects:[[NSNumber numberWithFloat:800.f] stringValue], 
												[[NSNumber numberWithFloat:600.f] stringValue], 
												@"Preview", 
												[(NSURL *)flameURL absoluteString], nil];
	[task setLaunchPath:path];
	[task setArguments:array];
	
	NSPipe *pipe = [[NSPipe alloc] init];
	[task setStandardOutput:pipe];
	[pipe release];
	
	@try {
		[task launch];
	}
	@catch (NSException * e) {
		showError(preview, [NSString stringWithFormat:@"Could not launch the rendering plugin\n\t(%@): %@", [cmdURL path], [e reason]]);
		[task release];
		[pool release];
		return noErr;
	}
	
	NSData *data   = [[pipe fileHandleForReading] readDataToEndOfFile];
	[task waitUntilExit];
	int status = [task terminationStatus];
	[task release];
	
	if (status != 0) { // render failed
		showError(preview, @"Render of flame recipe failed.");
	}
	else {  // render succeeded
		NSImage *image = [[NSImage alloc] initWithData:data];
		NSSize size = [image size];
		NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithFloat:size.width], kQLPreviewPropertyWidthKey,
							  [NSNumber numberWithFloat:size.height], kQLPreviewPropertyHeightKey, nil];
		
		QLPreviewRequestSetDataRepresentation(preview, (CFDataRef)data, kUTTypePNG, (CFDictionaryRef)dict);
		
		[image release];
	}
	
	[pool release];
    return noErr;
}

void CancelPreviewGeneration(void* thisInterface, QLPreviewRequestRef preview)
{
    // implement only if supported
}
