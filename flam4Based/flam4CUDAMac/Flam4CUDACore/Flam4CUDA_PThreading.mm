/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <mach/mach_time.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include "Flam4.h"
#include "Flam4CUDA_PThreading.h"
#include "Flam4_Kernal.cuh"

extern cudaArray**    d_g_Palette;
extern float4**       accumBuffer;
extern uchar4**       resultStagingBuffer;
extern point**        pointList;
extern unsigned int** perThreadRandSeeds;
extern unsigned int** perWarpRandSeeds;
extern float4**       d_dst;
extern textureReference** texRefPtr;
extern float4*        mergeStagingBuffer;
bool isMergePinned = true;

// initialize global host buffers
void initGlobalBuffers(int numDevices) {
	if (!d_g_Palette)			d_g_Palette         = new cudaArray*[numDevices];
	if (!accumBuffer)			accumBuffer         = new float4*[numDevices];
	if (!resultStagingBuffer)	resultStagingBuffer = new uchar4*[numDevices];
	if (!pointList)				pointList           = new point*[numDevices];
	if (!perThreadRandSeeds)	perThreadRandSeeds  = new unsigned int*[numDevices];
	if (!perWarpRandSeeds)		perWarpRandSeeds    = new unsigned int*[numDevices];
	if (!d_dst)					d_dst               = new float4*[numDevices];
	if (!texRefPtr)				texRefPtr           = new textureReference*[numDevices];
	
	for (int n = 0; n < numDevices; n++)
	{
		d_g_Palette[n]         = NULL;
		accumBuffer[n]         = NULL;
		resultStagingBuffer[n] = NULL;
		pointList[n]           = NULL;
		perThreadRandSeeds[n]  = NULL;
		perWarpRandSeeds[n]    = NULL;
		d_dst[n]               = NULL;
		texRefPtr[n]           = NULL;
	}		
}

// Get number of milliseconds that have elapsed since the system was started
unsigned long CUDAThreadingManager::GetTickCount(void)
{
    static mach_timebase_info_data_t    sTimebaseInfo;
	
    // If this is the first time we've run, get the timebase.
    // We can use denom == 0 to indicate that sTimebaseInfo is
    // uninitialised because it makes no sense to have a zero
    // denominator is a fraction.
	
    if ( sTimebaseInfo.denom == 0 ) {
        (void) mach_timebase_info(&sTimebaseInfo);
    }
	
    // Convert to nanoseconds.
	
    // Do the maths.  We hope that the multiplication doesn't
    // overflow; the price you pay for working in fixed point.
	
    return (long)(mach_absolute_time()/1000000) * sTimebaseInfo.numer / sTimebaseInfo.denom;
}

CUDAThreadingManager::CUDAThreadingManager()
{
	pthread_mutex_init(&readyMutex, NULL);
	pthread_mutex_init(&mutex, NULL);
	pthread_cond_init(&readyCond, NULL);

	cudaGetDeviceCount(&numDevices);
	deviceThreadHandles = new T_THREAD[numDevices];
#ifdef _WIN32
	threadIds           = new DWORD[numDevices];
#endif
	threadObjects       = (volatile WorkerPThread **)new WorkerPThread*[numDevices];
	
	initGlobalBuffers(numDevices);

	for (int n = 0; n < numDevices; n++)
	{
		activeThreadCount      = numDevices;
		threadObjects[n]       = new WorkerPThread(n);
		threadObjects[n]->parentThread = pthread_self();
		
	}
	// dont start the threads until all are initialized
	for (int n = 0; n < numDevices; n++)
	{
#ifdef _WIN32
		deviceThreadHandles[n] = CreateThread(
											  NULL,
											  0,
											  WorkerPThread::WorkerPThreadMessageProc,
											  threadObjects[n],
											  0,
											  &threadIds[n]);
#else
		pthread_create(&deviceThreadHandles[n], NULL, WorkerPThread::WorkerPThreadMessageProc, (void *)threadObjects[n]);
#endif
	}
}

#if defined(__APPLE__)
bool CUDAThreadingManager::DispatchStartCuda(NSOpenGLView *view, int x, int y)
#elif defined(_WIN32)
bool CUDAThreadingManager::DispatchStartCuda(unsigned int hwnd, int x, int y)
#endif
{
	isMergePinned = true;
	cudaHostAlloc((void**)&mergeStagingBuffer, sizeof(float4)*xDim*yDim,cudaHostAllocPortable|cudaHostAllocWriteCombined);
	if (cudaGetLastError() == cudaErrorMemoryAllocation)
	{
		mergeStagingBuffer = new float4[xDim*yDim];
		isMergePinned = false;
	}
#if defined(__APPLE__)
	this->view = view;
	threadObjects[0]->view = view;	
#elif defined(_WIN32)
	this->hwnd = (HWND) hwnd;
	threadObjects[0]->g_hWnd = hwnd;
#endif
	threadObjects[0]->xDim = x;
	threadObjects[0]->yDim = y;
	dispatchToThisDeviceWaitTillDone(0, NULL, &WorkerPThread::StartCuda);
	//NSLog(@"StartCuda finished on %d @ %u", 0, GetTickCount());
	
	for (int n = 1; n < numDevices; n++)
	{
#if defined(__APPLE__)
		threadObjects[n]->view = NULL;	
#elif defined(_WIN32)
		threadObjects[n]->g_hWnd = NULL;
#endif
		threadObjects[n]->xDim = x;
		threadObjects[n]->yDim = y;
		dispatchToThisDeviceWaitTillDone(n, NULL, &WorkerPThread::StartCuda);
		//NSLog(@"StartCuda finished on %d @ %u", n, GetTickCount());
	}	
	for (int n = 1; n < numDevices; n++) {
		if (!threadObjects[n]->cudaRunning)
			return false;
	}
	return true;
}

void CUDAThreadingManager::waitTillReady(int n)
{
	while (!threadObjects[n]->readyForCommand)
	{
		//NSLog(@"Waiting on %d cudaRunning:%d @ %u", n, threadObjects[n]->cudaRunning, GetTickCount());
		pthread_cond_wait((pthread_cond_t *)&threadObjects[n]->waitCond, (pthread_mutex_t *)&threadObjects[n]->waitMutex);
		//NSLog(@"Waiting over %d cudaRunning:%d @ %u", n, threadObjects[n]->cudaRunning, GetTickCount());
	}
}

void CUDAThreadingManager::dispatchToThisDeviceWait(int n, Flame *flame, WorkerFunction nextFunction)
{
	pthread_mutex_lock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
	waitTillReady(n);
	
	if (flame)
		flame->Clone((Flame **)&threadObjects[n]->currentParameters);
	threadObjects[n]->readyForCommand = false;
	threadObjects[n]->nextFunction = (VolatileWorkerFunction)nextFunction;
	pthread_cond_signal((pthread_cond_t *)&threadObjects[n]->waitCond);
#ifdef _WIN32
	SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
#endif
	pthread_mutex_unlock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
}	

void CUDAThreadingManager::dispatchToThisDeviceWaitTillDone(int n, Flame *flame, WorkerFunction nextFunction)
{
	pthread_mutex_lock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
	waitTillReady(n);
	
	if (flame)
		flame->Clone((Flame **)&threadObjects[n]->currentParameters);
	threadObjects[n]->readyForCommand = false;
	threadObjects[n]->nextFunction = (VolatileWorkerFunction)nextFunction;
	pthread_cond_signal((pthread_cond_t *)&threadObjects[n]->waitCond);
#ifdef _WIN32
	SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
#endif
	waitTillReady(n);
	pthread_mutex_unlock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
}	

bool CUDAThreadingManager::dispatchToThisDeviceNoWait(int n, Flame *flame, WorkerFunction nextFunction)
{
	if (threadObjects[n]->readyForCommand) {
		flame->Clone((Flame **)&threadObjects[n]->currentParameters);
		threadObjects[n]->readyForCommand = false;
		threadObjects[n]->nextFunction = (VolatileWorkerFunction)nextFunction;
		pthread_cond_signal((pthread_cond_t *)&threadObjects[n]->waitCond);
#ifdef _WIN32
		SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
#endif
		return true;
	}
	return false;
}

void CUDAThreadingManager::dispatchToAllDevicesWait(Flame *flame, WorkerFunction nextFunction)
{
	for (int n = 0; n < numDevices; n++)
	{
		dispatchToThisDeviceWait(n, flame, nextFunction);
	}
}	

// find the first device ready for a command, submit the command and return
// if none ready yield thread till later
void CUDAThreadingManager::dispatchToFirstReadyDevice(Flame *flame, WorkerFunction nextFunction)
{
	while (true) {
		for (int n = 0; n < numDevices; n++)
		{
			bool done = dispatchToThisDeviceNoWait(n, flame, nextFunction);
			if (done)
				return;
		}
		//NSLog(@"Waiting on any device @ %u", GetTickCount());
		pthread_mutex_lock(&readyMutex);
		pthread_cond_wait(&readyCond, &readyMutex);
		pthread_mutex_unlock(&readyMutex);
		//NSLog(@"Waiting over any device @ %u", GetTickCount());
	}
}	


void CUDAThreadingManager::DispatchRunFuse(Flame *flame)
{
	dispatchToAllDevicesWait(flame, &WorkerPThread::RunFuse);
}

void CUDAThreadingManager::DispatchStartFrame(Flame *flame)
{
	dispatchToAllDevicesWait(flame, &WorkerPThread::StartFrame);
}

void CUDAThreadingManager::DispatchRenderBatch(Flame *flame)
{
	dispatchToFirstReadyDevice(flame, &WorkerPThread::RenderBatch);
}

void CUDAThreadingManager::dispatchFinishFrameWait(int n, void** image, bool useAlpha)
{
	pthread_mutex_lock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
	waitTillReady(n);

	threadObjects[n]->image = *image;
	threadObjects[n]->useAlpha = useAlpha;
	threadObjects[n]->readyForCommand = false;
	threadObjects[n]->nextFunction = (VolatileWorkerFunction)&WorkerPThread::FinishFrame;
	pthread_cond_signal((pthread_cond_t *)&threadObjects[n]->waitCond);
#ifdef _WIN32
	SetThreadPriority(deviceThreadHandles[n],THREAD_PRIORITY_NORMAL);
#endif
	pthread_mutex_unlock((pthread_mutex_t *)&threadObjects[n]->waitMutex);
}	

void CUDAThreadingManager::DispatchFinishFrame(void** image, bool useAlpha)
{
	for (int n = 1; n < numDevices; n++)
	{
		dispatchToThisDeviceWaitTillDone(n, NULL, &WorkerPThread::GrabFrame);
		dispatchToThisDeviceWaitTillDone(0, NULL, &WorkerPThread::MergeFrames);
	}
	dispatchFinishFrameWait(0, image, useAlpha);

	pthread_mutex_lock((pthread_mutex_t *)&threadObjects[0]->waitMutex);
	waitTillReady(0);
	pthread_mutex_unlock((pthread_mutex_t *)&threadObjects[0]->waitMutex);
	
	*image = threadObjects[0]->image;
}

void CUDAThreadingManager::DispatchStopCuda()
{
	dispatchToAllDevicesWait(NULL, &WorkerPThread::StopCuda);
	
	if (isMergePinned)
		cudaFreeHost(mergeStagingBuffer);
	else
		delete mergeStagingBuffer;
}


CUDAThreadingManager::~CUDAThreadingManager()
{
	if (deviceThreadHandles)
	{
		delete deviceThreadHandles;
		deviceThreadHandles = NULL;
	}
#ifdef _WIN
	if (threadIds)
	{
		delete threadIds;
		threadIds = NULL;
	}
#endif
	if (threadObjects)
	{
		for (int n = 0; n < numDevices; n++)
		{
			delete threadObjects[n];
			threadObjects[n] = NULL;
		}
		delete threadObjects;
		threadObjects = NULL;
	}
	pthread_mutex_destroy(&readyMutex);
	pthread_mutex_destroy(&mutex);
	pthread_cond_destroy(&readyCond);
}
