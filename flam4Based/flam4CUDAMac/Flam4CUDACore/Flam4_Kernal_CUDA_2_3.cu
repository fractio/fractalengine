/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef _WIN32
#include "..\\Flam4CUDA\\Defines.h"
#else
#include "Defines.h"
#endif
#include "FlameData.h"
#include "Flam4_Kernal.cuh"

__constant__ FlameParams d_g_Flame;
__constant__ xForm d_g_Xforms[MAX_XFORMS];
__constant__ unsigned int d_g_varAccel[(MAX_XFORMS+1)];

__constant__ unsigned int shift1[4] = {6, 2, 13, 3};
__constant__ unsigned int shift2[4] = {13, 27, 21, 12};
__constant__ unsigned int shift3[4] = {18, 2, 7, 13};
__constant__ unsigned int offset[4] = {4294967294, 4294967288, 4294967280, 4294967168};

__shared__ shortPoint pointBuffer[NUM_POINTS];
__shared__ shortPoint activePoint[64];
__shared__ float redBuffer[64];
__shared__ float4 filterLocal[(DENSITY_KERNAL_RADIUS*2+16)*(DENSITY_KERNAL_RADIUS*2+16)];
__shared__ unsigned int randStates[32];

texture<uchar4, 1, cudaReadModeNormalizedFloat> texRef;

__device__ float warpRedMin(float* baseAddr)
{
	int warpIndex = threadIdx.x&31;
	if (warpIndex < 16)
		baseAddr[warpIndex]=min(baseAddr[warpIndex<<1],baseAddr[warpIndex<<1+1]);
	if (warpIndex < 8)
		baseAddr[warpIndex]=min(baseAddr[warpIndex<<1],baseAddr[warpIndex<<1+1]);
	if (warpIndex < 4)
		baseAddr[warpIndex]=min(baseAddr[warpIndex<<1],baseAddr[warpIndex<<1+1]);
	if (warpIndex < 2)
		baseAddr[warpIndex]=min(baseAddr[warpIndex<<1],baseAddr[warpIndex<<1+1]);
	if (warpIndex < 1)
		baseAddr[warpIndex]=min(baseAddr[warpIndex<<1],baseAddr[warpIndex<<1+1]);
	return *baseAddr;
}

__device__ unsigned int TausStep(unsigned int &z, int S1, int S2, int S3, unsigned int M)
{
	unsigned int b = (((z << S1) ^ z) >> S2);
	return z = (((z &M) << S3) ^ b);
}

__device__ unsigned int randInt()
{
	TausStep(randStates[threadIdx.x&31], shift1[threadIdx.x&3], shift2[threadIdx.x&3],shift3[threadIdx.x&3],offset[threadIdx.x&3]);
	return (randStates[(threadIdx.x)&31]^randStates[(threadIdx.x+1)&31]^randStates[(threadIdx.x+2)&31]^randStates[(threadIdx.x+3)&31]);
}



__device__ float randFloat()
//This function returns a random float in [0,1] and updates seed
{
	unsigned int y = randInt();
	return __int_as_float((y&0x007FFFFF)|0x3F800000)-1.0f;
}

__device__ float randFloatWarp()
//This function is a workaround for getting a warp wide rand number
{
	unsigned int y = randInt();
	return __int_as_float((randStates[(threadIdx.x&31)]&0x007FFFFF)|0x3F800000)-1.0f;
}

__device__ float4 RGBtoHSV(float4 color)
{
	float r = color.x;
	float g = color.y;
	float b = color.z;
	float mx = fmax(fmax(r,g),b);
	float mn = fmin(fmin(r,g),b);
	float h,s,v;
	if (mx == mn)
		h = 0.0f;
	else if (mx == r)
		h = .16666666667f*(g-b)/(mx-mn);
	else if (mx == g)
		h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
	else
		h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
	h = h-floorf(h);
	if (mx == 0.0f)
		s = 0.0f;
	else
		s = (mx-mn)/(mx);
	v = mx;
	return make_float4(h,s,v,color.w);
}

__device__ float4 RGBtoHSVHueAdjusted(float4 color)
{
	float r = color.x;
	float g = color.y;
	float b = color.z;
	float mx = fmax(fmax(r,g),b);
	float mn = fmin(fmin(r,g),b);
	float h,s,v;
	if (mx == mn)
		h = 0.0f;
	else if (mx == r)
		h = .16666666667f*(g-b)/(mx-mn);
	else if (mx == g)
		h = .16666666667f*(b-r)/(mx-mn)+.33333333f;
	else
		h = .16666666667f*(r-g)/(mx-mn)+.66666667f;
	h = h-floorf(h);
	if (mx == 0.0f)
		s = 0.0f;
	else
		s = (mx-mn)/(mx);
	v = mx;
	if (v > 1.0f)
	{
		if (h < .33333333f)
		{
			h += (.16666667f-h)*(1.0f-__powf(.75f,v-1.0f));
		}
		else if (h < 0.5f)
		{
			h += (h-0.5f)*(1.0f-__powf(.75f,v-1.0f));
		}
		else if (h > 0.8333333f)
		{
			h += (h-0.8333333f)*(1.0f-__powf(.75f,v-1.0f));
		}
		//float l = .2126f*r+.7152f*g+.0722f*b;
		float l = (40.0f*r+20.0f*g+b)/61.0f;
		s = min(s*__powf(l*s/v,.25f*(v-1.0f)),s);
	}
	return make_float4(h,s,v,color.w);
}

__device__ float4 HSVtoRGB(float4 color)
{
	float h = color.x;
	float s = color.y;
	float v = color.z;
	float r,g,b;
	int hi = ((int)floorf(h*6.0f))%6;
    float f = h*6.0f-floorf(h*6.0f);
    float p = v*(1.0f-s);
    float q = v*(1.0f-f*s);
    float t = v*(1.0f-(1.0f-f)*s);
    switch (hi)
    {
    case 0:
        {
            r = v;
            g = t;
            b = p;
        }break;
    case 1:
        {
            r = q;
            g = v;
            b = p;
        }break;
    case 2:
        {
            r = p;
            g = v;
            b = t;
        }break;
    case 3:
        {
            r = p;
            g = q;
            b = v;
        }break;
    case 4:
        {
            r = t;
            g = p;
            b = v;
        }break;
    case 5:
        {
            r = v;
            g = p;
            b = q;
        }break;
    }
	return make_float4(r,g,b,color.w);
}

__device__ void iteratePoint(int pointIndex, xForm* xForm, int xformIndex)
{
	//unsigned int s2 = seed2[0];
	activePoint[threadIdx.x].x = pointBuffer[pointIndex].x;
	activePoint[threadIdx.x].y = pointBuffer[pointIndex].y;
	activePoint[threadIdx.x].pal = pointBuffer[pointIndex].pal;
	float x = xForm->a*activePoint[threadIdx.x].x+xForm->b*activePoint[threadIdx.x].y+xForm->c;
	float y = xForm->d*activePoint[threadIdx.x].x+xForm->e*activePoint[threadIdx.x].y+xForm->f;
	activePoint[threadIdx.x].x = 0.0f;
	activePoint[threadIdx.x].y = 0.0f;
	float r2 = x*x+y*y;
	float r2inv = 1.0f/r2;
	float r = sqrtf(r2);
	float rinv = 1.0f/r;
	float phi = atan2f(x,y);
	float theta = .5f*PI-phi;
	if (theta > PI)
		theta -= 2.0f*PI;
	//Now apply the XForms
	if ((d_g_varAccel[xformIndex]&0x000000FF) != 0)
	{
        if ((d_g_varAccel[xformIndex]&0x00000003) != 0)
        {
			if ((d_g_varAccel[xformIndex]&0x00000001) != 0)
			{
				if (xForm->linear != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->linear*x;
					activePoint[threadIdx.x].y += xForm->linear*y;
				}
				if (xForm->sinusoidal != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->sinusoidal*__sinf(x);
					activePoint[threadIdx.x].y += xForm->sinusoidal*__sinf(y);
				}
				if (xForm->spherical != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->spherical*x*r2inv;
					activePoint[threadIdx.x].y += xForm->spherical*y*r2inv;
				}
				if (xForm->swirl != 0.0f)
				{
					float c1,s1;
					__sincosf(r2,&s1,&c1);
					activePoint[threadIdx.x].x += xForm->swirl*(x*s1-y*c1);
					activePoint[threadIdx.x].y += xForm->swirl*(x*c1+y*s1);
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000002) != 0)
			{
				if (xForm->horseshoe != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->horseshoe*(x-y)*(x+y)*rinv;
					activePoint[threadIdx.x].y += xForm->horseshoe*2.0f*x*y*rinv;
				}
				if (xForm->polar != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->polar*phi/PI;
					activePoint[threadIdx.x].y += xForm->polar*(r-1.0f);
				}
				if (xForm->handkerchief != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->handkerchief*r*__sinf(phi+r);
					activePoint[threadIdx.x].y += xForm->handkerchief*r*__cosf(phi-r);
				}
				if (xForm->heart != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->heart*r*__sinf(phi*r);
					activePoint[threadIdx.x].y += -xForm->heart*r*__cosf(phi*r);
				}
			}
        }
        if ((d_g_varAccel[xformIndex]&0x0000000C) != 0)
        {
			if ((d_g_varAccel[xformIndex]&0x00000004) != 0)
			{
				if (xForm->disc != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->disc*(phi/PI)*__sinf(PI*r);
					activePoint[threadIdx.x].y += xForm->disc*(phi/PI)*__cosf(PI*r);
				}
				if (xForm->spiral != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->spiral*rinv*(y*rinv+__sinf(r));
					activePoint[threadIdx.x].y += xForm->spiral*rinv*(x*rinv-__cosf(r));
				}
				if (xForm->hyperbolic != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->hyperbolic*x*r2inv;
					activePoint[threadIdx.x].y += xForm->hyperbolic*y;
				}
				if (xForm->diamond != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->diamond*x*rinv*__cosf(r);
					activePoint[threadIdx.x].y += xForm->diamond*y*rinv*__sinf(r);
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000008) != 0)
			{
				if (xForm->ex != 0.0f)
				{
					float m0 = __sinf(phi+r);
					float m1 = __cosf(phi-r);
					m0 = r*m0*m0*m0;
					m1 = r*m1*m1*m1;
					activePoint[threadIdx.x].x += xForm->ex*(m0+m1);
					activePoint[threadIdx.x].y += xForm->ex*(m0-m1);
				}
				if (xForm->julia != 0.0f)
				{
					float rn;
					rn = randFloat();
					float omega = ((rn<.5f)?0.0f:PI);
					activePoint[threadIdx.x].x += xForm->julia*sqrtf(r)*__cosf(.5f*phi+omega);
					activePoint[threadIdx.x].y += xForm->julia*sqrtf(r)*__sinf(.5f*phi+omega);
				}
				if (xForm->bent != 0.0f)
				{
					if (x < 0.0f)
					{
						activePoint[threadIdx.x].x += xForm->bent*2.0f*x;
					}
					else
					{
						activePoint[threadIdx.x].x += xForm->bent*x;
					}
					if (y < 0.0f)
					{
						activePoint[threadIdx.x].y += xForm->bent*.5f*y;
					}
					else
					{
						activePoint[threadIdx.x].y += xForm->bent*y;
					}
				}
				if (xForm->waves != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->waves*(x+xForm->b*__sinf(y/(xForm->c*xForm->c)));
					activePoint[threadIdx.x].y += xForm->waves*(y+xForm->e*__sinf(x/(xForm->f*xForm->f)));
				}
			}
        }
        if ((d_g_varAccel[xformIndex]&0x00000030) != 0)
        {
			if ((d_g_varAccel[xformIndex]&0x00000010) != 0)
			{
				if (xForm->fisheye != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->fisheye*y*2.0f/(r+1.0f);
					activePoint[threadIdx.x].y += xForm->fisheye*x*2.0f/(r+1.0f);
				}
				if (xForm->popcorn != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->popcorn*(x+xForm->c*__sinf(__tanf(3.0f*y)));
					activePoint[threadIdx.x].y += xForm->popcorn*(y+xForm->f*__sinf(__tanf(3.0f*x)));
				}
				if (xForm->exponential != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->exponential*__expf(x-1.0f)*__cosf(PI*y);
					activePoint[threadIdx.x].y += xForm->exponential*__expf(x-1.0f)*__sinf(PI*y);
				}
				if (xForm->power != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->power*y*rinv*__powf(r,x*rinv);
					activePoint[threadIdx.x].y += xForm->power*x*rinv*__powf(r,x*rinv);
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000020) != 0)
			{
				if (xForm->cosine != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->cosine*__cosf(PI*x)*coshf(y);
					activePoint[threadIdx.x].y += -xForm->cosine*__sinf(PI*x)*sinhf(y);
				}
				if (xForm->rings != 0.0f)
				{
					float dx = xForm->c*xForm->c;
					activePoint[threadIdx.x].x += xForm->rings*y*rinv*(fmodf((r+dx),(2.0f*dx))-dx+r*(1.0f-dx));
					activePoint[threadIdx.x].y += xForm->rings*x*rinv*(fmodf((r+dx),(2.0f*dx))-dx+r*(1.0f-dx));
				}
				if (xForm->fan != 0.0f)
				{
					float t = PI*xForm->c*xForm->c;
					float dx = (fmodf(phi+xForm->f,t)>.5f*t)
						?r*__cosf(phi-.5f*t)
						:r*__cosf(phi+.5f*t);
					activePoint[threadIdx.x].x += xForm->fan*dx;
					float dy = (fmodf(phi+xForm->f,t)>.5f*t)
						?r*__sinf(phi-.5f*t)
						:r*__sinf(phi+.5f*t);
					activePoint[threadIdx.x].y += xForm->fan*dy;
				}
				if (xForm->blob != 0.0f)		//p1p2p3
				{
					activePoint[threadIdx.x].x += xForm->blob*(xForm->blob_low+(xForm->blob_high-xForm->blob_low)*.5f*(__sinf(xForm->blob_waves*phi)+1.0f))*x;
					activePoint[threadIdx.x].y += xForm->blob*(xForm->blob_low+(xForm->blob_high-xForm->blob_low)*.5f*(__sinf(xForm->blob_waves*phi)+1.0f))*y;
				}
			}
		}
        if ((d_g_varAccel[xformIndex]&0x000000C0) != 0)
        {
			if ((d_g_varAccel[xformIndex]&0x00000040) != 0)
			{
				if (xForm->pdj	!= 0.0f)			//p4p5p6p7
				{
					activePoint[threadIdx.x].x += xForm->pdj*(__sinf(xForm->pdj_a*y)-__cosf(xForm->pdj_b*x));
					activePoint[threadIdx.x].y += xForm->pdj*(__sinf(xForm->pdj_c*x)-__cosf(xForm->pdj_d*y));
				}
				if (xForm->fan2 != 0.0f)		//p8p9
				{
					float v1 = PI*xForm->fan2_x*xForm->fan2_x;
					float v2 = xForm->fan2_y;
					float t = phi+v2-v1*floorf((phi+v2)/v1);
					float dx = t>(.5f*v1)
						?r*__sinf(phi-.5f*v1)
						:r*__sinf(phi+.5f*v1);
					activePoint[threadIdx.x].x = xForm->fan2*dx;
					float dy = t>(.5f*v1)
						?r*__cosf(phi-.5f*v1)
						:r*__cosf(phi+.5f*v1);
					activePoint[threadIdx.x].y = xForm->fan2*dy;
				}
				if (xForm->rings2 != 0.0f)		//p10
				{
					float v = xForm->rings2_val*xForm->rings2_val;
					float t = r-2.0f*v*floorf((r+v)/(2.0f*v))+r*(1.0f-v);
					activePoint[threadIdx.x].x+=xForm->rings2*t*x*rinv;
					activePoint[threadIdx.x].y += xForm->rings2*t*y*rinv;
				}
				if (xForm->eyefish != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->eyefish*(2.0f/(r+1.0f))*x;
					activePoint[threadIdx.x].y += xForm->eyefish*(2.0f/(r+1.0f))*y;
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000080) != 0)
			{
				if (xForm->bubble != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->bubble*(4.0f/(r2+4.0f))*x;
					activePoint[threadIdx.x].y += xForm->bubble*(4.0f/(r2+4.0f))*y;
				}
				if (xForm->cylinder != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->cylinder*__sinf(x);
					activePoint[threadIdx.x].y += xForm->cylinder*y;
				}
				if (xForm->perspective != 0.0f)	//p11p12
				{
					activePoint[threadIdx.x].x += xForm->perspective*(xForm->perspective_dist/(xForm->perspective_dist-y*__sinf(xForm->perspective_angle)))*x;
					activePoint[threadIdx.x].y += xForm->perspective*(xForm->perspective_dist/(xForm->perspective_dist-y*__sinf(xForm->perspective_angle)))*y*__cosf(xForm->perspective_angle);
				}
				if (xForm->noise != 0.0f)
				{
					float rn1,rn2;
					rn1 = randFloat();
					rn2 = randFloat();
					activePoint[threadIdx.x].x += xForm->noise*rn1*x*__cosf(2.0f*PI*rn2);
					activePoint[threadIdx.x].y += xForm->noise*rn1*y*__sinf(2.0f*PI*rn2);
				}
			}
        }
	}
	if ((d_g_varAccel[xformIndex]&0x0000FF00)!=0)
	{
		if ((d_g_varAccel[xformIndex]&0x00000300)!=0)
		{
			if ((d_g_varAccel[xformIndex]&0x00000100) != 0)
			{
				if (xForm->julian != 0.0f)		//p13p14
				{
					float rn;
					rn = randFloat();
					float t = (theta+2.0f*PI*truncf(rn*fabs(xForm->julian_power)))/xForm->julian_power;
					float rnew = __powf(r, xForm->julian_dist/xForm->julian_power);
					activePoint[threadIdx.x].x += xForm->julian*rnew*__cosf(t);
					activePoint[threadIdx.x].y += xForm->julian*rnew*__sinf(t);
				}
				if (xForm->juliascope != 0.0f)	//p15p16
				{
					float rn1;
					rn1 = randFloat();
					int rn = (int)truncf(rn1*fabs(xForm->juliascope_power));
					float rn0 = ((rn&1)!=0)?-1.0f:1.0f;
					float t = (rn0*theta+2.0f*PI*truncf(rn1*fabs(xForm->juliascope_power)))/xForm->juliascope_power;
					float rnew = __powf(r, xForm->juliascope_dist/xForm->juliascope_power);
					activePoint[threadIdx.x].x += xForm->juliascope*rnew*__cosf(t);
					activePoint[threadIdx.x].y += xForm->juliascope*rnew*__sinf(t);
				}
				if (xForm->blur != 0.0f)
				{
					float rn1,rn2;
					rn1 = randFloat();
					rn2 = randFloat();
					activePoint[threadIdx.x].x += xForm->blur*rn1*__cosf(2.0f*PI*rn2);
					activePoint[threadIdx.x].y += xForm->blur*rn1*__sinf(2.0f*PI*rn2);
				}
				if (xForm->gaussian_blur != 0.0f)
				{
					float rn1;
					rn1 = randFloat()+randFloat()+randFloat()+randFloat()-2.0f;
					float rn2;
					rn2 = randFloat();
					activePoint[threadIdx.x].x += xForm->gaussian_blur*rn1*__cosf(2.0f*PI*rn2);
					activePoint[threadIdx.x].y += xForm->gaussian_blur*rn1*__sinf(2.0f*PI*rn2);
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000200) != 0)
			{
				if (xForm->radial_blur != 0.0f)	//p17
				{
					float rn1;
					rn1 = randFloat()+randFloat()+randFloat()+randFloat()-2.0f;
					float t1 = xForm->radial_blur*rn1;
					float t2 = theta+t1*__sinf(xForm->radial_blur_angle*PI*.5f);
					float t3 = t1*__cosf(xForm->radial_blur_angle*PI*.5f)-1.0f;
					activePoint[threadIdx.x].x += r*__cosf(t2)+t3*x;
					activePoint[threadIdx.x].y += r*__sinf(t2)+t3*y;
				}
				if (xForm->pie != 0.0f)			//p18p19p20
				{
					float rn0;
					rn0=randFloat();
					float t1 = truncf(rn0*xForm->pie_slices+0.5f);
					rn0=randFloat();
					float t2 = xForm->pie_rotation+2.0f*PI*(t1+rn0*xForm->pie_thickness)/xForm->pie_slices;
					rn0=randFloat();
					float rn = xForm->pie*rn0;
					activePoint[threadIdx.x].x += rn*__cosf(t2);
					activePoint[threadIdx.x].y += rn*__sinf(t2);
				}
				if (xForm->ngon != 0.0f)		//p21p22p23p24
				{
					float rf = __powf(r,xForm->ngon_power);
					float b = 2*PI/xForm->ngon_sides;
					float p = theta-b*floorf(theta/b);
					if (p > .5f*b)
						p -= b;
					float amp = (xForm->ngon_corners*(1.0f/__cosf(p)-1.0f)+xForm->ngon_circle)/rf;
					activePoint[threadIdx.x].x += xForm->ngon*x*amp;
					activePoint[threadIdx.x].y += xForm->ngon*y*amp;
				}
				if (xForm->curl != 0.0f)		//p25p26
				{
					float t1 = 1.0f+xForm->curl_c1*x+xForm->curl_c2*(x*x-y*y);
					float t2 = xForm->curl_c1*y+2.0f*xForm->curl_c2*x*y;
					activePoint[threadIdx.x].x += xForm->curl*(1.0f/(t1*t1+t2*t2))*(x*t1+y*t2);
					activePoint[threadIdx.x].y += xForm->curl*(1.0f/(t1*t1+t2*t2))*(y*t1-x*t2);
				}
			}
		}
		if ((d_g_varAccel[xformIndex]&0x00000C00)!=0)
		{
			if ((d_g_varAccel[xformIndex]&0x00000400) != 0)
			{
				if (xForm->rectangles != 0.0f)	//p27p28
				{
					activePoint[threadIdx.x].x += (xForm->rectangles_x == 0.0f)? xForm->rectangles*x : xForm->rectangles*((2.0f*floor(x/xForm->rectangles_x)+1.0f)*xForm->rectangles_x-x);
					activePoint[threadIdx.x].y += (xForm->rectangles_y == 0.0f)? xForm->rectangles*y : xForm->rectangles*((2.0f*floor(y/xForm->rectangles_y)+1.0f)*xForm->rectangles_y-y);
				}
				if (xForm->arch != 0.0f)
				{
					float rn;
					rn = randFloat();
					float v = xForm->arch;
					activePoint[threadIdx.x].x += v*__sinf(rn*PI*v);
					activePoint[threadIdx.x].y += v*__sinf(rn*PI*v)*__tanf(rn*PI*v);
				}
				if (xForm->tangent != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->tangent*(__sinf(x)/__cosf(y));
					activePoint[threadIdx.x].y += xForm->tangent*(__tanf(y));
				}
				if (xForm->square != 0.0f)
				{
					float rn1,rn2;
					rn1 = randFloat();
					rn2 = randFloat();
					activePoint[threadIdx.x].x += xForm->square*(rn1-.5f);
					activePoint[threadIdx.x].y += xForm->square*(rn2-.5f);
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00000800) != 0)
			{
				if (xForm->rays != 0.0f)
				{
					float rn;
					rn = randFloat();
					float v = xForm->rays;
					float front = v*__tanf(rn*PI*v)*r2inv;
					activePoint[threadIdx.x].x += v*front*__cosf(x);
					activePoint[threadIdx.x].y += v*front*__sinf(y);
				}
				if (xForm->blade != 0.0f)
				{
					float rn;
					rn = randFloat();
					float v = xForm->blade;
					activePoint[threadIdx.x].x += v*x*(__cosf(rn*r*v)+__sinf(rn*r*v));
					activePoint[threadIdx.x].y += v*x*(__cosf(rn*r*v)-__sinf(rn*r*v));
				}
				if (xForm->secant != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->secant*x;
					activePoint[threadIdx.x].y += xForm->secant/(xForm->secant*__cosf(xForm->secant*r));
				}
				if (xForm->twintrian != 0.0f)
				{
					float rn;
					rn = randFloat();
					float v = xForm->twintrian;
					float t = __log10f(__sinf(rn*r*v)*__sinf(rn*r*v))+__cosf(rn*r*v);
					activePoint[threadIdx.x].x += v*x*t;
					activePoint[threadIdx.x].y += v*x*(t-PI*__sinf(rn*r*v));
				}
			}
		}
		if ((d_g_varAccel[xformIndex]&0x00003000)!=0)
		{
			if ((d_g_varAccel[xformIndex]&0x00001000) != 0)
			{
				if (xForm->cross != 0.0f)
				{
					float oddr = x*x-y*y;
					oddr*=oddr;
					activePoint[threadIdx.x].x += xForm->cross*sqrtf(1.0f/oddr)*x;
					activePoint[threadIdx.x].y += xForm->cross*sqrtf(1.0f/oddr)*y;
				}
				if (xForm->disc2 != 0.0f)
				{
					float sinadd, cosadd;
					__sincosf(xForm->disc2_twist,&sinadd,&cosadd);
					cosadd -= 1.0f;
					if (fabs(xForm->disc2_twist)>2.0f*PI)
					{
						float k = 1.0f+ xForm->disc2_twist-copysignf(2.0f*PI,xForm->disc2_twist);
						sinadd *= k;
						cosadd *= k;
					}
					float t = xForm->disc2_rot*PI*(x+y);
					activePoint[threadIdx.x].x += xForm->disc2*phi*(__sinf(t)+cosadd)/PI;
					activePoint[threadIdx.x].y += xForm->disc2*phi*(__cosf(t)+sinadd)/PI;
				}
				if (xForm->supershape != 0.0f)
				{
					float t1 = fabs(__cosf((xForm->supershape_m*theta+PI)*.25f));
					float t2 = fabs(__sinf((xForm->supershape_m*theta+PI)*.25f));
					t1 = __powf(t1,xForm->supershape_n2);
					t2 = __powf(t2,xForm->supershape_n3);
					float rn;
					rn = randFloat();
					float rnew = xForm->supershape*((xForm->supershape_rnd*rn+(1.0f-xForm->supershape_rnd)*r-xForm->supershape_holes)*__powf(t1+t2,-1.0f/xForm->supershape_n1)*rinv);
					activePoint[threadIdx.x].x += rnew*x;
					activePoint[threadIdx.x].y += rnew*y;
				}
				if (xForm->flower != 0.0f)
				{
					float rn;
					rn = randFloat();
					float rnew = xForm->flower*(rn-xForm->flower_holes)*__cosf(xForm->flower_petals*theta)*rinv;
					activePoint[threadIdx.x].x += rnew*x;
					activePoint[threadIdx.x].y += rnew*y;
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00002000) != 0)
			{
				if (xForm->conic != 0.0f)
				{
					float rn;
					rn = randFloat();
					float rnew = xForm->conic*(rn-xForm->conic_holes)*xForm->conic_eccen/(1.0f+xForm->conic_eccen*x*rinv)*rinv;
					activePoint[threadIdx.x].x += rnew*x;
					activePoint[threadIdx.x].y += rnew*y;
				}
				if (xForm->parabola != 0.0f)
				{
					float rn;
					rn = randFloat();
					activePoint[threadIdx.x].x += xForm->parabola*xForm->parabola_height*__sinf(r)*__sinf(r)*rn;
					rn = randFloat();
					activePoint[threadIdx.x].y += xForm->parabola*xForm->parabola_width*__cosf(r)*rn;
				}
				if (xForm->bent2 != 0.0f)
				{
					activePoint[threadIdx.x].x += xForm->bent2*((x < 0.0f) ? x*xForm->bent2_x : x);
					activePoint[threadIdx.x].y += xForm->bent2*((y < 0.0f) ? y*xForm->bent2_y : y);
				}
				if (xForm->bipolar != 0.0f)
				{
				}
			}
		}
		if ((d_g_varAccel[xformIndex]&0x0000C000) != 0)
		{
			if ((d_g_varAccel[xformIndex]&0x00004000) != 0)
			{
				if (xForm->boarders != 0.0f)
				{
				}
				if (xForm->butterfly != 0.0f)
				{
				}
				if (xForm->cell != 0.0f)
				{
				}
				if (xForm->cpow != 0.0f)
				{
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00008000) != 0)
			{
				if (xForm->curve != 0.0f)
				{
				}
				if (xForm->edisc != 0.0f)
				{
				}
				if (xForm->elliptic != 0.0f)
				{
				}
				if (xForm->escher != 0.0f)
				{
				}
			}
		}
	}
	if ((d_g_varAccel[xformIndex]&0x00FF0000)!= 0)
	{
		if ((d_g_varAccel[xformIndex]&0x00030000) != 0)
		{
			if ((d_g_varAccel[xformIndex]&0x00010000) != 0)
			{
				if (xForm->foci != 0.0f)
				{
				}
				if (xForm->lazysusan != 0.0f)
				{
				}
				if (xForm->loonie != 0.0f)
				{
				}
				if (xForm->pre_blur != 0.0f)
				{
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00020000) != 0)
			{
				if (xForm->modulus != 0.0f)
				{
				}
				if (xForm->oscilloscope != 0.0f)
				{
				}
				if (xForm->polar2 != 0.0f)
				{
				}
				if (xForm->popcorn2 != 0.0f)
				{
				}
			}
		}
		if ((d_g_varAccel[xformIndex]&0x000C0000) != 0)
		{
			if ((d_g_varAccel[xformIndex]&0x00040000) != 0)
			{
				if (xForm->scry != 0.0f)
				{
				}
				if (xForm->separation != 0.0f)
				{
				}
				if (xForm->split != 0.0f)
				{
				}
				if (xForm->splits != 0.0f)
				{
				}
			}
			if ((d_g_varAccel[xformIndex]&0x00080000) != 0)
			{
				if (xForm->stripes != 0.0f)
				{
				}
				if (xForm->wedge != 0.0f)
				{
				}
				if (xForm->wedge_julia != 0.0f)
				{
				}
				if (xForm->wedge_sph != 0.0f)
				{
				}
			}
		}
		if ((d_g_varAccel[xformIndex]&0x00300000) != 0)
		{
			if ((d_g_varAccel[xformIndex]&0x00100000) != 0)
			{
				if (xForm->whorl != 0.0f)
				{
				}
				if (xForm->waves2 != 0.0f)
				{
				}
			}
		}
	}
	if (d_g_Flame.symmetryKind != 0.0f)
	{
		if (d_g_Flame.symmetryKind > 0.0f)
		{
			float rn;
			rn = randFloat();
			x = __cosf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].x-__sinf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].y;
			y = __sinf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].x+__cosf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].y;
			activePoint[threadIdx.x].x = x;
			activePoint[threadIdx.x].y = y;
		}
		else
		{
			//pick a random symmetry plane and reflect across it.
			float rn;
			rn = randFloat();
			if (rn>0.5f)
				x = -x;
			rn = randFloat();
			x = __cosf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].x-__sinf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].y;
			y = __sinf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].x+__cosf(2.0f*PI*floorf(rn*d_g_Flame.symmetryKind)/d_g_Flame.symmetryKind)*activePoint[threadIdx.x].y;
			activePoint[threadIdx.x].x = x;
			activePoint[threadIdx.x].y = y;
		}
	}
	x = xForm->pa*activePoint[threadIdx.x].x+xForm->pb*activePoint[threadIdx.x].y+xForm->pc;
	y = xForm->pd*activePoint[threadIdx.x].x+xForm->pe*activePoint[threadIdx.x].y+xForm->pf;
	activePoint[threadIdx.x].x=x;
	activePoint[threadIdx.x].y=y;
	float s0 = xForm->symmetry;
	float s1 = .5f-.5f*s0;
	activePoint[threadIdx.x].pal = (activePoint[threadIdx.x].pal+xForm->color)*s1+activePoint[threadIdx.x].pal*s0;
}

__device__ void applyRotation(shortPoint* Point)
{
	float x,y;
	x = Point->x-d_g_Flame.center[0];
	y = Point->y-d_g_Flame.center[1];
	Point->x = x*__cosf(d_g_Flame.rotation)-y*__sinf(d_g_Flame.rotation)+d_g_Flame.center[0];
	Point->y = x*__sinf(d_g_Flame.rotation)+y*__cosf(d_g_Flame.rotation)+d_g_Flame.center[1];
}

__global__ void fuseKernal(point*points, unsigned int* perThreadRandSeeds, unsigned int* perWarpRandSeeds, int xDim, int yDim)
{
	const int ix = (blockDim.x*(blockIdx.y*gridDim.x+blockIdx.x))+threadIdx.x;
	const int bix = ix/warpSize;
	// Load up the local point buffer
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		pointBuffer[n].x = points[__mul24(NUM_POINTS,bix>>1)+n].x;
		pointBuffer[n].y = points[__mul24(NUM_POINTS,bix>>1)+n].y;
		pointBuffer[n].pal = points[__mul24(NUM_POINTS,bix>>1)+n].pal;
	}
	// Iterate some points!
	randStates[threadIdx.x&31]=perThreadRandSeeds[ix];
	randInt();
	for (int j = 0; j < NUM_POINTS; j++)
	{
		float w;
		w=randFloatWarp();
		if ((threadIdx.x&31)!=0)	//workaround for strange bug with compute 1.0 hardware where device crashes if all threads in a warp try to read from the same address
			w = __int_as_float((randStates[0]&0x007FFFFF)|0x3F800000)-1.0f;
		unsigned int r = 0;
		while ((w >= d_g_Xforms[r].weight)&&(r < d_g_Flame.numTrans))
		{
			r++;
		}
		//To ensure that every point gets iterated the full number of times, we use a simple hash to give them at least some xform independance
		unsigned int p = (__mul24(43,threadIdx.x)+j)%NUM_POINTS;
		randInt();
		iteratePoint(p,&d_g_Xforms[r],r);
		if (isfinite(activePoint[threadIdx.x].x)&&isfinite(activePoint[threadIdx.x].y))
			pointBuffer[p]=activePoint[threadIdx.x];
	}
	perThreadRandSeeds[ix]=randStates[threadIdx.x&31];
	// Store the new point buffer back to global memory
	__syncthreads();
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		points[__mul24(NUM_POINTS,bix>>1)+n].x = pointBuffer[n].x;
		points[__mul24(NUM_POINTS,bix>>1)+n].y = pointBuffer[n].y;
		points[__mul24(NUM_POINTS,bix>>1)+n].pal = pointBuffer[n].pal;
	}
}

__global__ void iteratePointsKernal(float4* renderTarget, point* points, unsigned int* perThreadRandSeeds, unsigned int* perWarpRandSeeds, int xDim, int yDim)
{
	const int ix = (blockDim.x*(blockIdx.y*gridDim.x+blockIdx.x))+threadIdx.x;
	const int bix = ix/warpSize;
	// Load up the local point buffer
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		pointBuffer[n].x = points[__mul24(NUM_POINTS,bix>>1)+n].x;
		pointBuffer[n].y = points[__mul24(NUM_POINTS,bix>>1)+n].y;
		pointBuffer[n].pal = points[__mul24(NUM_POINTS,bix>>1)+n].pal;
	}
	// Iterate some points!
	randStates[threadIdx.x&31]=perThreadRandSeeds[ix];
	for (int j = 0; j < NUM_ITERATIONS; j++)
	{
		//Pick xform for this iteration
		float w;
		w=randFloatWarp();
		if ((threadIdx.x&31)!=0)	//workaround for strange bug with compute 1.0 hardware where device crashes if all threads in a warp try to read from the same address
			w = __int_as_float((randStates[0]&0x007FFFFF)|0x3F800000)-1.0f;
		unsigned int r = 0;
		while ((w >= d_g_Xforms[r].weight)&&(r < d_g_Flame.numTrans))
		{
			r++;
		}
		//Now each thread chooses a point at random from the point pool.  This is done to allow each point to have a seperate xform path while retaining SIMD
		unsigned int p = randInt()%(NUM_POINTS>>1)+(bix&1)*(NUM_POINTS>>1);
		//Iterate the chosen point and store it back to the pool
		iteratePoint(p,&d_g_Xforms[r],r);
		if (isfinite(activePoint[threadIdx.x].x+activePoint[threadIdx.x].y))
			pointBuffer[p] = activePoint[threadIdx.x];
		//Prepare the point for displey.  First the final transformation is applied
		if (d_g_Flame.isFinalXform != 0)
			iteratePoint(p,&d_g_Flame.finalXform,MAX_XFORMS);
		//Then rotation
		applyRotation(&activePoint[threadIdx.x]);
		//Finally, we randomly jitter the point within a 1/2 pixel radius to obtain antialiasing
		float dr;
		dr = randFloat();
		dr = __expf(SUPERSAMPLE_WIDTH*sqrtf(-__logf(dr)))-1.0f;
		float rn;
		rn = randFloat();
		float dtheta = (rn)*2.0f*PI;
		int x,y;
		x = floorf((((activePoint[threadIdx.x].x-d_g_Flame.center[0])/d_g_Flame.size[0]+.5f)*(float)xDim)+dr*__cosf(dtheta));
		y = floorf(((-(activePoint[threadIdx.x].y-d_g_Flame.center[1])/d_g_Flame.size[1]+.5f)*(float)yDim)+dr*__sinf(dtheta));
		//And render the point to the accumulation buffer
		//redBuffer[threadIdx.x]=((x<xDim)&&(y<yDim)&&(x>=0)&&(y>=0))?renderTarget[y*xDim+x].w:1e100;
		//float minimum = warpRedMin(&redBuffer[threadIdx.x&~31]);
		if ((x < xDim)&&(y < yDim)&&(x>=0)&&(y>=0))
		{
			float4 output = tex1D(texRef,activePoint[threadIdx.x].pal);
			renderTarget[y*xDim+x].x += output.x*d_g_Xforms[r].opacity;
			renderTarget[y*xDim+x].y += output.y*d_g_Xforms[r].opacity;
			renderTarget[y*xDim+x].z += output.z*d_g_Xforms[r].opacity;
			renderTarget[y*xDim+x].w += output.w*d_g_Xforms[r].opacity;
		}
	}
	perThreadRandSeeds[ix]=randStates[threadIdx.x&31];
	// Store the new point buffer back to global memory
	__syncthreads();
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		points[__mul24(NUM_POINTS,bix>>1)+n].x = pointBuffer[n].x;
		points[__mul24(NUM_POINTS,bix>>1)+n].y = pointBuffer[n].y;
		points[__mul24(NUM_POINTS,bix>>1)+n].pal = pointBuffer[n].pal;
	}
}

__global__ void deNoiseKernal(float4* renderTarget, int xDim, int yDim)
{
	const int ix = (blockDim.x*blockIdx.x)+threadIdx.x;
	const int iy = (blockDim.y*blockIdx.y)+threadIdx.y;
	if ((ix < xDim)&&(iy < yDim))
	{
		if (renderTarget[iy*xDim+ix].w < 1.1f)
		{
			renderTarget[iy*xDim+ix].x -= 1.0f;
			renderTarget[iy*xDim+ix].y -= 1.0f;
			renderTarget[iy*xDim+ix].z -= 1.0f;
			renderTarget[iy*xDim+ix].w -= 1.0f;
		}
	}
}

__global__ void iteratePointsMHKernal(float4* renderTarget, point* points, unsigned int* perThreadRandSeeds, unsigned int* perWarpRandSeeds, int xDim, int yDim, float MHRadius, float MHprobCap)
{
	const int ix = (blockDim.x*(blockIdx.y*gridDim.x+blockIdx.x))+threadIdx.x;
	const int bix = ix/warpSize;
	// Load up the local point buffer
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		pointBuffer[n].x = points[__mul24(NUM_POINTS,bix>>1)+n].x;
		pointBuffer[n].y = points[__mul24(NUM_POINTS,bix>>1)+n].y;
		pointBuffer[n].pal = points[__mul24(NUM_POINTS,bix>>1)+n].pal;
	}
	// Iterate some points!
	randStates[threadIdx.x&31]=perThreadRandSeeds[ix];
	for (int j = 0; j < NUM_ITERATIONS; j++)
	{
		//Pick xform for this iteration
		float w;
		w=randFloatWarp();
		if ((threadIdx.x&31)!=0)	//workaround for strange bug with compute 1.0 hardware where device crashes if all threads in a warp try to read from the same address
			w = __int_as_float((randStates[0]&0x007FFFFF)|0x3F800000)-1.0f;
		unsigned int r = 0;
		while ((w >= d_g_Xforms[r].weight)&&(r < d_g_Flame.numTrans))
		{
			r++;
		}
		//Now each thread chooses a point at random from the point pool.  This is done to allow each point to have a seperate xform path while retaining SIMD
		unsigned int p = randInt()%(NUM_POINTS>>1)+(bix&1)*(NUM_POINTS>>1);
		shortPoint oldPoint = pointBuffer[p];
		//Iterate the chosen point and store it back to the pool
		iteratePoint(p,&d_g_Xforms[r],r);
		if (isfinite(activePoint[threadIdx.x].x+activePoint[threadIdx.x].y))
			pointBuffer[p] = activePoint[threadIdx.x];
		//Prepare the point for displey.  First the final transformation is applied
		if (d_g_Flame.isFinalXform != 0)
			iteratePoint(p,&d_g_Flame.finalXform,MAX_XFORMS);
		//Then rotation
		applyRotation(&activePoint[threadIdx.x]);
		//Finally, we randomly jitter the point within a 1/2 pixel radius to obtain antialiasing
		float dr;
		dr = randFloat();
		dr = __expf(SUPERSAMPLE_WIDTH*sqrtf(-__logf(dr)))-1.0f;
		float rn;
		rn = randFloat();
		float dtheta = (rn)*2.0f*PI;
		int x,y;
		x = floorf((((activePoint[threadIdx.x].x-d_g_Flame.center[0])/d_g_Flame.size[0]+.5f)*(float)xDim)+dr*__cosf(dtheta));
		y = floorf(((-(activePoint[threadIdx.x].y-d_g_Flame.center[1])/d_g_Flame.size[1]+.5f)*(float)yDim)+dr*__sinf(dtheta));
		//Now get the warp wide chance for jitter and reiteration
		//redBuffer[threadIdx.x]=((x<xDim)&&(y<yDim)&&(x>=0)&&(y>=0))?renderTarget[y*xDim+x].w:1e100;
		float minimum = ((x<xDim)&&(y<yDim)&&(x>=0)&&(y>=0))?renderTarget[y*xDim+x].w:1e100;//warpRedMin(&redBuffer[threadIdx.x&~31]);
		float probability = 1.0f/(0.3f*minimum+1.0f);
		probability = min(probability,0.95f);
		float retries = floorf(1.0f/(1.0f-probability));
		//And render the point to the accumulation buffer
		if ((x < xDim)&&(y < yDim)&&(x>=0)&&(y>=0))
		{
			float4 output = tex1D(texRef,activePoint[threadIdx.x].pal);
			renderTarget[y*xDim+x].x += output.x*d_g_Xforms[r].opacity/retries;
			renderTarget[y*xDim+x].y += output.y*d_g_Xforms[r].opacity/retries;
			renderTarget[y*xDim+x].z += output.z*d_g_Xforms[r].opacity/retries;
			renderTarget[y*xDim+x].w += output.w*d_g_Xforms[r].opacity/retries;
		}
		for (float q=1.0f; q < retries; q++)
		{
			//jitter the old point and feed it back into the iterator
			dr = randFloat();
			dr = (__expf(sqrtf(-__logf(dr)))-1.0f)*d_g_Flame.size[0]/(((float)xDim))*sqrtf(retries);
			rn = randFloat();
			dtheta = (rn)*2.0f*PI;
			pointBuffer[p].x = oldPoint.x+dr*__cosf(dtheta);
			pointBuffer[p].y = oldPoint.y+dr*__sinf(dtheta);
			pointBuffer[p].pal = oldPoint.pal;;
			//Iterate the chosen point and store it back to the pool
			iteratePoint(p,&d_g_Xforms[r],r);
			if (isfinite(activePoint[threadIdx.x].x+activePoint[threadIdx.x].y))
				pointBuffer[p] = activePoint[threadIdx.x];
			//Prepare the point for display.  First the final transformation is applied
			if (d_g_Flame.isFinalXform != 0)
				iteratePoint(p,&d_g_Flame.finalXform,MAX_XFORMS);
			//Then rotation
			applyRotation(&activePoint[threadIdx.x]);
			//Finally, we randomly jitter the point within a 1/2 pixel radius to obtain antialiasing
			dr = randFloat();
			dr = __expf(SUPERSAMPLE_WIDTH*sqrtf(-__logf(dr)))-1.0f;
			rn = randFloat();
			dtheta = (rn)*2.0f*PI;
			x = floorf((((activePoint[threadIdx.x].x-d_g_Flame.center[0])/d_g_Flame.size[0]+.5f)*(float)xDim)+dr*__cosf(dtheta));
			y = floorf(((-(activePoint[threadIdx.x].y-d_g_Flame.center[1])/d_g_Flame.size[1]+.5f)*(float)yDim)+dr*__sinf(dtheta));
			if ((x < xDim)&&(y < yDim)&&(x>=0)&&(y>=0))
			{
				float4 output = tex1D(texRef,activePoint[threadIdx.x].pal);
				renderTarget[y*xDim+x].x += output.x*d_g_Xforms[r].opacity/retries;
				renderTarget[y*xDim+x].y += output.y*d_g_Xforms[r].opacity/retries;
				renderTarget[y*xDim+x].z += output.z*d_g_Xforms[r].opacity/retries;
				renderTarget[y*xDim+x].w += output.w*d_g_Xforms[r].opacity/retries;
			}
		}
	}
	perThreadRandSeeds[ix]=randStates[threadIdx.x&31];
	// Store the new point buffer back to global memory
	__syncthreads();
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		points[__mul24(NUM_POINTS,bix>>1)+n].x = pointBuffer[n].x;
		points[__mul24(NUM_POINTS,bix>>1)+n].y = pointBuffer[n].y;
		points[__mul24(NUM_POINTS,bix>>1)+n].pal = pointBuffer[n].pal;
	}
}

__global__ void postProcessKernal(float4* renderTarget, float4* accumBuffer, int xDim, int yDim)
{
	const int ix = (blockDim.x*blockIdx.x)+threadIdx.x;
	const int iy = (blockDim.y*blockIdx.y)+threadIdx.y;
	if ((ix < xDim)&&(iy < yDim))
	{
		float k1 = (d_g_Flame.brightness*268.0f)/255.0f;
		float area = fabs(d_g_Flame.size[0]*d_g_Flame.size[1]);
		float k2 = ((float)(xDim*yDim))/(area*((float)(NUM_ITERATIONS))*d_g_Flame.quality*warpSize*1024.0f);
		float4 rgba = accumBuffer[iy*xDim+ix];
		float a = (k1* __logf(1.0f+k2*rgba.w));
		float ls = a/rgba.w;
		rgba.x = ls*rgba.x;
		rgba.y = ls*rgba.y;
		rgba.z = ls*rgba.z;
		float alpha = __powf(a, 1.0f/d_g_Flame.gamma-1.0f);
		ls = d_g_Flame.vibrancy*alpha;
		rgba.x = ls*rgba.x+(1.0f-d_g_Flame.vibrancy)*copysignf(__powf(fabsf(rgba.x),1.0f/d_g_Flame.gamma),rgba.x);
		rgba.y = ls*rgba.y+(1.0f-d_g_Flame.vibrancy)*copysignf(__powf(fabsf(rgba.y),1.0f/d_g_Flame.gamma),rgba.y);
		rgba.z = ls*rgba.z+(1.0f-d_g_Flame.vibrancy)*copysignf(__powf(fabsf(rgba.z),1.0f/d_g_Flame.gamma),rgba.z);
		alpha = __powf(a, 1.0f/d_g_Flame.gamma);
		alpha = min(alpha,1.0f);
		rgba = RGBtoHSVHueAdjusted(rgba);
		if (rgba.z > 1.0f)
		{
			//rgba.y /= rgba.z;
			rgba.z = 1.0f;
		}
		rgba = HSVtoRGB(rgba);
		if (isfinite(rgba.x))
		{
			renderTarget[iy*xDim+ix].x=rgba.x+renderTarget[iy*xDim+ix].x*(1.0f-alpha);
			renderTarget[iy*xDim+ix].y=rgba.y+renderTarget[iy*xDim+ix].y*(1.0f-alpha);
			renderTarget[iy*xDim+ix].z=rgba.z+renderTarget[iy*xDim+ix].z*(1.0f-alpha);
			renderTarget[iy*xDim+ix].w=alpha;
		}
	}
}

__global__ void setBufferKernal(float4* renderTarget, float4 value, int xDim, int yDim)
{
	//This kernal simply fills the render target with value
	const int ix = (blockDim.x*blockIdx.x)+threadIdx.x;
	const int iy = (blockDim.y*blockIdx.y)+threadIdx.y;
	if ((ix < xDim)&&(iy < yDim))
		renderTarget[iy*xDim+ix] = value;
}

__global__ void resetPointsKernal(point* points, unsigned int* perThreadRandSeeds)
{
	//All we're doing here is creating a batch of random points to seed things
	const int ix = (blockDim.x*((blockIdx.y*gridDim.x)+blockIdx.x))+threadIdx.x;
	int bix = ix/blockDim.x;
	randStates[threadIdx.x&31] = perThreadRandSeeds[ix];
	for (int n = threadIdx.x; n < NUM_POINTS; n+=blockDim.x)
	{
		point freshPoint;
		float rn;
		rn = randFloat();
		freshPoint.x = rn*2.0f-1.0f;
		rn = randFloat();
		freshPoint.y = rn*2.0f-1.0f;
		rn = randFloat();
		freshPoint.pal = rn;
		points[__mul24(NUM_POINTS,bix)+n] = freshPoint;
	}
	perThreadRandSeeds[ix] = randStates[threadIdx.x&31];
}

__global__ void DensityEstimationKernal(float4* output, float4* input, int xDim, int yDim, float baseThreshold)
{
	const int ix = blockDim.x*blockIdx.x+threadIdx.x;
	const int iy = blockDim.y*blockIdx.y+threadIdx.y;
	const int lidx = (blockDim.x+DENSITY_KERNAL_RADIUS*2)*threadIdx.y+threadIdx.x;
	//First, we load up the block of pixels we will be working on into shared memory
	for (int y = 0; y < blockDim.y+DENSITY_KERNAL_RADIUS*2-threadIdx.y; y += blockDim.y)
	{
		for(int x = 0; x < blockDim.x+DENSITY_KERNAL_RADIUS*2-threadIdx.x; x += blockDim.x)
		{
			filterLocal[lidx+x+y*(blockDim.x+DENSITY_KERNAL_RADIUS*2)] = input[max(min(ix+x-DENSITY_KERNAL_RADIUS,xDim-1),0)+max(min(iy+y-DENSITY_KERNAL_RADIUS,yDim-1),0)*xDim];//clamped addressing
		}
	}
	__syncthreads();
	//Next, apply the actual filter
	if ((ix < xDim)&& (iy < yDim))
	{
		float4 pnt = input[ix+iy*xDim];
		float4 sum = make_float4(0.0f,0.0f,0.0f,0.0f);
		float count = 0.0f;
		for (int y = 0; y < DENSITY_KERNAL_RADIUS*2+1; y++)
		{
			for (int x = 0; x<DENSITY_KERNAL_RADIUS*2+1; x++)
			{
				int cellIdx = lidx+x+y*(blockDim.x+2*DENSITY_KERNAL_RADIUS);
				float invDist = 1.0f/(((float)(x-DENSITY_KERNAL_RADIUS)*(float)(x-DENSITY_KERNAL_RADIUS)+(float)(y-DENSITY_KERNAL_RADIUS)*(float)(y-DENSITY_KERNAL_RADIUS))+1.0f);
				float deviation = fabsf(erff((filterLocal[cellIdx].w-pnt.w)/(sqrtf(8.0f*pnt.w)+5.0f)));
				if (deviation<=__powf(baseThreshold*.9f,sqrtf(1.0f/invDist))*__powf(pnt.w+1.0f,-0.25f))
				{
					sum.x += filterLocal[cellIdx].x*invDist;
					sum.y += filterLocal[cellIdx].y*invDist;
					sum.z += filterLocal[cellIdx].z*invDist;
					sum.w += filterLocal[cellIdx].w*invDist;
					count += invDist;
				}
			}
		}
		sum.x/=count;
		sum.y/=count;
		sum.z/=count;
		sum.w/=count;
		//And store the result
		output[ix+xDim*iy] = sum;
	}
}

__global__ void RGBA128FtoRGBA32UKernal(uchar4* output, float4* input, int xDim, int yDim, bool useAlpha)
{
	//This kernal converts a 32bit per channel floating point image to a 8bit per channel integer image
	const int ix = (blockDim.x*blockIdx.x)+threadIdx.x;
	const int iy = (blockDim.y*blockIdx.y)+threadIdx.y;
	if ((ix < xDim)&&(iy < yDim))
	{
		if (useAlpha)
		{
			if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w) && (input[iy*xDim+ix].w!=0.0f))
				output[iy*xDim+ix] = make_uchar4(
					max(min(input[iy*xDim+ix].x/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
					max(min(input[iy*xDim+ix].y/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
					max(min(input[iy*xDim+ix].z/input[iy*xDim+ix].w,1.0f),0.0f)*255.0f,
					max(min(input[iy*xDim+ix].w,1.0f),0.0f)*255.0f);
			else
				output[iy*xDim+ix]=make_uchar4(0,0,0,0);
		}
		else
		{
			if (isfinite(input[iy*xDim+ix].x+input[iy*xDim+ix].y+input[iy*xDim+ix].z+input[iy*xDim+ix].w))
				output[iy*xDim+ix] = make_uchar4(
					max(min(input[iy*xDim+ix].x,1.0f),0.0f)*255.0f,
					max(min(input[iy*xDim+ix].y,1.0f),0.0f)*255.0f,
					max(min(input[iy*xDim+ix].z,1.0f),0.0f)*255.0f,
					255);
			else
				output[iy*xDim+ix]=make_uchar4(0,0,0,255);
		}
	}
}

__global__ void MergeKernal(float4* accum, float4* input, int xDim, int yDim)
{
	const int ix = (blockDim.x*blockIdx.x)+threadIdx.x;
	const int iy = (blockDim.y*blockIdx.y)+threadIdx.y;
	if ((ix < xDim)&&(iy < yDim))
	{
		accum[iy*xDim+ix].x += input[iy*xDim+ix].x;
		accum[iy*xDim+ix].y += input[iy*xDim+ix].y;
		accum[iy*xDim+ix].z += input[iy*xDim+ix].z;
		accum[iy*xDim+ix].w += input[iy*xDim+ix].w;
	}
}
