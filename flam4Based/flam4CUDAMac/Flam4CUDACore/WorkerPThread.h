/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if defined(_WIN32)
#include <windows.h>
#include <GL/glew.h>
#include "..\\Flam4CUDA\\Defines.h"
typedef void T_THREADFUNC;
typedef HANDLE T_THREAD;

#elif defined(__APPLE__)
#include "Defines.h"
typedef void *T_THREADFUNC;
typedef pthread_t T_THREAD;

#import <Cocoa/Cocoa.h>
#import <pthread.h>
#define WINAPI
#endif

#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>

class WorkerPThread
{
private:
	bool terminateSignal;
	int  deviceNumber;
public:
	bool cudaRunning;
	int xDim;
	int yDim;
	int batchCounter;
	
	GLuint gl_PBO;
	GLuint gl_Tex;
	float4 *h_Src;
#if defined(__APPLE__)
	NSOpenGLView *view;
	pthread_mutex_t waitMutex;
	pthread_cond_t  waitCond;
#elif defined(_WIN32)
	HDC g_hDC;
	HGLRC g_hRC;
	unsigned int g_hWnd;
#endif
	void* image;
	bool useAlpha;

	Flame* currentParameters;
	volatile void (WorkerPThread::*nextFunction)(void);
	volatile bool readyForCommand;
	T_THREAD parentThread;

	WorkerPThread(int device);
	static T_THREADFUNC WorkerPThreadMessageProc(void *vptr_args);
	void WorkerPThreadMessageProcInternal();
	void signalFinished();
	
	void StartCuda();

	void RunFuse();

	void StartFrame();

	void RenderBatch();

	void GrabFrame();

	void MergeFrames();

	void FinishFrame();

	void CloseThread();

	void StopCuda();

	~WorkerPThread();
};
