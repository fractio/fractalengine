/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#if defined(_WIN32)
#include <GL/glew.h>
#include <windows.h>
#define DLLEXPORT __declspec(dllexport)

#elif defined(__APPLE__)
#define DLLEXPORT
#endif

#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include <math.h>
#include "Flam4.h"
#include "Flam4CUDA_PThreading.h"
#include <pthread.h>
#include "Defines.h"

int xDim;
int yDim;
#if defined(_WIN32)
unsigned int g_hWnd;

#elif defined(__APPLE__)
NSOpenGLView *_view;
#endif

volatile CUDAThreadingManager* threads;
volatile bool cudaRunning     = false;
pthread_mutex_t runningMutex  = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t stoppingCond   = PTHREAD_COND_INITIALIZER;

float4** d_dst = NULL;  //This either points to a device buffer or to a openGL buffer, depending on the mode.

int deviceID = 0;

#if defined(_WIN32)
extern "C" DLLEXPORT void cuStartCuda(unsigned int hwnd, int x, int y)
{
	if (cudaRunning)
		return;
	
	g_hWnd = hwnd;
	xDim = x;
	yDim = y;
	threads = new CUDAThreadingManager();
	if (threads->DispatchStartCuda(hwnd,x,y))
		cudaRunning = true;
	else
		cudaRunning = false;
}

#elif defined(__APPLE__)
extern "C" DLLEXPORT void cuStartCuda(NSOpenGLView *view, int x, int y)
{
	if (cudaRunning)
		return;

	_view = view;
	xDim = x;
	yDim = y;
	threads = new CUDAThreadingManager();
	if (((CUDAThreadingManager *)threads)
		->DispatchStartCuda(view,x,y))
		cudaRunning = true;
	else
		cudaRunning = false;
}
#endif

// we tell each thread to stop
extern "C" DLLEXPORT void cuStopCuda()
{
	// needs mutex here as the following cudaRunning get/set code is non-atomic
	pthread_mutex_lock(&runningMutex);
	if (!cudaRunning)
		return;
	cudaRunning = false;
	
	if (threads) { // make sure we are not stopping again - after already stopping
		((CUDAThreadingManager *)threads)->DispatchStopCuda();
		
		// wait here till the stop cuda is fully complete
		pthread_cond_wait( &stoppingCond, &runningMutex);			
	}
	pthread_mutex_unlock(&runningMutex);
}


// when all threads have been stopped, this is called by the last active thread
void cuDeleteThreads()
{
	pthread_mutex_lock(&runningMutex); // to prevent threads being deleted before the wait above
	if (threads) {
		delete threads;
		threads = NULL;
	}
	pthread_mutex_unlock(&runningMutex);
	pthread_cond_signal( &stoppingCond); // signal that stop cuda is fully complete
}

extern "C" bool cuCudaActive()
{
	return threads != NULL;
}

extern "C" DLLEXPORT void cuRunFuse(Flame* flame)
{
	if (!cudaRunning)
		return;
	((CUDAThreadingManager *)threads)->DispatchRunFuse(flame);
}

#if defined(_WIN32)
extern "C" DLLEXPORT void cuStartFrame(unsigned int hwnd, int x, int y, Flame* flame)
{
	/* if ((xDim != x)||(yDim != y)||(hwnd != g_hWnd)||(!cudaRunning))  //Recreate cuda buffers if the render target has changed
	{
		if (cudaRunning)
			cuStopCuda();
		cuStartCuda(hwnd,x,y);
	} */
	threads->DispatchStartFrame(flame);
}
#elif defined(__APPLE__)
extern "C" DLLEXPORT void cuStartFrame(NSOpenGLView *view, int x, int y, Flame* flame)
{
	if ((xDim != x)||(yDim != y)||(view != _view)||(!cudaRunning))  //Recreate cuda buffers if the render target has changed
	{
		if (cudaRunning)
			cuStopCuda();
		cuStartCuda(view,x,y);
	}
	((CUDAThreadingManager *)threads)->DispatchStartFrame(flame);
}
#endif

extern "C" DLLEXPORT void cuRenderBatch(Flame* flame)
{
	if (!cudaRunning)
		return;
	((CUDAThreadingManager *)threads)->DispatchRenderBatch(flame);
}

extern "C" DLLEXPORT void cuFinishFrame(void* image, unsigned int useAlpha)
{
	if (!cudaRunning)
		return;
	((CUDAThreadingManager *)threads)->DispatchFinishFrame(&image, useAlpha);
}

#if defined(_WIN32)
extern "C" DLLEXPORT void cuEzRenderFrame(unsigned int hwnd, int x, int y, Flame* flame, void* image, unsigned int useAlpha, int batches)
{
	if (!cudaRunning)
		return;
	flame->numBatches = batches;
	cuStartFrame(hwnd,x,y,flame);
	cuRunFuse(flame);
	cuStartFrame(hwnd, x, y, flame);
	for (int n = 0; n < batches; n++)
		cuRenderBatch(flame);
	cuFinishFrame(image, useAlpha);
}
#elif defined(__APPLE__)
extern "C" DLLEXPORT void cuEzRenderFrame(NSOpenGLView *view, int x, int y, Flame* flame, void* image, unsigned int useAlpha, int batches)
{
	if (!cudaRunning)
		return;
	flame->numBatches = batches;
	cuStartFrame(view,x,y,flame);
	cuRunFuse(flame);
	cuStartFrame(view, x, y, flame);
	for (int n = 0; n < batches; n++)
		cuRenderBatch(flame);
	cuFinishFrame(image, useAlpha);
}
#endif

extern "C" DLLEXPORT int cuCalcNumBatches(int xDim, int yDim, int quality)
{
	return ceil(((double)xDim*(double)yDim*(double)quality)/(1024.*32.*(double)NUM_ITERATIONS));
}

extern "C" DLLEXPORT int cuCudaDeviceCount()
{
	int numDevices = 0;
	cudaGetDeviceCount(&numDevices);
	return numDevices;
}

extern "C" DLLEXPORT int cuCudaDeviceProps(int deviceNumber, struct cudaDeviceProp *prop)
{
	return cudaGetDeviceProperties(prop,deviceNumber);

}

// check and see if we can allocate a the CUDA device buffers
// return success/failure and the total amount that the test was able to allocate
extern "C" DLLEXPORT bool cuMemCheck(unsigned deviceNumber, unsigned area, unsigned numColors, unsigned *amountAlloced)
{
	extern int BLOCKDIM;
	extern int BLOCKSX;
	extern int BLOCKSY;
	bool succeeded = false;
	
	cudaSetDevice(deviceNumber);

	cudaDeviceProp devProp;
	cudaGetDeviceProperties(&devProp,0);
	int warpSize = devProp.warpSize;
	int warpsPerBlock = 2;
	BLOCKDIM = warpSize*warpsPerBlock;
	
	void *pointList           = NULL;
	void *accumBuffer         = NULL;
	void *renderTarget        = NULL;
	void *renderTarget2       = NULL; // for a final copy of the image outside of the CUDA code
	void *renderTarget3       = NULL; // for an OS allocated copy ???
	void *resultStagingBuffer = NULL;
	void *perThreadRandSeeds  = NULL;
	void *perWarpRandSeeds    = NULL;
	void *PBOmemory           = NULL;
	cudaArray *array          = NULL;

	cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc(32, 0, 0, 0,	cudaChannelFormatKindFloat);

	*amountAlloced = 0;
	cudaError_t flag = cudaSuccess;

	int size = sizeof(float4)*NUM_POINTS*BLOCKSX*BLOCKSY;
	flag = cudaMalloc(&pointList, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(float4)*area;
	flag = cudaMalloc(&accumBuffer, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(float4)*area;
	flag = cudaMalloc(&renderTarget, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(float4)*area;
	flag = cudaMalloc(&renderTarget2, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(float4)*area;
	flag = cudaMalloc(&renderTarget3, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(uchar4)*area;
	flag = cudaMalloc(&resultStagingBuffer, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(int)*BLOCKSX*BLOCKSY*BLOCKDIM;
	flag = cudaMalloc(&perThreadRandSeeds, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = sizeof(int)*BLOCKSX*BLOCKSY*warpsPerBlock;
	flag = cudaMalloc(&perWarpRandSeeds, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	size = 4*numColors;
	cudaMallocArray(&array, &channelDesc, numColors, 1);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	
	*amountAlloced += size;
	
	size = xDim*yDim*sizeof(float4);
	flag = cudaMalloc(&PBOmemory, size);
	if (flag == cudaErrorMemoryAllocation) 
		goto freeThem;
	else 
		*amountAlloced += size;
	
	succeeded = true;
	
freeThem:	
	if (pointList)				cudaFree(pointList);
	if (accumBuffer)			cudaFree(accumBuffer);
	if (renderTarget)			cudaFree(renderTarget);
	if (renderTarget2)			cudaFree(renderTarget2);
	if (renderTarget3)			cudaFree(renderTarget3);
	if (resultStagingBuffer)	cudaFree(resultStagingBuffer);
	if (perThreadRandSeeds)		cudaFree(perThreadRandSeeds);
	if (perWarpRandSeeds)		cudaFree(perWarpRandSeeds);
	if (PBOmemory)              cudaFree(PBOmemory);
	if (array)					cudaFreeArray(array);
	
	return succeeded;
}
