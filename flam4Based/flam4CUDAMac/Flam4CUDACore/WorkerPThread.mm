/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include <pthread.h>
#include "Flam4.h"
#include "WorkerPThread.h"
#include "Flam4CUDA_PThreading.h"

extern float4** d_dst;
extern volatile CUDAThreadingManager* threads;
extern void cuDeleteThreads();

bool dontShowErrors = false;

#define BUFFER_DATA(i) ((char *)0 + i)

// convert RGBA buffer to a new equivalent RGB buffer
void *convertRGBAtoRGB(const void *_rgba, unsigned int width, unsigned int height, bool flip ) {
	const unsigned char *rgba = (const unsigned char *)_rgba;
	unsigned char *rgb  = (unsigned char *)malloc(3*width*height);
	
	// convert from incoming rgba array to rgb array - flip upside down		
	for (unsigned int y = 0; y < height; ++y)
	{
		for (unsigned int x = 0; x < width; ++x)
		{
			if (flip) {
				rgb[3*(width*(height-y-1) + x)    ] = rgba[4*(y*width + x)    ];  
				rgb[3*(width*(height-y-1) + x) + 1] = rgba[4*(y*width + x) + 1];  
				rgb[3*(width*(height-y-1) + x) + 2] = rgba[4*(y*width + x) + 2];  
			}
			else {
				rgb[3*(y*width + x)    ] = rgba[4*(y*width + x)    ];  
				rgb[3*(y*width + x) + 1] = rgba[4*(y*width + x) + 1];  
				rgb[3*(y*width + x) + 2] = rgba[4*(y*width + x) + 2];  
			}
		}
	}
	
	return (void *)rgb;
}

void WorkerPThread::signalFinished()
{
	pthread_mutex_lock(&waitMutex); // signal done to this device only waiters
	pthread_cond_signal(&waitCond);
	
	pthread_mutex_lock((pthread_mutex_t *)&threads->readyMutex); // signal done to ANY device waiters
	pthread_cond_signal((pthread_cond_t *)&threads->readyCond);
	pthread_mutex_unlock((pthread_mutex_t *)&threads->readyMutex);
	
	pthread_mutex_unlock(&waitMutex);
}


WorkerPThread::WorkerPThread(int device)
{
	terminateSignal = false;
	deviceNumber = device;
	cudaRunning = false;
	xDim = 0;
	yDim = 0;
	batchCounter = 0;
	useAlpha = false;
	gl_PBO = NULL;
	gl_Tex = NULL;
	h_Src = NULL;
#ifdef _WIN32
	g_hDC = NULL;
	g_hRC = NULL;
	g_hWnd = NULL;
#elif defined(__APPLE__)
	extern NSOpenGLView *_view;
	view = _view;
	pthread_mutex_init(&waitMutex, NULL);
	pthread_cond_init(&waitCond, NULL);
#endif
	image = NULL;

	currentParameters = NULL;
	nextFunction = NULL;
	readyForCommand = true;
}

void WorkerPThread::WorkerPThreadMessageProcInternal()
{
	while (!terminateSignal)
	{
		if (!nextFunction)
		{
			usleep(3000); // sleep for 3 milliseconds, then check again
		}
		else {
#if defined(__APPLE__)
			// create auto-release pool for this thread - then release it after function is done
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
#endif
			(this->*nextFunction)();
#if defined(__APPLE__)
			[pool release];
#endif
		}
	}
	// exiting the thread handler =====
	// we can delete the threads when no more threads are active
	// activeThreadCount is a key synchronization variable shared by the threads and
	// must be mutex protected as the following expression is non-atomic
	if (threads) {
		bool zapThreads = false;

		pthread_mutex_lock((pthread_mutex_t *)&threads->mutex);
		if (--threads->activeThreadCount == 0)
			
			zapThreads = true;
		
		pthread_mutex_unlock((pthread_mutex_t *)&threads->mutex);
		if (zapThreads) {
			cuDeleteThreads();
		}
	}
}

T_THREADFUNC WorkerPThread::WorkerPThreadMessageProc(void *vptr_args)
{
	((WorkerPThread*)vptr_args)->WorkerPThreadMessageProcInternal();
	return NULL;
}

void showAnyError(const char *msg, int  deviceNumber)
{	
	cudaError_t err = cudaGetLastError();
	if ((err!=cudaSuccess)&&(!dontShowErrors))
	{
		cudaDeviceProp props;
		cudaGetDeviceProperties(&props,deviceNumber);
		char str[256];
		sprintf(str,"%s\n Device: %d (%s)",msg,deviceNumber,props.name);
#if defined(_WIN32)
		dontShowErrors=(IDCANCEL==MessageBox((HWND)g_hWnd,str,cudaGetErrorString(err),MB_OKCANCEL));
#elif defined(__APPLE__)
		NSString *_msg   = [NSString stringWithUTF8String:str];
		NSString *errStr = [NSString stringWithUTF8String:cudaGetErrorString(err)];
		//dontShowErrors=(NSRunAlertPanel(_msg, errStr, @"Ok", @"Cancel", nil) == NSAlertAlternateReturn);
		NSString *msg = [NSString stringWithFormat:@"%@ -- %@", _msg, errStr];
		[NSApp sendAction:@selector(logError:) to:nil from:msg];
#endif
	}
}
bool showAnyGLError(const char *prefix)
{
	GLenum errorCode = glGetError();
	if (errorCode != GL_NO_ERROR) {
		NSString *_msg   = [NSString stringWithUTF8String:prefix];
		
		NSString *errStr = nil;
		switch (errorCode) {
			case GL_INVALID_ENUM:
				errStr = @"OpenGL: Invalid enum";
				break;
			case GL_INVALID_VALUE:
				errStr = @"OpenGL: Invalid value";
				break;
			case GL_INVALID_OPERATION:
				errStr = @"OpenGL: Invalid operation for current state";
				break;
			case GL_STACK_OVERFLOW:
				errStr = @"OpenGL: Stack overflow";
				break;
			case GL_STACK_UNDERFLOW:
				errStr = @"OpenGL: Stack underflow";
				break;
			case GL_OUT_OF_MEMORY:
				errStr = @"OpenGL: Out of memory";
				break;
			case GL_TABLE_TOO_LARGE:
				errStr = @"OpenGL: Table too large";
				break;
		}
		NSString *msg = [NSString stringWithFormat:@"%@ -- %@", _msg, errStr];
		[NSApp sendAction:@selector(logError:) to:nil from:msg];

		return YES;
	}
	return NO;
}


void WorkerPThread::CloseThread()
{
	NSString *msg = [NSString stringWithFormat:@"CloseThread: device:%d", deviceNumber];
		[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	terminateSignal = true;
	readyForCommand = true;
}

void WorkerPThread::StartCuda()
{
	NSString *msg = [NSString stringWithFormat:@"StartCuda: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	cudaSetDevice(deviceNumber);
	cudaSetDeviceFlags(cudaDeviceScheduleYield);
#if defined(__APPLE__)
	if (view != 0)
	{	
//		NSOpenGLPixelFormatAttribute attributes [] = {
//			NSOpenGLPFAWindow,
//			NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
//			NSOpenGLPFAColorSize, (NSOpenGLPixelFormatAttribute)24, // 24 bit color
//			NSOpenGLPFADoubleBuffer,								// double buffered
//			NSOpenGLPFAAccelerated,									// graphic card accelerated
//			(NSOpenGLPixelFormatAttribute)nil
//		};
//		NSOpenGLPixelFormat pixelFormat = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
//		[view setPixelFormat: pixelFormat];

		NSOpenGLContext *ctx = [view openGLContext];
		[ctx makeCurrentContext];

#elif defined(_WIN32)
	if (g_hWnd != 0)
	{
		RECT rc;
		GetClientRect((HWND)g_hWnd,&rc);
		int x = rc.right;
		int y = rc.bottom;
		g_hDC = GetDC((HWND)g_hWnd);
		int nPixelFormat;
		PIXELFORMATDESCRIPTOR pfd;
		ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		pfd.nVersion = 1;
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.iPixelType = PFD_TYPE_RGBA;
		pfd.cColorBits = 64;
		nPixelFormat = ChoosePixelFormat(g_hDC, &pfd);
		SetPixelFormat(g_hDC,nPixelFormat,&pfd);
		g_hRC = wglCreateContext(g_hDC);
		wglMakeCurrent(g_hDC, g_hRC);
		glewInit();
#endif
//		glViewport(0, 0, x, y);
//		glMatrixMode(GL_PROJECTION);
//		glLoadIdentity();
//		glMatrixMode(GL_MODELVIEW);
//		glLoadIdentity();
//		glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);
//		h_Src = new float4[xDim*yDim];
//		glEnable(GL_TEXTURE_2D);
//		glGenTextures(1,&gl_Tex);
//		glBindTexture(GL_TEXTURE_2D, gl_Tex);
//		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
//		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
//		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
//		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
//		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
//		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, xDim, yDim, 0, GL_RGBA, GL_FLOAT,h_Src);
		
		h_Src = new float4[xDim*yDim];
		glGenBuffers(1, &gl_PBO);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB,gl_PBO);
		
		glGetError(); // reset errors before next call
		glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB,xDim*yDim*sizeof(float4),h_Src,GL_STREAM_COPY);
		if (showAnyGLError("Error in Start CUDA")) { // we can run out of memory on graphics card
			glDeleteBuffers(1,&gl_PBO);
			cudaRunning = false;
			signalFinished(/*@"StartCuda failed"*/);
			return;
		}
		cudaGLRegisterBufferObject(gl_PBO);
		createCUDAbuffers(NULL,deviceNumber, (bool *)&readyForCommand);
		glBindBufferARB(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	}
	else
	{
		createCUDAbuffers(&(d_dst[deviceNumber]),deviceNumber, (bool *)&readyForCommand);
	}
	cudaRunning = true;
	signalFinished(/*@"StartCuda"*/);
		
	showAnyError("Error in Start CUDA", deviceNumber);
}

void WorkerPThread::RunFuse()
{
	NSString *msg = [NSString stringWithFormat:@"RunFuse: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	runFuse(currentParameters, deviceNumber, (bool *)&readyForCommand);
	signalFinished(/*@"RunFuse"*/);
	showAnyError("Error in Start Batch", deviceNumber);
}

void WorkerPThread::StartFrame()
{
	NSString *msg = [NSString stringWithFormat:@"StartFrame: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	
#if defined(_WIN32)
	if (g_hWnd != 0)
#elif defined(__APPLE__)
	if (view != nil)
#endif
	{
		cudaGLMapBufferObject((void**)&(d_dst[deviceNumber]),gl_PBO);
	}
	else
	{
		//no GL context, use internal buffer
	}
	startFrame(currentParameters,d_dst[deviceNumber], deviceNumber, (bool *)&readyForCommand);
	batchCounter = 0;
	signalFinished(/*@"StartFrame"*/);
	showAnyError("Error in Start Frame", deviceNumber);
}


void WorkerPThread::RenderBatch()
{
	NSString *msg = [NSString stringWithFormat:@"RenderBatch: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	//if (batchCounter < 10)		//Disable the new iteration engine for now
	//{
		renderBatch(currentParameters, d_dst[deviceNumber], deviceNumber, (bool *)&readyForCommand);
	//}
	//else
	//{
		//if (batchCounter == 10)
		//{
			//deNoise(d_dst[deviceNumber], deviceNumber, &readyForCommand);
		//}
		//renderBatchMH(currentParameters, d_dst[deviceNumber], deviceNumber, &readyForCommand,(float)batchCounter);
	//}
	batchCounter++;
	signalFinished(/*@"RenderBatch"*/);
	showAnyError("Error in Render Batch", deviceNumber);
}

void WorkerPThread::GrabFrame()
{
	NSString *msg = [NSString stringWithFormat:@"GrabFrame: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	grabFrame(d_dst[deviceNumber], deviceNumber, (bool *)&readyForCommand);

	signalFinished(/*@"GrabFrame"*/);
}

void WorkerPThread::MergeFrames()
{
	NSString *msg = [NSString stringWithFormat:@"MergeFrames: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	mergeFrames(d_dst[deviceNumber],deviceNumber, (bool *)&readyForCommand);

	signalFinished(/*@"MergeFrames"*/);
}

void WorkerPThread::FinishFrame()
{
	NSString *msg = [NSString stringWithFormat:@"FinishFrame: device:%d", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	if (!cudaRunning)
		return;
	finishFrame(currentParameters, d_dst[deviceNumber], (uchar4*)image, deviceNumber, (bool *)&readyForCommand, useAlpha);
	
#if defined(_WIN32)
	if (g_hWnd != 0)
	{
#elif defined(__APPLE__)
	if (view != nil)
	{
#endif
		cudaGLUnmapBufferObject(gl_PBO);
				
		/* glTexSubImage2D(GL_TEXTURE_2D,0,0,0,xDim,yDim,GL_RGBA,GL_FLOAT,BUFFER_DATA(0));
				
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f,0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(1.0f,0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(1.0f,1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f,1.0f);
		glEnd();
		 */					
#if defined(_WIN32)
		SwapBuffers(g_hDC);
#elif defined(__APPLE__)
		
#endif
	}
	signalFinished(/*@"FinishFrame"*/);
	showAnyError("Error in Finish Frame", deviceNumber);
}

void WorkerPThread::StopCuda()
{
	NSString *msg = [NSString stringWithFormat:@"StopCuda: device:%d\n", deviceNumber];
	[NSApp sendAction:@selector(logRenderSteps:) to:nil from:msg];
	nextFunction = NULL;
	
	if (!threads) {
		NSLog(@"bailing from StopCuda - nil threads");
		return;
	}
	cudaRunning = false;
	
#if defined(_WIN32)
	if (g_hWnd != 0)
#elif defined(__APPLE__)
	if (view != nil)
#endif
		{
		deleteCUDAbuffers(NULL, deviceNumber, (bool *)&readyForCommand);
		if (h_Src)
			delete h_Src;
		h_Src = 0;
		if (gl_Tex)
			glDeleteTextures(1,&gl_Tex);
		gl_Tex = 0;
		if (gl_PBO)
		{
			cudaGLUnregisterBufferObject(gl_PBO);
			glDeleteBuffers(1,&gl_PBO);
		}
		gl_PBO = 0;
		
#if defined(_WIN32)
		wglMakeCurrent(g_hDC, NULL);
		wglDeleteContext(g_hRC);
		ReleaseDC((HWND)g_hWnd,g_hDC);
#endif
	}
	else
	{
		deleteCUDAbuffers(&(d_dst[deviceNumber]),deviceNumber, (bool *)&readyForCommand);
	}
	CloseThread();
	
	signalFinished(/*@"StopCUDA"*/);
	showAnyError("Error in Stop CUDA", deviceNumber);
}

WorkerPThread::~WorkerPThread()
{
	pthread_mutex_destroy(&waitMutex);
	pthread_cond_destroy(&waitCond);

}
