/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef _WIN32
#include "..\\Flam4CUDA\\FlameData.h"
#define DLL_EXPORT __declspec(dllexport)
#else

#include <libkern/OSAtomic.h>
#include "FlameData.h"
#define DLL_EXPORT
#endif



extern int xDim;
extern int yDim;

extern "C" void createCUDAbuffers(float4** renderTarget, int deviceID, bool* ready);
extern "C" void deleteCUDAbuffers(float4** renderTarget, int deviceID, bool* ready);
extern "C" void runFuse(Flame* flm, int deviceID,bool* ready);
extern "C" void startFrame(Flame* flm, float4* renderTarget, int deviceID,bool* ready);
extern "C" void renderBatch(Flame* flm, float4* renderTarget, int deviceID, bool* ready);
extern "C" void renderBatchMH(Flame* flm, float4* renderTarget, int deviceID, bool* ready, float batchNum);
extern "C" void deNoise(float4* renderTarget, int deviceID, bool* ready);
extern "C" void grabFrame(float4* renderTarget, int deviceID, bool* ready);
extern "C" void mergeFrames(float4* renderTarget, int deviceID, bool* ready);
extern "C" void finishFrame(Flame* flm, float4* renderTarget, uchar4* outputBuffer, int deviceID, bool* ready, bool useAlpha, bool useDensityEstimation = true);
