/*
Copyright 2008 Steven Brodhead

This file is part of flam4.

flam4 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flam4 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flam4.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <pthread.h>
#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>
#include <stdio.h>
#include "WorkerPThread.h"

typedef void (WorkerPThread::*WorkerFunction)(void);
typedef volatile void (WorkerPThread::*VolatileWorkerFunction)(void);


class CUDAThreadingManager
{
public:
	T_THREAD* deviceThreadHandles;
#if defined(__APPLE__)
	
	NSOpenGLView *view;
	pthread_mutex_t readyMutex;
	pthread_mutex_t mutex;
	pthread_cond_t  readyCond;
#elif defined(_WIN32)
	DWORD* threadIds;
	HWND hwnd;
#endif
	
	volatile WorkerPThread** threadObjects;
	int                      numDevices;
	volatile int             activeThreadCount;

	CUDAThreadingManager();
	
	~CUDAThreadingManager();
	
	void waitTillReady(int n);
	void dispatchToThisDeviceWait(int n, Flame *flame, WorkerFunction nextFunction);
	void dispatchToThisDeviceWaitTillDone(int n, Flame *flame, WorkerFunction nextFunction);
	bool dispatchToThisDeviceNoWait(int n, Flame *flame, WorkerFunction nextFunction);
	void dispatchToAllDevicesWait(Flame *flame, WorkerFunction nextFunction);
	void dispatchToFirstReadyDevice(Flame *flame, WorkerFunction nextFunction);
	void dispatchFinishFrameWait(int n, void** image, bool useAlpha);
	
#if defined(__APPLE__)
	bool DispatchStartCuda(NSOpenGLView *view, int x, int y);
#elif defined(_WIN32)
	bool DispatchStartCuda(unsigned int hwnd, int x, int y);
#endif
	void DispatchStopCuda();
	void DispatchRunFuse(Flame* flame);
	void DispatchStartFrame(Flame* flame);
	void DispatchRenderBatch(Flame* flame);
	void DispatchFinishFrame(void** image, bool useAlpha);
	
	static unsigned long GetTickCount(void);
};
